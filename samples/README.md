# Sample programs

The following directories contains several c program samples compiled in LLVM IR with `clang-7`.

## Running a sample

To test a sample, first compile it with the `clang-7`:

`clang-7 -emit-llvm -O0 -g -S main.c -o source.ll`

Then, modify ScEpTIC configuration if needed (`config.py` file) and run the emulator `python3 run.py`

## Simple

In order to test Robert, we used several programs stored in `simple` folder:

| Program name              | Working | Description                                                                                                                                                                                  |
|---------------------------|---------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `main_condition.c`        | Yes     | a simple program with a condition                                                                                                                                                            |                                                                                                                                                           |
| `main_double_condition.c` | Yes      | a program with 2 conditions (one after the other)                                                                                                                                            |                                                                                                                                           |
| `main_nop_condition.c`    | Yes     | A variation of `main_nop` with a condition                                                                                                                                                   |
| `main_function.c`         | Yes      | An example with a function                                                                                                                                                                   |
| `main_loop.c`             | Yes      | An example with a loop                                                                                                                                                                       |


For example, to test `main_condition.c`:

1. `clang-7 -emit-llvm -O0 -g -S main_condition.c -o source.ll`
2. `python3 run.py`

By default the configuration file run the ScEpTIC emulator with the Schematic pass, which will generate the following output:

- The `analysis_results/program_<timestamp>` folder, which contains :
  - a folder `code` with the modified LLVM IR code, containing the checkpoint placement decided by Schematic, and the memory allocation informations
  - a folder `energy` with the energy consumption of the program
  - a folder `states` with the end state of the program, in the different run configurations
- The `dot_cfgs` folder, if pygraphviz is installed, with the dot graphs of the CFGs of the program

To modify the configuration of the emulator, you can edit the `config.py` file. The configuration file is documented, and you can find more information about it in the [documentation](https://neslabpolimi.bitbucket.io/ScEpTIC/guide/configuration.html).
