import csv
import os
import sys
from turtle import width

file_extension = ".csv"
from glob import glob
import matplotlib.pyplot as plt
import numpy as np

if len(sys.argv) <= 1:
    print("USAGE: python3 show_results.py <file.csv>")
    exit(1)

file = sys.argv[1]
print("Opening {}".format(file))

labels = []
bars_label = [
    "Computation",
    "Normalization",
    "Reexecution",
    "Checkpoint",
    "Restore",
]
data = {}
width = 0.35

for bar in bars_label:
    data[bar] = []

with open(file, "r") as f:
    reader = csv.reader(f)
    next(reader)
    for row in reader:
        labels.append(row[0])
        # if the benchmark could execute
        if row[-1] == "True":
            i = 1
            for bar in bars_label:
                row_data = np.array(row[i], dtype=np.float64)
                row_data *= 1e6
                data[bar].append(np.round(row_data))
                i += 1
        else:
            for bar in bars_label:
                data[bar].append(0)

stacked_data = np.zeros(len(labels), dtype=np.float64)

fig, ax = plt.subplots()
for bar_label in bars_label:
    print(bar_label)
    print(stacked_data)
    ax.bar(labels, data[bar_label], width, bottom=stacked_data, label=bar_label)
    stacked_data = np.add(stacked_data, np.array(data[bar_label], dtype=np.float64))


ax.set_ylabel("Energy consumption (J)")
ax.set_title("Energy consumption per benchmarks")
ax.legend()

plt.show()
