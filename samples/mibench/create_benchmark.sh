# Create a benchmark folder for each baselinE, copy the benchmark files and create the necessary links
baselines="ratchet schematic schematic_all_nvm alfred mementos ref"
benchmark=$1

if [ $# -lt 2 ]
then
    echo "USAGE ./create_benchmarks <benchmark_name> <benchmark folder>"
    exit 1
fi

for baseline in $baselines
do
    echo $baseline
    baseline_folder=${benchmark}_${baseline}
    mkdir -p ${benchmark}/${baseline_folder}
    cp -r $2/* ${benchmark}/${baseline_folder}/
    ln -s ../../BenchmarkMakefile ${benchmark}/${baseline_folder}/Makefile
    ln -s ../../config_${baseline}.py ${benchmark}/${baseline_folder}/config.py
    ln -s ../../../../ScEpTIC/ ${benchmark}/${baseline_folder}/ScEpTIC
done
