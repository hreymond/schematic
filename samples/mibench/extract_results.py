import copy
import csv
import os
import sys
import re
import json
import logging

file_extension = ".csv"
from glob import glob

RESULTS_FOLDERS_REGEX = "results/results_*"

legend = [
    "Benchmark",
    "Baseline",
    "Capacitor Size",
    "Frequency",
    "Computation",
    "Normalization",
    "Reexecution",
    "Checkpoint",
    "Restore",
    "Nb checkpoint",
    "Nb restore",
    "VM accesses",
    "NVM accesses",
    "Computation no mem",
    "Computation VM",
    "Computation NVM",
    "VM utilization",
    "NVM utilization",
    "Execution time (clock cycles)",
    "able_to_complete",
]


def get_energy(data, metric_group):
    return data[0]["measures"]["data"][metric_group]["total"]["energy"]


def get_total_exec_time(data, metric_group="total"):
    return data[0]["measures"]["data"][metric_group]["total"]["clock_cycles"]


def get_nb_memory_accesses(data, memory_type, metric_group="computation"):
    if memory_type == "VM":
        memory = "volatile_accesses"
    elif memory_type == "NVM":
        memory = "non_volatile_accesses"
    else:
        raise Exception(f"Incorrect memory type: {memory_type}")
    return data[0]["measures"]["data"][metric_group][memory]["executed_instructions"]


def check_benchmark_outputs(benchmark_paths):
    first_output = None
    for benchmark in benchmark_paths:
        with open(os.path.join(benchmark, "output.txt")) as f:
            if first_output:
                if first_output != f.read():
                    logging.WARNING("Different outputs for benchmarks {} and {} !".format(benchmark_paths[0], benchmark))
            else:
                first_output = f.read()


def process_benchmark(path):
    """
    :param str path: The folder path for the benchmark results
    """
    folder_name = path.split("/")[-1]
    print("\tBenchmark/baseline " + folder_name)
    folder_name = folder_name.split("_")
    benchmark = folder_name[0]
    baseline = "_".join(folder_name[1:])
    data_expe = None
    try:
        with open(os.path.join(path, "energy", "results.json")) as f:
            d = json.loads(f.read())
    except FileNotFoundError:
        logging.warning("No energy measure for {}-{}".format(benchmark, baseline))
        return [benchmark, baseline] + [-1] * (len(legend) - 3) + [False]

    try:
        with open(os.path.join(path, "memory_size", "results.json")) as f:
            mem = json.loads(f.read())
    except FileNotFoundError:
        mem = {"vm_total": -1, "nvm_total": -1}

    data_expe = [
        benchmark,
        baseline,
        get_energy(d, "computation"),
        get_energy(d, "normalization"),
        get_energy(d, "reexecution"),
        get_energy(d, "checkpoint"),
        get_energy(d, "restore"),
        d[0]["measures"]["summary"]["checkpoint"],
        d[0]["measures"]["summary"]["restore"],
        get_nb_memory_accesses(d, "VM"),
        get_nb_memory_accesses(d, "NVM"),
        d[0]["measures"]["data"]["computation"]["no_memory_accesses"]["energy"],
        d[0]["measures"]["data"]["computation"]["volatile_accesses"]["energy"],
        d[0]["measures"]["data"]["computation"]["non_volatile_accesses"]["energy"],
        mem["vm_total"],
        mem["nvm_total"],
        get_total_exec_time(d),
        d[0]["measures"]["able_to_complete"],
    ]

    return data_expe


if __name__ == "__main__":
    print("Hello, world !")
    row = []
    if len(sys.argv) > 1:
        RESULTS_FOLDERS_REGEX = sys.argv[1]

    experiments_dirs = list(glob(RESULTS_FOLDERS_REGEX))

    with open(os.path.join("results", "results.csv"), "w") as final_csv:
        writer = csv.writer(final_csv)
        writer.writerow(legend)
        i = 1
        for experiment_name in experiments_dirs:
            re_result = re.search(".*results_([0-9]+Mhz)_([0-9]+)", experiment_name)
            if re_result is None:
                raise Exception(
                    "Couldn't parse frequency and energy budget from experiment name: {}".format(experiment_name)
                )
            frequency = re_result.group(1)
            capa_size = re_result.group(2)
            print(f"Setup #{i}: {frequency} and capacity {capa_size}")
            benchmarks = list(glob(experiment_name + "/*"))
            # benchmarks_to_check = copy.copy(benchmarks)
            for benchmark in benchmarks:
                baselines = list(glob(benchmark + "/*"))
                for baseline in baselines:
                    # Consider only directories
                    if os.path.isfile(baseline):
                        continue
                    benchmark_data = process_benchmark(baseline)
                    benchmark_data.insert(2, frequency)
                    benchmark_data.insert(2, capa_size)
                    if benchmark_data[-1] == False:
                        benchmark_data[4:9] = [0] * (9 - 4)
                        benchmark_data[-2] = 0
                    writer.writerow(benchmark_data)
            # Function not working ATM TODO -> RE
            # check_benchmark_outputs(benchmarks_to_check)
            i += 1
