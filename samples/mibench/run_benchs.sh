#!bash
PYTHON=pypy3
RESULT_FOLDER="results/tmp"
LOG_FOLDER="logs"
BENCHMARKS="rc4  basicmath bitcount rsa randmath dijkstra fft aes crc"
# find . -name "config.py" -type f -exec sed -i "s/save_dir_datetime = True/save_dir_datetime = False/g" {} \;
run_bench() {
  local name=$1
  echo "Running $name"
  cd $name
  make clean-results
  make
  if [[ "$name" == *"schematic"* ]]; then
    make trace > "../../$LOG_FOLDER/${name}_trace.txt"
  fi

  rm -rf analysis_results output.txt
  ${PYTHON} run.py > "../../$LOG_FOLDER/$name"
  cat "../../$LOG_FOLDER/$name" | grep PRINTF > analysis_results/program/output.txt
  cat config.py | grep energy_budget
  cp -r "analysis_results/program" "../../$RESULT_FOLDER/$name"
  echo "$name done"
}

rm -rf $RESULT_FOLDER $LOG_FOLDER

for benchmark in $BENCHMARKS; do
  FOLDERS=$(find ${benchmark}/* -maxdepth 0 -type d -printf '%f\n')
  echo $FOLDERS

  for d in $FOLDERS; do
    mkdir -p ${LOG_FOLDER}/${benchmark}/
    mkdir -p ${RESULT_FOLDER}/${benchmark}/
    (run_bench ${benchmark}/$d) &
  done
done
wait
# python3 extract_energy.py $FOLDERS
