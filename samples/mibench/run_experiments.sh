capa_sizes="1000 10000 100000"
frequencies="8Mhz 16Mhz"
INPUT_FOLDER="results/tmp"
OUTPUT_FOLDER="results"
ARCHIVE_FOLDER="archives"

rm -rf $OUTPUT_FOLDER

for freq in $frequencies
do
  echo "Test with frequency $freq"
  sed_cmd_frequency="s/clock_frequency = \"[0-9]Mhz\"/clock_frequency = \"${freq}\"/g"
  find . -name config.py -exec sed -i "$sed_cmd_frequency" {} +
  for capa_size in $capa_sizes
  do
    echo "Test with capa $capa_size"
    sed_cmd_clock_cycles="s/\"clock_cycles_between_power_failures\": [0-9]*/\"clock_cycles_between_power_failures\": ${capa_size}/g"
    sed_cmd_energy_budget="s/\"energy_budget\": [0-9]*/\"energy_budget\": ${capa_size}/g"
    find . -name config.py -exec sed -i "$sed_cmd_clock_cycles" {} +
    find . -name config.py -exec sed -i "$sed_cmd_energy_budget" {} +
    sh ./run_benchs.sh
    cp -rf $INPUT_FOLDER $OUTPUT_FOLDER/results_${freq}_${capa_size}/
  done
done
date=$(date -Iminutes)
echo "Creating archive at date ${date}"
mkdir -p archives
tar -czvf archives/results-${date}.tar.gz $OUTPUT_FOLDER/ > /dev/null

python3 merge_results.py
