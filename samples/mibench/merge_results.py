import csv
import os
import sys
import re

file_extension = ".csv"
from glob import glob

RESULTS_FOLDERS_REGEX = "results/result_*"

if len(sys.argv) > 1:
    RESULTS_FOLDERS_REGEX = sys.argv[1]


folders = list(glob(RESULTS_FOLDERS_REGEX))

legend = [
    "0_Name",
    "capa_size",
    "frequency",
    "Computation",
    "Normalization",
    "Reexecution",
    "Checkpoint",
    "Restore",
    "nb_checkpoint",
    "nb_restore",
    "able_to_complete",
]

row = []
with open(os.path.join("results", "results.csv"), "w") as final_csv:
    writer = csv.writer(final_csv)
    writer.writerow(legend)
    for folder in folders:
        with open(os.path.join(folder, "results.csv")) as f:
            reader = csv.reader(f)
            next(reader)
            for r in reader:
                re_result = re.search("result_([0-9]+MHz)_([0-9]+)", folder)
                frequency = re_result.group(1)
                capa_size = re_result.group(2)
                r.insert(1, frequency)
                r.insert(1, capa_size)
                if r[-1] == "False":
                    r[3:8] = [0] * (8 - 3)
                writer.writerow(r)
