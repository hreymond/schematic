import sys
import os
import json
import csv

RESULT_FOLDER = "results/tmp"
legend = [
    "0s_Name",
    "Computation",
    "Normalization",
    "Reexecution",
    "Checkpoint",
    "Restore",
    "nb_checkpoint",
    "nb_restore",
    "able_to_complete",
]
rows = [legend]


def get_energy(data, metric_group):
    return data[0]["measures"]["data"][metric_group]["total"]["energy"]


for arg in sys.argv[1:]:
    with open(os.path.join(RESULT_FOLDER, arg, "energy", "results.json")) as f:
        d = json.loads(f.read())
        data_expe = [
            arg,
            get_energy(d, "computation"),
            get_energy(d, "normalization"),
            get_energy(d, "reexecution"),
            get_energy(d, "checkpoint"),
            get_energy(d, "restore"),
            d[0]["measures"]["summary"]["checkpoint"],
            d[0]["measures"]["summary"]["restore"],
            d[0]["measures"]["able_to_complete"],
        ]

        rows.append(data_expe)

with open(os.path.join(RESULT_FOLDER, "results.csv"), "w") as f:
    writer = csv.writer(f)
    for row in rows:
        writer.writerow(row)
