find */ -name "*_mementos" -execdir ln -fs ../../config_mementos.py {}/config.py \;
find */ -name "*_ratchet" -execdir ln -fs ../../config_ratchet.py {}/config.py \;
find */ -name "*_alfred" -execdir ln -fs ../../config_alfred.py {}/config.py \;
find */ -name "*_schematic" -execdir ln -fs ../../config_schematic.py {}/config.py \;
find */ -name "*_schematic_all_nvm" -execdir ln -fs ../../config_schematic_all_nvm.py {}/config.py \;
find */ -name "*_ref" -execdir ln -fs ../../config_ref.py {}/config.py \;
find */ -name "*_pfi" -execdir ln -fs ../../config_pfi.py {}/config.py \;
