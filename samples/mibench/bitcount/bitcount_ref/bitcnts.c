/* +++Date last modified: 05-Jul-1997 */

/*
**  BITCNTS.C - Test program for bit counting functions
**
**  public domain by Bob Stout & Auke Reitsma
*/

// #include <stdio.h>
// #include <stdlib.h>
// #include "conio.h"
// #include <limits.h>
// #include <float.h>
#include "bitops.h"
#include "../../bareBench.h"

#define FUNCS  7

static int CDECL bit_shifter(long int x);

int main(void)
{
  long j, n, seed;
  long unsigned int iterations, i;
  iterations=1125;

  printf("Bit counter algorithm benchmark\n");
  i = 0;
  for (j = n = 0, seed = 0x078FAF; j < iterations; j++, seed += 13) {
    n += bit_count(seed);
  }
  printf("Optimized 1 bit/loop counter > Bits: %ld\n", n);
  i++;

  for (j = n = 0, seed = 0x078FAF; j < iterations; j++, seed += 13) {
    n += bitcount(seed);
  }
  printf("Ratko's mystery algorithm > Bits: %ld\n",  n);
  i++;
  //   for (j = n = 0, seed = 0x078FAF; j < iterations; j++, seed += 13) {
  //   n += ntbl_bitcnt(seed);
  // }
  // printf("Recursive bit count by nybbles> Bits: %ld\n", n);
  // i++;
  for (j = n = 0, seed = 0x078FAF; j < iterations; j++, seed += 13) {
    n += ntbl_bitcount(seed);
  }
  printf("Non-recursive bit count by nybbles> Bits: %ld\n", n);
  i++;
  //   for (j = n = 0, seed = 0x078FAF; j < iterations; j++, seed += 13) {
  //   n += BW_btbl_bitcount(seed);
  // }
  // printf("Non-recursive bit count by bytes (BW)> Bits: %ld\n", n);
  // i++;
  //     for (j = n = 0, seed = 0x078FAF; j < iterations; j++, seed += 13) {
  //   n += AR_btbl_bitcount(seed);
  // }
  // printf("Non-recursive bit count by bytes (AR)> Bits: %ld\n", n);
  // i++;
      for (j = n = 0, seed = 0x078FAF; j < iterations; j++, seed += 13) {
    n += bit_shifter(seed);
  }
  printf("Shift and count bits> Bits: %ld\n", n);
  return 0;
}

static int CDECL bit_shifter(long int x)
{
  int i, n;

  for (i = n = 0; x && (i < (sizeof(long) * CHAR_BIT)); ++i, x >>= 1)
    n += (int)(x & 1L);
  return n;
}
