#include "snipmath.h"
#include "../../bareBench.h"

//-------- Start of rad2deg.c -----------//
#undef rad2deg /* These are macros defined in PI.H */
#undef deg2rad

double rad2deg(double rad)
{
    return (180.0 * rad / (PI));
}

double deg2rad(double deg)
{
    return (PI * deg / 180.0);
}
//---------- End of rad2deg.c -----------//

//---------- Start of isqrt.c -----------//
#define BITSPERLONG 32

#define TOP2BITS(x) ((x & (3L << (BITSPERLONG - 2))) >> (BITSPERLONG - 2))

void usqrt(unsigned long x, struct int_sqrt *q)
{
    unsigned long a = 0L; /* accumulator      */
    unsigned long r = 0L; /* remainder        */
    unsigned long e = 0L; /* trial product    */

    int i;

    for (i = 0; i < BITSPERLONG; i++) /* NOTE 1 */
    {
        __max_iter(BITSPERLONG);
        r = (r << 2) + TOP2BITS(x);
        x <<= 2; /* NOTE 2 */
        a <<= 1;
        e = (a << 1) + 1;
        if (r >= e)
        {
            r -= e;
            a++;
        }
    }
    q->sqrt = (a & 0xFFFF0000) >> 16;
    q->frac = a & 0xFFFF;
}
//------------ End of isqrt.c ------------------------------------//

//------------ Start of cubic.c -----------------------------------//
void SolveCubic(double a,
                double b,
                double c,
                double d,
                int *solutions,
                double *x)
{
    long double a1 = b / a, a2 = c / a, a3 = d / a;
    long double Q = (a1 * a1 - 3.0 * a2) / 9.0;
    long double R = (2.0 * a1 * a1 * a1 - 9.0 * a1 * a2 + 27.0 * a3) / 54.0;
    double R2_Q3 = R * R - Q * Q * Q;

    double theta;

    if (R2_Q3 <= 0)
    {
        *solutions = 3;
        theta = acos(R / sqrt(Q * Q * Q));
        x[0] = -2.0 * sqrt(Q) * cos(theta / 3.0) - a1 / 3.0;
        x[1] = -2.0 * sqrt(Q) * cos((theta + 2.0 * PI) / 3.0) - a1 / 3.0;
        x[2] = -2.0 * sqrt(Q) * cos((theta + 4.0 * PI) / 3.0) - a1 / 3.0;
    }
    else
    {
        *solutions = 1;
        x[0] = pow(sqrt(R2_Q3) + fabs(R), 1 / 3.0);
        x[0] += Q / x[0];
        x[0] *= (R < 0.0) ? 1 : -1;
        x[0] -= a1 / 3.0;
    }
}
//------------ End of cubic.c ---------------------//

/* The printf's may be removed to isolate just the math calculations */
int main(void)
{
    double a1 = 1.0, b1 = -10.5, c1 = 32.0, d1 = -30.0;
    double a2 = 1.0, b2 = -4.5, c2 = 17.0, d2 = -30.0;
    double a3 = 1.0, b3 = -3.5, c3 = 22.0, d3 = -31.0;
    double a4 = 1.0, b4 = -13.7, c4 = 1.0, d4 = -35.0;
    double x[3];
    double X;
    int solutions;
    int i;
    unsigned long l = 0x3fed0169L;
    struct int_sqrt q;

    /* solve soem cubic functions */
    printf("********* CUBIC FUNCTIONS ***********\n\r");
    /* should get 3 solutions: 2, 6 & 2.5   */
    SolveCubic(a1, b1, c1, d1, &solutions, x);
    printf("Solutions:");
    for (i = 0; i < solutions; i++) {
        printf(" %f", x[i]);
        __max_iter(3);
    }

    printf("\n\r");
    /* should get 1 solution: 2.5           */
    SolveCubic(a2, b2, c2, d2, &solutions, x);
    printf("Solutions:");
    for (i = 0; i < solutions; i++) {
        printf(" %f", x[i]);
        __max_iter(3);
    }
    printf("\n\r");
    SolveCubic(a3, b3, c3, d3, &solutions, x);
    printf("Solutions:");
    for (i = 0; i < solutions; i++) {
        printf(" %f", x[i]);
        __max_iter(3);
    }
    printf("\n\r");
    SolveCubic(a4, b4, c4, d4, &solutions, x);
    printf("Solutions:");
     for (i = 0; i < solutions; i++) {
        printf(" %f", x[i]);
        __max_iter(3);
    }
    printf("\n\r");
    /* Now solve some random equations */
    for (a1 = 1; a1 < 10; a1++)
    {
        for (b1 = 10; b1 > 0; b1--)
        {
            c1 = 5;
            d1 = -1;
            SolveCubic(a1, b1, c1, d1, &solutions, x);
            printf("Solutions:");
            for (i = 0; i < solutions; i++) {
                printf(" %f", x[i]);
                __max_iter(3);
            }
            printf("\n\r");
            __max_iter(10);
        }
        __max_iter(9);
    }

    printf("********* INTEGER SQR ROOTS ***********\n\r");
    /* perform some integer square roots */
    for (i = 0; i < 100; ++i)
    {
        usqrt(i, &q);
        // remainder differs on some machines
        // printf("sqrt(%3d) = %2d, remainder = %2d\n\r",
        printf("sqrt(%3d) = %2d\n\r", i, q.sqrt);
        __max_iter(100);
    }
    usqrt(l, &q);
    // printf("\n\rsqrt(%lX) = %X, remainder = %X\n\r", l, q.sqrt, q.frac);
    printf("\n\rsqrt(%lX) = %X\n\r", l, q.sqrt);

    printf("********* ANGLE CONVERSION ***********\n\r");
    /* convert some rads to degrees */
//    for (X = 0.0; X <= 360.0; X += 1.0)
//    {
//        printf("%3.0f degrees = %.12f radians\n\r", X, deg2rad(X));
//        __max_iter(360);
//    }
//
//    printf("\n\r");
//    for (X = 0.0; X <= (2 * PI + 1e-6); X += (PI / 180)) {
//        printf("%.12f radians = %3.0f degrees\n\r", X, rad2deg(X));
//        __max_iter(360);
//    }

    return 0;
}
