; ModuleID = 'fftmisc.c'
source_filename = "fftmisc.c"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

@.str = private unnamed_addr constant [73 x i8] c">>> Error in fftmisc.c: argument %d to NumberOfBitsNeeded is too small.\0A\00", align 1

; Function Attrs: noinline nounwind optnone
define dso_local i16 @IsPowerOfTwo(i16) #0 !dbg !7 {
  %2 = alloca i16, align 2
  %3 = alloca i16, align 2
  store i16 %0, i16* %3, align 2
  call void @llvm.dbg.declare(metadata i16* %3, metadata !12, metadata !DIExpression()), !dbg !13
  %4 = load i16, i16* %3, align 2, !dbg !14
  %5 = icmp ult i16 %4, 2, !dbg !16
  br i1 %5, label %6, label %7, !dbg !17

; <label>:6:                                      ; preds = %1
  store i16 0, i16* %2, align 2, !dbg !18
  br label %15, !dbg !18

; <label>:7:                                      ; preds = %1
  %8 = load i16, i16* %3, align 2, !dbg !19
  %9 = load i16, i16* %3, align 2, !dbg !21
  %10 = sub i16 %9, 1, !dbg !22
  %11 = and i16 %8, %10, !dbg !23
  %12 = icmp ne i16 %11, 0, !dbg !23
  br i1 %12, label %13, label %14, !dbg !24

; <label>:13:                                     ; preds = %7
  store i16 0, i16* %2, align 2, !dbg !25
  br label %15, !dbg !25

; <label>:14:                                     ; preds = %7
  store i16 1, i16* %2, align 2, !dbg !26
  br label %15, !dbg !26

; <label>:15:                                     ; preds = %14, %13, %6
  %16 = load i16, i16* %2, align 2, !dbg !27
  ret i16 %16, !dbg !27
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

; Function Attrs: noinline nounwind optnone
define dso_local i16 @NumberOfBitsNeeded(i16) #0 !dbg !28 {
  %2 = alloca i16, align 2
  %3 = alloca i16, align 2
  store i16 %0, i16* %2, align 2
  call void @llvm.dbg.declare(metadata i16* %2, metadata !31, metadata !DIExpression()), !dbg !32
  call void @llvm.dbg.declare(metadata i16* %3, metadata !33, metadata !DIExpression()), !dbg !34
  %4 = load i16, i16* %2, align 2, !dbg !35
  %5 = icmp ult i16 %4, 2, !dbg !37
  br i1 %5, label %6, label %9, !dbg !38

; <label>:6:                                      ; preds = %1
  %7 = load i16, i16* %2, align 2, !dbg !39
  %8 = call i16 (i8*, ...) @printf(i8* getelementptr inbounds ([73 x i8], [73 x i8]* @.str, i32 0, i32 0), i16 %7), !dbg !41
  call void @exit(i16 1) #4, !dbg !42
  unreachable, !dbg !42

; <label>:9:                                      ; preds = %1
  store i16 0, i16* %3, align 2, !dbg !43
  br label %10, !dbg !45

; <label>:10:                                     ; preds = %20, %9
  %11 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 16), !dbg !46
  %12 = load i16, i16* %2, align 2, !dbg !49
  %13 = load i16, i16* %3, align 2, !dbg !51
  %14 = shl i16 1, %13, !dbg !52
  %15 = and i16 %12, %14, !dbg !53
  %16 = icmp ne i16 %15, 0, !dbg !53
  br i1 %16, label %17, label %19, !dbg !54

; <label>:17:                                     ; preds = %10
  %18 = load i16, i16* %3, align 2, !dbg !55
  ret i16 %18, !dbg !57

; <label>:19:                                     ; preds = %10
  br label %20, !dbg !58

; <label>:20:                                     ; preds = %19
  %21 = load i16, i16* %3, align 2, !dbg !59
  %22 = add i16 %21, 1, !dbg !59
  store i16 %22, i16* %3, align 2, !dbg !59
  br label %10, !dbg !60, !llvm.loop !61
}

declare dso_local i16 @printf(i8*, ...) #2

; Function Attrs: noreturn
declare dso_local void @exit(i16) #3

declare dso_local i16 @__max_iter(...) #2

; Function Attrs: noinline nounwind optnone
define dso_local i16 @ReverseBits(i16, i16) #0 !dbg !64 {
  %3 = alloca i16, align 2
  %4 = alloca i16, align 2
  %5 = alloca i16, align 2
  %6 = alloca i16, align 2
  store i16 %0, i16* %3, align 2
  call void @llvm.dbg.declare(metadata i16* %3, metadata !67, metadata !DIExpression()), !dbg !68
  store i16 %1, i16* %4, align 2
  call void @llvm.dbg.declare(metadata i16* %4, metadata !69, metadata !DIExpression()), !dbg !70
  call void @llvm.dbg.declare(metadata i16* %5, metadata !71, metadata !DIExpression()), !dbg !72
  call void @llvm.dbg.declare(metadata i16* %6, metadata !73, metadata !DIExpression()), !dbg !74
  store i16 0, i16* %6, align 2, !dbg !75
  store i16 0, i16* %5, align 2, !dbg !77
  br label %7, !dbg !78

; <label>:7:                                      ; preds = %20, %2
  %8 = load i16, i16* %5, align 2, !dbg !79
  %9 = load i16, i16* %4, align 2, !dbg !81
  %10 = icmp ult i16 %8, %9, !dbg !82
  br i1 %10, label %11, label %23, !dbg !83

; <label>:11:                                     ; preds = %7
  %12 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 16), !dbg !84
  %13 = load i16, i16* %6, align 2, !dbg !86
  %14 = shl i16 %13, 1, !dbg !87
  %15 = load i16, i16* %3, align 2, !dbg !88
  %16 = and i16 %15, 1, !dbg !89
  %17 = or i16 %14, %16, !dbg !90
  store i16 %17, i16* %6, align 2, !dbg !91
  %18 = load i16, i16* %3, align 2, !dbg !92
  %19 = lshr i16 %18, 1, !dbg !92
  store i16 %19, i16* %3, align 2, !dbg !92
  br label %20, !dbg !93

; <label>:20:                                     ; preds = %11
  %21 = load i16, i16* %5, align 2, !dbg !94
  %22 = add i16 %21, 1, !dbg !94
  store i16 %22, i16* %5, align 2, !dbg !94
  br label %7, !dbg !95, !llvm.loop !96

; <label>:23:                                     ; preds = %7
  %24 = load i16, i16* %6, align 2, !dbg !98
  ret i16 %24, !dbg !99
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!3, !4, !5}
!llvm.ident = !{!6}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 8.0.1-9 (tags/RELEASE_801/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2, nameTableKind: None)
!1 = !DIFile(filename: "fftmisc.c", directory: "/home/user/Desktop/alfred/samples/fft")
!2 = !{}
!3 = !{i32 2, !"Dwarf Version", i32 4}
!4 = !{i32 2, !"Debug Info Version", i32 3}
!5 = !{i32 1, !"wchar_size", i32 2}
!6 = !{!"clang version 8.0.1-9 (tags/RELEASE_801/final)"}
!7 = distinct !DISubprogram(name: "IsPowerOfTwo", scope: !1, file: !1, line: 35, type: !8, scopeLine: 36, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !2)
!8 = !DISubroutineType(types: !9)
!9 = !{!10, !11}
!10 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!11 = !DIBasicType(name: "unsigned int", size: 16, encoding: DW_ATE_unsigned)
!12 = !DILocalVariable(name: "x", arg: 1, scope: !7, file: !1, line: 35, type: !11)
!13 = !DILocation(line: 35, column: 29, scope: !7)
!14 = !DILocation(line: 37, column: 10, scope: !15)
!15 = distinct !DILexicalBlock(scope: !7, file: !1, line: 37, column: 10)
!16 = !DILocation(line: 37, column: 12, scope: !15)
!17 = !DILocation(line: 37, column: 10, scope: !7)
!18 = !DILocation(line: 38, column: 9, scope: !15)
!19 = !DILocation(line: 40, column: 10, scope: !20)
!20 = distinct !DILexicalBlock(scope: !7, file: !1, line: 40, column: 10)
!21 = !DILocation(line: 40, column: 15, scope: !20)
!22 = !DILocation(line: 40, column: 16, scope: !20)
!23 = !DILocation(line: 40, column: 12, scope: !20)
!24 = !DILocation(line: 40, column: 10, scope: !7)
!25 = !DILocation(line: 41, column: 9, scope: !20)
!26 = !DILocation(line: 43, column: 5, scope: !7)
!27 = !DILocation(line: 44, column: 1, scope: !7)
!28 = distinct !DISubprogram(name: "NumberOfBitsNeeded", scope: !1, file: !1, line: 47, type: !29, scopeLine: 48, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !2)
!29 = !DISubroutineType(types: !30)
!30 = !{!11, !11}
!31 = !DILocalVariable(name: "PowerOfTwo", arg: 1, scope: !28, file: !1, line: 47, type: !11)
!32 = !DILocation(line: 47, column: 40, scope: !28)
!33 = !DILocalVariable(name: "i", scope: !28, file: !1, line: 49, type: !11)
!34 = !DILocation(line: 49, column: 14, scope: !28)
!35 = !DILocation(line: 51, column: 10, scope: !36)
!36 = distinct !DILexicalBlock(scope: !28, file: !1, line: 51, column: 10)
!37 = !DILocation(line: 51, column: 21, scope: !36)
!38 = !DILocation(line: 51, column: 10, scope: !28)
!39 = !DILocation(line: 54, column: 13, scope: !40)
!40 = distinct !DILexicalBlock(scope: !36, file: !1, line: 52, column: 5)
!41 = !DILocation(line: 53, column: 9, scope: !40)
!42 = !DILocation(line: 56, column: 9, scope: !40)
!43 = !DILocation(line: 59, column: 12, scope: !44)
!44 = distinct !DILexicalBlock(scope: !28, file: !1, line: 59, column: 5)
!45 = !DILocation(line: 59, column: 11, scope: !44)
!46 = !DILocation(line: 61, column: 9, scope: !47)
!47 = distinct !DILexicalBlock(scope: !48, file: !1, line: 60, column: 5)
!48 = distinct !DILexicalBlock(scope: !44, file: !1, line: 59, column: 5)
!49 = !DILocation(line: 62, column: 14, scope: !50)
!50 = distinct !DILexicalBlock(scope: !47, file: !1, line: 62, column: 14)
!51 = !DILocation(line: 62, column: 33, scope: !50)
!52 = !DILocation(line: 62, column: 30, scope: !50)
!53 = !DILocation(line: 62, column: 25, scope: !50)
!54 = !DILocation(line: 62, column: 14, scope: !47)
!55 = !DILocation(line: 63, column: 20, scope: !56)
!56 = distinct !DILexicalBlock(scope: !50, file: !1, line: 62, column: 38)
!57 = !DILocation(line: 63, column: 13, scope: !56)
!58 = !DILocation(line: 65, column: 5, scope: !47)
!59 = !DILocation(line: 59, column: 19, scope: !48)
!60 = !DILocation(line: 59, column: 5, scope: !48)
!61 = distinct !{!61, !62, !63}
!62 = !DILocation(line: 59, column: 5, scope: !44)
!63 = !DILocation(line: 65, column: 5, scope: !44)
!64 = distinct !DISubprogram(name: "ReverseBits", scope: !1, file: !1, line: 70, type: !65, scopeLine: 71, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !2)
!65 = !DISubroutineType(types: !66)
!66 = !{!11, !11, !11}
!67 = !DILocalVariable(name: "index", arg: 1, scope: !64, file: !1, line: 70, type: !11)
!68 = !DILocation(line: 70, column: 33, scope: !64)
!69 = !DILocalVariable(name: "NumBits", arg: 2, scope: !64, file: !1, line: 70, type: !11)
!70 = !DILocation(line: 70, column: 49, scope: !64)
!71 = !DILocalVariable(name: "i", scope: !64, file: !1, line: 72, type: !11)
!72 = !DILocation(line: 72, column: 14, scope: !64)
!73 = !DILocalVariable(name: "rev", scope: !64, file: !1, line: 72, type: !11)
!74 = !DILocation(line: 72, column: 17, scope: !64)
!75 = !DILocation(line: 74, column: 16, scope: !76)
!76 = distinct !DILexicalBlock(scope: !64, file: !1, line: 74, column: 5)
!77 = !DILocation(line: 74, column: 12, scope: !76)
!78 = !DILocation(line: 74, column: 11, scope: !76)
!79 = !DILocation(line: 74, column: 20, scope: !80)
!80 = distinct !DILexicalBlock(scope: !76, file: !1, line: 74, column: 5)
!81 = !DILocation(line: 74, column: 24, scope: !80)
!82 = !DILocation(line: 74, column: 22, scope: !80)
!83 = !DILocation(line: 74, column: 5, scope: !76)
!84 = !DILocation(line: 76, column: 9, scope: !85)
!85 = distinct !DILexicalBlock(scope: !80, file: !1, line: 75, column: 5)
!86 = !DILocation(line: 77, column: 16, scope: !85)
!87 = !DILocation(line: 77, column: 20, scope: !85)
!88 = !DILocation(line: 77, column: 29, scope: !85)
!89 = !DILocation(line: 77, column: 35, scope: !85)
!90 = !DILocation(line: 77, column: 26, scope: !85)
!91 = !DILocation(line: 77, column: 13, scope: !85)
!92 = !DILocation(line: 78, column: 15, scope: !85)
!93 = !DILocation(line: 80, column: 5, scope: !85)
!94 = !DILocation(line: 74, column: 34, scope: !80)
!95 = !DILocation(line: 74, column: 5, scope: !80)
!96 = distinct !{!96, !83, !97}
!97 = !DILocation(line: 80, column: 5, scope: !76)
!98 = !DILocation(line: 82, column: 12, scope: !64)
!99 = !DILocation(line: 82, column: 5, scope: !64)
