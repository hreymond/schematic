; ModuleID = 'llvm-link'
source_filename = "llvm-link"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

@.str = private unnamed_addr constant [7 x i8] c"RealIn\00", align 1
@.str.1 = private unnamed_addr constant [8 x i8] c"RealOut\00", align 1
@.str.2 = private unnamed_addr constant [8 x i8] c"ImagOut\00", align 1
@invfft = dso_local global i16 0, align 2, !dbg !0
@MAXSIZE = common dso_local global i16 0, align 2, !dbg !6
@realin = internal global [1024 x float] zeroinitializer, align 4, !dbg !9
@imagin = internal global [1024 x float] zeroinitializer, align 4, !dbg !15
@realout = internal global [1024 x float] zeroinitializer, align 4, !dbg !17
@imagout = internal global [1024 x float] zeroinitializer, align 4, !dbg !19
@Coeff = internal global [16 x float] zeroinitializer, align 4, !dbg !21
@Amp = internal global [16 x float] zeroinitializer, align 4, !dbg !26
@.str.5 = private unnamed_addr constant [73 x i8] c">>> Error in fftmisc.c: argument %d to NumberOfBitsNeeded is too small.\0A\00", align 1

; Function Attrs: noinline nounwind optnone
define dso_local void @fft_float(i16, i16, float*, float*, float*, float*) #0 !dbg !39 {
  %7 = alloca i16, align 2
  %8 = alloca i16, align 2
  %9 = alloca float*, align 2
  %10 = alloca float*, align 2
  %11 = alloca float*, align 2
  %12 = alloca float*, align 2
  %13 = alloca i16, align 2
  %14 = alloca i16, align 2
  %15 = alloca i16, align 2
  %16 = alloca i16, align 2
  %17 = alloca i16, align 2
  %18 = alloca i16, align 2
  %19 = alloca i16, align 2
  %20 = alloca double, align 8
  %21 = alloca double, align 8
  %22 = alloca double, align 8
  %23 = alloca double, align 8
  %24 = alloca double, align 8
  %25 = alloca double, align 8
  %26 = alloca double, align 8
  %27 = alloca double, align 8
  %28 = alloca double, align 8
  %29 = alloca [3 x double], align 8
  %30 = alloca [3 x double], align 8
  %31 = alloca double, align 8
  store i16 %0, i16* %7, align 2
  call void @llvm.dbg.declare(metadata i16* %7, metadata !43, metadata !DIExpression()), !dbg !44
  store i16 %1, i16* %8, align 2
  call void @llvm.dbg.declare(metadata i16* %8, metadata !45, metadata !DIExpression()), !dbg !46
  store float* %2, float** %9, align 2
  call void @llvm.dbg.declare(metadata float** %9, metadata !47, metadata !DIExpression()), !dbg !48
  store float* %3, float** %10, align 2
  call void @llvm.dbg.declare(metadata float** %10, metadata !49, metadata !DIExpression()), !dbg !50
  store float* %4, float** %11, align 2
  call void @llvm.dbg.declare(metadata float** %11, metadata !51, metadata !DIExpression()), !dbg !52
  store float* %5, float** %12, align 2
  call void @llvm.dbg.declare(metadata float** %12, metadata !53, metadata !DIExpression()), !dbg !54
  call void @llvm.dbg.declare(metadata i16* %13, metadata !55, metadata !DIExpression()), !dbg !56
  call void @llvm.dbg.declare(metadata i16* %14, metadata !57, metadata !DIExpression()), !dbg !58
  call void @llvm.dbg.declare(metadata i16* %15, metadata !59, metadata !DIExpression()), !dbg !60
  call void @llvm.dbg.declare(metadata i16* %16, metadata !61, metadata !DIExpression()), !dbg !62
  call void @llvm.dbg.declare(metadata i16* %17, metadata !63, metadata !DIExpression()), !dbg !64
  call void @llvm.dbg.declare(metadata i16* %18, metadata !65, metadata !DIExpression()), !dbg !66
  call void @llvm.dbg.declare(metadata i16* %19, metadata !67, metadata !DIExpression()), !dbg !68
  call void @llvm.dbg.declare(metadata double* %20, metadata !69, metadata !DIExpression()), !dbg !70
  store double 0x401921FB54442D18, double* %20, align 8, !dbg !70
  call void @llvm.dbg.declare(metadata double* %21, metadata !71, metadata !DIExpression()), !dbg !72
  call void @llvm.dbg.declare(metadata double* %22, metadata !73, metadata !DIExpression()), !dbg !74
  %32 = load i16, i16* %7, align 2, !dbg !75
  %33 = call i16 @IsPowerOfTwo(i16 %32), !dbg !77
  %34 = icmp ne i16 %33, 0, !dbg !77
  br i1 %34, label %36, label %35, !dbg !78

; <label>:35:                                     ; preds = %6
  call void @exit(i16 1) #5, !dbg !79
  unreachable, !dbg !79

; <label>:36:                                     ; preds = %6
  %37 = load i16, i16* %8, align 2, !dbg !81
  %38 = icmp ne i16 %37, 0, !dbg !81
  br i1 %38, label %39, label %42, !dbg !83

; <label>:39:                                     ; preds = %36
  %40 = load double, double* %20, align 8, !dbg !84
  %41 = fsub double -0.000000e+00, %40, !dbg !85
  store double %41, double* %20, align 8, !dbg !86
  br label %42, !dbg !87

; <label>:42:                                     ; preds = %39, %36
  %43 = load float*, float** %9, align 2, !dbg !88
  %44 = bitcast float* %43 to i8*, !dbg !88
  call void @CheckPointer(i8* %44, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0)), !dbg !88
  %45 = load float*, float** %11, align 2, !dbg !89
  %46 = bitcast float* %45 to i8*, !dbg !89
  call void @CheckPointer(i8* %46, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.1, i32 0, i32 0)), !dbg !89
  %47 = load float*, float** %12, align 2, !dbg !90
  %48 = bitcast float* %47 to i8*, !dbg !90
  call void @CheckPointer(i8* %48, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.2, i32 0, i32 0)), !dbg !90
  %49 = load i16, i16* %7, align 2, !dbg !91
  %50 = call i16 @NumberOfBitsNeeded(i16 %49), !dbg !92
  store i16 %50, i16* %13, align 2, !dbg !93
  store i16 0, i16* %14, align 2, !dbg !94
  br label %51, !dbg !96

; <label>:51:                                     ; preds = %82, %42
  %52 = load i16, i16* %14, align 2, !dbg !97
  %53 = load i16, i16* %7, align 2, !dbg !99
  %54 = icmp ult i16 %52, %53, !dbg !100
  br i1 %54, label %55, label %85, !dbg !101

; <label>:55:                                     ; preds = %51
  %56 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 1024), !dbg !102
  %57 = load i16, i16* %14, align 2, !dbg !104
  %58 = load i16, i16* %13, align 2, !dbg !105
  %59 = call i16 @ReverseBits(i16 %57, i16 %58), !dbg !106
  store i16 %59, i16* %15, align 2, !dbg !107
  %60 = load float*, float** %9, align 2, !dbg !108
  %61 = load i16, i16* %14, align 2, !dbg !109
  %62 = getelementptr inbounds float, float* %60, i16 %61, !dbg !108
  %63 = load float, float* %62, align 4, !dbg !108
  %64 = load float*, float** %11, align 2, !dbg !110
  %65 = load i16, i16* %15, align 2, !dbg !111
  %66 = getelementptr inbounds float, float* %64, i16 %65, !dbg !110
  store float %63, float* %66, align 4, !dbg !112
  %67 = load float*, float** %10, align 2, !dbg !113
  %68 = icmp eq float* %67, null, !dbg !114
  br i1 %68, label %69, label %70, !dbg !115

; <label>:69:                                     ; preds = %55
  br label %76, !dbg !115

; <label>:70:                                     ; preds = %55
  %71 = load float*, float** %10, align 2, !dbg !116
  %72 = load i16, i16* %14, align 2, !dbg !117
  %73 = getelementptr inbounds float, float* %71, i16 %72, !dbg !116
  %74 = load float, float* %73, align 4, !dbg !116
  %75 = fpext float %74 to double, !dbg !116
  br label %76, !dbg !115

; <label>:76:                                     ; preds = %70, %69
  %77 = phi double [ 0.000000e+00, %69 ], [ %75, %70 ], !dbg !115
  %78 = fptrunc double %77 to float, !dbg !115
  %79 = load float*, float** %12, align 2, !dbg !118
  %80 = load i16, i16* %15, align 2, !dbg !119
  %81 = getelementptr inbounds float, float* %79, i16 %80, !dbg !118
  store float %78, float* %81, align 4, !dbg !120
  br label %82, !dbg !121

; <label>:82:                                     ; preds = %76
  %83 = load i16, i16* %14, align 2, !dbg !122
  %84 = add i16 %83, 1, !dbg !122
  store i16 %84, i16* %14, align 2, !dbg !122
  br label %51, !dbg !123, !llvm.loop !124

; <label>:85:                                     ; preds = %51
  store i16 1, i16* %19, align 2, !dbg !126
  store i16 2, i16* %18, align 2, !dbg !127
  br label %86, !dbg !129

; <label>:86:                                     ; preds = %246, %85
  %87 = load i16, i16* %18, align 2, !dbg !130
  %88 = load i16, i16* %7, align 2, !dbg !132
  %89 = icmp ule i16 %87, %88, !dbg !133
  br i1 %89, label %90, label %249, !dbg !134

; <label>:90:                                     ; preds = %86
  %91 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 1024), !dbg !135
  call void @llvm.dbg.declare(metadata double* %23, metadata !137, metadata !DIExpression()), !dbg !138
  %92 = load double, double* %20, align 8, !dbg !139
  %93 = load i16, i16* %18, align 2, !dbg !140
  %94 = uitofp i16 %93 to double, !dbg !141
  %95 = fdiv double %92, %94, !dbg !142
  store double %95, double* %23, align 8, !dbg !138
  call void @llvm.dbg.declare(metadata double* %24, metadata !143, metadata !DIExpression()), !dbg !144
  %96 = load double, double* %23, align 8, !dbg !145
  %97 = fmul double -2.000000e+00, %96, !dbg !146
  %98 = call double @sin(double %97) #6, !dbg !147
  store double %98, double* %24, align 8, !dbg !144
  call void @llvm.dbg.declare(metadata double* %25, metadata !148, metadata !DIExpression()), !dbg !149
  %99 = load double, double* %23, align 8, !dbg !150
  %100 = fsub double -0.000000e+00, %99, !dbg !151
  %101 = call double @sin(double %100) #6, !dbg !152
  store double %101, double* %25, align 8, !dbg !149
  call void @llvm.dbg.declare(metadata double* %26, metadata !153, metadata !DIExpression()), !dbg !154
  %102 = load double, double* %23, align 8, !dbg !155
  %103 = fmul double -2.000000e+00, %102, !dbg !156
  %104 = call double @cos(double %103) #6, !dbg !157
  store double %104, double* %26, align 8, !dbg !154
  call void @llvm.dbg.declare(metadata double* %27, metadata !158, metadata !DIExpression()), !dbg !159
  %105 = load double, double* %23, align 8, !dbg !160
  %106 = fsub double -0.000000e+00, %105, !dbg !161
  %107 = call double @cos(double %106) #6, !dbg !162
  store double %107, double* %27, align 8, !dbg !159
  call void @llvm.dbg.declare(metadata double* %28, metadata !163, metadata !DIExpression()), !dbg !164
  %108 = load double, double* %27, align 8, !dbg !165
  %109 = fmul double 2.000000e+00, %108, !dbg !166
  store double %109, double* %28, align 8, !dbg !164
  call void @llvm.dbg.declare(metadata [3 x double]* %29, metadata !167, metadata !DIExpression()), !dbg !171
  call void @llvm.dbg.declare(metadata [3 x double]* %30, metadata !172, metadata !DIExpression()), !dbg !173
  store i16 0, i16* %14, align 2, !dbg !174
  br label %110, !dbg !176

; <label>:110:                                    ; preds = %240, %90
  %111 = load i16, i16* %14, align 2, !dbg !177
  %112 = load i16, i16* %7, align 2, !dbg !179
  %113 = icmp ult i16 %111, %112, !dbg !180
  br i1 %113, label %114, label %244, !dbg !181

; <label>:114:                                    ; preds = %110
  %115 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 512), !dbg !182
  %116 = load double, double* %26, align 8, !dbg !184
  %117 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 2, !dbg !185
  store double %116, double* %117, align 8, !dbg !186
  %118 = load double, double* %27, align 8, !dbg !187
  %119 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !188
  store double %118, double* %119, align 8, !dbg !189
  %120 = load double, double* %24, align 8, !dbg !190
  %121 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 2, !dbg !191
  store double %120, double* %121, align 8, !dbg !192
  %122 = load double, double* %25, align 8, !dbg !193
  %123 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !194
  store double %122, double* %123, align 8, !dbg !195
  %124 = load i16, i16* %14, align 2, !dbg !196
  store i16 %124, i16* %15, align 2, !dbg !198
  store i16 0, i16* %17, align 2, !dbg !199
  br label %125, !dbg !200

; <label>:125:                                    ; preds = %234, %114
  %126 = load i16, i16* %17, align 2, !dbg !201
  %127 = load i16, i16* %19, align 2, !dbg !203
  %128 = icmp ult i16 %126, %127, !dbg !204
  br i1 %128, label %129, label %239, !dbg !205

; <label>:129:                                    ; preds = %125
  %130 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 1024), !dbg !206
  %131 = load double, double* %28, align 8, !dbg !208
  %132 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !209
  %133 = load double, double* %132, align 8, !dbg !209
  %134 = fmul double %131, %133, !dbg !210
  %135 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 2, !dbg !211
  %136 = load double, double* %135, align 8, !dbg !211
  %137 = fsub double %134, %136, !dbg !212
  %138 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !213
  store double %137, double* %138, align 8, !dbg !214
  %139 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !215
  %140 = load double, double* %139, align 8, !dbg !215
  %141 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 2, !dbg !216
  store double %140, double* %141, align 8, !dbg !217
  %142 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !218
  %143 = load double, double* %142, align 8, !dbg !218
  %144 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !219
  store double %143, double* %144, align 8, !dbg !220
  %145 = load double, double* %28, align 8, !dbg !221
  %146 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !222
  %147 = load double, double* %146, align 8, !dbg !222
  %148 = fmul double %145, %147, !dbg !223
  %149 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 2, !dbg !224
  %150 = load double, double* %149, align 8, !dbg !224
  %151 = fsub double %148, %150, !dbg !225
  %152 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !226
  store double %151, double* %152, align 8, !dbg !227
  %153 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !228
  %154 = load double, double* %153, align 8, !dbg !228
  %155 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 2, !dbg !229
  store double %154, double* %155, align 8, !dbg !230
  %156 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !231
  %157 = load double, double* %156, align 8, !dbg !231
  %158 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !232
  store double %157, double* %158, align 8, !dbg !233
  %159 = load i16, i16* %15, align 2, !dbg !234
  %160 = load i16, i16* %19, align 2, !dbg !235
  %161 = add i16 %159, %160, !dbg !236
  store i16 %161, i16* %16, align 2, !dbg !237
  %162 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !238
  %163 = load double, double* %162, align 8, !dbg !238
  %164 = load float*, float** %11, align 2, !dbg !239
  %165 = load i16, i16* %16, align 2, !dbg !240
  %166 = getelementptr inbounds float, float* %164, i16 %165, !dbg !239
  %167 = load float, float* %166, align 4, !dbg !239
  %168 = fpext float %167 to double, !dbg !239
  %169 = fmul double %163, %168, !dbg !241
  %170 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !242
  %171 = load double, double* %170, align 8, !dbg !242
  %172 = load float*, float** %12, align 2, !dbg !243
  %173 = load i16, i16* %16, align 2, !dbg !244
  %174 = getelementptr inbounds float, float* %172, i16 %173, !dbg !243
  %175 = load float, float* %174, align 4, !dbg !243
  %176 = fpext float %175 to double, !dbg !243
  %177 = fmul double %171, %176, !dbg !245
  %178 = fsub double %169, %177, !dbg !246
  store double %178, double* %21, align 8, !dbg !247
  %179 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !248
  %180 = load double, double* %179, align 8, !dbg !248
  %181 = load float*, float** %12, align 2, !dbg !249
  %182 = load i16, i16* %16, align 2, !dbg !250
  %183 = getelementptr inbounds float, float* %181, i16 %182, !dbg !249
  %184 = load float, float* %183, align 4, !dbg !249
  %185 = fpext float %184 to double, !dbg !249
  %186 = fmul double %180, %185, !dbg !251
  %187 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !252
  %188 = load double, double* %187, align 8, !dbg !252
  %189 = load float*, float** %11, align 2, !dbg !253
  %190 = load i16, i16* %16, align 2, !dbg !254
  %191 = getelementptr inbounds float, float* %189, i16 %190, !dbg !253
  %192 = load float, float* %191, align 4, !dbg !253
  %193 = fpext float %192 to double, !dbg !253
  %194 = fmul double %188, %193, !dbg !255
  %195 = fadd double %186, %194, !dbg !256
  store double %195, double* %22, align 8, !dbg !257
  %196 = load float*, float** %11, align 2, !dbg !258
  %197 = load i16, i16* %15, align 2, !dbg !259
  %198 = getelementptr inbounds float, float* %196, i16 %197, !dbg !258
  %199 = load float, float* %198, align 4, !dbg !258
  %200 = fpext float %199 to double, !dbg !258
  %201 = load double, double* %21, align 8, !dbg !260
  %202 = fsub double %200, %201, !dbg !261
  %203 = fptrunc double %202 to float, !dbg !258
  %204 = load float*, float** %11, align 2, !dbg !262
  %205 = load i16, i16* %16, align 2, !dbg !263
  %206 = getelementptr inbounds float, float* %204, i16 %205, !dbg !262
  store float %203, float* %206, align 4, !dbg !264
  %207 = load float*, float** %12, align 2, !dbg !265
  %208 = load i16, i16* %15, align 2, !dbg !266
  %209 = getelementptr inbounds float, float* %207, i16 %208, !dbg !265
  %210 = load float, float* %209, align 4, !dbg !265
  %211 = fpext float %210 to double, !dbg !265
  %212 = load double, double* %22, align 8, !dbg !267
  %213 = fsub double %211, %212, !dbg !268
  %214 = fptrunc double %213 to float, !dbg !265
  %215 = load float*, float** %12, align 2, !dbg !269
  %216 = load i16, i16* %16, align 2, !dbg !270
  %217 = getelementptr inbounds float, float* %215, i16 %216, !dbg !269
  store float %214, float* %217, align 4, !dbg !271
  %218 = load double, double* %21, align 8, !dbg !272
  %219 = load float*, float** %11, align 2, !dbg !273
  %220 = load i16, i16* %15, align 2, !dbg !274
  %221 = getelementptr inbounds float, float* %219, i16 %220, !dbg !273
  %222 = load float, float* %221, align 4, !dbg !275
  %223 = fpext float %222 to double, !dbg !275
  %224 = fadd double %223, %218, !dbg !275
  %225 = fptrunc double %224 to float, !dbg !275
  store float %225, float* %221, align 4, !dbg !275
  %226 = load double, double* %22, align 8, !dbg !276
  %227 = load float*, float** %12, align 2, !dbg !277
  %228 = load i16, i16* %15, align 2, !dbg !278
  %229 = getelementptr inbounds float, float* %227, i16 %228, !dbg !277
  %230 = load float, float* %229, align 4, !dbg !279
  %231 = fpext float %230 to double, !dbg !279
  %232 = fadd double %231, %226, !dbg !279
  %233 = fptrunc double %232 to float, !dbg !279
  store float %233, float* %229, align 4, !dbg !279
  br label %234, !dbg !280

; <label>:234:                                    ; preds = %129
  %235 = load i16, i16* %15, align 2, !dbg !281
  %236 = add i16 %235, 1, !dbg !281
  store i16 %236, i16* %15, align 2, !dbg !281
  %237 = load i16, i16* %17, align 2, !dbg !282
  %238 = add i16 %237, 1, !dbg !282
  store i16 %238, i16* %17, align 2, !dbg !282
  br label %125, !dbg !283, !llvm.loop !284

; <label>:239:                                    ; preds = %125
  br label %240, !dbg !286

; <label>:240:                                    ; preds = %239
  %241 = load i16, i16* %18, align 2, !dbg !287
  %242 = load i16, i16* %14, align 2, !dbg !288
  %243 = add i16 %242, %241, !dbg !288
  store i16 %243, i16* %14, align 2, !dbg !288
  br label %110, !dbg !289, !llvm.loop !290

; <label>:244:                                    ; preds = %110
  %245 = load i16, i16* %18, align 2, !dbg !292
  store i16 %245, i16* %19, align 2, !dbg !293
  br label %246, !dbg !294

; <label>:246:                                    ; preds = %244
  %247 = load i16, i16* %18, align 2, !dbg !295
  %248 = shl i16 %247, 1, !dbg !295
  store i16 %248, i16* %18, align 2, !dbg !295
  br label %86, !dbg !296, !llvm.loop !297

; <label>:249:                                    ; preds = %86
  %250 = load i16, i16* %8, align 2, !dbg !299
  %251 = icmp ne i16 %250, 0, !dbg !299
  br i1 %251, label %252, label %281, !dbg !301

; <label>:252:                                    ; preds = %249
  call void @llvm.dbg.declare(metadata double* %31, metadata !302, metadata !DIExpression()), !dbg !304
  %253 = load i16, i16* %7, align 2, !dbg !305
  %254 = uitofp i16 %253 to double, !dbg !306
  store double %254, double* %31, align 8, !dbg !304
  store i16 0, i16* %14, align 2, !dbg !307
  br label %255, !dbg !309

; <label>:255:                                    ; preds = %277, %252
  %256 = load i16, i16* %14, align 2, !dbg !310
  %257 = load i16, i16* %7, align 2, !dbg !312
  %258 = icmp ult i16 %256, %257, !dbg !313
  br i1 %258, label %259, label %280, !dbg !314

; <label>:259:                                    ; preds = %255
  %260 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 1024), !dbg !315
  %261 = load double, double* %31, align 8, !dbg !317
  %262 = load float*, float** %11, align 2, !dbg !318
  %263 = load i16, i16* %14, align 2, !dbg !319
  %264 = getelementptr inbounds float, float* %262, i16 %263, !dbg !318
  %265 = load float, float* %264, align 4, !dbg !320
  %266 = fpext float %265 to double, !dbg !320
  %267 = fdiv double %266, %261, !dbg !320
  %268 = fptrunc double %267 to float, !dbg !320
  store float %268, float* %264, align 4, !dbg !320
  %269 = load double, double* %31, align 8, !dbg !321
  %270 = load float*, float** %12, align 2, !dbg !322
  %271 = load i16, i16* %14, align 2, !dbg !323
  %272 = getelementptr inbounds float, float* %270, i16 %271, !dbg !322
  %273 = load float, float* %272, align 4, !dbg !324
  %274 = fpext float %273 to double, !dbg !324
  %275 = fdiv double %274, %269, !dbg !324
  %276 = fptrunc double %275 to float, !dbg !324
  store float %276, float* %272, align 4, !dbg !324
  br label %277, !dbg !325

; <label>:277:                                    ; preds = %259
  %278 = load i16, i16* %14, align 2, !dbg !326
  %279 = add i16 %278, 1, !dbg !326
  store i16 %279, i16* %14, align 2, !dbg !326
  br label %255, !dbg !327, !llvm.loop !328

; <label>:280:                                    ; preds = %255
  br label %281, !dbg !330

; <label>:281:                                    ; preds = %280, %249
  ret void, !dbg !331
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

; Function Attrs: noreturn
declare dso_local void @exit(i16) #2

; Function Attrs: noinline nounwind optnone
define internal void @CheckPointer(i8*, i8*) #0 !dbg !332 {
  %3 = alloca i8*, align 2
  %4 = alloca i8*, align 2
  store i8* %0, i8** %3, align 2
  call void @llvm.dbg.declare(metadata i8** %3, metadata !338, metadata !DIExpression()), !dbg !339
  store i8* %1, i8** %4, align 2
  call void @llvm.dbg.declare(metadata i8** %4, metadata !340, metadata !DIExpression()), !dbg !341
  %5 = load i8*, i8** %3, align 2, !dbg !342
  %6 = icmp eq i8* %5, null, !dbg !344
  br i1 %6, label %7, label %8, !dbg !345

; <label>:7:                                      ; preds = %2
  call void @exit(i16 1) #5, !dbg !346
  unreachable, !dbg !346

; <label>:8:                                      ; preds = %2
  ret void, !dbg !348
}

declare dso_local i16 @__max_iter(...) #3

; Function Attrs: nounwind
declare dso_local double @sin(double) #4

; Function Attrs: nounwind
declare dso_local double @cos(double) #4

; Function Attrs: noinline nounwind optnone
define dso_local i16 @main() #0 !dbg !349 {
  %1 = alloca i16, align 2
  store i16 0, i16* %1, align 2
  %2 = call i16 bitcast (i16 (...)* @restore to i16 ()*)(), !dbg !352
  store i16 128, i16* @MAXSIZE, align 2, !dbg !353
  %3 = call i16 @old_main(), !dbg !354
  store i16 1, i16* @invfft, align 2, !dbg !355
  store i16 256, i16* @MAXSIZE, align 2, !dbg !356
  %4 = call i16 @old_main(), !dbg !357
  ret i16 0, !dbg !358
}

declare dso_local i16 @restore(...) #3

; Function Attrs: noinline nounwind optnone
define dso_local i16 @old_main() #0 !dbg !359 {
  %1 = alloca i16, align 2
  %2 = alloca i16, align 2
  %3 = alloca float*, align 2
  %4 = alloca float*, align 2
  %5 = alloca float*, align 2
  %6 = alloca float*, align 2
  %7 = alloca float*, align 2
  %8 = alloca float*, align 2
  call void @llvm.dbg.declare(metadata i16* %1, metadata !360, metadata !DIExpression()), !dbg !361
  call void @llvm.dbg.declare(metadata i16* %2, metadata !362, metadata !DIExpression()), !dbg !363
  call void @llvm.dbg.declare(metadata float** %3, metadata !364, metadata !DIExpression()), !dbg !365
  call void @llvm.dbg.declare(metadata float** %4, metadata !366, metadata !DIExpression()), !dbg !367
  call void @llvm.dbg.declare(metadata float** %5, metadata !368, metadata !DIExpression()), !dbg !369
  call void @llvm.dbg.declare(metadata float** %6, metadata !370, metadata !DIExpression()), !dbg !371
  call void @llvm.dbg.declare(metadata float** %7, metadata !372, metadata !DIExpression()), !dbg !373
  call void @llvm.dbg.declare(metadata float** %8, metadata !374, metadata !DIExpression()), !dbg !375
  %9 = call i16 bitcast (i16 (...)* @srand to i16 (i16)*)(i16 1), !dbg !376
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @realin, i32 0, i32 0), float** %3, align 2, !dbg !377
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @imagin, i32 0, i32 0), float** %4, align 2, !dbg !378
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @realout, i32 0, i32 0), float** %5, align 2, !dbg !379
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @imagout, i32 0, i32 0), float** %6, align 2, !dbg !380
  store float* getelementptr inbounds ([16 x float], [16 x float]* @Coeff, i32 0, i32 0), float** %7, align 2, !dbg !381
  store float* getelementptr inbounds ([16 x float], [16 x float]* @Amp, i32 0, i32 0), float** %8, align 2, !dbg !382
  store i16 0, i16* %1, align 2, !dbg !383
  br label %10, !dbg !385

; <label>:10:                                     ; preds = %27, %0
  %11 = load i16, i16* %1, align 2, !dbg !386
  %12 = icmp ult i16 %11, 4, !dbg !388
  br i1 %12, label %13, label %30, !dbg !389

; <label>:13:                                     ; preds = %10
  %14 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 4), !dbg !390
  %15 = call i16 bitcast (i16 (...)* @rand to i16 ()*)(), !dbg !392
  %16 = srem i16 %15, 1000, !dbg !393
  %17 = sitofp i16 %16 to float, !dbg !392
  %18 = load float*, float** %7, align 2, !dbg !394
  %19 = load i16, i16* %1, align 2, !dbg !395
  %20 = getelementptr inbounds float, float* %18, i16 %19, !dbg !394
  store float %17, float* %20, align 4, !dbg !396
  %21 = call i16 bitcast (i16 (...)* @rand to i16 ()*)(), !dbg !397
  %22 = srem i16 %21, 1000, !dbg !398
  %23 = sitofp i16 %22 to float, !dbg !397
  %24 = load float*, float** %8, align 2, !dbg !399
  %25 = load i16, i16* %1, align 2, !dbg !400
  %26 = getelementptr inbounds float, float* %24, i16 %25, !dbg !399
  store float %23, float* %26, align 4, !dbg !401
  br label %27, !dbg !402

; <label>:27:                                     ; preds = %13
  %28 = load i16, i16* %1, align 2, !dbg !403
  %29 = add i16 %28, 1, !dbg !403
  store i16 %29, i16* %1, align 2, !dbg !403
  br label %10, !dbg !404, !llvm.loop !405

; <label>:30:                                     ; preds = %10
  store i16 0, i16* %1, align 2, !dbg !407
  br label %31, !dbg !409

; <label>:31:                                     ; preds = %102, %30
  %32 = load i16, i16* %1, align 2, !dbg !410
  %33 = load i16, i16* @MAXSIZE, align 2, !dbg !412
  %34 = icmp ult i16 %32, %33, !dbg !413
  br i1 %34, label %35, label %105, !dbg !414

; <label>:35:                                     ; preds = %31
  %36 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 256), !dbg !415
  %37 = load float*, float** %3, align 2, !dbg !417
  %38 = load i16, i16* %1, align 2, !dbg !418
  %39 = getelementptr inbounds float, float* %37, i16 %38, !dbg !417
  store float 0.000000e+00, float* %39, align 4, !dbg !419
  store i16 0, i16* %2, align 2, !dbg !420
  br label %40, !dbg !422

; <label>:40:                                     ; preds = %98, %35
  %41 = load i16, i16* %2, align 2, !dbg !423
  %42 = icmp ult i16 %41, 4, !dbg !425
  br i1 %42, label %43, label %101, !dbg !426

; <label>:43:                                     ; preds = %40
  %44 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 4), !dbg !427
  %45 = call i16 bitcast (i16 (...)* @rand to i16 ()*)(), !dbg !429
  %46 = srem i16 %45, 2, !dbg !431
  %47 = icmp ne i16 %46, 0, !dbg !431
  br i1 %47, label %48, label %71, !dbg !432

; <label>:48:                                     ; preds = %43
  %49 = load float*, float** %7, align 2, !dbg !433
  %50 = load i16, i16* %2, align 2, !dbg !435
  %51 = getelementptr inbounds float, float* %49, i16 %50, !dbg !433
  %52 = load float, float* %51, align 4, !dbg !433
  %53 = fpext float %52 to double, !dbg !433
  %54 = load float*, float** %8, align 2, !dbg !436
  %55 = load i16, i16* %2, align 2, !dbg !437
  %56 = getelementptr inbounds float, float* %54, i16 %55, !dbg !436
  %57 = load float, float* %56, align 4, !dbg !436
  %58 = load i16, i16* %1, align 2, !dbg !438
  %59 = uitofp i16 %58 to float, !dbg !438
  %60 = fmul float %57, %59, !dbg !439
  %61 = fpext float %60 to double, !dbg !436
  %62 = call double @cos(double %61) #6, !dbg !440
  %63 = fmul double %53, %62, !dbg !441
  %64 = load float*, float** %3, align 2, !dbg !442
  %65 = load i16, i16* %1, align 2, !dbg !443
  %66 = getelementptr inbounds float, float* %64, i16 %65, !dbg !442
  %67 = load float, float* %66, align 4, !dbg !444
  %68 = fpext float %67 to double, !dbg !444
  %69 = fadd double %68, %63, !dbg !444
  %70 = fptrunc double %69 to float, !dbg !444
  store float %70, float* %66, align 4, !dbg !444
  br label %94, !dbg !445

; <label>:71:                                     ; preds = %43
  %72 = load float*, float** %7, align 2, !dbg !446
  %73 = load i16, i16* %2, align 2, !dbg !448
  %74 = getelementptr inbounds float, float* %72, i16 %73, !dbg !446
  %75 = load float, float* %74, align 4, !dbg !446
  %76 = fpext float %75 to double, !dbg !446
  %77 = load float*, float** %8, align 2, !dbg !449
  %78 = load i16, i16* %2, align 2, !dbg !450
  %79 = getelementptr inbounds float, float* %77, i16 %78, !dbg !449
  %80 = load float, float* %79, align 4, !dbg !449
  %81 = load i16, i16* %1, align 2, !dbg !451
  %82 = uitofp i16 %81 to float, !dbg !451
  %83 = fmul float %80, %82, !dbg !452
  %84 = fpext float %83 to double, !dbg !449
  %85 = call double @sin(double %84) #6, !dbg !453
  %86 = fmul double %76, %85, !dbg !454
  %87 = load float*, float** %3, align 2, !dbg !455
  %88 = load i16, i16* %1, align 2, !dbg !456
  %89 = getelementptr inbounds float, float* %87, i16 %88, !dbg !455
  %90 = load float, float* %89, align 4, !dbg !457
  %91 = fpext float %90 to double, !dbg !457
  %92 = fadd double %91, %86, !dbg !457
  %93 = fptrunc double %92 to float, !dbg !457
  store float %93, float* %89, align 4, !dbg !457
  br label %94

; <label>:94:                                     ; preds = %71, %48
  %95 = load float*, float** %4, align 2, !dbg !458
  %96 = load i16, i16* %1, align 2, !dbg !459
  %97 = getelementptr inbounds float, float* %95, i16 %96, !dbg !458
  store float 0.000000e+00, float* %97, align 4, !dbg !460
  br label %98, !dbg !461

; <label>:98:                                     ; preds = %94
  %99 = load i16, i16* %2, align 2, !dbg !462
  %100 = add i16 %99, 1, !dbg !462
  store i16 %100, i16* %2, align 2, !dbg !462
  br label %40, !dbg !463, !llvm.loop !464

; <label>:101:                                    ; preds = %40
  br label %102, !dbg !466

; <label>:102:                                    ; preds = %101
  %103 = load i16, i16* %1, align 2, !dbg !467
  %104 = add i16 %103, 1, !dbg !467
  store i16 %104, i16* %1, align 2, !dbg !467
  br label %31, !dbg !468, !llvm.loop !469

; <label>:105:                                    ; preds = %31
  %106 = load i16, i16* @MAXSIZE, align 2, !dbg !471
  %107 = load i16, i16* @invfft, align 2, !dbg !472
  %108 = load float*, float** %3, align 2, !dbg !473
  %109 = load float*, float** %4, align 2, !dbg !474
  %110 = load float*, float** %5, align 2, !dbg !475
  %111 = load float*, float** %6, align 2, !dbg !476
  call void @fft_float(i16 %106, i16 %107, float* %108, float* %109, float* %110, float* %111), !dbg !477
  %112 = call i16 bitcast (i16 (...)* @checkpoint to i16 ()*)(), !dbg !478
  ret i16 0, !dbg !479
}

declare dso_local i16 @srand(...) #3

declare dso_local i16 @rand(...) #3

declare dso_local i16 @checkpoint(...) #3

; Function Attrs: noinline nounwind optnone
define dso_local i16 @IsPowerOfTwo(i16) #0 !dbg !480 {
  %2 = alloca i16, align 2
  %3 = alloca i16, align 2
  store i16 %0, i16* %3, align 2
  call void @llvm.dbg.declare(metadata i16* %3, metadata !483, metadata !DIExpression()), !dbg !484
  %4 = load i16, i16* %3, align 2, !dbg !485
  %5 = icmp ult i16 %4, 2, !dbg !487
  br i1 %5, label %6, label %7, !dbg !488

; <label>:6:                                      ; preds = %1
  store i16 0, i16* %2, align 2, !dbg !489
  br label %15, !dbg !489

; <label>:7:                                      ; preds = %1
  %8 = load i16, i16* %3, align 2, !dbg !490
  %9 = load i16, i16* %3, align 2, !dbg !492
  %10 = sub i16 %9, 1, !dbg !493
  %11 = and i16 %8, %10, !dbg !494
  %12 = icmp ne i16 %11, 0, !dbg !494
  br i1 %12, label %13, label %14, !dbg !495

; <label>:13:                                     ; preds = %7
  store i16 0, i16* %2, align 2, !dbg !496
  br label %15, !dbg !496

; <label>:14:                                     ; preds = %7
  store i16 1, i16* %2, align 2, !dbg !497
  br label %15, !dbg !497

; <label>:15:                                     ; preds = %14, %13, %6
  %16 = load i16, i16* %2, align 2, !dbg !498
  ret i16 %16, !dbg !498
}

; Function Attrs: noinline nounwind optnone
define dso_local i16 @NumberOfBitsNeeded(i16) #0 !dbg !499 {
  %2 = alloca i16, align 2
  %3 = alloca i16, align 2
  store i16 %0, i16* %2, align 2
  call void @llvm.dbg.declare(metadata i16* %2, metadata !502, metadata !DIExpression()), !dbg !503
  call void @llvm.dbg.declare(metadata i16* %3, metadata !504, metadata !DIExpression()), !dbg !505
  %4 = load i16, i16* %2, align 2, !dbg !506
  %5 = icmp ult i16 %4, 2, !dbg !508
  br i1 %5, label %6, label %9, !dbg !509

; <label>:6:                                      ; preds = %1
  %7 = load i16, i16* %2, align 2, !dbg !510
  %8 = call i16 (i8*, ...) @printf(i8* getelementptr inbounds ([73 x i8], [73 x i8]* @.str.5, i32 0, i32 0), i16 %7), !dbg !512
  call void @exit(i16 1) #5, !dbg !513
  unreachable, !dbg !513

; <label>:9:                                      ; preds = %1
  store i16 0, i16* %3, align 2, !dbg !514
  br label %10, !dbg !516

; <label>:10:                                     ; preds = %20, %9
  %11 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 16), !dbg !517
  %12 = load i16, i16* %2, align 2, !dbg !520
  %13 = load i16, i16* %3, align 2, !dbg !522
  %14 = shl i16 1, %13, !dbg !523
  %15 = and i16 %12, %14, !dbg !524
  %16 = icmp ne i16 %15, 0, !dbg !524
  br i1 %16, label %17, label %19, !dbg !525

; <label>:17:                                     ; preds = %10
  %18 = load i16, i16* %3, align 2, !dbg !526
  ret i16 %18, !dbg !528

; <label>:19:                                     ; preds = %10
  br label %20, !dbg !529

; <label>:20:                                     ; preds = %19
  %21 = load i16, i16* %3, align 2, !dbg !530
  %22 = add i16 %21, 1, !dbg !530
  store i16 %22, i16* %3, align 2, !dbg !530
  br label %10, !dbg !531, !llvm.loop !532
}

declare dso_local i16 @printf(i8*, ...) #3

; Function Attrs: noinline nounwind optnone
define dso_local i16 @ReverseBits(i16, i16) #0 !dbg !535 {
  %3 = alloca i16, align 2
  %4 = alloca i16, align 2
  %5 = alloca i16, align 2
  %6 = alloca i16, align 2
  store i16 %0, i16* %3, align 2
  call void @llvm.dbg.declare(metadata i16* %3, metadata !538, metadata !DIExpression()), !dbg !539
  store i16 %1, i16* %4, align 2
  call void @llvm.dbg.declare(metadata i16* %4, metadata !540, metadata !DIExpression()), !dbg !541
  call void @llvm.dbg.declare(metadata i16* %5, metadata !542, metadata !DIExpression()), !dbg !543
  call void @llvm.dbg.declare(metadata i16* %6, metadata !544, metadata !DIExpression()), !dbg !545
  store i16 0, i16* %6, align 2, !dbg !546
  store i16 0, i16* %5, align 2, !dbg !548
  br label %7, !dbg !549

; <label>:7:                                      ; preds = %20, %2
  %8 = load i16, i16* %5, align 2, !dbg !550
  %9 = load i16, i16* %4, align 2, !dbg !552
  %10 = icmp ult i16 %8, %9, !dbg !553
  br i1 %10, label %11, label %23, !dbg !554

; <label>:11:                                     ; preds = %7
  %12 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 16), !dbg !555
  %13 = load i16, i16* %6, align 2, !dbg !557
  %14 = shl i16 %13, 1, !dbg !558
  %15 = load i16, i16* %3, align 2, !dbg !559
  %16 = and i16 %15, 1, !dbg !560
  %17 = or i16 %14, %16, !dbg !561
  store i16 %17, i16* %6, align 2, !dbg !562
  %18 = load i16, i16* %3, align 2, !dbg !563
  %19 = lshr i16 %18, 1, !dbg !563
  store i16 %19, i16* %3, align 2, !dbg !563
  br label %20, !dbg !564

; <label>:20:                                     ; preds = %11
  %21 = load i16, i16* %5, align 2, !dbg !565
  %22 = add i16 %21, 1, !dbg !565
  store i16 %22, i16* %5, align 2, !dbg !565
  br label %7, !dbg !566, !llvm.loop !567

; <label>:23:                                     ; preds = %7
  %24 = load i16, i16* %6, align 2, !dbg !569
  ret i16 %24, !dbg !570
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn }
attributes #6 = { nounwind }

!llvm.dbg.cu = !{!29, !2, !33}
!llvm.ident = !{!35, !35, !35}
!llvm.module.flags = !{!36, !37, !38}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "invfft", scope: !2, file: !3, line: 5, type: !28, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 8.0.1-9 (tags/RELEASE_801/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5, nameTableKind: None)
!3 = !DIFile(filename: "main.c", directory: "/home/user/Desktop/alfred/samples/fft")
!4 = !{}
!5 = !{!0, !6, !9, !15, !17, !19, !21, !26}
!6 = !DIGlobalVariableExpression(var: !7, expr: !DIExpression())
!7 = distinct !DIGlobalVariable(name: "MAXSIZE", scope: !2, file: !3, line: 6, type: !8, isLocal: false, isDefinition: true)
!8 = !DIBasicType(name: "unsigned int", size: 16, encoding: DW_ATE_unsigned)
!9 = !DIGlobalVariableExpression(var: !10, expr: !DIExpression())
!10 = distinct !DIGlobalVariable(name: "realin", scope: !2, file: !3, line: 8, type: !11, isLocal: true, isDefinition: true)
!11 = !DICompositeType(tag: DW_TAG_array_type, baseType: !12, size: 32768, elements: !13)
!12 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!13 = !{!14}
!14 = !DISubrange(count: 1024)
!15 = !DIGlobalVariableExpression(var: !16, expr: !DIExpression())
!16 = distinct !DIGlobalVariable(name: "imagin", scope: !2, file: !3, line: 9, type: !11, isLocal: true, isDefinition: true)
!17 = !DIGlobalVariableExpression(var: !18, expr: !DIExpression())
!18 = distinct !DIGlobalVariable(name: "realout", scope: !2, file: !3, line: 10, type: !11, isLocal: true, isDefinition: true)
!19 = !DIGlobalVariableExpression(var: !20, expr: !DIExpression())
!20 = distinct !DIGlobalVariable(name: "imagout", scope: !2, file: !3, line: 11, type: !11, isLocal: true, isDefinition: true)
!21 = !DIGlobalVariableExpression(var: !22, expr: !DIExpression())
!22 = distinct !DIGlobalVariable(name: "Coeff", scope: !2, file: !3, line: 12, type: !23, isLocal: true, isDefinition: true)
!23 = !DICompositeType(tag: DW_TAG_array_type, baseType: !12, size: 512, elements: !24)
!24 = !{!25}
!25 = !DISubrange(count: 16)
!26 = !DIGlobalVariableExpression(var: !27, expr: !DIExpression())
!27 = distinct !DIGlobalVariable(name: "Amp", scope: !2, file: !3, line: 13, type: !23, isLocal: true, isDefinition: true)
!28 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!29 = distinct !DICompileUnit(language: DW_LANG_C99, file: !30, producer: "clang version 8.0.1-9 (tags/RELEASE_801/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, retainedTypes: !31, nameTableKind: None)
!30 = !DIFile(filename: "fourierf.c", directory: "/home/user/Desktop/alfred/samples/fft")
!31 = !{!32}
!32 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!33 = distinct !DICompileUnit(language: DW_LANG_C99, file: !34, producer: "clang version 8.0.1-9 (tags/RELEASE_801/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, nameTableKind: None)
!34 = !DIFile(filename: "fftmisc.c", directory: "/home/user/Desktop/alfred/samples/fft")
!35 = !{!"clang version 8.0.1-9 (tags/RELEASE_801/final)"}
!36 = !{i32 2, !"Dwarf Version", i32 4}
!37 = !{i32 2, !"Debug Info Version", i32 3}
!38 = !{i32 1, !"wchar_size", i32 2}
!39 = distinct !DISubprogram(name: "fft_float", scope: !30, file: !30, line: 36, type: !40, scopeLine: 43, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !29, retainedNodes: !4)
!40 = !DISubroutineType(types: !41)
!41 = !{null, !8, !28, !42, !42, !42, !42}
!42 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !12, size: 16)
!43 = !DILocalVariable(name: "NumSamples", arg: 1, scope: !39, file: !30, line: 37, type: !8)
!44 = !DILocation(line: 37, column: 15, scope: !39)
!45 = !DILocalVariable(name: "InverseTransform", arg: 2, scope: !39, file: !30, line: 38, type: !28)
!46 = !DILocation(line: 38, column: 15, scope: !39)
!47 = !DILocalVariable(name: "RealIn", arg: 3, scope: !39, file: !30, line: 39, type: !42)
!48 = !DILocation(line: 39, column: 15, scope: !39)
!49 = !DILocalVariable(name: "ImagIn", arg: 4, scope: !39, file: !30, line: 40, type: !42)
!50 = !DILocation(line: 40, column: 15, scope: !39)
!51 = !DILocalVariable(name: "RealOut", arg: 5, scope: !39, file: !30, line: 41, type: !42)
!52 = !DILocation(line: 41, column: 15, scope: !39)
!53 = !DILocalVariable(name: "ImagOut", arg: 6, scope: !39, file: !30, line: 42, type: !42)
!54 = !DILocation(line: 42, column: 15, scope: !39)
!55 = !DILocalVariable(name: "NumBits", scope: !39, file: !30, line: 44, type: !8)
!56 = !DILocation(line: 44, column: 14, scope: !39)
!57 = !DILocalVariable(name: "i", scope: !39, file: !30, line: 45, type: !8)
!58 = !DILocation(line: 45, column: 14, scope: !39)
!59 = !DILocalVariable(name: "j", scope: !39, file: !30, line: 45, type: !8)
!60 = !DILocation(line: 45, column: 17, scope: !39)
!61 = !DILocalVariable(name: "k", scope: !39, file: !30, line: 45, type: !8)
!62 = !DILocation(line: 45, column: 20, scope: !39)
!63 = !DILocalVariable(name: "n", scope: !39, file: !30, line: 45, type: !8)
!64 = !DILocation(line: 45, column: 23, scope: !39)
!65 = !DILocalVariable(name: "BlockSize", scope: !39, file: !30, line: 46, type: !8)
!66 = !DILocation(line: 46, column: 14, scope: !39)
!67 = !DILocalVariable(name: "BlockEnd", scope: !39, file: !30, line: 46, type: !8)
!68 = !DILocation(line: 46, column: 25, scope: !39)
!69 = !DILocalVariable(name: "angle_numerator", scope: !39, file: !30, line: 48, type: !32)
!70 = !DILocation(line: 48, column: 12, scope: !39)
!71 = !DILocalVariable(name: "tr", scope: !39, file: !30, line: 49, type: !32)
!72 = !DILocation(line: 49, column: 12, scope: !39)
!73 = !DILocalVariable(name: "ti", scope: !39, file: !30, line: 49, type: !32)
!74 = !DILocation(line: 49, column: 16, scope: !39)
!75 = !DILocation(line: 51, column: 24, scope: !76)
!76 = distinct !DILexicalBlock(scope: !39, file: !30, line: 51, column: 10)
!77 = !DILocation(line: 51, column: 11, scope: !76)
!78 = !DILocation(line: 51, column: 10, scope: !39)
!79 = !DILocation(line: 58, column: 9, scope: !80)
!80 = distinct !DILexicalBlock(scope: !76, file: !30, line: 52, column: 5)
!81 = !DILocation(line: 61, column: 10, scope: !82)
!82 = distinct !DILexicalBlock(scope: !39, file: !30, line: 61, column: 10)
!83 = !DILocation(line: 61, column: 10, scope: !39)
!84 = !DILocation(line: 62, column: 28, scope: !82)
!85 = !DILocation(line: 62, column: 27, scope: !82)
!86 = !DILocation(line: 62, column: 25, scope: !82)
!87 = !DILocation(line: 62, column: 9, scope: !82)
!88 = !DILocation(line: 64, column: 5, scope: !39)
!89 = !DILocation(line: 65, column: 5, scope: !39)
!90 = !DILocation(line: 66, column: 5, scope: !39)
!91 = !DILocation(line: 68, column: 36, scope: !39)
!92 = !DILocation(line: 68, column: 15, scope: !39)
!93 = !DILocation(line: 68, column: 13, scope: !39)
!94 = !DILocation(line: 74, column: 12, scope: !95)
!95 = distinct !DILexicalBlock(scope: !39, file: !30, line: 74, column: 5)
!96 = !DILocation(line: 74, column: 11, scope: !95)
!97 = !DILocation(line: 74, column: 16, scope: !98)
!98 = distinct !DILexicalBlock(scope: !95, file: !30, line: 74, column: 5)
!99 = !DILocation(line: 74, column: 20, scope: !98)
!100 = !DILocation(line: 74, column: 18, scope: !98)
!101 = !DILocation(line: 74, column: 5, scope: !95)
!102 = !DILocation(line: 76, column: 9, scope: !103)
!103 = distinct !DILexicalBlock(scope: !98, file: !30, line: 75, column: 5)
!104 = !DILocation(line: 77, column: 27, scope: !103)
!105 = !DILocation(line: 77, column: 30, scope: !103)
!106 = !DILocation(line: 77, column: 13, scope: !103)
!107 = !DILocation(line: 77, column: 11, scope: !103)
!108 = !DILocation(line: 78, column: 22, scope: !103)
!109 = !DILocation(line: 78, column: 29, scope: !103)
!110 = !DILocation(line: 78, column: 9, scope: !103)
!111 = !DILocation(line: 78, column: 17, scope: !103)
!112 = !DILocation(line: 78, column: 20, scope: !103)
!113 = !DILocation(line: 79, column: 23, scope: !103)
!114 = !DILocation(line: 79, column: 30, scope: !103)
!115 = !DILocation(line: 79, column: 22, scope: !103)
!116 = !DILocation(line: 79, column: 47, scope: !103)
!117 = !DILocation(line: 79, column: 54, scope: !103)
!118 = !DILocation(line: 79, column: 9, scope: !103)
!119 = !DILocation(line: 79, column: 17, scope: !103)
!120 = !DILocation(line: 79, column: 20, scope: !103)
!121 = !DILocation(line: 81, column: 5, scope: !103)
!122 = !DILocation(line: 74, column: 33, scope: !98)
!123 = !DILocation(line: 74, column: 5, scope: !98)
!124 = distinct !{!124, !101, !125}
!125 = !DILocation(line: 81, column: 5, scope: !95)
!126 = !DILocation(line: 87, column: 14, scope: !39)
!127 = !DILocation(line: 88, column: 21, scope: !128)
!128 = distinct !DILexicalBlock(scope: !39, file: !30, line: 88, column: 5)
!129 = !DILocation(line: 88, column: 11, scope: !128)
!130 = !DILocation(line: 88, column: 26, scope: !131)
!131 = distinct !DILexicalBlock(scope: !128, file: !30, line: 88, column: 5)
!132 = !DILocation(line: 88, column: 39, scope: !131)
!133 = !DILocation(line: 88, column: 36, scope: !131)
!134 = !DILocation(line: 88, column: 5, scope: !128)
!135 = !DILocation(line: 90, column: 9, scope: !136)
!136 = distinct !DILexicalBlock(scope: !131, file: !30, line: 89, column: 5)
!137 = !DILocalVariable(name: "delta_angle", scope: !136, file: !30, line: 91, type: !32)
!138 = !DILocation(line: 91, column: 16, scope: !136)
!139 = !DILocation(line: 91, column: 30, scope: !136)
!140 = !DILocation(line: 91, column: 56, scope: !136)
!141 = !DILocation(line: 91, column: 48, scope: !136)
!142 = !DILocation(line: 91, column: 46, scope: !136)
!143 = !DILocalVariable(name: "sm2", scope: !136, file: !30, line: 92, type: !32)
!144 = !DILocation(line: 92, column: 16, scope: !136)
!145 = !DILocation(line: 92, column: 33, scope: !136)
!146 = !DILocation(line: 92, column: 31, scope: !136)
!147 = !DILocation(line: 92, column: 22, scope: !136)
!148 = !DILocalVariable(name: "sm1", scope: !136, file: !30, line: 93, type: !32)
!149 = !DILocation(line: 93, column: 16, scope: !136)
!150 = !DILocation(line: 93, column: 29, scope: !136)
!151 = !DILocation(line: 93, column: 28, scope: !136)
!152 = !DILocation(line: 93, column: 22, scope: !136)
!153 = !DILocalVariable(name: "cm2", scope: !136, file: !30, line: 94, type: !32)
!154 = !DILocation(line: 94, column: 16, scope: !136)
!155 = !DILocation(line: 94, column: 33, scope: !136)
!156 = !DILocation(line: 94, column: 31, scope: !136)
!157 = !DILocation(line: 94, column: 22, scope: !136)
!158 = !DILocalVariable(name: "cm1", scope: !136, file: !30, line: 95, type: !32)
!159 = !DILocation(line: 95, column: 16, scope: !136)
!160 = !DILocation(line: 95, column: 29, scope: !136)
!161 = !DILocation(line: 95, column: 28, scope: !136)
!162 = !DILocation(line: 95, column: 22, scope: !136)
!163 = !DILocalVariable(name: "w", scope: !136, file: !30, line: 96, type: !32)
!164 = !DILocation(line: 96, column: 16, scope: !136)
!165 = !DILocation(line: 96, column: 24, scope: !136)
!166 = !DILocation(line: 96, column: 22, scope: !136)
!167 = !DILocalVariable(name: "ar", scope: !136, file: !30, line: 97, type: !168)
!168 = !DICompositeType(tag: DW_TAG_array_type, baseType: !32, size: 192, elements: !169)
!169 = !{!170}
!170 = !DISubrange(count: 3)
!171 = !DILocation(line: 97, column: 16, scope: !136)
!172 = !DILocalVariable(name: "ai", scope: !136, file: !30, line: 97, type: !168)
!173 = !DILocation(line: 97, column: 23, scope: !136)
!174 = !DILocation(line: 99, column: 16, scope: !175)
!175 = distinct !DILexicalBlock(scope: !136, file: !30, line: 99, column: 9)
!176 = !DILocation(line: 99, column: 15, scope: !175)
!177 = !DILocation(line: 99, column: 20, scope: !178)
!178 = distinct !DILexicalBlock(scope: !175, file: !30, line: 99, column: 9)
!179 = !DILocation(line: 99, column: 24, scope: !178)
!180 = !DILocation(line: 99, column: 22, scope: !178)
!181 = !DILocation(line: 99, column: 9, scope: !175)
!182 = !DILocation(line: 101, column: 13, scope: !183)
!183 = distinct !DILexicalBlock(scope: !178, file: !30, line: 100, column: 9)
!184 = !DILocation(line: 102, column: 21, scope: !183)
!185 = !DILocation(line: 102, column: 13, scope: !183)
!186 = !DILocation(line: 102, column: 19, scope: !183)
!187 = !DILocation(line: 103, column: 21, scope: !183)
!188 = !DILocation(line: 103, column: 13, scope: !183)
!189 = !DILocation(line: 103, column: 19, scope: !183)
!190 = !DILocation(line: 105, column: 21, scope: !183)
!191 = !DILocation(line: 105, column: 13, scope: !183)
!192 = !DILocation(line: 105, column: 19, scope: !183)
!193 = !DILocation(line: 106, column: 21, scope: !183)
!194 = !DILocation(line: 106, column: 13, scope: !183)
!195 = !DILocation(line: 106, column: 19, scope: !183)
!196 = !DILocation(line: 108, column: 21, scope: !197)
!197 = distinct !DILexicalBlock(scope: !183, file: !30, line: 108, column: 13)
!198 = !DILocation(line: 108, column: 20, scope: !197)
!199 = !DILocation(line: 108, column: 25, scope: !197)
!200 = !DILocation(line: 108, column: 19, scope: !197)
!201 = !DILocation(line: 108, column: 29, scope: !202)
!202 = distinct !DILexicalBlock(scope: !197, file: !30, line: 108, column: 13)
!203 = !DILocation(line: 108, column: 33, scope: !202)
!204 = !DILocation(line: 108, column: 31, scope: !202)
!205 = !DILocation(line: 108, column: 13, scope: !197)
!206 = !DILocation(line: 110, column: 17, scope: !207)
!207 = distinct !DILexicalBlock(scope: !202, file: !30, line: 109, column: 13)
!208 = !DILocation(line: 111, column: 25, scope: !207)
!209 = !DILocation(line: 111, column: 27, scope: !207)
!210 = !DILocation(line: 111, column: 26, scope: !207)
!211 = !DILocation(line: 111, column: 35, scope: !207)
!212 = !DILocation(line: 111, column: 33, scope: !207)
!213 = !DILocation(line: 111, column: 17, scope: !207)
!214 = !DILocation(line: 111, column: 23, scope: !207)
!215 = !DILocation(line: 112, column: 25, scope: !207)
!216 = !DILocation(line: 112, column: 17, scope: !207)
!217 = !DILocation(line: 112, column: 23, scope: !207)
!218 = !DILocation(line: 113, column: 25, scope: !207)
!219 = !DILocation(line: 113, column: 17, scope: !207)
!220 = !DILocation(line: 113, column: 23, scope: !207)
!221 = !DILocation(line: 115, column: 25, scope: !207)
!222 = !DILocation(line: 115, column: 27, scope: !207)
!223 = !DILocation(line: 115, column: 26, scope: !207)
!224 = !DILocation(line: 115, column: 35, scope: !207)
!225 = !DILocation(line: 115, column: 33, scope: !207)
!226 = !DILocation(line: 115, column: 17, scope: !207)
!227 = !DILocation(line: 115, column: 23, scope: !207)
!228 = !DILocation(line: 116, column: 25, scope: !207)
!229 = !DILocation(line: 116, column: 17, scope: !207)
!230 = !DILocation(line: 116, column: 23, scope: !207)
!231 = !DILocation(line: 117, column: 25, scope: !207)
!232 = !DILocation(line: 117, column: 17, scope: !207)
!233 = !DILocation(line: 117, column: 23, scope: !207)
!234 = !DILocation(line: 119, column: 21, scope: !207)
!235 = !DILocation(line: 119, column: 25, scope: !207)
!236 = !DILocation(line: 119, column: 23, scope: !207)
!237 = !DILocation(line: 119, column: 19, scope: !207)
!238 = !DILocation(line: 120, column: 22, scope: !207)
!239 = !DILocation(line: 120, column: 28, scope: !207)
!240 = !DILocation(line: 120, column: 36, scope: !207)
!241 = !DILocation(line: 120, column: 27, scope: !207)
!242 = !DILocation(line: 120, column: 41, scope: !207)
!243 = !DILocation(line: 120, column: 47, scope: !207)
!244 = !DILocation(line: 120, column: 55, scope: !207)
!245 = !DILocation(line: 120, column: 46, scope: !207)
!246 = !DILocation(line: 120, column: 39, scope: !207)
!247 = !DILocation(line: 120, column: 20, scope: !207)
!248 = !DILocation(line: 121, column: 22, scope: !207)
!249 = !DILocation(line: 121, column: 28, scope: !207)
!250 = !DILocation(line: 121, column: 36, scope: !207)
!251 = !DILocation(line: 121, column: 27, scope: !207)
!252 = !DILocation(line: 121, column: 41, scope: !207)
!253 = !DILocation(line: 121, column: 47, scope: !207)
!254 = !DILocation(line: 121, column: 55, scope: !207)
!255 = !DILocation(line: 121, column: 46, scope: !207)
!256 = !DILocation(line: 121, column: 39, scope: !207)
!257 = !DILocation(line: 121, column: 20, scope: !207)
!258 = !DILocation(line: 123, column: 30, scope: !207)
!259 = !DILocation(line: 123, column: 38, scope: !207)
!260 = !DILocation(line: 123, column: 43, scope: !207)
!261 = !DILocation(line: 123, column: 41, scope: !207)
!262 = !DILocation(line: 123, column: 17, scope: !207)
!263 = !DILocation(line: 123, column: 25, scope: !207)
!264 = !DILocation(line: 123, column: 28, scope: !207)
!265 = !DILocation(line: 124, column: 30, scope: !207)
!266 = !DILocation(line: 124, column: 38, scope: !207)
!267 = !DILocation(line: 124, column: 43, scope: !207)
!268 = !DILocation(line: 124, column: 41, scope: !207)
!269 = !DILocation(line: 124, column: 17, scope: !207)
!270 = !DILocation(line: 124, column: 25, scope: !207)
!271 = !DILocation(line: 124, column: 28, scope: !207)
!272 = !DILocation(line: 126, column: 31, scope: !207)
!273 = !DILocation(line: 126, column: 17, scope: !207)
!274 = !DILocation(line: 126, column: 25, scope: !207)
!275 = !DILocation(line: 126, column: 28, scope: !207)
!276 = !DILocation(line: 127, column: 31, scope: !207)
!277 = !DILocation(line: 127, column: 17, scope: !207)
!278 = !DILocation(line: 127, column: 25, scope: !207)
!279 = !DILocation(line: 127, column: 28, scope: !207)
!280 = !DILocation(line: 128, column: 13, scope: !207)
!281 = !DILocation(line: 108, column: 44, scope: !202)
!282 = !DILocation(line: 108, column: 49, scope: !202)
!283 = !DILocation(line: 108, column: 13, scope: !202)
!284 = distinct !{!284, !205, !285}
!285 = !DILocation(line: 128, column: 13, scope: !197)
!286 = !DILocation(line: 129, column: 9, scope: !183)
!287 = !DILocation(line: 99, column: 41, scope: !178)
!288 = !DILocation(line: 99, column: 38, scope: !178)
!289 = !DILocation(line: 99, column: 9, scope: !178)
!290 = distinct !{!290, !181, !291}
!291 = !DILocation(line: 129, column: 9, scope: !175)
!292 = !DILocation(line: 131, column: 20, scope: !136)
!293 = !DILocation(line: 131, column: 18, scope: !136)
!294 = !DILocation(line: 132, column: 5, scope: !136)
!295 = !DILocation(line: 88, column: 61, scope: !131)
!296 = !DILocation(line: 88, column: 5, scope: !131)
!297 = distinct !{!297, !134, !298}
!298 = !DILocation(line: 132, column: 5, scope: !128)
!299 = !DILocation(line: 138, column: 10, scope: !300)
!300 = distinct !DILexicalBlock(scope: !39, file: !30, line: 138, column: 10)
!301 = !DILocation(line: 138, column: 10, scope: !39)
!302 = !DILocalVariable(name: "denom", scope: !303, file: !30, line: 140, type: !32)
!303 = distinct !DILexicalBlock(scope: !300, file: !30, line: 139, column: 5)
!304 = !DILocation(line: 140, column: 16, scope: !303)
!305 = !DILocation(line: 140, column: 32, scope: !303)
!306 = !DILocation(line: 140, column: 24, scope: !303)
!307 = !DILocation(line: 142, column: 16, scope: !308)
!308 = distinct !DILexicalBlock(scope: !303, file: !30, line: 142, column: 9)
!309 = !DILocation(line: 142, column: 15, scope: !308)
!310 = !DILocation(line: 142, column: 20, scope: !311)
!311 = distinct !DILexicalBlock(scope: !308, file: !30, line: 142, column: 9)
!312 = !DILocation(line: 142, column: 24, scope: !311)
!313 = !DILocation(line: 142, column: 22, scope: !311)
!314 = !DILocation(line: 142, column: 9, scope: !308)
!315 = !DILocation(line: 144, column: 13, scope: !316)
!316 = distinct !DILexicalBlock(scope: !311, file: !30, line: 143, column: 9)
!317 = !DILocation(line: 145, column: 27, scope: !316)
!318 = !DILocation(line: 145, column: 13, scope: !316)
!319 = !DILocation(line: 145, column: 21, scope: !316)
!320 = !DILocation(line: 145, column: 24, scope: !316)
!321 = !DILocation(line: 146, column: 27, scope: !316)
!322 = !DILocation(line: 146, column: 13, scope: !316)
!323 = !DILocation(line: 146, column: 21, scope: !316)
!324 = !DILocation(line: 146, column: 24, scope: !316)
!325 = !DILocation(line: 148, column: 9, scope: !316)
!326 = !DILocation(line: 142, column: 37, scope: !311)
!327 = !DILocation(line: 142, column: 9, scope: !311)
!328 = distinct !{!328, !314, !329}
!329 = !DILocation(line: 148, column: 9, scope: !308)
!330 = !DILocation(line: 149, column: 5, scope: !303)
!331 = !DILocation(line: 150, column: 1, scope: !39)
!332 = distinct !DISubprogram(name: "CheckPointer", scope: !30, file: !30, line: 26, type: !333, scopeLine: 27, flags: DIFlagPrototyped, spFlags: DISPFlagLocalToUnit | DISPFlagDefinition, unit: !29, retainedNodes: !4)
!333 = !DISubroutineType(types: !334)
!334 = !{null, !335, !336}
!335 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 16)
!336 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !337, size: 16)
!337 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!338 = !DILocalVariable(name: "p", arg: 1, scope: !332, file: !30, line: 26, type: !335)
!339 = !DILocation(line: 26, column: 34, scope: !332)
!340 = !DILocalVariable(name: "name", arg: 2, scope: !332, file: !30, line: 26, type: !336)
!341 = !DILocation(line: 26, column: 43, scope: !332)
!342 = !DILocation(line: 28, column: 10, scope: !343)
!343 = distinct !DILexicalBlock(scope: !332, file: !30, line: 28, column: 10)
!344 = !DILocation(line: 28, column: 12, scope: !343)
!345 = !DILocation(line: 28, column: 10, scope: !332)
!346 = !DILocation(line: 31, column: 9, scope: !347)
!347 = distinct !DILexicalBlock(scope: !343, file: !30, line: 29, column: 5)
!348 = !DILocation(line: 33, column: 1, scope: !332)
!349 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 18, type: !350, scopeLine: 18, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!350 = !DISubroutineType(types: !351)
!351 = !{!28}
!352 = !DILocation(line: 19, column: 5, scope: !349)
!353 = !DILocation(line: 20, column: 13, scope: !349)
!354 = !DILocation(line: 21, column: 5, scope: !349)
!355 = !DILocation(line: 22, column: 12, scope: !349)
!356 = !DILocation(line: 23, column: 13, scope: !349)
!357 = !DILocation(line: 24, column: 5, scope: !349)
!358 = !DILocation(line: 25, column: 5, scope: !349)
!359 = distinct !DISubprogram(name: "old_main", scope: !3, file: !3, line: 28, type: !350, scopeLine: 28, spFlags: DISPFlagDefinition, unit: !2, retainedNodes: !4)
!360 = !DILocalVariable(name: "i", scope: !359, file: !3, line: 29, type: !8)
!361 = !DILocation(line: 29, column: 11, scope: !359)
!362 = !DILocalVariable(name: "j", scope: !359, file: !3, line: 29, type: !8)
!363 = !DILocation(line: 29, column: 13, scope: !359)
!364 = !DILocalVariable(name: "RealIn", scope: !359, file: !3, line: 30, type: !42)
!365 = !DILocation(line: 30, column: 9, scope: !359)
!366 = !DILocalVariable(name: "ImagIn", scope: !359, file: !3, line: 31, type: !42)
!367 = !DILocation(line: 31, column: 9, scope: !359)
!368 = !DILocalVariable(name: "RealOut", scope: !359, file: !3, line: 32, type: !42)
!369 = !DILocation(line: 32, column: 9, scope: !359)
!370 = !DILocalVariable(name: "ImagOut", scope: !359, file: !3, line: 33, type: !42)
!371 = !DILocation(line: 33, column: 9, scope: !359)
!372 = !DILocalVariable(name: "coeff", scope: !359, file: !3, line: 34, type: !42)
!373 = !DILocation(line: 34, column: 9, scope: !359)
!374 = !DILocalVariable(name: "amp", scope: !359, file: !3, line: 35, type: !42)
!375 = !DILocation(line: 35, column: 9, scope: !359)
!376 = !DILocation(line: 38, column: 2, scope: !359)
!377 = !DILocation(line: 49, column: 9, scope: !359)
!378 = !DILocation(line: 50, column: 9, scope: !359)
!379 = !DILocation(line: 51, column: 10, scope: !359)
!380 = !DILocation(line: 52, column: 10, scope: !359)
!381 = !DILocation(line: 53, column: 8, scope: !359)
!382 = !DILocation(line: 54, column: 6, scope: !359)
!383 = !DILocation(line: 57, column: 7, scope: !384)
!384 = distinct !DILexicalBlock(scope: !359, file: !3, line: 57, column: 2)
!385 = !DILocation(line: 57, column: 6, scope: !384)
!386 = !DILocation(line: 57, column: 10, scope: !387)
!387 = distinct !DILexicalBlock(scope: !384, file: !3, line: 57, column: 2)
!388 = !DILocation(line: 57, column: 11, scope: !387)
!389 = !DILocation(line: 57, column: 2, scope: !384)
!390 = !DILocation(line: 59, column: 6, scope: !391)
!391 = distinct !DILexicalBlock(scope: !387, file: !3, line: 58, column: 2)
!392 = !DILocation(line: 60, column: 14, scope: !391)
!393 = !DILocation(line: 60, column: 20, scope: !391)
!394 = !DILocation(line: 60, column: 3, scope: !391)
!395 = !DILocation(line: 60, column: 9, scope: !391)
!396 = !DILocation(line: 60, column: 12, scope: !391)
!397 = !DILocation(line: 61, column: 12, scope: !391)
!398 = !DILocation(line: 61, column: 18, scope: !391)
!399 = !DILocation(line: 61, column: 3, scope: !391)
!400 = !DILocation(line: 61, column: 7, scope: !391)
!401 = !DILocation(line: 61, column: 10, scope: !391)
!402 = !DILocation(line: 63, column: 2, scope: !391)
!403 = !DILocation(line: 57, column: 22, scope: !387)
!404 = !DILocation(line: 57, column: 2, scope: !387)
!405 = distinct !{!405, !389, !406}
!406 = !DILocation(line: 63, column: 2, scope: !384)
!407 = !DILocation(line: 64, column: 7, scope: !408)
!408 = distinct !DILexicalBlock(scope: !359, file: !3, line: 64, column: 2)
!409 = !DILocation(line: 64, column: 6, scope: !408)
!410 = !DILocation(line: 64, column: 10, scope: !411)
!411 = distinct !DILexicalBlock(scope: !408, file: !3, line: 64, column: 2)
!412 = !DILocation(line: 64, column: 12, scope: !411)
!413 = !DILocation(line: 64, column: 11, scope: !411)
!414 = !DILocation(line: 64, column: 2, scope: !408)
!415 = !DILocation(line: 66, column: 5, scope: !416)
!416 = distinct !DILexicalBlock(scope: !411, file: !3, line: 65, column: 2)
!417 = !DILocation(line: 68, column: 3, scope: !416)
!418 = !DILocation(line: 68, column: 10, scope: !416)
!419 = !DILocation(line: 68, column: 12, scope: !416)
!420 = !DILocation(line: 69, column: 8, scope: !421)
!421 = distinct !DILexicalBlock(scope: !416, file: !3, line: 69, column: 3)
!422 = !DILocation(line: 69, column: 7, scope: !421)
!423 = !DILocation(line: 69, column: 11, scope: !424)
!424 = distinct !DILexicalBlock(scope: !421, file: !3, line: 69, column: 3)
!425 = !DILocation(line: 69, column: 12, scope: !424)
!426 = !DILocation(line: 69, column: 3, scope: !421)
!427 = !DILocation(line: 71, column: 7, scope: !428)
!428 = distinct !DILexicalBlock(scope: !424, file: !3, line: 70, column: 3)
!429 = !DILocation(line: 73, column: 8, scope: !430)
!430 = distinct !DILexicalBlock(scope: !428, file: !3, line: 73, column: 8)
!431 = !DILocation(line: 73, column: 14, scope: !430)
!432 = !DILocation(line: 73, column: 8, scope: !428)
!433 = !DILocation(line: 75, column: 17, scope: !434)
!434 = distinct !DILexicalBlock(scope: !430, file: !3, line: 74, column: 4)
!435 = !DILocation(line: 75, column: 23, scope: !434)
!436 = !DILocation(line: 75, column: 30, scope: !434)
!437 = !DILocation(line: 75, column: 34, scope: !434)
!438 = !DILocation(line: 75, column: 37, scope: !434)
!439 = !DILocation(line: 75, column: 36, scope: !434)
!440 = !DILocation(line: 75, column: 26, scope: !434)
!441 = !DILocation(line: 75, column: 25, scope: !434)
!442 = !DILocation(line: 75, column: 6, scope: !434)
!443 = !DILocation(line: 75, column: 13, scope: !434)
!444 = !DILocation(line: 75, column: 15, scope: !434)
!445 = !DILocation(line: 76, column: 4, scope: !434)
!446 = !DILocation(line: 79, column: 16, scope: !447)
!447 = distinct !DILexicalBlock(scope: !430, file: !3, line: 78, column: 4)
!448 = !DILocation(line: 79, column: 22, scope: !447)
!449 = !DILocation(line: 79, column: 29, scope: !447)
!450 = !DILocation(line: 79, column: 33, scope: !447)
!451 = !DILocation(line: 79, column: 36, scope: !447)
!452 = !DILocation(line: 79, column: 35, scope: !447)
!453 = !DILocation(line: 79, column: 25, scope: !447)
!454 = !DILocation(line: 79, column: 24, scope: !447)
!455 = !DILocation(line: 79, column: 5, scope: !447)
!456 = !DILocation(line: 79, column: 12, scope: !447)
!457 = !DILocation(line: 79, column: 14, scope: !447)
!458 = !DILocation(line: 81, column: 5, scope: !428)
!459 = !DILocation(line: 81, column: 12, scope: !428)
!460 = !DILocation(line: 81, column: 14, scope: !428)
!461 = !DILocation(line: 83, column: 3, scope: !428)
!462 = !DILocation(line: 69, column: 23, scope: !424)
!463 = !DILocation(line: 69, column: 3, scope: !424)
!464 = distinct !{!464, !426, !465}
!465 = !DILocation(line: 83, column: 3, scope: !421)
!466 = !DILocation(line: 85, column: 2, scope: !416)
!467 = !DILocation(line: 64, column: 21, scope: !411)
!468 = !DILocation(line: 64, column: 2, scope: !411)
!469 = distinct !{!469, !414, !470}
!470 = !DILocation(line: 85, column: 2, scope: !408)
!471 = !DILocation(line: 88, column: 13, scope: !359)
!472 = !DILocation(line: 88, column: 21, scope: !359)
!473 = !DILocation(line: 88, column: 28, scope: !359)
!474 = !DILocation(line: 88, column: 35, scope: !359)
!475 = !DILocation(line: 88, column: 42, scope: !359)
!476 = !DILocation(line: 88, column: 50, scope: !359)
!477 = !DILocation(line: 88, column: 2, scope: !359)
!478 = !DILocation(line: 100, column: 2, scope: !359)
!479 = !DILocation(line: 101, column: 2, scope: !359)
!480 = distinct !DISubprogram(name: "IsPowerOfTwo", scope: !34, file: !34, line: 35, type: !481, scopeLine: 36, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !33, retainedNodes: !4)
!481 = !DISubroutineType(types: !482)
!482 = !{!28, !8}
!483 = !DILocalVariable(name: "x", arg: 1, scope: !480, file: !34, line: 35, type: !8)
!484 = !DILocation(line: 35, column: 29, scope: !480)
!485 = !DILocation(line: 37, column: 10, scope: !486)
!486 = distinct !DILexicalBlock(scope: !480, file: !34, line: 37, column: 10)
!487 = !DILocation(line: 37, column: 12, scope: !486)
!488 = !DILocation(line: 37, column: 10, scope: !480)
!489 = !DILocation(line: 38, column: 9, scope: !486)
!490 = !DILocation(line: 40, column: 10, scope: !491)
!491 = distinct !DILexicalBlock(scope: !480, file: !34, line: 40, column: 10)
!492 = !DILocation(line: 40, column: 15, scope: !491)
!493 = !DILocation(line: 40, column: 16, scope: !491)
!494 = !DILocation(line: 40, column: 12, scope: !491)
!495 = !DILocation(line: 40, column: 10, scope: !480)
!496 = !DILocation(line: 41, column: 9, scope: !491)
!497 = !DILocation(line: 43, column: 5, scope: !480)
!498 = !DILocation(line: 44, column: 1, scope: !480)
!499 = distinct !DISubprogram(name: "NumberOfBitsNeeded", scope: !34, file: !34, line: 47, type: !500, scopeLine: 48, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !33, retainedNodes: !4)
!500 = !DISubroutineType(types: !501)
!501 = !{!8, !8}
!502 = !DILocalVariable(name: "PowerOfTwo", arg: 1, scope: !499, file: !34, line: 47, type: !8)
!503 = !DILocation(line: 47, column: 40, scope: !499)
!504 = !DILocalVariable(name: "i", scope: !499, file: !34, line: 49, type: !8)
!505 = !DILocation(line: 49, column: 14, scope: !499)
!506 = !DILocation(line: 51, column: 10, scope: !507)
!507 = distinct !DILexicalBlock(scope: !499, file: !34, line: 51, column: 10)
!508 = !DILocation(line: 51, column: 21, scope: !507)
!509 = !DILocation(line: 51, column: 10, scope: !499)
!510 = !DILocation(line: 54, column: 13, scope: !511)
!511 = distinct !DILexicalBlock(scope: !507, file: !34, line: 52, column: 5)
!512 = !DILocation(line: 53, column: 9, scope: !511)
!513 = !DILocation(line: 56, column: 9, scope: !511)
!514 = !DILocation(line: 59, column: 12, scope: !515)
!515 = distinct !DILexicalBlock(scope: !499, file: !34, line: 59, column: 5)
!516 = !DILocation(line: 59, column: 11, scope: !515)
!517 = !DILocation(line: 61, column: 9, scope: !518)
!518 = distinct !DILexicalBlock(scope: !519, file: !34, line: 60, column: 5)
!519 = distinct !DILexicalBlock(scope: !515, file: !34, line: 59, column: 5)
!520 = !DILocation(line: 62, column: 14, scope: !521)
!521 = distinct !DILexicalBlock(scope: !518, file: !34, line: 62, column: 14)
!522 = !DILocation(line: 62, column: 33, scope: !521)
!523 = !DILocation(line: 62, column: 30, scope: !521)
!524 = !DILocation(line: 62, column: 25, scope: !521)
!525 = !DILocation(line: 62, column: 14, scope: !518)
!526 = !DILocation(line: 63, column: 20, scope: !527)
!527 = distinct !DILexicalBlock(scope: !521, file: !34, line: 62, column: 38)
!528 = !DILocation(line: 63, column: 13, scope: !527)
!529 = !DILocation(line: 65, column: 5, scope: !518)
!530 = !DILocation(line: 59, column: 19, scope: !519)
!531 = !DILocation(line: 59, column: 5, scope: !519)
!532 = distinct !{!532, !533, !534}
!533 = !DILocation(line: 59, column: 5, scope: !515)
!534 = !DILocation(line: 65, column: 5, scope: !515)
!535 = distinct !DISubprogram(name: "ReverseBits", scope: !34, file: !34, line: 70, type: !536, scopeLine: 71, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !33, retainedNodes: !4)
!536 = !DISubroutineType(types: !537)
!537 = !{!8, !8, !8}
!538 = !DILocalVariable(name: "index", arg: 1, scope: !535, file: !34, line: 70, type: !8)
!539 = !DILocation(line: 70, column: 33, scope: !535)
!540 = !DILocalVariable(name: "NumBits", arg: 2, scope: !535, file: !34, line: 70, type: !8)
!541 = !DILocation(line: 70, column: 49, scope: !535)
!542 = !DILocalVariable(name: "i", scope: !535, file: !34, line: 72, type: !8)
!543 = !DILocation(line: 72, column: 14, scope: !535)
!544 = !DILocalVariable(name: "rev", scope: !535, file: !34, line: 72, type: !8)
!545 = !DILocation(line: 72, column: 17, scope: !535)
!546 = !DILocation(line: 74, column: 16, scope: !547)
!547 = distinct !DILexicalBlock(scope: !535, file: !34, line: 74, column: 5)
!548 = !DILocation(line: 74, column: 12, scope: !547)
!549 = !DILocation(line: 74, column: 11, scope: !547)
!550 = !DILocation(line: 74, column: 20, scope: !551)
!551 = distinct !DILexicalBlock(scope: !547, file: !34, line: 74, column: 5)
!552 = !DILocation(line: 74, column: 24, scope: !551)
!553 = !DILocation(line: 74, column: 22, scope: !551)
!554 = !DILocation(line: 74, column: 5, scope: !547)
!555 = !DILocation(line: 76, column: 9, scope: !556)
!556 = distinct !DILexicalBlock(scope: !551, file: !34, line: 75, column: 5)
!557 = !DILocation(line: 77, column: 16, scope: !556)
!558 = !DILocation(line: 77, column: 20, scope: !556)
!559 = !DILocation(line: 77, column: 29, scope: !556)
!560 = !DILocation(line: 77, column: 35, scope: !556)
!561 = !DILocation(line: 77, column: 26, scope: !556)
!562 = !DILocation(line: 77, column: 13, scope: !556)
!563 = !DILocation(line: 78, column: 15, scope: !556)
!564 = !DILocation(line: 80, column: 5, scope: !556)
!565 = !DILocation(line: 74, column: 34, scope: !551)
!566 = !DILocation(line: 74, column: 5, scope: !551)
!567 = distinct !{!567, !554, !568}
!568 = !DILocation(line: 80, column: 5, scope: !547)
!569 = !DILocation(line: 82, column: 12, scope: !535)
!570 = !DILocation(line: 82, column: 5, scope: !535)
