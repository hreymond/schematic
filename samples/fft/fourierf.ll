; ModuleID = 'fourierf.c'
source_filename = "fourierf.c"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

@.str = private unnamed_addr constant [7 x i8] c"RealIn\00", align 1
@.str.1 = private unnamed_addr constant [8 x i8] c"RealOut\00", align 1
@.str.2 = private unnamed_addr constant [8 x i8] c"ImagOut\00", align 1

; Function Attrs: noinline nounwind optnone
define dso_local void @fft_float(i16, i16, float*, float*, float*, float*) #0 !dbg !9 {
  %7 = alloca i16, align 2
  %8 = alloca i16, align 2
  %9 = alloca float*, align 2
  %10 = alloca float*, align 2
  %11 = alloca float*, align 2
  %12 = alloca float*, align 2
  %13 = alloca i16, align 2
  %14 = alloca i16, align 2
  %15 = alloca i16, align 2
  %16 = alloca i16, align 2
  %17 = alloca i16, align 2
  %18 = alloca i16, align 2
  %19 = alloca i16, align 2
  %20 = alloca double, align 8
  %21 = alloca double, align 8
  %22 = alloca double, align 8
  %23 = alloca double, align 8
  %24 = alloca double, align 8
  %25 = alloca double, align 8
  %26 = alloca double, align 8
  %27 = alloca double, align 8
  %28 = alloca double, align 8
  %29 = alloca [3 x double], align 8
  %30 = alloca [3 x double], align 8
  %31 = alloca double, align 8
  store i16 %0, i16* %7, align 2
  call void @llvm.dbg.declare(metadata i16* %7, metadata !16, metadata !DIExpression()), !dbg !17
  store i16 %1, i16* %8, align 2
  call void @llvm.dbg.declare(metadata i16* %8, metadata !18, metadata !DIExpression()), !dbg !19
  store float* %2, float** %9, align 2
  call void @llvm.dbg.declare(metadata float** %9, metadata !20, metadata !DIExpression()), !dbg !21
  store float* %3, float** %10, align 2
  call void @llvm.dbg.declare(metadata float** %10, metadata !22, metadata !DIExpression()), !dbg !23
  store float* %4, float** %11, align 2
  call void @llvm.dbg.declare(metadata float** %11, metadata !24, metadata !DIExpression()), !dbg !25
  store float* %5, float** %12, align 2
  call void @llvm.dbg.declare(metadata float** %12, metadata !26, metadata !DIExpression()), !dbg !27
  call void @llvm.dbg.declare(metadata i16* %13, metadata !28, metadata !DIExpression()), !dbg !29
  call void @llvm.dbg.declare(metadata i16* %14, metadata !30, metadata !DIExpression()), !dbg !31
  call void @llvm.dbg.declare(metadata i16* %15, metadata !32, metadata !DIExpression()), !dbg !33
  call void @llvm.dbg.declare(metadata i16* %16, metadata !34, metadata !DIExpression()), !dbg !35
  call void @llvm.dbg.declare(metadata i16* %17, metadata !36, metadata !DIExpression()), !dbg !37
  call void @llvm.dbg.declare(metadata i16* %18, metadata !38, metadata !DIExpression()), !dbg !39
  call void @llvm.dbg.declare(metadata i16* %19, metadata !40, metadata !DIExpression()), !dbg !41
  call void @llvm.dbg.declare(metadata double* %20, metadata !42, metadata !DIExpression()), !dbg !43
  store double 0x401921FB54442D18, double* %20, align 8, !dbg !43
  call void @llvm.dbg.declare(metadata double* %21, metadata !44, metadata !DIExpression()), !dbg !45
  call void @llvm.dbg.declare(metadata double* %22, metadata !46, metadata !DIExpression()), !dbg !47
  %32 = load i16, i16* %7, align 2, !dbg !48
  %33 = call i16 @IsPowerOfTwo(i16 %32), !dbg !50
  %34 = icmp ne i16 %33, 0, !dbg !50
  br i1 %34, label %36, label %35, !dbg !51

; <label>:35:                                     ; preds = %6
  call void @exit(i16 1) #5, !dbg !52
  unreachable, !dbg !52

; <label>:36:                                     ; preds = %6
  %37 = load i16, i16* %8, align 2, !dbg !54
  %38 = icmp ne i16 %37, 0, !dbg !54
  br i1 %38, label %39, label %42, !dbg !56

; <label>:39:                                     ; preds = %36
  %40 = load double, double* %20, align 8, !dbg !57
  %41 = fsub double -0.000000e+00, %40, !dbg !58
  store double %41, double* %20, align 8, !dbg !59
  br label %42, !dbg !60

; <label>:42:                                     ; preds = %39, %36
  %43 = load float*, float** %9, align 2, !dbg !61
  %44 = bitcast float* %43 to i8*, !dbg !61
  call void @CheckPointer(i8* %44, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0)), !dbg !61
  %45 = load float*, float** %11, align 2, !dbg !62
  %46 = bitcast float* %45 to i8*, !dbg !62
  call void @CheckPointer(i8* %46, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.1, i32 0, i32 0)), !dbg !62
  %47 = load float*, float** %12, align 2, !dbg !63
  %48 = bitcast float* %47 to i8*, !dbg !63
  call void @CheckPointer(i8* %48, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.2, i32 0, i32 0)), !dbg !63
  %49 = load i16, i16* %7, align 2, !dbg !64
  %50 = call i16 @NumberOfBitsNeeded(i16 %49), !dbg !65
  store i16 %50, i16* %13, align 2, !dbg !66
  store i16 0, i16* %14, align 2, !dbg !67
  br label %51, !dbg !69

; <label>:51:                                     ; preds = %82, %42
  %52 = load i16, i16* %14, align 2, !dbg !70
  %53 = load i16, i16* %7, align 2, !dbg !72
  %54 = icmp ult i16 %52, %53, !dbg !73
  br i1 %54, label %55, label %85, !dbg !74

; <label>:55:                                     ; preds = %51
  %56 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 1024), !dbg !75
  %57 = load i16, i16* %14, align 2, !dbg !77
  %58 = load i16, i16* %13, align 2, !dbg !78
  %59 = call i16 @ReverseBits(i16 %57, i16 %58), !dbg !79
  store i16 %59, i16* %15, align 2, !dbg !80
  %60 = load float*, float** %9, align 2, !dbg !81
  %61 = load i16, i16* %14, align 2, !dbg !82
  %62 = getelementptr inbounds float, float* %60, i16 %61, !dbg !81
  %63 = load float, float* %62, align 4, !dbg !81
  %64 = load float*, float** %11, align 2, !dbg !83
  %65 = load i16, i16* %15, align 2, !dbg !84
  %66 = getelementptr inbounds float, float* %64, i16 %65, !dbg !83
  store float %63, float* %66, align 4, !dbg !85
  %67 = load float*, float** %10, align 2, !dbg !86
  %68 = icmp eq float* %67, null, !dbg !87
  br i1 %68, label %69, label %70, !dbg !88

; <label>:69:                                     ; preds = %55
  br label %76, !dbg !88

; <label>:70:                                     ; preds = %55
  %71 = load float*, float** %10, align 2, !dbg !89
  %72 = load i16, i16* %14, align 2, !dbg !90
  %73 = getelementptr inbounds float, float* %71, i16 %72, !dbg !89
  %74 = load float, float* %73, align 4, !dbg !89
  %75 = fpext float %74 to double, !dbg !89
  br label %76, !dbg !88

; <label>:76:                                     ; preds = %70, %69
  %77 = phi double [ 0.000000e+00, %69 ], [ %75, %70 ], !dbg !88
  %78 = fptrunc double %77 to float, !dbg !88
  %79 = load float*, float** %12, align 2, !dbg !91
  %80 = load i16, i16* %15, align 2, !dbg !92
  %81 = getelementptr inbounds float, float* %79, i16 %80, !dbg !91
  store float %78, float* %81, align 4, !dbg !93
  br label %82, !dbg !94

; <label>:82:                                     ; preds = %76
  %83 = load i16, i16* %14, align 2, !dbg !95
  %84 = add i16 %83, 1, !dbg !95
  store i16 %84, i16* %14, align 2, !dbg !95
  br label %51, !dbg !96, !llvm.loop !97

; <label>:85:                                     ; preds = %51
  store i16 1, i16* %19, align 2, !dbg !99
  store i16 2, i16* %18, align 2, !dbg !100
  br label %86, !dbg !102

; <label>:86:                                     ; preds = %246, %85
  %87 = load i16, i16* %18, align 2, !dbg !103
  %88 = load i16, i16* %7, align 2, !dbg !105
  %89 = icmp ule i16 %87, %88, !dbg !106
  br i1 %89, label %90, label %249, !dbg !107

; <label>:90:                                     ; preds = %86
  %91 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 1024), !dbg !108
  call void @llvm.dbg.declare(metadata double* %23, metadata !110, metadata !DIExpression()), !dbg !111
  %92 = load double, double* %20, align 8, !dbg !112
  %93 = load i16, i16* %18, align 2, !dbg !113
  %94 = uitofp i16 %93 to double, !dbg !114
  %95 = fdiv double %92, %94, !dbg !115
  store double %95, double* %23, align 8, !dbg !111
  call void @llvm.dbg.declare(metadata double* %24, metadata !116, metadata !DIExpression()), !dbg !117
  %96 = load double, double* %23, align 8, !dbg !118
  %97 = fmul double -2.000000e+00, %96, !dbg !119
  %98 = call double @sin(double %97) #6, !dbg !120
  store double %98, double* %24, align 8, !dbg !117
  call void @llvm.dbg.declare(metadata double* %25, metadata !121, metadata !DIExpression()), !dbg !122
  %99 = load double, double* %23, align 8, !dbg !123
  %100 = fsub double -0.000000e+00, %99, !dbg !124
  %101 = call double @sin(double %100) #6, !dbg !125
  store double %101, double* %25, align 8, !dbg !122
  call void @llvm.dbg.declare(metadata double* %26, metadata !126, metadata !DIExpression()), !dbg !127
  %102 = load double, double* %23, align 8, !dbg !128
  %103 = fmul double -2.000000e+00, %102, !dbg !129
  %104 = call double @cos(double %103) #6, !dbg !130
  store double %104, double* %26, align 8, !dbg !127
  call void @llvm.dbg.declare(metadata double* %27, metadata !131, metadata !DIExpression()), !dbg !132
  %105 = load double, double* %23, align 8, !dbg !133
  %106 = fsub double -0.000000e+00, %105, !dbg !134
  %107 = call double @cos(double %106) #6, !dbg !135
  store double %107, double* %27, align 8, !dbg !132
  call void @llvm.dbg.declare(metadata double* %28, metadata !136, metadata !DIExpression()), !dbg !137
  %108 = load double, double* %27, align 8, !dbg !138
  %109 = fmul double 2.000000e+00, %108, !dbg !139
  store double %109, double* %28, align 8, !dbg !137
  call void @llvm.dbg.declare(metadata [3 x double]* %29, metadata !140, metadata !DIExpression()), !dbg !144
  call void @llvm.dbg.declare(metadata [3 x double]* %30, metadata !145, metadata !DIExpression()), !dbg !146
  store i16 0, i16* %14, align 2, !dbg !147
  br label %110, !dbg !149

; <label>:110:                                    ; preds = %240, %90
  %111 = load i16, i16* %14, align 2, !dbg !150
  %112 = load i16, i16* %7, align 2, !dbg !152
  %113 = icmp ult i16 %111, %112, !dbg !153
  br i1 %113, label %114, label %244, !dbg !154

; <label>:114:                                    ; preds = %110
  %115 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 512), !dbg !155
  %116 = load double, double* %26, align 8, !dbg !157
  %117 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 2, !dbg !158
  store double %116, double* %117, align 8, !dbg !159
  %118 = load double, double* %27, align 8, !dbg !160
  %119 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !161
  store double %118, double* %119, align 8, !dbg !162
  %120 = load double, double* %24, align 8, !dbg !163
  %121 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 2, !dbg !164
  store double %120, double* %121, align 8, !dbg !165
  %122 = load double, double* %25, align 8, !dbg !166
  %123 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !167
  store double %122, double* %123, align 8, !dbg !168
  %124 = load i16, i16* %14, align 2, !dbg !169
  store i16 %124, i16* %15, align 2, !dbg !171
  store i16 0, i16* %17, align 2, !dbg !172
  br label %125, !dbg !173

; <label>:125:                                    ; preds = %234, %114
  %126 = load i16, i16* %17, align 2, !dbg !174
  %127 = load i16, i16* %19, align 2, !dbg !176
  %128 = icmp ult i16 %126, %127, !dbg !177
  br i1 %128, label %129, label %239, !dbg !178

; <label>:129:                                    ; preds = %125
  %130 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 1024), !dbg !179
  %131 = load double, double* %28, align 8, !dbg !181
  %132 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !182
  %133 = load double, double* %132, align 8, !dbg !182
  %134 = fmul double %131, %133, !dbg !183
  %135 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 2, !dbg !184
  %136 = load double, double* %135, align 8, !dbg !184
  %137 = fsub double %134, %136, !dbg !185
  %138 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !186
  store double %137, double* %138, align 8, !dbg !187
  %139 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !188
  %140 = load double, double* %139, align 8, !dbg !188
  %141 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 2, !dbg !189
  store double %140, double* %141, align 8, !dbg !190
  %142 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !191
  %143 = load double, double* %142, align 8, !dbg !191
  %144 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !192
  store double %143, double* %144, align 8, !dbg !193
  %145 = load double, double* %28, align 8, !dbg !194
  %146 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !195
  %147 = load double, double* %146, align 8, !dbg !195
  %148 = fmul double %145, %147, !dbg !196
  %149 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 2, !dbg !197
  %150 = load double, double* %149, align 8, !dbg !197
  %151 = fsub double %148, %150, !dbg !198
  %152 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !199
  store double %151, double* %152, align 8, !dbg !200
  %153 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !201
  %154 = load double, double* %153, align 8, !dbg !201
  %155 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 2, !dbg !202
  store double %154, double* %155, align 8, !dbg !203
  %156 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !204
  %157 = load double, double* %156, align 8, !dbg !204
  %158 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !205
  store double %157, double* %158, align 8, !dbg !206
  %159 = load i16, i16* %15, align 2, !dbg !207
  %160 = load i16, i16* %19, align 2, !dbg !208
  %161 = add i16 %159, %160, !dbg !209
  store i16 %161, i16* %16, align 2, !dbg !210
  %162 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !211
  %163 = load double, double* %162, align 8, !dbg !211
  %164 = load float*, float** %11, align 2, !dbg !212
  %165 = load i16, i16* %16, align 2, !dbg !213
  %166 = getelementptr inbounds float, float* %164, i16 %165, !dbg !212
  %167 = load float, float* %166, align 4, !dbg !212
  %168 = fpext float %167 to double, !dbg !212
  %169 = fmul double %163, %168, !dbg !214
  %170 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !215
  %171 = load double, double* %170, align 8, !dbg !215
  %172 = load float*, float** %12, align 2, !dbg !216
  %173 = load i16, i16* %16, align 2, !dbg !217
  %174 = getelementptr inbounds float, float* %172, i16 %173, !dbg !216
  %175 = load float, float* %174, align 4, !dbg !216
  %176 = fpext float %175 to double, !dbg !216
  %177 = fmul double %171, %176, !dbg !218
  %178 = fsub double %169, %177, !dbg !219
  store double %178, double* %21, align 8, !dbg !220
  %179 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !221
  %180 = load double, double* %179, align 8, !dbg !221
  %181 = load float*, float** %12, align 2, !dbg !222
  %182 = load i16, i16* %16, align 2, !dbg !223
  %183 = getelementptr inbounds float, float* %181, i16 %182, !dbg !222
  %184 = load float, float* %183, align 4, !dbg !222
  %185 = fpext float %184 to double, !dbg !222
  %186 = fmul double %180, %185, !dbg !224
  %187 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !225
  %188 = load double, double* %187, align 8, !dbg !225
  %189 = load float*, float** %11, align 2, !dbg !226
  %190 = load i16, i16* %16, align 2, !dbg !227
  %191 = getelementptr inbounds float, float* %189, i16 %190, !dbg !226
  %192 = load float, float* %191, align 4, !dbg !226
  %193 = fpext float %192 to double, !dbg !226
  %194 = fmul double %188, %193, !dbg !228
  %195 = fadd double %186, %194, !dbg !229
  store double %195, double* %22, align 8, !dbg !230
  %196 = load float*, float** %11, align 2, !dbg !231
  %197 = load i16, i16* %15, align 2, !dbg !232
  %198 = getelementptr inbounds float, float* %196, i16 %197, !dbg !231
  %199 = load float, float* %198, align 4, !dbg !231
  %200 = fpext float %199 to double, !dbg !231
  %201 = load double, double* %21, align 8, !dbg !233
  %202 = fsub double %200, %201, !dbg !234
  %203 = fptrunc double %202 to float, !dbg !231
  %204 = load float*, float** %11, align 2, !dbg !235
  %205 = load i16, i16* %16, align 2, !dbg !236
  %206 = getelementptr inbounds float, float* %204, i16 %205, !dbg !235
  store float %203, float* %206, align 4, !dbg !237
  %207 = load float*, float** %12, align 2, !dbg !238
  %208 = load i16, i16* %15, align 2, !dbg !239
  %209 = getelementptr inbounds float, float* %207, i16 %208, !dbg !238
  %210 = load float, float* %209, align 4, !dbg !238
  %211 = fpext float %210 to double, !dbg !238
  %212 = load double, double* %22, align 8, !dbg !240
  %213 = fsub double %211, %212, !dbg !241
  %214 = fptrunc double %213 to float, !dbg !238
  %215 = load float*, float** %12, align 2, !dbg !242
  %216 = load i16, i16* %16, align 2, !dbg !243
  %217 = getelementptr inbounds float, float* %215, i16 %216, !dbg !242
  store float %214, float* %217, align 4, !dbg !244
  %218 = load double, double* %21, align 8, !dbg !245
  %219 = load float*, float** %11, align 2, !dbg !246
  %220 = load i16, i16* %15, align 2, !dbg !247
  %221 = getelementptr inbounds float, float* %219, i16 %220, !dbg !246
  %222 = load float, float* %221, align 4, !dbg !248
  %223 = fpext float %222 to double, !dbg !248
  %224 = fadd double %223, %218, !dbg !248
  %225 = fptrunc double %224 to float, !dbg !248
  store float %225, float* %221, align 4, !dbg !248
  %226 = load double, double* %22, align 8, !dbg !249
  %227 = load float*, float** %12, align 2, !dbg !250
  %228 = load i16, i16* %15, align 2, !dbg !251
  %229 = getelementptr inbounds float, float* %227, i16 %228, !dbg !250
  %230 = load float, float* %229, align 4, !dbg !252
  %231 = fpext float %230 to double, !dbg !252
  %232 = fadd double %231, %226, !dbg !252
  %233 = fptrunc double %232 to float, !dbg !252
  store float %233, float* %229, align 4, !dbg !252
  br label %234, !dbg !253

; <label>:234:                                    ; preds = %129
  %235 = load i16, i16* %15, align 2, !dbg !254
  %236 = add i16 %235, 1, !dbg !254
  store i16 %236, i16* %15, align 2, !dbg !254
  %237 = load i16, i16* %17, align 2, !dbg !255
  %238 = add i16 %237, 1, !dbg !255
  store i16 %238, i16* %17, align 2, !dbg !255
  br label %125, !dbg !256, !llvm.loop !257

; <label>:239:                                    ; preds = %125
  br label %240, !dbg !259

; <label>:240:                                    ; preds = %239
  %241 = load i16, i16* %18, align 2, !dbg !260
  %242 = load i16, i16* %14, align 2, !dbg !261
  %243 = add i16 %242, %241, !dbg !261
  store i16 %243, i16* %14, align 2, !dbg !261
  br label %110, !dbg !262, !llvm.loop !263

; <label>:244:                                    ; preds = %110
  %245 = load i16, i16* %18, align 2, !dbg !265
  store i16 %245, i16* %19, align 2, !dbg !266
  br label %246, !dbg !267

; <label>:246:                                    ; preds = %244
  %247 = load i16, i16* %18, align 2, !dbg !268
  %248 = shl i16 %247, 1, !dbg !268
  store i16 %248, i16* %18, align 2, !dbg !268
  br label %86, !dbg !269, !llvm.loop !270

; <label>:249:                                    ; preds = %86
  %250 = load i16, i16* %8, align 2, !dbg !272
  %251 = icmp ne i16 %250, 0, !dbg !272
  br i1 %251, label %252, label %281, !dbg !274

; <label>:252:                                    ; preds = %249
  call void @llvm.dbg.declare(metadata double* %31, metadata !275, metadata !DIExpression()), !dbg !277
  %253 = load i16, i16* %7, align 2, !dbg !278
  %254 = uitofp i16 %253 to double, !dbg !279
  store double %254, double* %31, align 8, !dbg !277
  store i16 0, i16* %14, align 2, !dbg !280
  br label %255, !dbg !282

; <label>:255:                                    ; preds = %277, %252
  %256 = load i16, i16* %14, align 2, !dbg !283
  %257 = load i16, i16* %7, align 2, !dbg !285
  %258 = icmp ult i16 %256, %257, !dbg !286
  br i1 %258, label %259, label %280, !dbg !287

; <label>:259:                                    ; preds = %255
  %260 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 1024), !dbg !288
  %261 = load double, double* %31, align 8, !dbg !290
  %262 = load float*, float** %11, align 2, !dbg !291
  %263 = load i16, i16* %14, align 2, !dbg !292
  %264 = getelementptr inbounds float, float* %262, i16 %263, !dbg !291
  %265 = load float, float* %264, align 4, !dbg !293
  %266 = fpext float %265 to double, !dbg !293
  %267 = fdiv double %266, %261, !dbg !293
  %268 = fptrunc double %267 to float, !dbg !293
  store float %268, float* %264, align 4, !dbg !293
  %269 = load double, double* %31, align 8, !dbg !294
  %270 = load float*, float** %12, align 2, !dbg !295
  %271 = load i16, i16* %14, align 2, !dbg !296
  %272 = getelementptr inbounds float, float* %270, i16 %271, !dbg !295
  %273 = load float, float* %272, align 4, !dbg !297
  %274 = fpext float %273 to double, !dbg !297
  %275 = fdiv double %274, %269, !dbg !297
  %276 = fptrunc double %275 to float, !dbg !297
  store float %276, float* %272, align 4, !dbg !297
  br label %277, !dbg !298

; <label>:277:                                    ; preds = %259
  %278 = load i16, i16* %14, align 2, !dbg !299
  %279 = add i16 %278, 1, !dbg !299
  store i16 %279, i16* %14, align 2, !dbg !299
  br label %255, !dbg !300, !llvm.loop !301

; <label>:280:                                    ; preds = %255
  br label %281, !dbg !303

; <label>:281:                                    ; preds = %280, %249
  ret void, !dbg !304
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

declare dso_local i16 @IsPowerOfTwo(i16) #2

; Function Attrs: noreturn
declare dso_local void @exit(i16) #3

; Function Attrs: noinline nounwind optnone
define internal void @CheckPointer(i8*, i8*) #0 !dbg !305 {
  %3 = alloca i8*, align 2
  %4 = alloca i8*, align 2
  store i8* %0, i8** %3, align 2
  call void @llvm.dbg.declare(metadata i8** %3, metadata !311, metadata !DIExpression()), !dbg !312
  store i8* %1, i8** %4, align 2
  call void @llvm.dbg.declare(metadata i8** %4, metadata !313, metadata !DIExpression()), !dbg !314
  %5 = load i8*, i8** %3, align 2, !dbg !315
  %6 = icmp eq i8* %5, null, !dbg !317
  br i1 %6, label %7, label %8, !dbg !318

; <label>:7:                                      ; preds = %2
  call void @exit(i16 1) #5, !dbg !319
  unreachable, !dbg !319

; <label>:8:                                      ; preds = %2
  ret void, !dbg !321
}

declare dso_local i16 @NumberOfBitsNeeded(i16) #2

declare dso_local i16 @__max_iter(...) #2

declare dso_local i16 @ReverseBits(i16, i16) #2

; Function Attrs: nounwind
declare dso_local double @sin(double) #4

; Function Attrs: nounwind
declare dso_local double @cos(double) #4

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn }
attributes #6 = { nounwind }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!5, !6, !7}
!llvm.ident = !{!8}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 8.0.1-9 (tags/RELEASE_801/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2, retainedTypes: !3, nameTableKind: None)
!1 = !DIFile(filename: "fourierf.c", directory: "/home/user/Desktop/alfred/samples/fft")
!2 = !{}
!3 = !{!4}
!4 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!5 = !{i32 2, !"Dwarf Version", i32 4}
!6 = !{i32 2, !"Debug Info Version", i32 3}
!7 = !{i32 1, !"wchar_size", i32 2}
!8 = !{!"clang version 8.0.1-9 (tags/RELEASE_801/final)"}
!9 = distinct !DISubprogram(name: "fft_float", scope: !1, file: !1, line: 36, type: !10, scopeLine: 43, flags: DIFlagPrototyped, spFlags: DISPFlagDefinition, unit: !0, retainedNodes: !2)
!10 = !DISubroutineType(types: !11)
!11 = !{null, !12, !13, !14, !14, !14, !14}
!12 = !DIBasicType(name: "unsigned int", size: 16, encoding: DW_ATE_unsigned)
!13 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!14 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !15, size: 16)
!15 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!16 = !DILocalVariable(name: "NumSamples", arg: 1, scope: !9, file: !1, line: 37, type: !12)
!17 = !DILocation(line: 37, column: 15, scope: !9)
!18 = !DILocalVariable(name: "InverseTransform", arg: 2, scope: !9, file: !1, line: 38, type: !13)
!19 = !DILocation(line: 38, column: 15, scope: !9)
!20 = !DILocalVariable(name: "RealIn", arg: 3, scope: !9, file: !1, line: 39, type: !14)
!21 = !DILocation(line: 39, column: 15, scope: !9)
!22 = !DILocalVariable(name: "ImagIn", arg: 4, scope: !9, file: !1, line: 40, type: !14)
!23 = !DILocation(line: 40, column: 15, scope: !9)
!24 = !DILocalVariable(name: "RealOut", arg: 5, scope: !9, file: !1, line: 41, type: !14)
!25 = !DILocation(line: 41, column: 15, scope: !9)
!26 = !DILocalVariable(name: "ImagOut", arg: 6, scope: !9, file: !1, line: 42, type: !14)
!27 = !DILocation(line: 42, column: 15, scope: !9)
!28 = !DILocalVariable(name: "NumBits", scope: !9, file: !1, line: 44, type: !12)
!29 = !DILocation(line: 44, column: 14, scope: !9)
!30 = !DILocalVariable(name: "i", scope: !9, file: !1, line: 45, type: !12)
!31 = !DILocation(line: 45, column: 14, scope: !9)
!32 = !DILocalVariable(name: "j", scope: !9, file: !1, line: 45, type: !12)
!33 = !DILocation(line: 45, column: 17, scope: !9)
!34 = !DILocalVariable(name: "k", scope: !9, file: !1, line: 45, type: !12)
!35 = !DILocation(line: 45, column: 20, scope: !9)
!36 = !DILocalVariable(name: "n", scope: !9, file: !1, line: 45, type: !12)
!37 = !DILocation(line: 45, column: 23, scope: !9)
!38 = !DILocalVariable(name: "BlockSize", scope: !9, file: !1, line: 46, type: !12)
!39 = !DILocation(line: 46, column: 14, scope: !9)
!40 = !DILocalVariable(name: "BlockEnd", scope: !9, file: !1, line: 46, type: !12)
!41 = !DILocation(line: 46, column: 25, scope: !9)
!42 = !DILocalVariable(name: "angle_numerator", scope: !9, file: !1, line: 48, type: !4)
!43 = !DILocation(line: 48, column: 12, scope: !9)
!44 = !DILocalVariable(name: "tr", scope: !9, file: !1, line: 49, type: !4)
!45 = !DILocation(line: 49, column: 12, scope: !9)
!46 = !DILocalVariable(name: "ti", scope: !9, file: !1, line: 49, type: !4)
!47 = !DILocation(line: 49, column: 16, scope: !9)
!48 = !DILocation(line: 51, column: 24, scope: !49)
!49 = distinct !DILexicalBlock(scope: !9, file: !1, line: 51, column: 10)
!50 = !DILocation(line: 51, column: 11, scope: !49)
!51 = !DILocation(line: 51, column: 10, scope: !9)
!52 = !DILocation(line: 58, column: 9, scope: !53)
!53 = distinct !DILexicalBlock(scope: !49, file: !1, line: 52, column: 5)
!54 = !DILocation(line: 61, column: 10, scope: !55)
!55 = distinct !DILexicalBlock(scope: !9, file: !1, line: 61, column: 10)
!56 = !DILocation(line: 61, column: 10, scope: !9)
!57 = !DILocation(line: 62, column: 28, scope: !55)
!58 = !DILocation(line: 62, column: 27, scope: !55)
!59 = !DILocation(line: 62, column: 25, scope: !55)
!60 = !DILocation(line: 62, column: 9, scope: !55)
!61 = !DILocation(line: 64, column: 5, scope: !9)
!62 = !DILocation(line: 65, column: 5, scope: !9)
!63 = !DILocation(line: 66, column: 5, scope: !9)
!64 = !DILocation(line: 68, column: 36, scope: !9)
!65 = !DILocation(line: 68, column: 15, scope: !9)
!66 = !DILocation(line: 68, column: 13, scope: !9)
!67 = !DILocation(line: 74, column: 12, scope: !68)
!68 = distinct !DILexicalBlock(scope: !9, file: !1, line: 74, column: 5)
!69 = !DILocation(line: 74, column: 11, scope: !68)
!70 = !DILocation(line: 74, column: 16, scope: !71)
!71 = distinct !DILexicalBlock(scope: !68, file: !1, line: 74, column: 5)
!72 = !DILocation(line: 74, column: 20, scope: !71)
!73 = !DILocation(line: 74, column: 18, scope: !71)
!74 = !DILocation(line: 74, column: 5, scope: !68)
!75 = !DILocation(line: 76, column: 9, scope: !76)
!76 = distinct !DILexicalBlock(scope: !71, file: !1, line: 75, column: 5)
!77 = !DILocation(line: 77, column: 27, scope: !76)
!78 = !DILocation(line: 77, column: 30, scope: !76)
!79 = !DILocation(line: 77, column: 13, scope: !76)
!80 = !DILocation(line: 77, column: 11, scope: !76)
!81 = !DILocation(line: 78, column: 22, scope: !76)
!82 = !DILocation(line: 78, column: 29, scope: !76)
!83 = !DILocation(line: 78, column: 9, scope: !76)
!84 = !DILocation(line: 78, column: 17, scope: !76)
!85 = !DILocation(line: 78, column: 20, scope: !76)
!86 = !DILocation(line: 79, column: 23, scope: !76)
!87 = !DILocation(line: 79, column: 30, scope: !76)
!88 = !DILocation(line: 79, column: 22, scope: !76)
!89 = !DILocation(line: 79, column: 47, scope: !76)
!90 = !DILocation(line: 79, column: 54, scope: !76)
!91 = !DILocation(line: 79, column: 9, scope: !76)
!92 = !DILocation(line: 79, column: 17, scope: !76)
!93 = !DILocation(line: 79, column: 20, scope: !76)
!94 = !DILocation(line: 81, column: 5, scope: !76)
!95 = !DILocation(line: 74, column: 33, scope: !71)
!96 = !DILocation(line: 74, column: 5, scope: !71)
!97 = distinct !{!97, !74, !98}
!98 = !DILocation(line: 81, column: 5, scope: !68)
!99 = !DILocation(line: 87, column: 14, scope: !9)
!100 = !DILocation(line: 88, column: 21, scope: !101)
!101 = distinct !DILexicalBlock(scope: !9, file: !1, line: 88, column: 5)
!102 = !DILocation(line: 88, column: 11, scope: !101)
!103 = !DILocation(line: 88, column: 26, scope: !104)
!104 = distinct !DILexicalBlock(scope: !101, file: !1, line: 88, column: 5)
!105 = !DILocation(line: 88, column: 39, scope: !104)
!106 = !DILocation(line: 88, column: 36, scope: !104)
!107 = !DILocation(line: 88, column: 5, scope: !101)
!108 = !DILocation(line: 90, column: 9, scope: !109)
!109 = distinct !DILexicalBlock(scope: !104, file: !1, line: 89, column: 5)
!110 = !DILocalVariable(name: "delta_angle", scope: !109, file: !1, line: 91, type: !4)
!111 = !DILocation(line: 91, column: 16, scope: !109)
!112 = !DILocation(line: 91, column: 30, scope: !109)
!113 = !DILocation(line: 91, column: 56, scope: !109)
!114 = !DILocation(line: 91, column: 48, scope: !109)
!115 = !DILocation(line: 91, column: 46, scope: !109)
!116 = !DILocalVariable(name: "sm2", scope: !109, file: !1, line: 92, type: !4)
!117 = !DILocation(line: 92, column: 16, scope: !109)
!118 = !DILocation(line: 92, column: 33, scope: !109)
!119 = !DILocation(line: 92, column: 31, scope: !109)
!120 = !DILocation(line: 92, column: 22, scope: !109)
!121 = !DILocalVariable(name: "sm1", scope: !109, file: !1, line: 93, type: !4)
!122 = !DILocation(line: 93, column: 16, scope: !109)
!123 = !DILocation(line: 93, column: 29, scope: !109)
!124 = !DILocation(line: 93, column: 28, scope: !109)
!125 = !DILocation(line: 93, column: 22, scope: !109)
!126 = !DILocalVariable(name: "cm2", scope: !109, file: !1, line: 94, type: !4)
!127 = !DILocation(line: 94, column: 16, scope: !109)
!128 = !DILocation(line: 94, column: 33, scope: !109)
!129 = !DILocation(line: 94, column: 31, scope: !109)
!130 = !DILocation(line: 94, column: 22, scope: !109)
!131 = !DILocalVariable(name: "cm1", scope: !109, file: !1, line: 95, type: !4)
!132 = !DILocation(line: 95, column: 16, scope: !109)
!133 = !DILocation(line: 95, column: 29, scope: !109)
!134 = !DILocation(line: 95, column: 28, scope: !109)
!135 = !DILocation(line: 95, column: 22, scope: !109)
!136 = !DILocalVariable(name: "w", scope: !109, file: !1, line: 96, type: !4)
!137 = !DILocation(line: 96, column: 16, scope: !109)
!138 = !DILocation(line: 96, column: 24, scope: !109)
!139 = !DILocation(line: 96, column: 22, scope: !109)
!140 = !DILocalVariable(name: "ar", scope: !109, file: !1, line: 97, type: !141)
!141 = !DICompositeType(tag: DW_TAG_array_type, baseType: !4, size: 192, elements: !142)
!142 = !{!143}
!143 = !DISubrange(count: 3)
!144 = !DILocation(line: 97, column: 16, scope: !109)
!145 = !DILocalVariable(name: "ai", scope: !109, file: !1, line: 97, type: !141)
!146 = !DILocation(line: 97, column: 23, scope: !109)
!147 = !DILocation(line: 99, column: 16, scope: !148)
!148 = distinct !DILexicalBlock(scope: !109, file: !1, line: 99, column: 9)
!149 = !DILocation(line: 99, column: 15, scope: !148)
!150 = !DILocation(line: 99, column: 20, scope: !151)
!151 = distinct !DILexicalBlock(scope: !148, file: !1, line: 99, column: 9)
!152 = !DILocation(line: 99, column: 24, scope: !151)
!153 = !DILocation(line: 99, column: 22, scope: !151)
!154 = !DILocation(line: 99, column: 9, scope: !148)
!155 = !DILocation(line: 101, column: 13, scope: !156)
!156 = distinct !DILexicalBlock(scope: !151, file: !1, line: 100, column: 9)
!157 = !DILocation(line: 102, column: 21, scope: !156)
!158 = !DILocation(line: 102, column: 13, scope: !156)
!159 = !DILocation(line: 102, column: 19, scope: !156)
!160 = !DILocation(line: 103, column: 21, scope: !156)
!161 = !DILocation(line: 103, column: 13, scope: !156)
!162 = !DILocation(line: 103, column: 19, scope: !156)
!163 = !DILocation(line: 105, column: 21, scope: !156)
!164 = !DILocation(line: 105, column: 13, scope: !156)
!165 = !DILocation(line: 105, column: 19, scope: !156)
!166 = !DILocation(line: 106, column: 21, scope: !156)
!167 = !DILocation(line: 106, column: 13, scope: !156)
!168 = !DILocation(line: 106, column: 19, scope: !156)
!169 = !DILocation(line: 108, column: 21, scope: !170)
!170 = distinct !DILexicalBlock(scope: !156, file: !1, line: 108, column: 13)
!171 = !DILocation(line: 108, column: 20, scope: !170)
!172 = !DILocation(line: 108, column: 25, scope: !170)
!173 = !DILocation(line: 108, column: 19, scope: !170)
!174 = !DILocation(line: 108, column: 29, scope: !175)
!175 = distinct !DILexicalBlock(scope: !170, file: !1, line: 108, column: 13)
!176 = !DILocation(line: 108, column: 33, scope: !175)
!177 = !DILocation(line: 108, column: 31, scope: !175)
!178 = !DILocation(line: 108, column: 13, scope: !170)
!179 = !DILocation(line: 110, column: 17, scope: !180)
!180 = distinct !DILexicalBlock(scope: !175, file: !1, line: 109, column: 13)
!181 = !DILocation(line: 111, column: 25, scope: !180)
!182 = !DILocation(line: 111, column: 27, scope: !180)
!183 = !DILocation(line: 111, column: 26, scope: !180)
!184 = !DILocation(line: 111, column: 35, scope: !180)
!185 = !DILocation(line: 111, column: 33, scope: !180)
!186 = !DILocation(line: 111, column: 17, scope: !180)
!187 = !DILocation(line: 111, column: 23, scope: !180)
!188 = !DILocation(line: 112, column: 25, scope: !180)
!189 = !DILocation(line: 112, column: 17, scope: !180)
!190 = !DILocation(line: 112, column: 23, scope: !180)
!191 = !DILocation(line: 113, column: 25, scope: !180)
!192 = !DILocation(line: 113, column: 17, scope: !180)
!193 = !DILocation(line: 113, column: 23, scope: !180)
!194 = !DILocation(line: 115, column: 25, scope: !180)
!195 = !DILocation(line: 115, column: 27, scope: !180)
!196 = !DILocation(line: 115, column: 26, scope: !180)
!197 = !DILocation(line: 115, column: 35, scope: !180)
!198 = !DILocation(line: 115, column: 33, scope: !180)
!199 = !DILocation(line: 115, column: 17, scope: !180)
!200 = !DILocation(line: 115, column: 23, scope: !180)
!201 = !DILocation(line: 116, column: 25, scope: !180)
!202 = !DILocation(line: 116, column: 17, scope: !180)
!203 = !DILocation(line: 116, column: 23, scope: !180)
!204 = !DILocation(line: 117, column: 25, scope: !180)
!205 = !DILocation(line: 117, column: 17, scope: !180)
!206 = !DILocation(line: 117, column: 23, scope: !180)
!207 = !DILocation(line: 119, column: 21, scope: !180)
!208 = !DILocation(line: 119, column: 25, scope: !180)
!209 = !DILocation(line: 119, column: 23, scope: !180)
!210 = !DILocation(line: 119, column: 19, scope: !180)
!211 = !DILocation(line: 120, column: 22, scope: !180)
!212 = !DILocation(line: 120, column: 28, scope: !180)
!213 = !DILocation(line: 120, column: 36, scope: !180)
!214 = !DILocation(line: 120, column: 27, scope: !180)
!215 = !DILocation(line: 120, column: 41, scope: !180)
!216 = !DILocation(line: 120, column: 47, scope: !180)
!217 = !DILocation(line: 120, column: 55, scope: !180)
!218 = !DILocation(line: 120, column: 46, scope: !180)
!219 = !DILocation(line: 120, column: 39, scope: !180)
!220 = !DILocation(line: 120, column: 20, scope: !180)
!221 = !DILocation(line: 121, column: 22, scope: !180)
!222 = !DILocation(line: 121, column: 28, scope: !180)
!223 = !DILocation(line: 121, column: 36, scope: !180)
!224 = !DILocation(line: 121, column: 27, scope: !180)
!225 = !DILocation(line: 121, column: 41, scope: !180)
!226 = !DILocation(line: 121, column: 47, scope: !180)
!227 = !DILocation(line: 121, column: 55, scope: !180)
!228 = !DILocation(line: 121, column: 46, scope: !180)
!229 = !DILocation(line: 121, column: 39, scope: !180)
!230 = !DILocation(line: 121, column: 20, scope: !180)
!231 = !DILocation(line: 123, column: 30, scope: !180)
!232 = !DILocation(line: 123, column: 38, scope: !180)
!233 = !DILocation(line: 123, column: 43, scope: !180)
!234 = !DILocation(line: 123, column: 41, scope: !180)
!235 = !DILocation(line: 123, column: 17, scope: !180)
!236 = !DILocation(line: 123, column: 25, scope: !180)
!237 = !DILocation(line: 123, column: 28, scope: !180)
!238 = !DILocation(line: 124, column: 30, scope: !180)
!239 = !DILocation(line: 124, column: 38, scope: !180)
!240 = !DILocation(line: 124, column: 43, scope: !180)
!241 = !DILocation(line: 124, column: 41, scope: !180)
!242 = !DILocation(line: 124, column: 17, scope: !180)
!243 = !DILocation(line: 124, column: 25, scope: !180)
!244 = !DILocation(line: 124, column: 28, scope: !180)
!245 = !DILocation(line: 126, column: 31, scope: !180)
!246 = !DILocation(line: 126, column: 17, scope: !180)
!247 = !DILocation(line: 126, column: 25, scope: !180)
!248 = !DILocation(line: 126, column: 28, scope: !180)
!249 = !DILocation(line: 127, column: 31, scope: !180)
!250 = !DILocation(line: 127, column: 17, scope: !180)
!251 = !DILocation(line: 127, column: 25, scope: !180)
!252 = !DILocation(line: 127, column: 28, scope: !180)
!253 = !DILocation(line: 128, column: 13, scope: !180)
!254 = !DILocation(line: 108, column: 44, scope: !175)
!255 = !DILocation(line: 108, column: 49, scope: !175)
!256 = !DILocation(line: 108, column: 13, scope: !175)
!257 = distinct !{!257, !178, !258}
!258 = !DILocation(line: 128, column: 13, scope: !170)
!259 = !DILocation(line: 129, column: 9, scope: !156)
!260 = !DILocation(line: 99, column: 41, scope: !151)
!261 = !DILocation(line: 99, column: 38, scope: !151)
!262 = !DILocation(line: 99, column: 9, scope: !151)
!263 = distinct !{!263, !154, !264}
!264 = !DILocation(line: 129, column: 9, scope: !148)
!265 = !DILocation(line: 131, column: 20, scope: !109)
!266 = !DILocation(line: 131, column: 18, scope: !109)
!267 = !DILocation(line: 132, column: 5, scope: !109)
!268 = !DILocation(line: 88, column: 61, scope: !104)
!269 = !DILocation(line: 88, column: 5, scope: !104)
!270 = distinct !{!270, !107, !271}
!271 = !DILocation(line: 132, column: 5, scope: !101)
!272 = !DILocation(line: 138, column: 10, scope: !273)
!273 = distinct !DILexicalBlock(scope: !9, file: !1, line: 138, column: 10)
!274 = !DILocation(line: 138, column: 10, scope: !9)
!275 = !DILocalVariable(name: "denom", scope: !276, file: !1, line: 140, type: !4)
!276 = distinct !DILexicalBlock(scope: !273, file: !1, line: 139, column: 5)
!277 = !DILocation(line: 140, column: 16, scope: !276)
!278 = !DILocation(line: 140, column: 32, scope: !276)
!279 = !DILocation(line: 140, column: 24, scope: !276)
!280 = !DILocation(line: 142, column: 16, scope: !281)
!281 = distinct !DILexicalBlock(scope: !276, file: !1, line: 142, column: 9)
!282 = !DILocation(line: 142, column: 15, scope: !281)
!283 = !DILocation(line: 142, column: 20, scope: !284)
!284 = distinct !DILexicalBlock(scope: !281, file: !1, line: 142, column: 9)
!285 = !DILocation(line: 142, column: 24, scope: !284)
!286 = !DILocation(line: 142, column: 22, scope: !284)
!287 = !DILocation(line: 142, column: 9, scope: !281)
!288 = !DILocation(line: 144, column: 13, scope: !289)
!289 = distinct !DILexicalBlock(scope: !284, file: !1, line: 143, column: 9)
!290 = !DILocation(line: 145, column: 27, scope: !289)
!291 = !DILocation(line: 145, column: 13, scope: !289)
!292 = !DILocation(line: 145, column: 21, scope: !289)
!293 = !DILocation(line: 145, column: 24, scope: !289)
!294 = !DILocation(line: 146, column: 27, scope: !289)
!295 = !DILocation(line: 146, column: 13, scope: !289)
!296 = !DILocation(line: 146, column: 21, scope: !289)
!297 = !DILocation(line: 146, column: 24, scope: !289)
!298 = !DILocation(line: 148, column: 9, scope: !289)
!299 = !DILocation(line: 142, column: 37, scope: !284)
!300 = !DILocation(line: 142, column: 9, scope: !284)
!301 = distinct !{!301, !287, !302}
!302 = !DILocation(line: 148, column: 9, scope: !281)
!303 = !DILocation(line: 149, column: 5, scope: !276)
!304 = !DILocation(line: 150, column: 1, scope: !9)
!305 = distinct !DISubprogram(name: "CheckPointer", scope: !1, file: !1, line: 26, type: !306, scopeLine: 27, flags: DIFlagPrototyped, spFlags: DISPFlagLocalToUnit | DISPFlagDefinition, unit: !0, retainedNodes: !2)
!306 = !DISubroutineType(types: !307)
!307 = !{null, !308, !309}
!308 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 16)
!309 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !310, size: 16)
!310 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!311 = !DILocalVariable(name: "p", arg: 1, scope: !305, file: !1, line: 26, type: !308)
!312 = !DILocation(line: 26, column: 34, scope: !305)
!313 = !DILocalVariable(name: "name", arg: 2, scope: !305, file: !1, line: 26, type: !309)
!314 = !DILocation(line: 26, column: 43, scope: !305)
!315 = !DILocation(line: 28, column: 10, scope: !316)
!316 = distinct !DILexicalBlock(scope: !305, file: !1, line: 28, column: 10)
!317 = !DILocation(line: 28, column: 12, scope: !316)
!318 = !DILocation(line: 28, column: 10, scope: !305)
!319 = !DILocation(line: 31, column: 9, scope: !320)
!320 = distinct !DILexicalBlock(scope: !316, file: !1, line: 29, column: 5)
!321 = !DILocation(line: 33, column: 1, scope: !305)
