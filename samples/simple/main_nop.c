// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll
/*** This example has been inspired by the presentation done during NOP Seminar

save_state();
int sum = 0;
for(int i=0; i<SIZE; i++)
  sum += tab[i];

save_state() ?

int pkt_sent = 0;
int variation = var(sum);

save_state() ?

pkt_sent = tx_send(variation);
save_state();
*/
int old_sum = 5;
int variation;
int buff = 0;

int main(){
    int sum = 0;
    sum++;
    sum++;
    sum++;
    sum++;
    sum++;
    goto bb2;
bb2:
    variation = sum - old_sum;
    int nb_packet = 0;
    goto bb3;
bb3:
    variation = variation << 2;
    nb_packet++;
    variation = variation << 2;
    nb_packet++;
    variation = variation << 2;
    nb_packet++;
}