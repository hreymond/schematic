// Compile with clang-7 -emit-llvm -O0 -g -S main_loop.c -o source.ll

int main(){
    checkpoint();
    int i = 0;
    int sum = 0;
    for(;i < 10; i++) {
        sum += i;
    }
    checkpoint();
    if(sum < 2) {
        return 2;
    } else {
        return sum;
    }
}