// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll

int main(){
    int i = 0;
    int sum = 0;
    for(;i < 100; i++) {
        sum += i;
    }
    if(sum < 2) {
        return 2;
    } else {
        return sum;
    }
}
