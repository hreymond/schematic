// Compile with clang-7 -emit-llvm -O0 -g -S main_function_loop.c -o source.ll -target msp430
int f(){
    return 2;
}

int main(){
    int a = 0;
    f();
    for (int i = 0; i<100; i++) {
        __max_iter(100);
        if(i > 10) {
            f();
        }
        a += 1;
    	checkpoint();
    }
    return a;
}
