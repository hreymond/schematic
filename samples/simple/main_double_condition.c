// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll
int old_sum = 5;
int variation;
int buff = 0;
int nb_packet;
int computation_mode = 0;

int main(){
    int sum = 0;
    sum++;
    sum++;
    sum++;
    sum++;

    if(computation_mode){
        variation = sum - old_sum;
        nb_packet = 0;
    } else {
        variation = old_sum -sum;
        variation = variation / 10;
        nb_packet = 1;
    }

    variation = variation << 2;
    nb_packet++;
    variation = variation << 2;
    nb_packet++;
    variation = variation << 2;
    nb_packet++;

    if(nb_packet == 3) {
        return 0;
    } else {
        return -1;
    }
}