; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @main() #0 !dbg !7 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !11, metadata !DIExpression()), !dbg !12
  store i32 0, i32* %2, align 4, !dbg !12
  call void @llvm.dbg.declare(metadata i32* %3, metadata !13, metadata !DIExpression()), !dbg !14
  store i32 0, i32* %3, align 4, !dbg !14
  br label %4, !dbg !15

; <label>:4:                                      ; preds = %11, %0
  %5 = load i32, i32* %2, align 4, !dbg !16
  %6 = icmp slt i32 %5, 100, !dbg !19
  br i1 %6, label %7, label %14, !dbg !20

; <label>:7:                                      ; preds = %4
  %8 = load i32, i32* %2, align 4, !dbg !21
  %9 = load i32, i32* %3, align 4, !dbg !23
  %10 = add nsw i32 %9, %8, !dbg !23
  store i32 %10, i32* %3, align 4, !dbg !23
  br label %11, !dbg !24

; <label>:11:                                     ; preds = %7
  %12 = load i32, i32* %2, align 4, !dbg !25
  %13 = add nsw i32 %12, 1, !dbg !25
  store i32 %13, i32* %2, align 4, !dbg !25
  br label %4, !dbg !26, !llvm.loop !27

; <label>:14:                                     ; preds = %4
  %15 = load i32, i32* %3, align 4, !dbg !29
  %16 = icmp slt i32 %15, 2, !dbg !31
  br i1 %16, label %17, label %18, !dbg !32

; <label>:17:                                     ; preds = %14
  store i32 2, i32* %1, align 4, !dbg !33
  br label %20, !dbg !33

; <label>:18:                                     ; preds = %14
  %19 = load i32, i32* %3, align 4, !dbg !35
  store i32 %19, i32* %1, align 4, !dbg !37
  br label %20, !dbg !37

; <label>:20:                                     ; preds = %18, %17
  %21 = load i32, i32* %1, align 4, !dbg !38
  ret i32 %21, !dbg !38
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

attributes #0 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!3, !4, !5}
!llvm.ident = !{!6}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 7.0.0 (tags/RELEASE_700/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2)
!1 = !DIFile(filename: "main.c", directory: "/home/hreymond/code/SCHEMATIC/schematic/samples/simple")
!2 = !{}
!3 = !{i32 2, !"Dwarf Version", i32 4}
!4 = !{i32 2, !"Debug Info Version", i32 3}
!5 = !{i32 1, !"wchar_size", i32 4}
!6 = !{!"clang version 7.0.0 (tags/RELEASE_700/final)"}
!7 = distinct !DISubprogram(name: "main", scope: !1, file: !1, line: 3, type: !8, isLocal: false, isDefinition: true, scopeLine: 3, isOptimized: false, unit: !0, retainedNodes: !2)
!8 = !DISubroutineType(types: !9)
!9 = !{!10}
!10 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!11 = !DILocalVariable(name: "i", scope: !7, file: !1, line: 4, type: !10)
!12 = !DILocation(line: 4, column: 9, scope: !7)
!13 = !DILocalVariable(name: "sum", scope: !7, file: !1, line: 5, type: !10)
!14 = !DILocation(line: 5, column: 9, scope: !7)
!15 = !DILocation(line: 6, column: 5, scope: !7)
!16 = !DILocation(line: 6, column: 10, scope: !17)
!17 = distinct !DILexicalBlock(scope: !18, file: !1, line: 6, column: 5)
!18 = distinct !DILexicalBlock(scope: !7, file: !1, line: 6, column: 5)
!19 = !DILocation(line: 6, column: 12, scope: !17)
!20 = !DILocation(line: 6, column: 5, scope: !18)
!21 = !DILocation(line: 7, column: 16, scope: !22)
!22 = distinct !DILexicalBlock(scope: !17, file: !1, line: 6, column: 24)
!23 = !DILocation(line: 7, column: 13, scope: !22)
!24 = !DILocation(line: 8, column: 5, scope: !22)
!25 = !DILocation(line: 6, column: 20, scope: !17)
!26 = !DILocation(line: 6, column: 5, scope: !17)
!27 = distinct !{!27, !20, !28}
!28 = !DILocation(line: 8, column: 5, scope: !18)
!29 = !DILocation(line: 9, column: 8, scope: !30)
!30 = distinct !DILexicalBlock(scope: !7, file: !1, line: 9, column: 8)
!31 = !DILocation(line: 9, column: 12, scope: !30)
!32 = !DILocation(line: 9, column: 8, scope: !7)
!33 = !DILocation(line: 10, column: 9, scope: !34)
!34 = distinct !DILexicalBlock(scope: !30, file: !1, line: 9, column: 17)
!35 = !DILocation(line: 12, column: 16, scope: !36)
!36 = distinct !DILexicalBlock(scope: !30, file: !1, line: 11, column: 12)
!37 = !DILocation(line: 12, column: 9, scope: !36)
!38 = !DILocation(line: 14, column: 1, scope: !7)
