// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll

int main(){
    int a = 0;
    int b = 1;
    int res = 0;
    if(a > b) {
        a = a + 1;
        res = a;
    } else {
        res = b;
    }
    return res;
}