root_dir=$(pwd)
test_dir="$root_dir/tests"
echo "Running unit tests..."
set -e
python3 -m unittest discover -s "$test_dir/unit_tests"
exit $?