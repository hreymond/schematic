ROOT_DIR=$(pwd)
PYTHON="python3"
TEST_DIR="$ROOT_DIR/tests/functional"
TESTS="dijkstra_base function_all_nvm_1 function_all_nvm_2 function_different_allocations simple simple2 condition double_condition fft fft_schematic"
TEST_TMP="test_tmp"

function error {
  echo -e "\e[31m[Error]\e[0m $1"
}

# Check if python3 is installed
if ! command -v $PYTHON &> /dev/null
then
    echo "Python3 could not be found"
    exit
fi

# Check if the pygraphviz python package is installed
if ! $PYTHON -c "import pygraphviz" &> /dev/null
then
    error "The pygraphviz python package is not installed"
    error "Please follow the instructions at https://pygraphviz.github.io/documentation/stable/install.html file to install it"
    exit
fi

set -e
# Create a folder to run the test, cd into it a get the default run.py and Sceptic link
rm -rf $TEST_TMP
mkdir $TEST_TMP
cp samples/fft/run.py $TEST_TMP
cd $TEST_TMP || exit


mkdir logs

echo "Testing in folder: $TEST_TMP"

for test in $TESTS
do
  echo "[Test: $test]"
  # Remove previous analysis files and keep logs and run.py
  rm -rf `ls | grep -v "logs\|run.py"`
  ln -s ../ScEpTIC/ .
  cp $TEST_DIR/data/$test/* .

  # Run the emulation
  echo "Running emulation..."
  $PYTHON run.py >  logs/$test

  # Run the tests
  echo "Running tests..."
  $PYTHON -m unittest discover -s "$TEST_DIR/tests/$test/"
  if [ $? -ne 0 ]
  then
    exit $?
  fi
done
echo "Done"
