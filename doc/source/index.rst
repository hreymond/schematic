.. Schematic documentation master file, created by
   sphinx-quickstart on Fri Mar 25 17:38:32 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Schematic's documentation!
=====================================

.. toctree::
   :maxdepth: 2

   install.md
   samples.md
   configuration.md
   checkpoint_placement_and_memory_allocation
   schematic/api_reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
