elements package
================

AST.elements.function module
----------------------------

.. automodule:: AST.elements.function
   :members:
   :undoc-members:
   :show-inheritance:

AST.elements.global\_var module
-------------------------------

.. automodule:: AST.elements.global_var
   :members:
   :undoc-members:
   :show-inheritance:

AST.elements.instruction module
-------------------------------

.. automodule:: AST.elements.instruction
   :members:
   :undoc-members:
   :show-inheritance:

AST.elements.metadata module
----------------------------

.. automodule:: AST.elements.metadata
   :members:
   :undoc-members:
   :show-inheritance:

AST.elements.types module
-------------------------

.. automodule:: AST.elements.types
   :members:
   :undoc-members:
   :show-inheritance:

AST.elements.value module
-------------------------

.. automodule:: AST.elements.value
   :members:
   :undoc-members:
   :show-inheritance:
