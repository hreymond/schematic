ScEpTIC emulator
==================================

ScEpTIC documentation can be found on https://neslabpolimi.bitbucket.io/ScEpTIC/

The alfred modified version is accessible at https://neslabpolimi.bitbucket.io/ALFRED/

ScEpTIC AST Elements
--------------------


.. toctree::
   :maxdepth: 2

   sceptic.elements

ScEpTIC AST instructions
------------------------

.. toctree::
   :maxdepth: 2

   sceptic.elements.instructions