analysis package
===================================

Contains the different analysis that need to be performed in before the creation of the reachable checkpoint graph.

analysis.energy\_consumption module
------------------------------------------------------

.. automodule:: analysis.energy_consumption
   :members:
   :undoc-members:
   :show-inheritance:

analysis.memory\_allocator module
----------------------------------------------------

.. automodule:: analysis.memory_allocator
   :members:
   :undoc-members:
   :show-inheritance:

analysis.variable\_access\_count module
----------------------------------------------------------

.. automodule:: analysis.variable_access_count
   :members:
   :undoc-members:
   :show-inheritance: