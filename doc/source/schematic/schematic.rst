schematic
===========================================

.. automodule:: schematic
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: cfg_modification
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: code_transformation
   :members:
   :undoc-members:
   :show-inheritance:
