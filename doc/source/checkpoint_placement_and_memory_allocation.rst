.. currentmodule:: ScEpTIC.AST.transformations.schematic

Checkpoint placement and Memory Allocation
==========================================

AST parsing
-----------

The CFGs of the different functions composing the program are extracted from the ScEpTIC AST.

The CFG built is represented with a Digraph networkx where the nodes of the graph are the basic blocs. Two abstract
basic bloc named `START_function` and `END_function` are placed at the beginning and the end of each function.
On the edges of the graph, potential checkpoint are placed (they are named C1, C2, C3, ...).

The following image shows an example of our tool cfg representation:

.. image:: ressources/cfg.svg

The function used to build the cfg is the following:

.. autofunction:: parsers.cfg_parser.asts_to_cfgs
    :noindex:

The llvm code parsed by llvm is then converted in CFGs objects

.. autoclass:: elements.cfg.CFG
    :show-inheritance:

Energy analysis
---------------

In order to determine the energy consumed by each basic bloc with all variables in NVM, we analyse the basic blocks
with a worst case energy consumption model. The analysis is done through the EnergyEstimationPass


.. autoclass:: analysis.energy_consumption.EnergyConsumptionEstimationPass
   :members:
   :undoc-members:
   :show-inheritance:
   :noindex:
