# Installation

## Requirements

In order to run the ScEpTIC emulator, you will need to have `numpy` installed.

In order to use the Schematic pass, you will also require `networkx`

To install those packages, you can use the following command: `pip3 install -r requirements.txt`

## Development requirements

In order to visualize CFGs as dot graphs, you will need to install the following dependencies:

- gcc
- graphviz (library and sources)
- python3 (library and sources)
- pygraphviz

To do so, you can use the following commands on ubuntu:

`sudo apt install gcc graphviz graphviz-dev python3 python3-dev`

`pip3 install pygraphviz`

## Documentation generation

To generate the documentation, you will need to install the following python packages

`pip3 install sphynx sphinx-rtd-theme myst-parser`
