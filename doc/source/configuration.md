# Configuration

# ScEpTIC configuration

The parameters of the emulator are given using the `config.py` file. The configuration file is documented, and you can find more information about it in the [ScEpTIC documentation](https://neslabpolimi.bitbucket.io/ScEpTIC/guide/configuration.html).

# Schematic configuration

Options need to be given to the Schematic pass to configure the checkpoint placement algorithm. The options are the following:

```python
# [CONFIGURATION FOR CUSTOM TRANSFORMATION PASSES]
transformation_options = {
    "schematic": {
        "worst_case_current_consumption": worst_case_current_consumption[clock_frequency],
        "energy_budget": 10000,
        "trace_file": "traces.json",
        "vm_size": 2000,
        "joint_allocation_and_checkpoint": True,
        "authorize_conditional_checkpoints": True,
    }
}
```

- `worst_case_current_consumption`: a dictionary containing the worst case current consumption of the device (see existing 'config.py' examples in `samples` folder)
- `energy_budget`: the energy budget of the device (in cycles)
- `trace_file`: the path to the trace file (see [Trace file](#trace-file))
- `vm_size`: the size of the virtual memory (in bytes)
- `joint_allocation_and_checkpoint`: if `True`, the pass will try to allocate memory and place checkpoints at the same time. If `False`, the pass will first place checkpoints, and then allocate memory.
- `authorize_conditional_checkpoints`: if `True`, the pass will be allowed to insert checkpoint in loops can be executed only every X iterations (see [Conditional checkpoints](#conditional-checkpoints)). If `False`, the pass will not insert checkpoints in loops.
