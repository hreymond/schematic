import logging

from ScEpTIC.AST.builtins.builtin import Builtin
from ScEpTIC.emulator.io.output import OutputManager
from ScEpTIC.emulator.io.input import InputManager
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.emulator.energy.msp430 import MSP430EnergyCalculator

# [LOGGING SETUP]
LOGGING_ENABLED = False
LOGGING_LEVEL = logging.CRITICAL
# LOGGING_LEVEL = logging.WARNING
# LOGGING_LEVEL = logging.INFO
# LOGGING_LEVEL = logging.DEBUG

# [PARSER LOGGING SETUP]
LOG_SECTION_CONTENT = False
PARSER_LOG_LEVEL = logging.CRITICAL

# [DEBUG NAME]
test_name = "program"

# [TESTING FILE CONFIGURATION]
file = "samples/base.ll"

# [TEST OUTPUT CONFIG]
save_test_results = True
save_llvmir_code = True
save_vm_state = True
save_dir = "analysis_results"
save_dir_datetime = True  # append current date to result folder name

# [TEST TO RUN]
run_continuous = True
run_locate_memory_test = True
run_evaluate_memory_test = True
run_input_consistency_test = True
run_output_profiling = True
run_profiling = True
run_energy_measure = True
run_memory_size_measure = True

# [PROGRAM CONFIGURATION]
program_configuration = {
    "ir_function_prefix": "@",  # llvm ir function name prefix
    "main_function_name": "main",
    "before_restore_function_name": "sceptic_before_restore",
    "after_restore_function_name": "sceptic_after_restore",
    "before_checkpoint_function_name": "sceptic_before_checkpoint",
    "after_checkpoint_function_name": "sceptic_after_checkpoint",
    "program_transformations": ["ratchet", "virtual_memory"],
}

# [REGISTER FILE CONFIGURATION]
register_file_configuration = {
    "use_physical_registers": True,
    "physical_registers_number": 10,
    "allocator_module_location": "ScEpTIC.AST.register_allocation",
    "allocator_module_name": "linear_scan",
    "allocator_function_name": "allocate_registers",  # nb: must take those arguments: functions, registers_number, reg_prefix, spill_prefix, spill_type
    "physical_registers_prefix": "R",
    "spill_virtual_registers_prefix": "%spill_",
    "spill_virtual_registers_type": "i32",
    "param_regs_count": 4,  # number of registers used for passing parameters to functions (in arm/msp430 is 4)
}


# [EXECUTION DEPTH]
# NB: ignored if checkpoint mechanism is static
execution_depth = 10

# [STOP ON FIRST ANOMALY]
# If set to True, if ScEpTIC finds a real anomaly, it stops
stop_on_first_anomaly = True

# [SYSTEM CONFIGURATION]
system = "custom"
"""
Available systems: DINO, Hibernus, Mementos, Quickrecall, Ratchet, Custom
To add a system, add a .py file named with the system name you want to support in ScEpTIC/emulator/configurator/.
Use other system files as template.

Custom considers the user-defined settings for memory configuration and checkpoint mechanisms.
Addresses, prefixes and dimensions are never overwritten by pre-defined systems configurations.
The following configurations will be overwritten, if the system is different from 'custom':
    - memory_configuration['sram']['enabled']
    - memory_configuration['sram']['stack']
    - memory_configuration['sram']['heap']
    - memory_configuration['sram']['gst']

    - memory_configuration['nvm']['enabled']
    - memory_configuration['nvm']['stack']
    - memory_configuration['nvm']['heap']
    - memory_configuration['nvm']['gst']


    - checkpoint_mechanism_configuration['checkpoint_placement']

    - checkpoint_mechanism_configuration['on_dynamic_voltage_alert']

    - checkpoint_mechanism_configuration['restore_register_file']

    - checkpoint_mechanism_configuration['sram']['restore_stack']
    - checkpoint_mechanism_configuration['sram']['restore_heap']
    - checkpoint_mechanism_configuration['sram']['restore_gst']
    
    - checkpoint_mechanism_configuration['nvm']['restore_stack']
    - checkpoint_mechanism_configuration['nvm']['restore_heap']
    - checkpoint_mechanism_configuration['nvm']['restore_gst']
"""

# [MEMORY CONFIGURATION]
memory_configuration = {
    "sram": {"enabled": True, "stack": True, "heap": True, "gst": True, "gst_prefix": "SGST", "gst_base_address": 0},
    "nvm": {"enabled": True, "stack": False, "heap": False, "gst": True, "gst_prefix": "FGST", "gst_base_address": 0},
    "base_addresses": {"stack": 0, "heap": 0},
    "prefixes": {"stack": "S", "heap": "H"},
    "gst": {"default_ram": "SRAM", "other_ram_section": ".NVM"},  # SRAM or NVM
    "address_dimension": 32,  # bit
}


# [CHECKPOINT MECHANISM CONFIGURATION]
"""
NB: if the checkpoint mechanism creates incremental checkpoints of nvm, it restores the whole nvm.
This configuration doesn't need to know the granularity of the checkpoint mechanism, it just needs to know
if something is restored (even partially).
checkpoint_placement can be static or dynamic. If it is set to static, checkpoints must be already present
inside the provided code. In such case, the user must provide the names of the checkpoint and restore routines.
on_dynamic_voltage_alert is considered only if checkpoint_placement is set to dynamic. it can be set on continue or stop.
if is set to stop, after a power interrupt the simulator will save a checkpoint and stop the execution (forces the execution_depth to 0).
if is set to continue, the simulator will save a checkpoint and continue the execution for #execution_dept operations (dynamic) or until another
checkpoint is reached (static).
"""
checkpoint_mechanism_configuration = {
    "checkpoint_placement": "dynamic",
    "on_dynamic_voltage_alert": "continue",
    "checkpoint_routine_name": "checkpoint",  # required if checkpoint_placement is static
    "restore_routine_name": "restore",  # required if checkpoint_placement is static
    "restore_register_file": True,
    "sram": {"restore_stack": True, "restore_heap": True, "restore_gst": True},
    "nvm": {"restore_stack": False, "restore_heap": False, "restore_gst": False},
    "environment": False,
}


# [LOGGER SETUP FINALIZATION]
logging.basicConfig(format="[%(levelname)s - %(asctime)s] %(message)s", level=LOGGING_LEVEL, datefmt="%H:%M:%S")
logging.getLogger().disabled = not LOGGING_ENABLED


# [USER-DEFINED BUILTINS]


class Test(Builtin):

    tick_count = 2000

    def get_val(self):
        for i in self.args:
            print(i)

        print("OK")
        return self.args[3]


Test.define_builtin("test_builtin", "double, i32, i32, i32", "i32")

# [USER-DEFINED OUTPUTS]
#
# For output analysis, set the default idempotency model used for each output function
OutputManager.default_idempotent = True
OutputManager.create_output("test", "output_1", "i32")
OutputManager.create_output("test2", "output_2", "i32")

# For output analysis, set the idempotency model for a given output
OutputManager.set_idempotency("test", OutputManager.NON_IDEMPOTENT)

# [USER-DEFINED INPUTS]
InputManager.create_input("PIR", "pir_input", "i32")
InputManager.set_input_value("PIR", "10000000")
InputManager.set_consistency_model("PIR", InputManager.LONG_TERM)

# [CONFIGURATION FOR MEMORY SIZE]
memory_size_config = {"double_buffer_for_anomalies": True, "global_variables_default_memory": VirtualMemoryEnum.VOLATILE}

# [CONFIGURATION FOR ENERGY MEASURES]
clock_frequency = "8Mhz"

energy_simulation_config = {
    "use_trigger_calls": True,  # sets if checkpoints are trigger calls that queries an ADC or directly execute a checkpoint
    "trigger_call_to_checkpoint_probability_range": [
        0,
        10,
        1,
    ],  # probability range of trigger calls converting in checkpoints (ignored if use_trigger_calls is False) (min, max, step)
    "simulate_power_failures": True,  # simulate the occurrence of power failures
    "clock_cycles_between_power_failures": 50000,  # number of clock cycles executed before a power failure (ignored if simulate_power_failures is False)
}

msp430fr5969_datasheet = {
    "registers": 10,
    "current_consumption": {
        "8Mhz": {
            "frequency": "8M",  # Clock Frequency
            "voltage": "3",  # Voltage
            "n_waits": "0",  # Number of wait cycles for FRAM accesses
            "I_am_ram": "585u",  # Current consumption for one clock cycle when program and data resides in SRAM
            "I_am_fram_uni": "1220u",  # Current consumption for one clock cycle when program and data resides in FRAM
            "I_am_fram": "890u",  # Current consumption for one clock cycle when program resides in FRAM and data in SRAM
            "I_am_fram_hit": "0.75",  # Cache hit rate of I_am_fram
        },
        "16Mhz": {
            "frequency": "16M",  # Clock Frequency
            "voltage": "3",  # Voltage
            "n_waits": "1",  # Number of wait cycles for FRAM accesses
            "I_am_ram": "1070u",  # Current consumption for one clock cycle when program and data resides in SRAM
            "I_am_fram_uni": "1845u",  # Current consumption for one clock cycle when program and data resides in FRAM
            "I_am_fram": "1420u",  # Current consumption for one clock cycle when program resides in FRAM and data in SRAM (cache hit 75%)
            "I_am_fram_hit": "0.75",  # Cache hit rate of I_am_fram
        },
    },
    "ADC": {
        "T_off_on": "100n",  # Time to wait for switching the ADC on
        "T_sampling": "1u",  # Time to get a sample
        "I_min": "145u",  # Minimum current consumption of the ADC when it is active
        "I_max": "185u",  # Maximum current consumption of the ADC when it is active
        "N_init": "3",  # Number of instructions to init the ADC
        # set ADC mode(single); set ADC op_type(read); set ADC on
        "N_off": "1",  # Number of instructions to stop the ADC
        # set ADC mode(off)
        "N_transfer_ops": "1",  # Number of instructions to retrieve data from the ADC
        # var_data <- REG_ADC
        "I_to_consider": "min",  # Current consumption of the ADC to consider (min, max, avg)
    },
}

mcu_datasheet = msp430fr5969_datasheet["current_consumption"][clock_frequency]

# Import the energy calculator module
# Note: you need to extend EnergyCalculator with your own module
# We provide a MSP430EnergyCalculator for the MSP430 MCUs in ScEpTIC.emulator.energy.msp430
energy_calculator = MSP430EnergyCalculator(
    mcu_datasheet, msp430fr5969_datasheet["registers"], msp430fr5969_datasheet["ADC"]
)
n_min_function = lambda x: energy_calculator.n_min_function(x)

# [CONFIGURATION FOR CUSTOM TRANSFORMATION PASSES]
transformation_options = {
    "virtual_memory": {"n_min_function": n_min_function, "optimize_saved_registers": True},
    "ratchet": {"functions_with_outside_frame_accesses": ["function_name"], "optimize_saved_registers": True},
}
