# Schematic Prototype

Schematic (Simultaneous Checkpoint Placement and Memory Allocation Tailored for Intermittent Computing) is a tool for placing checkpoints in an intermittent program and wisely allocate memory to volatile or on-volatile memory, to reduce the program energy consumption.

Schematic is developed as an extension of ScEpTIC, and benefit from a previous ScEpTIC extension named ALFRED. ScEpTIC and ALFRED are developed by the [Networked Embedded Systems Lab](https://neslabpolimi.bitbucket.io/) at [Politecnico di Milano](https://www.polimi.it/), and released under the [MIT License](https://opensource.org/licenses/MIT).

## Quick Start

Install the requirements:

`pip3 install -r requirements.txt`

Compile your target program to LLVM IR (e.g, with the `main.c` file in samples/simple):

`clang-7 -emit-llvm -O0 -g -S main.c -o source.ll -target msp430`

:warning: Note that ScEpTIC rely on `clang` version 7. Support for other versions is not guaranteed.
In this case, we are compiling for the MSP430 target, but you can compile for other targets as well.

In order to emulate the behavior of your platform, you might need to configure ScEpTIC with the correct parameters. You can do so by editing the `config.py` file, as explained in the [ScEpTIC documentation](https://neslabpolimi.bitbucket.io/ScEpTIC/).

In the `samples/simple` directory, you can find a simple example of how to use Schematic. You can run it with:

`python3 run.py`

With the default configuration and the trace file provided, Schematic pass will place checkpoints and allocate memory for the target program, and the ScEpTIC emulator will perform several analysis. The analysis results are stored in the `analysis_results/program_<timestamp>` directory.

Note that if don't have any trace of your program execution, you can either use ScEpTIC to generate one (with the run_trace parameter), or give a dummy trace file to Schematic (a `traces.json` file, containing an empty json dict `{}`).

## Documentation

You can find Schematic documentation at [https://hreymond.gitlabpages.inria.fr/schematic](https://hreymond.gitlabpages.inria.fr/schematic)

### Dev requirements

To generate dot graphs:

`sudo apt install gcc graphviz graphviz-dev python3 python3-dev`

`pip3 install pygrahpviz`

## Formatting

`black . -l 122`

## ScEpTIC and ALFRED

More information about ScEpTIC and ALFRED can be found below, or in the [ScEpTIC documentation](https://neslabpolimi.bitbucket.io/ScEpTIC/) and [ALFRED documentation](https://neslabpolimi.bitbucket.io/ALFRED/). Sources are available on [Bitbucket](https://bitbucket.org/neslabpolimi/sceptic/) (branches master and alfred).

------

# ALFRED prototype
**ALFRED** (**A**utomatic a**L**location o**F** non-volatile memo**R**y for transi**E**ntly-powered **D**evices) is a virtual memory abstraction that resolves the dichotomy between volatile and non-volatile memory in intermittent computing.

ALFRED prototype is implemented as an extension of ScEpTIC.

## Documentation
You can find ALFRED documentation at [https://neslabpolimi.bitbucket.io/ALFRED/](https://neslabpolimi.bitbucket.io/ALFRED/)

## Cite ALFRED
You can cite ALFRED and/or our program transformation techniques as:

_Andrea Maioli, Luca Mottola. 2021. ALFRED: Virtual Memory for Intermittent Computing.  In Proceedings of the the 19th ACM Conference on Embedded Networked Sensor Systems (SenSys '21)._

**Bibtex**:

```latex
    @inproceedings{ALFRED,
        author = {Andrea Maioli and Luca Mottola},
        title  = {ALFRED: Virtual Memory for Intermittent Computing},
        year = {2021},
        series = {SenSys '21},
        booktitle = {Proceedings of the the 19th ACM Conference on Embedded Networked Sensor Systems (SenSys '21)}
    }
```

------

# ScEpTIC
**ScEpTIC** (**S**imulator for **E**xecuting and **T**esting **I**ntermittent **C**omputation) is a python tool that analyzes intermittent execution of programs.

## Documentation
You can find ScEpTIC documentation at [https://neslabpolimi.bitbucket.io/ScEpTIC/](https://neslabpolimi.bitbucket.io/ScEpTIC/)

## Cite ScEpTIC
You can cite ScEpTIC and/or our analysis techniques as:

_Andrea Maioli, Luca Mottola, Muhammad Hamad Alizai, and Junaid Haroon Siddiqui. 2021. Discovering the Hidden Anomalies of Intermittent Computing. In Proceedings of the 2021 International Conference on Embedded Wireless Systems and Networks (EWSN '21)._

**Bibtex**:

```latex
    @inproceedings{ScEpTIC,
        author = {Andrea Maioli and Luca Mottola and Muhammad Hamad Alizai and Junaid Haroon Siddiqui},
        title  = {Discovering the Hidden Anomalies of Intermittent Computing},
        year = {2021},
        series = {EWSN 2021},
        booktitle = {Proceedings of the 2021 International Conference on Embedded Wireless Systems and Networks}
    }
```
