import unittest
from copy import deepcopy

import networkx as nx

from ScEpTIC.AST.elements.instructions.binary_operation import BinaryOperation
from ScEpTIC.AST.elements.instructions.memory_operations import AllocaOperation
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import BranchOperation
from ScEpTIC.AST.elements.types import BaseType, Type
from ScEpTIC.AST.elements.value import Value
from ScEpTIC.AST.transformations.schematic import MyTransformation
from ScEpTIC.AST.transformations.schematic.cfg_modification import isolate_function_calls
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import Checkpoint, CheckpointTypeEnum
from ScEpTIC.AST.transformations.schematic.elements.memory_allocation import MemoryAllocation, VariableAllocation
from tests.data.data_generation import data

nb_reg = 0


def generate_tmp_register():
    """
    Generate a new temporary virtual_register
    """
    global nb_reg
    name = "%tmp" + str(nb_reg)
    nb_reg += 1
    return Value("virtual_reg", name, Type.empty())


inst0 = AllocaOperation(None, None, 0, 0)
call = CallOperation(generate_tmp_register(), "@f", None, None, None, None)
inst1 = BinaryOperation("add", None, None, None, None, {})


class FunctionIsolationTest(unittest.TestCase):
    """
    Test the function isolation
    """

    def setUp(self):
        config = {"energy_budget": 50, "chkpt_restore": 2, "chkpt_save": 2, "call_cost": 4, "vm_size": 2000}
        self.transformation = MyTransformation(data.ENERGY_CALCULATOR, config)

    def test_simple_bb(self):
        bb = BasicBlock(0, "@main")
        bb.instructions = [inst0, call, inst1]
        bb.llvm_label = 0
        cfg = nx.DiGraph()
        cfg.add_node(bb)
        isolate_function_calls({"@main": cfg})
        f_entry_name = str(bb.llvm_label) + "_@f_entry"
        f_exit_name = str(bb.llvm_label) + "_@f_exit"
        self.assertEqual(4, len(cfg.nodes))
        self.assertEqual(3, len(cfg.edges))
        bbs = {bb.llvm_label: bb for bb in list(cfg.nodes)}

        self.assertEqual(2, len(bbs[0].instructions))
        self.assertIn(inst0, bbs[0].instructions)
        self.assertNotIn(call, bbs[0].instructions)
        self.assertNotIn(inst1, bbs[0].instructions)
        self.assertIsInstance(bbs[0].instructions[-1], BranchOperation)
        self.assertEqual("%" + str(bbs[f_entry_name].label), bbs[0].instructions[-1].target_true)
        self.assertEqual(0, bbs[0].instructions[-1].tick_count)

        self.assertEqual(1, len(bbs["0_1"].instructions))
        self.assertNotIn(inst0, bbs["0_1"].instructions)
        self.assertNotIn(call, bbs["0_1"].instructions)
        self.assertIn(inst1, bbs["0_1"].instructions)
        self.assertEqual(inst1, bbs["0_1"].instructions[-1])

        self.assertEqual(2, len(bbs[f_entry_name].instructions))
        self.assertNotIn(inst0, bbs[f_entry_name].instructions)
        self.assertIn(call, bbs[f_entry_name].instructions)
        self.assertNotIn(inst1, bbs[f_entry_name].instructions)
        self.assertIsInstance(bbs[f_entry_name].instructions[-1], BranchOperation)

        self.assertEqual(1, len(bbs[f_exit_name].instructions))
        self.assertIsInstance(bbs[f_exit_name].instructions[-1], BranchOperation)

    def test_empty_bb(self):
        # A minimal BB with a call and a return
        bb = BasicBlock(0, "@main")
        bb.instructions = [call, inst1]
        bb.llvm_label = 0
        cfg = nx.DiGraph()
        cfg.add_node(bb)
        isolate_function_calls({"@main": cfg})
        f_entry_name = str(bb.llvm_label) + "_@f_entry"
        f_exit_name = str(bb.llvm_label) + "_@f_exit"
        self.assertEqual(4, len(cfg.nodes))
        self.assertEqual(3, len(cfg.edges))
        bbs = {bb.llvm_label: bb for bb in list(cfg.nodes)}

        self.assertEqual(2, len(bbs[f_entry_name].instructions))
        self.assertNotIn(inst0, bbs[f_entry_name].instructions)
        self.assertIn(call, bbs[f_entry_name].instructions)
        self.assertNotIn(inst1, bbs[f_entry_name].instructions)
        self.assertIsInstance(bbs[f_entry_name].instructions[-1], BranchOperation)

        self.assertEqual(1, len(bbs[f_exit_name].instructions))
        self.assertIsInstance(bbs[f_exit_name].instructions[-1], BranchOperation)

    def test_function_basic_block_update(self):
        cfg = CFG()
        bb = BasicBlock(0, "@main")
        bb.llvm_label = "@f_entry"
        bb.is_function_call = True
        bb.cost_all_nvm = 4
        bb.instructions = [call, BranchOperation(None, "%5", None)]
        bb_exit = BasicBlock(5, "@main")
        bb_exit.llvm_label = "@f_exit"
        bb_exit.is_function_call = True
        bb_exit.instructions.append(BranchOperation(None, "%12", None))
        bb_exit.instructions[0].label = "%5"

        cfg.add_node(bb)
        cfg.add_node(bb_exit)
        cfg.add_edge(bb, bb_exit, checkpoint=Checkpoint(bb, bb_exit))
        self.assertEqual(CheckpointTypeEnum.POTENTIAL, cfg[bb][bb_exit]["checkpoint"].type)

        INT16 = Type.empty()
        INT16.is_base_type = True
        INT16.base_type = BaseType("int", 16)
        mem_alloc = MemoryAllocation()
        mem_alloc.vars["f.a"] = VariableAllocation("f.a", INT16, Value("virtual_reg", "%2", INT16))
        bb_start_f = BasicBlock(0, "@f")
        bb_start_f.llvm_label = "START_f"
        bb_start_f.memory_allocation = mem_alloc
        bb_start_f.cost_all_nvm = 0
        bb_start_f.final_cost = 0
        bb_start_f.energy_left = 10
        bb_start_f.energy_to_leave = 8
        bb_start_f.is_function_call = True
        bb_end_f = BasicBlock(0, "@f")
        bb_end_f.llvm_label = "END_f"
        bb_end_f.memory_allocation = mem_alloc
        bb_end_f.cost_all_nvm = 0
        bb_end_f.final_cost = 0
        bb_end_f.energy_left = 4
        bb_end_f.energy_to_leave = 2
        bb_end_f.is_function_call = True

        function_cfg = CFG()
        function_cfg.first_bb = bb_start_f
        function_cfg.last_bb = bb_end_f

        self.transformation.update_function_basic_blocks(cfg, function_cfg, (bb, bb_exit), False)
        self.assertEqual(CheckpointTypeEnum.DISABLED, cfg[bb][bb_exit]["checkpoint"].type)
        self.assertEqual(6 + 4, bb.final_cost)
        self.assertEqual(6 + 4, bb.cost_all_nvm)
        self.assertIsNone(bb.energy_left)
        self.assertIsNone(bb.energy_to_leave)
        self.assertEqual(0, bb_exit.cost_all_nvm)
        self.assertEqual(0, bb_exit.cost_all_nvm)
        self.assertIsNone(bb_exit.energy_left)
        self.assertIsNone(bb_exit.energy_to_leave)
        self.assertEqual(set(bb_start_f.memory_allocation.vars), set(bb.memory_allocation.vars))
        self.assertEqual(set(bb_end_f.memory_allocation.vars), set(bb_exit.memory_allocation.vars))

        cfg = CFG()
        bb = BasicBlock(0, "@main")
        bb.llvm_label = "@f_entry"
        bb.is_function_call = True
        bb.cost_all_nvm = 4
        bb.instructions = [call, BranchOperation(None, "%5", None)]
        bb_exit = BasicBlock(5, "@main")
        bb_exit.llvm_label = "@f_exit"
        bb_exit.is_function_call = True
        bb_exit.instructions.append(BranchOperation(None, "%12", None))
        bb_exit.instructions[0].label = "%5"

        cfg.add_node(bb)
        cfg.add_node(bb_exit)
        cfg.add_edge(bb, bb_exit, checkpoint=Checkpoint(bb, bb_exit))
        self.assertEqual(CheckpointTypeEnum.POTENTIAL, cfg[bb][bb_exit]["checkpoint"].type)
        bb_end_f.memory_allocation = deepcopy(bb_end_f.memory_allocation)
        bb_end_f.memory_allocation.vars["f.b"] = VariableAllocation("f.b", INT16, Value("virtual_reg", "%2", INT16))
        self.transformation.update_function_basic_blocks(cfg, function_cfg, (bb, bb_exit), True)
        self.assertEqual(CheckpointTypeEnum.VIRTUAL, cfg[bb][bb_exit]["checkpoint"].type)
        self.assertEqual(8 + 4, bb.final_cost)
        self.assertEqual(8 + 4, bb.cost_all_nvm)
        # self.assertIsNone(bb.energy_left)
        self.assertIsNotNone(bb.energy_to_leave)
        self.assertEqual(bb_start_f.energy_to_leave + 4, bb.energy_to_leave)
        self.assertEqual(46, bb_exit.cost_all_nvm)
        self.assertEqual(46, bb_exit.cost_all_nvm)
        self.assertIsNotNone(bb_exit.energy_left)
        self.assertEqual(bb_end_f.energy_left, bb_exit.energy_left)
        # self.assertIsNone(bb_exit.energy_to_leave)
        self.assertEqual(set(bb_start_f.memory_allocation.vars), set(bb.memory_allocation.vars))
        self.assertEqual(set(bb_end_f.memory_allocation.vars), set(bb_exit.memory_allocation.vars))
        self.assertNotEqual(set(bb.memory_allocation.vars), set(bb_exit.memory_allocation.vars))
