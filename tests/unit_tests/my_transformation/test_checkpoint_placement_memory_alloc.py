import unittest
from copy import deepcopy

import networkx as nx

from ScEpTIC.AST.misc.trace import FunctionTraces, Trace
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.schematic import MyTransformation, EnergyConsumptionEstimationPass
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import CheckpointTypeEnum
from tests.data.data_generation import data


class CheckpointPlacementMemoryAllocationTest(unittest.TestCase):
    """
    Test the checkpoint placement and memory allocation of the CFG from a pre-analysed CFG
    """

    def setUp(self):
        self.cfg = deepcopy(data.SIMPLE_CFG)
        self.bbs = {node.llvm_label: node for node in self.cfg.nodes}
        paths = nx.all_simple_paths(self.cfg, self.bbs["START_main"], self.bbs["END_main"])
        config = {"energy_budget": 50, "chkpt_restore": 2, "chkpt_save": 2, "call_cost": 4, "vm_size": 2000}
        # Estimate energy consumption of the basic blocks
        energy_estimation_pass = EnergyConsumptionEstimationPass(data.ENERGY_CALCULATOR, None, {})
        energy_estimation_pass.apply({"@main": self.cfg})

        transformation = MyTransformation(data.ENERGY_CALCULATOR, config)
        function_depgraph = nx.DiGraph()
        function_depgraph.add_node("@main")
        ftraces = FunctionTraces()
        ftraces.traces = {hash(tuple(trace)): Trace(0, trace, 0) for trace in paths}
        transformation.apply({"@main": self.cfg}, {"@main": ftraces}, function_depgraph)

    def test_cfg_is_unmodified(self):
        self.assertEqual(5, len(self.cfg.nodes))
        self.assertEqual(4, len(self.cfg.edges))
        # Assert there is always checkpoints between the edges
        for edge in self.cfg.edges:
            self.assertIsNotNone(self.cfg[edge[0]][edge[1]]["checkpoint"])

    def test_cost_all_nvm_unmodified(self):
        self.assertEqual(29, self.bbs[0].cost_all_nvm)
        self.assertEqual(9, self.bbs[13].cost_all_nvm)
        self.assertEqual(38, self.bbs[17].cost_all_nvm)

    def test_checkpoint_placement(self):
        checkpoints = {}
        for edge in self.cfg.edges:
            c = self.cfg[edge[0]][edge[1]]["checkpoint"]
            checkpoints[c.name] = c
        self.assertIn("C3", checkpoints)
        self.assertIn("C1", checkpoints)
        self.assertIn("C2", checkpoints)
        self.assertIn("C4", checkpoints)
        self.assertEqual(CheckpointTypeEnum.DISABLED, checkpoints["C2"].type)
        self.assertEqual(CheckpointTypeEnum.DISABLED, checkpoints["C3"].type)
        self.assertEqual(CheckpointTypeEnum.DISABLED, checkpoints["C4"].type)
        self.assertEqual(CheckpointTypeEnum.ACTIVE, checkpoints["C1"].type)

    def test_memory_allocation(self):
        first_alloc_bbs = [self.bbs["START_main"], self.bbs[0]]
        first_alloc = self.bbs["START_main"].memory_allocation
        # Assert that the only variable is sum
        self.assertEqual(1, len(first_alloc.vars))
        self.assertIn("main.sum", first_alloc.vars)
        self.assertEqual(VirtualMemoryEnum.VOLATILE, first_alloc.vars["main.sum"].allocation)

        # Make sure that the memory allocation is shared with all the bbs before C2
        for bb in first_alloc_bbs:
            self.assertEqual(first_alloc, bb.memory_allocation)

        second_alloc_bbs = [self.bbs[0], self.bbs[13], self.bbs[17], self.bbs["END_main"]]
        second_alloc = self.bbs["END_main"].memory_allocation
        self.assertEqual(4, len(second_alloc.vars))
        self.assertIn("main.sum", second_alloc.vars)
        self.assertEqual(VirtualMemoryEnum.NON_VOLATILE, second_alloc.vars["main.sum"].allocation)
        self.assertIn("@old_sum", second_alloc.vars)
        self.assertEqual(VirtualMemoryEnum.NON_VOLATILE, second_alloc.vars["@old_sum"].allocation)
        self.assertIn("@variation", second_alloc.vars)
        self.assertEqual(VirtualMemoryEnum.VOLATILE, second_alloc.vars["@variation"].allocation)
        self.assertIn("main.nb_packet", second_alloc.vars)
        self.assertEqual(VirtualMemoryEnum.VOLATILE, second_alloc.vars["main.nb_packet"].allocation)

    def test_final_cost(self):
        self.assertEqual(18, self.bbs[0].final_cost)
        self.assertEqual(7, self.bbs[13].final_cost)
        self.assertEqual(26, self.bbs[17].final_cost)
