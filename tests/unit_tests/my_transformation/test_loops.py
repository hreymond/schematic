import unittest
import copy

from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.parsers.trace_parser import *
from ScEpTIC.AST.transformations.schematic.utils.loop_utils import extract_loop_subgraph


class LoopsTest(unittest.TestCase):
    def setUp(self):
        self.cfg_simple_loop = CFG()

        bb0 = BasicBlock("%0", "@main")
        bb0.cost_all_nvm = 10
        bb0.variables = {}

        bb1 = BasicBlock("%1", "@main")
        bb1.cost_all_nvm = 10
        bb1.variables = {}

        bb2 = BasicBlock("%2", "@main")
        bb2.cost_all_nvm = 10
        bb2.variables = {}

        bb3 = BasicBlock("%3", "@main")
        bb3.cost_all_nvm = 10
        bb3.variables = {}

        bb4 = BasicBlock("%4", "@main")
        bb4.cost_all_nvm = 10
        bb4.variables = {}
        self.bbs = [bb0, bb1, bb2, bb3, bb4]
        self.cfg_simple_loop.add_nodes_from([bb0, bb1, bb2, bb3, bb4])

        self.cfg_simple_loop.add_edge(bb0, bb1)
        self.cfg_simple_loop.add_edge(bb1, bb2)
        self.cfg_simple_loop.add_edge(bb2, bb3)
        self.cfg_simple_loop.add_edge(bb3, bb1)

        self.cfg_simple_loop.add_edge(bb1, bb4)

        self.simple_loop = LLVMNaturalLoopBB([bb1], [bb1], [bb1, bb2, bb3], [bb3], 1, 100)

    def test_process_loop_trace(self):
        l_traces = {}
        l_traces["loop"] = LLVMNaturalLoop("%1", ["%1"], ["%1", "%2", "%3"], ["%3"], 1).to_dict()
        l_traces["traces"] = [Trace(1, ["%1", "%2", "%3"], 100).to_dict()]
        trace_bb = process_loop(l_traces, self.cfg_simple_loop, {bb.llvm_label: bb for bb in self.bbs})
        trace_bb.loop.max_iter = 100
        # Assert that the LLVMNaturalLoopBB parsed has been well parsed
        self.assertDictEqual(self.simple_loop.__dict__, trace_bb.loop.__dict__)
        self.assertEqual(len(trace_bb.traces.values()), 1)
        self.assertListEqual(self.simple_loop.basic_blocks, list(trace_bb.traces.values())[0].trace)

    def test_process_loop_splitted_trace(self):
        self.bbs[2].is_splitted = True
        self.cfg_simple_loop.remove_edge(self.bbs[2], self.bbs[3])
        splitted_bb = BasicBlock("splitted", "@main")
        splitted_bb.original_bb = self.bbs[2]
        self.cfg_simple_loop.add_node(splitted_bb)
        self.cfg_simple_loop.add_edge(self.bbs[2], splitted_bb)
        self.cfg_simple_loop.add_edge(splitted_bb, self.bbs[3])
        l_traces = {}
        l_traces["loop"] = LLVMNaturalLoop("%1", ["%1"], ["%1", "%2", "%3"], ["%3"], 1).to_dict()
        l_traces["traces"] = [Trace(1, ["%1", "%2", "%3"], 100).to_dict()]
        trace_bb = process_loop(l_traces, self.cfg_simple_loop, {bb.llvm_label: bb for bb in self.bbs})
        trace_bb.loop.max_iter = 100
        simple_loop_cpy = copy.copy(self.simple_loop)
        simple_loop_cpy.basic_blocks.insert(2, splitted_bb)
        # Assert that the LLVMNaturalLoopBB parsed has been well parsed
        self.assertListEqual(simple_loop_cpy.basic_blocks, trace_bb.loop.basic_blocks)
        self.assertEqual(len(trace_bb.traces.values()), 1)
        self.assertListEqual(simple_loop_cpy.basic_blocks, list(trace_bb.traces.values())[0].trace)

    def test_loop_extraction(self):
        loop_cfg = extract_loop_subgraph(self.simple_loop, self.cfg_simple_loop)
        # 3 BBs + start and end
        self.assertEqual(3 + 2, len(loop_cfg.nodes))

        # 5 BBs - 1
        self.assertEqual(5 - 1, len(loop_cfg.edges))

        # Assert start and end are well created
        self.assertNotIn(loop_cfg.first_bb, self.bbs)
        self.assertNotIn(loop_cfg.last_bb, self.bbs)

        # Assert all loop basic blocks are in the extracted cfg
        for bb in self.simple_loop.basic_blocks:
            self.assertIn(bb, list(loop_cfg.nodes))
