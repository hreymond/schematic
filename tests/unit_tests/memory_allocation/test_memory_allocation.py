import unittest
from copy import deepcopy

from ScEpTIC.AST.elements.instructions.memory_operations import StoreOperation, LoadOperation
from ScEpTIC.AST.elements.types import BaseType, Type
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.schematic import EnergyConsumptionEstimationPass
from ScEpTIC.AST.transformations.schematic.analysis.memory_allocator import MemoryAllocator
from ScEpTIC.AST.transformations.schematic.elements.memory_allocation import VariableAccesses
from tests.data.data_generation.data import ENERGY_CALCULATOR, SIMPLE_CFG

INT16 = Type.empty()
INT16.is_base_type = True
INT16.base_type = BaseType("int", 16)


class MemoryAllocationTest(unittest.TestCase):
    def setUp(self):
        self.cfg = deepcopy(SIMPLE_CFG)

        self.assertEqual(len(self.cfg.edges), 4, "Error loading simple_cfg/cfg.dump")
        self.assertEqual(len(self.cfg.nodes), 5, "Error loading simple_cfg/cfg.dump")
        self.memory_allocator = MemoryAllocator(ENERGY_CALCULATOR)
        self.energy_estimator = EnergyConsumptionEstimationPass(ENERGY_CALCULATOR, None, set())

    def test_energy_gain_estimation_without_lifetime(self):

        a = VariableAccesses("a", INT16, None, 7, StoreOperation)
        b = VariableAccesses("b", INT16, None, 2, StoreOperation)
        ret = VariableAccesses("ret", INT16, None, 1, StoreOperation)
        # Since checkpoint energy = 4 and 5 access to VM instead of NVM reduce the energy consumed by 1
        gain = self.memory_allocator.estimate_energy_gain(a)
        self.assertEqual(gain, 1)

        # Since checkpoint energy = 4 and energy saved = 2
        gain = self.memory_allocator.estimate_energy_gain(b)
        self.assertEqual(gain, -4)

        # Since checkpoint energy = 4 and energy saved = 1
        gain = self.memory_allocator.estimate_energy_gain(ret)
        self.assertEqual(gain, -5)

    def test_memory_alloc_simple(self):
        a = VariableAccesses("a", INT16, None, 7, LoadOperation)
        b = VariableAccesses("b", INT16, None, 2, LoadOperation)
        c = VariableAccesses("c", INT16, None, 1, LoadOperation)
        alloc_expected = {
            "a": VirtualMemoryEnum.VOLATILE,
            "b": VirtualMemoryEnum.NON_VOLATILE,
            "c": VirtualMemoryEnum.NON_VOLATILE,
        }
        alloc, gain = self.memory_allocator.choose_memory_allocation({"a": a, "b": b, "c": c}, None, None, [])
        self.assertEqual(gain, 1)
        self.assertDictEqual(alloc.to_dict(), alloc_expected)

    def test_memory_alloc_size_limit(self):
        a = VariableAccesses("a", INT16, None, 7, LoadOperation)
        b = VariableAccesses("b", INT16, None, 2, LoadOperation)
        c = VariableAccesses("c", INT16, None, 1, LoadOperation)
        alloc_expected = {
            "a": VirtualMemoryEnum.NON_VOLATILE,
            "b": VirtualMemoryEnum.NON_VOLATILE,
            "c": VirtualMemoryEnum.NON_VOLATILE,
        }
        # Test with no VM
        self.memory_allocator.vm_size = 0
        alloc, gain = self.memory_allocator.choose_memory_allocation({"a": a, "b": b, "c": c}, None, None, [])
        self.assertEqual(gain, 0)
        self.assertDictEqual(alloc.to_dict(), alloc_expected)

        # Test with little VM size
        self.memory_allocator.vm_size = 1
        alloc, gain = self.memory_allocator.choose_memory_allocation({"a": a, "b": b, "c": c}, None, None, [])
        self.assertEqual(gain, 0)
        self.assertDictEqual(alloc.to_dict(), alloc_expected)

        # Test vm_size = variable size
        self.memory_allocator.vm_size = 2
        alloc, gain = self.memory_allocator.choose_memory_allocation({"a": a, "b": b, "c": c}, None, None, [])
        alloc_expected["a"] = VirtualMemoryEnum.VOLATILE
        self.assertEqual(gain, 1)
        self.assertDictEqual(alloc.to_dict(), alloc_expected)
        self.memory_allocator.vm_size = 64

    def test_simple_simple_cfg(self):
        cfgs = {"@main": self.cfg}

        # Create traces: t0=basic blocs %0, %8, %14 and t1=%0,%12,%14
        bbs = {node.llvm_label: node for node in cfgs["@main"].nodes}
        trace = [bbs["START_main"], bbs[0], bbs[13], bbs[17], bbs["END_main"]]

        # Compute the cost of the trace with all variables in NVM
        cost_t0 = sum([bb.cost_all_nvm for bb in trace])

        # Test the choice of a memory allocation on path %0, %13, %17
        # See tests/data/data_generation/simple_cfg for more info (source.ll, main.c)
        self.memory_allocator.vm_size = 10
        mem_alloc, cost = self.memory_allocator.compute_cost(trace)
        alloc_expected_trace_0 = {
            # Accessed 12 times, so gain is 12 - 3*1 = 9
            "main.sum": VirtualMemoryEnum.VOLATILE,
            "@old_sum": VirtualMemoryEnum.NON_VOLATILE,
            # Accessed 7 times, so gain is 7 - 3*1 = 4
            "main.nb_packet": VirtualMemoryEnum.VOLATILE,
            # Accessed 7 times, so gain is 7 - 3*1 = 4
            "@variation": VirtualMemoryEnum.VOLATILE,
        }
        self.assertEqual(cost_t0 - 9 - 4 * 2, cost)
        self.assertDictEqual(mem_alloc.to_dict(), alloc_expected_trace_0)

        # Test the path if there is no memory available
        self.memory_allocator.reset()
        self.memory_allocator.vm_size = 0
        alloc_expected_all_nvm = {
            # Accessed 12 times, no gain
            "main.sum": VirtualMemoryEnum.NON_VOLATILE,
            "@old_sum": VirtualMemoryEnum.NON_VOLATILE,
            # Accessed 7 times
            "main.nb_packet": VirtualMemoryEnum.NON_VOLATILE,
            "@variation": VirtualMemoryEnum.NON_VOLATILE,
        }
        mem_alloc, cost = self.memory_allocator.compute_cost(trace)
        self.assertEqual(cost, cost_t0)
        self.assertDictEqual(mem_alloc.to_dict(), alloc_expected_all_nvm)

        # Test if there is only 2 bytes are available in the memory
        self.memory_allocator.reset()
        self.memory_allocator.vm_size = 2
        alloc_expected_sum_vm = {
            # Accessed 12 times, so gain is 12 - 3*1 = 9
            "main.sum": VirtualMemoryEnum.VOLATILE,
            "@old_sum": VirtualMemoryEnum.NON_VOLATILE,
            # Accessed 7 times, so gain is 7 - 3*1 = 4
            "main.nb_packet": VirtualMemoryEnum.NON_VOLATILE,
            "@variation": VirtualMemoryEnum.NON_VOLATILE,
        }
        mem_alloc, cost = self.memory_allocator.compute_cost(trace)
        self.assertEqual(cost, cost_t0 - 9)
        self.assertDictEqual(mem_alloc.to_dict(), alloc_expected_sum_vm)
        self.memory_allocator.vm_size = 64
