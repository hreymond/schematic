import unittest
from copy import deepcopy

import networkx as nx
from networkx import has_path

from ScEpTIC.AST.transformations.schematic import (
    VariableAccessCountPass,
    EnergyConsumptionEstimationPass,
    MyTransformation,
)

from tests.data.data_generation.data import CONDITIONAL_CFG, CONDITIONAL_CFG_MEMORY_TAGS, ENERGY_CALCULATOR


class GraphConstructionTest(unittest.TestCase):
    def setUp(self):
        self.schematic = MyTransformation(ENERGY_CALCULATOR)
        self.schematic.energy_budget = 5000
        self.schematic.function_analyzed = "@main"
        self.cfg = deepcopy(CONDITIONAL_CFG)
        self.cfgs = {"@main": self.cfg}

        EnergyConsumptionEstimationPass(ENERGY_CALCULATOR, None, set()).apply(self.cfgs)
        VariableAccessCountPass.apply(self.cfgs, CONDITIONAL_CFG_MEMORY_TAGS)
        self.bbs = {bb.llvm_label: bb for bb in self.cfgs["@main"].nodes}

    def test_trace_malformed_trace(self):
        trace = [self.bbs["START_main"], self.bbs[0]]
        checkpoints = self.schematic.get_checkpoints_from_trace(self.cfg, trace)
        self.assertRaises(
            Exception, self.schematic.create_reachable_checkpoint_graph, (self.cfg, trace, checkpoints)
        )

    def test_trace_one_bb(self):
        trace = [self.bbs["START_main"], self.bbs[0], self.bbs[8]]
        checkpoints = self.schematic.get_checkpoints_from_trace(self.cfg, trace)
        rcgraph = self.schematic.create_reachable_checkpoint_graph(self.cfg, trace, checkpoints)
        self.assertIsInstance(rcgraph, nx.DiGraph)
        # Nodes = [Start, C3, End]
        nodes = list(rcgraph.nodes)
        self.assertEqual(len(nodes), 4)
        self.assertEqual(nodes[0].name, "Start")
        self.assertEqual(nodes[3].name, "End")
        self.assertEqual(len(rcgraph.edges), 6)

        # Since the basic bloc start has a cost of 0, the only cost to pay is saving the register + shutdown
        weight_start_C3 = rcgraph[nodes[0]][nodes[1]]["weight"]
        self.assertEqual(weight_start_C3, self.schematic.chkpt_save)

        # Since the basic bloc 0 has a cost of 18. We pay 18 + 2 for boot + register restoration
        weight_start_C1 = rcgraph[nodes[0]][nodes[2]]["weight"]
        self.assertEqual(weight_start_C1, 18 + self.schematic.chkpt_restore)

        # Since the basic bloc 0 has a cost of 18. We pay 18 + 2 for boot + register restoration/save
        weight_C3_C1 = rcgraph[nodes[1]][nodes[2]]["weight"]
        self.assertEqual(weight_C3_C1, 18 + self.schematic.chkpt_restore + self.schematic.chkpt_save)

        # Since the basic bloc 0 has a cost of 18. We pay 18 + 2 for boot + register restoration
        weight_C3_end = rcgraph[nodes[1]][nodes[3]]["weight"]
        self.assertEqual(weight_C3_end, 18 + self.schematic.chkpt_restore)

        # From C1 to end, we only do boot + register restoration
        weight_C1_end = rcgraph[nodes[2]][nodes[3]]["weight"]
        self.assertEqual(weight_C1_end, self.schematic.chkpt_restore)

        # From Start to end we do not need to pay the restauration/save cost, we will only pay 18
        weight_start_end = rcgraph[nodes[0]][nodes[3]]["weight"]
        self.assertEqual(weight_start_end, 18)

    def test_trace_no_limit(self):
        trace = [self.bbs["START_main"], self.bbs[0], self.bbs[8], self.bbs[14], self.bbs["END_main"]]
        checkpoints = self.schematic.get_checkpoints_from_trace(self.cfg, trace)
        rcgraph = self.schematic.create_reachable_checkpoint_graph(self.cfg, trace, checkpoints)
        self.assertIsInstance(rcgraph, nx.DiGraph)

        # Nodes = [Start, C3, C1, C4, C6, End]
        nodes = {chkpt.name: chkpt for chkpt in rcgraph.nodes}
        self.assertEqual(len(nodes), 6)

        # With unlimited energy, the number of arcs is the binomial coefficient
        # with k=2, n=6 since we are taking pairs of nodes out of 6 nodes
        self.assertEqual(len(rcgraph.edges), 15)

        # Arc C3/C1
        weight_C3_C1 = rcgraph[nodes["C3"]][nodes["C1"]]["weight"]
        self.assertEqual(weight_C3_C1, 18 + self.schematic.chkpt_save + self.schematic.chkpt_restore)

    def test_trace_no_energy(self):
        trace = [self.bbs["START_main"], self.bbs[0], self.bbs[8], self.bbs[14], self.bbs["END_main"]]
        self.schematic.energy_budget = 0
        checkpoints = self.schematic.get_checkpoints_from_trace(self.cfg, trace)
        rcgraph = self.schematic.create_reachable_checkpoint_graph(self.cfg, trace, checkpoints)
        self.assertIsInstance(rcgraph, nx.DiGraph)

        # Nodes = [Start, C3, C1, C4, C6, End]
        nodes = {chkpt.name: chkpt for chkpt in rcgraph.nodes}
        self.assertEqual(6, len(nodes))

        # No energy is available, so no edges should be present
        self.assertEqual(len(rcgraph.edges), 0)

    def test_trace_50_energy(self):
        trace = [self.bbs["START_main"], self.bbs[0], self.bbs[8], self.bbs[14], self.bbs["END_main"]]
        self.schematic.energy_budget = 30
        checkpoints = self.schematic.get_checkpoints_from_trace(self.cfg, trace)
        rcgraph = self.schematic.create_reachable_checkpoint_graph(self.cfg, trace, checkpoints)
        self.assertIsInstance(rcgraph, nx.DiGraph)

        # Nodes = [Start, C3, C1, C4, C6, End]
        nodes = {chkpt.name: chkpt for chkpt in rcgraph.nodes}
        self.assertEqual(6, len(nodes))

        # There should be a path from start to end
        self.assertTrue(has_path(rcgraph, nodes["Start"], nodes["End"]))

        # But start and end should not be neighbours
        self.assertNotIn(nodes["End"], rcgraph[nodes["Start"]])
        self.assertNotIn(nodes["Start"], rcgraph[nodes["End"]])

        # Start should be neighbour with C3 and C1 only
        self.assertIn(nodes["C3"], rcgraph[nodes["Start"]])
        self.assertIn(nodes["C1"], rcgraph[nodes["Start"]])
        self.assertNotIn(nodes["C4"], rcgraph[nodes["Start"]])
        self.assertNotIn(nodes["C6"], rcgraph[nodes["Start"]])

        # C4 should be reachable from C1
        self.assertIn(nodes["C4"], rcgraph[nodes["C1"]])
        weight_C1_C4 = rcgraph[nodes["C1"]][nodes["C4"]]["weight"]
        self.assertEqual(10 + self.schematic.chkpt_save + self.schematic.chkpt_restore, weight_C1_C4)
