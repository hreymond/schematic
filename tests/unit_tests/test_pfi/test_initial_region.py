import unittest

from ScEpTIC.AST.misc.trace import LLVMNaturalLoopBB
from ScEpTIC.AST.transformations.PFI.initial_region_formation import form_initial_regions
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import CheckpointTypeEnum


class TestInitialRegion(unittest.TestCase):
    def test_function_call(self):
        bb0 = BasicBlock("bb0", "@main")
        bb1 = BasicBlock("bb1", "@main")
        bb2 = BasicBlock("bb2", "@main")
        bb3 = BasicBlock("bb3", "@main")

        bb2.is_function_call = True
        cfg = CFG()
        cfg.add_nodes_from([bb0, bb1, bb2, bb3])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)
        cfg.add_edge(bb2, bb3)

        form_initial_regions({"@main": cfg})

        self.assertTrue(hasattr(bb0, "is_region_boundary"))
        self.assertTrue(hasattr(bb1, "is_region_boundary"))
        self.assertTrue(hasattr(bb2, "is_region_boundary"))
        self.assertTrue(hasattr(bb3, "is_region_boundary"))

        self.assertTrue(bb2.is_region_boundary)
        self.assertFalse(bb0.is_region_boundary)
        self.assertFalse(bb1.is_region_boundary)
        self.assertFalse(bb3.is_region_boundary)

    def test_simple_loop(self):
        """
        Test on a simple loop:

        +-----+
        | bb0 |
        +-----+
          |
          |
          v
        +-----+
        | bb1 | <+
        +-----+  |
          |      |
          |      |
          v      |
        +-----+  |
        | bb2 |  |
        +-----+  |
          |      |
          |      |
          v      |
        +-----+  |
        | bb3 | -+
        +-----+

        """
        bb0 = BasicBlock("bb0", "@main")
        bb1 = BasicBlock("bb1", "@main")
        bb2 = BasicBlock("bb2", "@main")
        bb3 = BasicBlock("bb3", "@main")
        cfg = CFG()
        cfg.first_bb = bb0

        cfg.add_nodes_from([bb0, bb1, bb2, bb3])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)
        cfg.add_edge(bb2, bb3)
        cfg.add_edge(bb3, bb1)
        loop = LLVMNaturalLoopBB([bb1], [], [bb1, bb2, bb3], [bb3], 0, None)
        for bb in {bb1, bb2, bb3}:
            bb.loop = loop

        form_initial_regions({"@main": cfg})
        self.assertTrue(hasattr(bb0, "is_region_boundary"))
        self.assertTrue(hasattr(bb1, "is_region_boundary"))
        self.assertTrue(hasattr(bb2, "is_region_boundary"))
        self.assertTrue(hasattr(bb3, "is_region_boundary"))

        self.assertTrue(bb1.is_region_boundary)
        self.assertFalse(bb2.is_region_boundary)
        self.assertFalse(bb0.is_region_boundary)
        self.assertFalse(bb3.is_region_boundary)

    def test_nested_loop(self):
        bb0 = BasicBlock("bb0", "@main")
        bb1 = BasicBlock("bb1", "@main")
        bb2 = BasicBlock("bb2", "@main")
        bb3 = BasicBlock("bb3", "@main")
        cfg = CFG()
        cfg.first_bb = bb0

        cfg.add_nodes_from([bb0, bb1, bb2, bb3])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)
        cfg.add_edge(bb2, bb3)
        cfg.add_edge(bb3, bb1)
        cfg.add_edge(bb2, bb2)
        loop1 = LLVMNaturalLoopBB([bb1], [], [bb1, bb2, bb3], [bb3], 0, None)
        loop2 = LLVMNaturalLoopBB([bb2], [], [bb2], [bb2], 1, None)
        bb1.loop = loop1
        bb2.loop = loop2
        bb3.loop = loop1

        form_initial_regions({"@main": cfg})

        self.assertTrue(hasattr(bb0, "is_region_boundary"))
        self.assertTrue(hasattr(bb1, "is_region_boundary"))
        self.assertTrue(hasattr(bb2, "is_region_boundary"))
        self.assertTrue(hasattr(bb3, "is_region_boundary"))

        self.assertTrue(bb1.is_region_boundary)
        self.assertTrue(bb2.is_region_boundary)
        self.assertFalse(bb0.is_region_boundary)
        self.assertFalse(bb3.is_region_boundary)


if __name__ == "__main__":
    unittest.main()
