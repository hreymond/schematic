import unittest

from ScEpTIC.AST.elements.instructions.binary_operation import BinaryOperation
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import BranchOperation
from ScEpTIC.AST.transformations.PFI.initial_region_formation import isolate_function_calls
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG


class TestFunctionIsolation(unittest.TestCase):
    def test_no_function(self):
        bb0 = BasicBlock("bb0", "@main")
        bb1 = BasicBlock("bb1", "@main")
        bb2 = BasicBlock("bb2", "@main")
        cfg = CFG()
        cfg.add_nodes_from([bb0, bb1, bb2])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)

        isolate_function_calls({"@main": cfg})

        self.assertEqual(3, len(cfg.nodes()))
        self.assertEqual(2, len(cfg.edges()))
        self.assertIn(bb0, cfg.nodes())
        self.assertIn(bb1, cfg.nodes())
        self.assertIn(bb2, cfg.nodes())

    def test_call_and_branch_only(self):
        bb0 = BasicBlock("bb0", "@main")
        bb1 = BasicBlock("bb1", "@main")
        bb2 = BasicBlock("bb2", "@main")

        bb1.instructions.append(CallOperation(None, "hello", None, None, None, None))
        bb1.instructions.append(BranchOperation("a<2", bb2.label, None))
        cfg = CFG()
        cfg.add_nodes_from([bb0, bb1, bb2])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)

        isolate_function_calls({"@main": cfg})

        # Make sure new basic blocks were created
        self.assertEqual(5, len(cfg.nodes()))
        self.assertEqual(4, len(cfg.edges()))
        # Make sure that old basic blocks are still there
        self.assertIn(bb0, cfg.nodes())
        self.assertIn(bb1, cfg.nodes())
        self.assertIn(bb2, cfg.nodes())
        # Check a branching instruction was added to bb1 and that this operation is free
        self.assertEqual(1, len(bb1.instructions))
        self.assertEqual(BranchOperation, type(bb1.instructions[0]))
        self.assertEqual(0, bb1.instructions[0].tick_count)
        # Check that the successor of bb1 contains the call instruction
        self.assertEqual(1, len(list(cfg.successors(bb1))))
        self.assertEqual(2, len(list(cfg.successors(bb1))[0].instructions))
        call_inst = list(cfg.successors(bb1))[0].instructions[0]
        self.assertEqual(CallOperation, type(call_inst))
        self.assertEqual("hello", call_inst.name)
        # Check that the predecessors of bb2 contains the old branch instruction of bb1
        self.assertEqual(1, len(list(cfg.predecessors(bb2))))
        self.assertEqual(1, len(list(cfg.predecessors(bb2))[0].instructions))
        branch_inst = list(cfg.predecessors(bb2))[0].instructions[0]
        self.assertEqual(BranchOperation, type(branch_inst))
        self.assertEqual("a<2", branch_inst.condition)
        self.assertEqual(bb2.label, branch_inst.target_true)

    def test_multiple_instrs(self):
        bb0 = BasicBlock("bb0", "@main")
        bb1 = BasicBlock("bb1", "@main")
        bb2 = BasicBlock("bb2", "@main")
        bb3 = BasicBlock("bb3", "@main")

        bb1.instructions.append(BinaryOperation("add", None, None, None, None, {}))
        bb1.instructions.append(CallOperation(None, "hello", None, None, None, None))
        bb1.instructions.append(BranchOperation("a<2", bb2.label, None))

        cfg = CFG()
        cfg.add_nodes_from([bb0, bb1, bb2, bb3])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)
        cfg.add_edge(bb1, bb3)

        isolate_function_calls({"@main": cfg})

        # Make sure a new basic block has been created
        self.assertEqual(6, len(cfg.nodes()))
        self.assertEqual(5, len(cfg.edges()))
        # Make sure that old basic blocks are still there
        self.assertIn(bb0, cfg.nodes())
        self.assertIn(bb1, cfg.nodes())
        self.assertIn(bb2, cfg.nodes())
        self.assertIn(bb3, cfg.nodes())
        # Get the newly created basic blocks
        self.assertEqual(1, len(list(cfg.successors(bb1))))
        call_bb = list(cfg.successors(bb1))[0]

        self.assertEqual(1, len(list(cfg.predecessors(bb2))))
        new_bb = list(cfg.predecessors(bb2))[0]
        self.assertNotEqual(call_bb, new_bb)

        # Check that instructions were split in bb1, and that a branching instruction was added
        self.assertEqual(2, len(bb1.instructions))  # 2 instructions in bb1: BinaryOperation and BranchOperation
        self.assertEqual(BinaryOperation, type(bb1.instructions[0]))
        self.assertEqual(BranchOperation, type(bb1.instructions[1]))
        self.assertEqual(0, bb1.instructions[1].tick_count)  # BranchOperation must be free

        # Check that the successor of bb1 contains the call instruction
        self.assertEqual(2, len(call_bb.instructions))
        call = call_bb.instructions[0]
        self.assertEqual(CallOperation, type(call))
        self.assertEqual("hello", call.name)

        # Check that the branch following the function call is correct
        self.assertEqual(BranchOperation, type(call_bb.instructions[1]))
        self.assertEqual(0, call_bb.instructions[1].tick_count)  # BranchOperation must be free
        self.assertEqual(None, call_bb.instructions[1].condition)
        self.assertEqual("%" + str(new_bb.label), call_bb.instructions[1].target_true)

        # Check that the predecessors of bb2 contains the old branch instruction of bb1
        self.assertEqual(1, len(list(cfg.predecessors(bb2))))
        branch = list(cfg.predecessors(bb2))[0].instructions[0]
        self.assertEqual(BranchOperation, type(branch))
        self.assertEqual("a<2", branch.condition)
        self.assertEqual(bb2.label, branch.target_true)
