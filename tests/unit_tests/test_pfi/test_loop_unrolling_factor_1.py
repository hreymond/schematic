import unittest
from copy import copy

import networkx as nx

from ScEpTIC.AST.elements.instructions.binary_operation import BinaryOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import BranchOperation
from ScEpTIC.AST.elements.value import Value
from ScEpTIC.AST.misc.trace import LLVMNaturalLoopBB
from ScEpTIC.AST.transformations.PFI.loop_unrolling import unroll_loop
from ScEpTIC.AST.transformations.schematic.code_transformation import INT1_TYPE
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.utils.cfg_utils import save_cfg


class TestLoopUnrollingUnrollFactor1(unittest.TestCase):
    def test_simple_loop(self):
        """
        Test on a simple loop:

         Input:
         bb0 -> bb1 -> bb2 -> bb3
                 ^--------------/

         Expected output:
         bb0 --> bb1 --> bb2 --> bb3 --> bb1' --> bb2' --> bb3'
                 ^-----------------------------------------/
        """
        bb0 = BasicBlock(0, "@main")
        bb1 = BasicBlock(1, "@main")
        bb2 = BasicBlock(2, "@main")
        bb3 = BasicBlock(3, "@main")

        bb0.llvm_label = "bb0"
        bb1.llvm_label = "bb1"
        bb2.llvm_label = "bb2"
        bb3.llvm_label = "bb3"

        bb0.add_instruction(BranchOperation(None, bb1.label_str, None))
        bb1.add_instruction(BranchOperation(None, bb2.label_str, None))
        bb2.add_instruction(BranchOperation(None, bb3.label_str, None))
        bb3.add_instruction(BranchOperation(None, bb1.label_str, None))
        cfg = CFG()
        cfg.first_bb = bb0

        cfg.add_nodes_from([bb0, bb1, bb2, bb3])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)
        cfg.add_edge(bb2, bb3)
        cfg.add_edge(bb3, bb1)
        loop = LLVMNaturalLoopBB([bb1], [], [bb1, bb2, bb3], [bb3], 0)
        # Save old loop for the tests
        old_loop = copy(loop)

        save_cfg("test_simple_loop_before", cfg)
        unroll_loop(loop, cfg, 1)
        save_cfg("test_simple_loop", cfg)

        self.assertEqual(len(cfg.nodes), 7)
        self.assertEqual(len(cfg.edges), 7)
        self.assertEqual(len(loop.basic_blocks), 6)
        self.assertEqual(len(loop.exiting), 0)
        self.assertEqual(len(loop.header), 1)
        self.assertEqual(len(loop.latch), 1)

        # Check the back-edge of the loop as been removed
        self.assertNotIn((bb3, bb1), cfg.edges)

        expected_labels = ["bb0", "bb1", "bb2", "bb3", "bb1_cloned1", "bb2_cloned1", "bb3_cloned1"]
        labels = []
        i = 0
        # Traverse the CFG to check that it is as expected
        for edge in nx.dfs_edges(cfg, source=bb0):
            source = edge[0]
            destination = edge[1]
            labels.append(source.llvm_label)

            # Assert that the last instruction of the basic block is a branch to the next bb
            self.assertEqual(BranchOperation, type(source.instructions[-1]))
            self.assertEqual(None, source.instructions[-1].condition)
            self.assertEqual(destination.label_str, source.instructions[-1].target_true)
            self.assertEqual(None, source.instructions[-1].target_false)
            i += 1
        labels.append(destination.llvm_label)
        self.assertEqual(expected_labels, labels)
    def test_loop_2bb(self):
        """
        Test on a simple loop:

         Input:
         bb0 -> bb1 -> bb2
                 ^--------/

         Expected output:
         bb0 --> bb1 --> bb2 --> bb1' --> bb2'
                 ^--------------------------/
        """
        bb0 = BasicBlock(0, "@main")
        bb1 = BasicBlock(1, "@main")
        bb2 = BasicBlock(2, "@main")

        bb0.add_instruction(BranchOperation(None, bb1.label_str, None))
        bb1.add_instruction(BranchOperation(None, bb2.label_str, None))
        bb2.add_instruction(BranchOperation(None, bb1.label_str, None))

        bb0.llvm_label = "bb0"
        bb1.llvm_label = "bb1"
        bb2.llvm_label = "bb2"

        cfg = CFG()
        cfg.first_bb = bb0

        cfg.add_nodes_from([bb0, bb1, bb2])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)
        cfg.add_edge(bb2, bb1)

        loop = LLVMNaturalLoopBB([bb1], [], [bb1, bb2], [bb2], 0, None)

        # Save old loop for the tests
        old_loop = copy(loop)

        unroll_loop(loop, cfg, 1)
        save_cfg("test_loop_2bb", cfg)

        # Check that the loop body has been cloned
        self.assertEqual(len(cfg.nodes), 5)
        self.assertEqual(len(cfg.edges), 5)

        # Check that the loop structure has been updated
        self.assertEqual(len(loop.basic_blocks), 4)
        self.assertEqual(len(loop.exiting), 0)
        self.assertEqual(len(loop.header), 1)
        self.assertEqual(len(loop.latch), 1)
        # Check that the loop header has not been modified
        self.assertEqual(loop.header[0], old_loop.header[0])
        # Check that the loop latch has been updated to the cloned latch
        self.assertNotEqual(loop.latch[0], old_loop.latch[0])

        # Check the back-edge of the loop as been removed
        self.assertNotIn((bb2, bb1), cfg.edges)

        expected_labels = ["bb0", "bb1", "bb2", "bb1_cloned1", "bb2_cloned1"]
        labels = []
        i = 0
        # Traverse the CFG to check that it is as expected
        for edge in nx.dfs_edges(cfg, source=bb0):
            source = edge[0]
            destination = edge[1]

            labels.append(source.llvm_label)
            # Assert that the last instruction of the basic block is a branch to the next bb
            self.assertEqual(BranchOperation, type(source.instructions[-1]))
            self.assertEqual(None, source.instructions[-1].condition)
            self.assertEqual(destination.label_str, source.instructions[-1].target_true)
            self.assertEqual(None, source.instructions[-1].target_false)
            i += 1
        labels.append(destination.llvm_label)
        self.assertEqual(expected_labels, labels)
    def test_simple_loop_with_exit(self):
        """
        Test on a simple loop with an exit on the header

         Input:
                 /----------------------> bb4
         bb0 -> bb1 -> bb2 -> bb3
                 ^--------------/

         Expected output:
                 /----------------------> bb4
         bb0 --> bb1 --> bb2 --> bb3 --> bb1' --> bb2' --> bb3'
                 ^-----------------------------------------/
        """
        bb0 = BasicBlock(0, "@main")
        bb1 = BasicBlock(1, "@main")
        bb2 = BasicBlock(2, "@main")
        bb3 = BasicBlock(3, "@main")
        bb4 = BasicBlock(4, "@main")

        bb0.add_instruction(BranchOperation(None, bb1.label_str, None))
        cond_reg = Value("virtual_reg", "%cond", INT1_TYPE())
        bb1.add_instruction(BranchOperation(cond_reg, bb2.label_str, bb4.label_str))
        bb2.add_instruction(BranchOperation(None, bb3.label_str, None))
        bb3.add_instruction(BranchOperation(None, bb1.label_str, None))
        bb4.add_instruction(BinaryOperation("and", cond_reg, cond_reg, cond_reg, True, {}))

        bb0.instructions[0].label = "%0"
        bb1.instructions[0].label = "%1"
        bb2.instructions[0].label = "%2"
        bb3.instructions[0].label = "%3"
        bb4.instructions[0].label = "%4"

        cfg = CFG()
        cfg.first_bb = bb0
        cfg.add_nodes_from([bb0, bb1, bb2, bb3, bb4])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)
        cfg.add_edge(bb2, bb3)
        cfg.add_edge(bb3, bb1)
        cfg.add_edge(bb1, bb4)

        loop = LLVMNaturalLoopBB([bb1], [bb1], [bb1, bb2, bb3], [bb3], 0, None)
        # Save old loop for the tests
        old_loop = copy(loop)

        unroll_loop(loop, cfg, 1)
        save_cfg("test_simple_loop_with_exit", cfg)

        self.assertEqual(len(cfg.nodes), 8)
        self.assertEqual(len(cfg.edges), 8)
        self.assertEqual(len(loop.basic_blocks), 6)
        self.assertEqual(len(loop.exiting), 1)
        self.assertEqual(len(loop.header), 1)
        self.assertEqual(len(loop.latch), 1)

        cfg.check_edges_label_coherency()
        # Assert that the bb1 clone is not linked
    def test_loop_exit_not_on_header(self):
        """
        Test on a simple loop with an exit not on the header

         Input:
                        /---> bb3
         bb0 -> bb1 -> bb2
                 ^------/

         Expected output: Raise NotImplementedError
        """
        bb0 = BasicBlock(0, "@main")
        bb1 = BasicBlock(1, "@main")
        bb2 = BasicBlock(2, "@main")
        bb3 = BasicBlock(3, "@main")

        bb0.add_instruction(BranchOperation(None, "%" + str(bb1.label), None))
        bb1.add_instruction(BranchOperation(None, "%" + str(bb2.label), None))
        varcond = Value("virtual_reg", "%cond", INT1_TYPE())
        bb2.add_instruction(BranchOperation(varcond, "%" + str(bb3.label), "%" + str(bb1.label)))

        cfg = CFG()
        cfg.first_bb = bb0
        cfg.add_nodes_from([bb0, bb1, bb2, bb3])
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)
        cfg.add_edge(bb2, bb3)
        cfg.add_edge(bb2, bb1)

        loop = LLVMNaturalLoopBB([bb1], [bb2], [bb1, bb2], [bb2], 0, None)

        with self.assertRaises(NotImplementedError):
            unroll_loop(loop, cfg, 1)
