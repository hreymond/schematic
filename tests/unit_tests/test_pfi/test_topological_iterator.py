import unittest

from ScEpTIC.AST.misc.trace import LLVMNaturalLoopBB
from ScEpTIC.AST.transformations.PFI.pfi import TopologicalIterator
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG


class MyTestCase(unittest.TestCase):
    def test_one_path(self):
        """
        Test the topological iterator on a simple CFG with one path
        """
        bb0 = BasicBlock("bb0", "@main")
        bb1 = BasicBlock("bb1", "@main")
        bb2 = BasicBlock("bb2", "@main")

        cfg = CFG()
        cfg.add_node(bb0)
        cfg.add_node(bb1)
        cfg.add_node(bb2)
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)

        topoIterator = TopologicalIterator(cfg)
        self.assertEqual(bb0, next(topoIterator))
        self.assertEqual(bb1, next(topoIterator))
        self.assertEqual(bb2, next(topoIterator))
        with self.assertRaises(StopIteration):
            next(topoIterator)

    def test_two_path(self):
        """
        Test the topological iterator on a simple CFG with two paths (a diamond)
        """
        bb0 = BasicBlock("bb0", "@main")
        bb1 = BasicBlock("bb1", "@main")
        bb2 = BasicBlock("bb2", "@main")
        bb3 = BasicBlock("bb3", "@main")

        cfg = CFG()
        cfg.add_node(bb0)
        cfg.add_node(bb1)
        cfg.add_node(bb2)
        cfg.add_node(bb3)
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb0, bb2)
        cfg.add_edge(bb1, bb3)
        cfg.add_edge(bb2, bb3)

        topoIterator = TopologicalIterator(cfg)
        self.assertEqual(bb0, next(topoIterator))
        self.assertIn(next(topoIterator), [bb1, bb2])
        self.assertIn(next(topoIterator), [bb1, bb2])
        self.assertEqual(bb3, next(topoIterator))
        with self.assertRaises(StopIteration):
            next(topoIterator)

    def test_simple_loop(self):
        """
        Test the topological iterator on a simple CFG with a loop

        bb0 -> bb1 -> bb2 -> bb3
                ^--------------|
        """
        bb0 = BasicBlock("bb0", "@main")
        bb1 = BasicBlock("bb1", "@main")
        bb2 = BasicBlock("bb2", "@main")
        bb3 = BasicBlock("bb3", "@main")

        cfg = CFG()
        cfg.add_node(bb0)
        cfg.add_node(bb1)
        cfg.add_node(bb2)
        cfg.add_node(bb3)
        cfg.add_edge(bb0, bb1)
        cfg.add_edge(bb1, bb2)
        cfg.add_edge(bb2, bb3)
        cfg.add_edge(bb3, bb1)

        loop = LLVMNaturalLoopBB(
            header=[bb1], basic_blocks=[bb1, bb2, bb3], exiting=[], latch=[bb3], depth=0, max_iter=-1
        )
        for bb in loop.basic_blocks:
            bb.loop = loop

        topoIterator = TopologicalIterator(cfg)
        self.assertEqual(bb0, next(topoIterator))
        self.assertEqual(bb1, next(topoIterator))
        self.assertEqual(bb2, next(topoIterator))
        self.assertEqual(bb3, next(topoIterator))
        with self.assertRaises(StopIteration):
            next(topoIterator)


if __name__ == "__main__":
    unittest.main()
