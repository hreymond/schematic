import unittest
from copy import deepcopy

from ScEpTIC.AST.elements.types import BaseType
from ScEpTIC.AST.transformations.schematic.analysis.variable_access_count import VariableAccessCountPass
from tests.data.data_generation.data import CONDITIONAL_CFG, CONDITIONAL_CFG_MEMORY_TAGS


class VariableAccessCountTest(unittest.TestCase):
    def setUp(self):
        self.conditional_cfg = deepcopy(CONDITIONAL_CFG)
        self.cfg_memory_tags = deepcopy(CONDITIONAL_CFG_MEMORY_TAGS)
        self.assertEqual(len(self.conditional_cfg.edges), 6, "Error loading conditional_cfg/cfg.dump")
        self.assertEqual(len(self.conditional_cfg.nodes), 6, "Error loading conditional_cfg/cfg.dump")

    def test_first_bb_accesses(self):
        nodes = {node.llvm_label: node for node in self.conditional_cfg.nodes}
        bb = deepcopy(nodes[0])
        self.assertIsNone(bb.variables)
        VariableAccessCountPass.update_variable_accesses_bb(bb, self.cfg_memory_tags["@main"], "@main")
        self.assertIsNotNone(bb.variables)
        self.assertEqual(bb.variables["main.first_alloca"].nb_access, 1)  # Store 0 -> retval
        self.assertEqual(bb.variables["main.a"].nb_access, 2)  # Load 0 -> a, Load a -> RX
        self.assertEqual(bb.variables["main.b"].nb_access, 2)  # Load 1 ->b, Load b -> RY
        self.assertEqual(bb.variables["main.res"].nb_access, 1)  # Load 0 -> res

    def test_first_bb_var_type_element(self):
        nodes = {node.llvm_label: node for node in self.conditional_cfg.nodes}
        bb = deepcopy(nodes[0])
        self.assertIsNone(bb.variables)
        VariableAccessCountPass.update_variable_accesses_bb(bb, self.cfg_memory_tags["@main"], "@main")
        self.assertIsNotNone(bb.variables)
        self.assertEqual(bb.variables["main.first_alloca"].type.base_type, BaseType("int", 32))  # Store 0 -> retval
        self.assertEqual(bb.variables["main.a"].type.base_type, BaseType("int", 32))  # Load 0 -> a, Load a -> RX
        self.assertEqual(bb.variables["main.b"].type.base_type, BaseType("int", 32))  # Load 1 ->b, Load b -> RY
        self.assertEqual(bb.variables["main.res"].type.base_type, BaseType("int", 32))  # Load 0 -> res
        self.assertEqual("virtual_reg", bb.variables["main.first_alloca"].element.value_class)

    def test_cfg(self):
        cfgs = {"@main": deepcopy(self.conditional_cfg)}
        VariableAccessCountPass.apply(cfgs, self.cfg_memory_tags)
        nodes = {node.llvm_label: node for node in cfgs["@main"].nodes}
        bb0 = nodes[0]
        self.assertIsNotNone(bb0.variables)
        self.assertEqual(bb0.variables["main.first_alloca"].nb_access, 1)  # Store 0 -> retval
        self.assertEqual(bb0.variables["main.a"].nb_access, 2)  # Load 0 -> a, Load a -> RX
        self.assertEqual(bb0.variables["main.b"].nb_access, 2)  # Load 1 ->b, Load b -> RY
        self.assertEqual(bb0.variables["main.res"].nb_access, 1)  # Load 0 -> res

        bb8 = nodes[8]
        self.assertIsNotNone(bb8.variables)
        self.assertEqual(bb8.variables["main.a"].nb_access, 3)  # Load a -> RX, RX++, Store RX -> a,  Load a -> RX
        self.assertEqual(bb8.variables["main.res"].nb_access, 1)  # Load RX -> res

        bb12 = nodes[12]
        self.assertIsNotNone(bb12.variables)
        self.assertEqual(bb12.variables["main.b"].nb_access, 1)  # Load b -> RX
        self.assertEqual(bb12.variables["main.res"].nb_access, 1)  # Load RX -> res


if __name__ == "__main__":
    unittest.main()
