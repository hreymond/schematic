import unittest
from copy import deepcopy

from ScEpTIC.AST.elements.instructions.binary_operation import BinaryOperation
from ScEpTIC.AST.elements.instructions.memory_operations import StoreOperation, LoadOperation
from ScEpTIC.AST.transformations.schematic import EnergyConsumptionEstimationPass
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.emulator.energy.msp430_worst_case import MSP430WorstCaseEnergyCalculator
from tests.data.data_generation.data import IS_EVEN_CFG


class EstimateWithoutAccessTest(unittest.TestCase):
    """
    Test of the energy estimation pass (without memory accesses).

    TODO: test pointer access
    """

    def setUp(self):
        # Not used since energy per cycle is redefined
        wc_current_consumption = {
            "frequency": "8M",  # Clock Frequency
            "voltage": "3.6",  # Voltage
            "n_waits": "0",  # Number of wait cycles for FRAM accesses
            "I_am_fram": "2510u",
            # Current consumption for one clock cycle when program resides in FRAM and data in SRAM (cache hit 0%)
            "I_am_ram": "585u",  # Current consumption for one clock cycle when program and data resides in SRAM
            "fram_access_overhead": "1.3",  # Overhead per cycle for memory access to data in FRAM compared to SRAM
        }
        msp430_energy_calculator = MSP430WorstCaseEnergyCalculator(wc_current_consumption)
        # Parameters for testing purpose
        msp430_energy_calculator.energy_clock_cycle = 1
        msp430_energy_calculator.nvm_extra_cycles = 1
        msp430_energy_calculator.energy_non_volatile_memory_access = 0
        msp430_energy_calculator.energy_volatile_memory_access = 0
        msp430_energy_calculator.non_volatile_increase = 1

        self.energy_estimator = EnergyConsumptionEstimationPass(msp430_energy_calculator, None, builtins_functions=set())
        self.energy_estimator.energy_calculator.energy_clock_cycle = 1

    def test_empty_bb(self):
        bb = BasicBlock(0, "@main")
        energy = self.energy_estimator.estimate_energy_all_nvm(bb)
        self.assertEqual(energy, 0, "Empty BB must be estimated to 0 energy")

    def test_one_access(self):
        bb = BasicBlock(0, "@main")
        bb.instructions.append(StoreOperation("%1", "0", 0, False))
        energy = self.energy_estimator.estimate_energy_all_nvm(bb)
        self.assertEqual(energy, 2)

    def test_no_access(self):
        bb = BasicBlock(0, "@main")
        bb.instructions.append(BinaryOperation("add", "R1", "R2", "%0", False, {}))
        energy = self.energy_estimator.estimate_energy_all_nvm(bb)
        self.assertEqual(energy, 1)

    def test_mixed_bb(self):
        bb = BasicBlock(0, "@main")
        bb.instructions.append(BinaryOperation("add", "R1", "R2", "%0", False, {}))
        bb.instructions.append(StoreOperation("%1", "0", 0, False))
        bb.instructions.append(StoreOperation("%1", "0", 0, False))
        bb.instructions.append(BinaryOperation("add", "R1", "R2", "%0", False, {}))
        bb.instructions.append(LoadOperation("%1", None, "0", 0, False))
        energy = self.energy_estimator.estimate_energy_all_nvm(bb)
        self.assertEqual(energy, 8)

    def test_estimation_cfg_1_bb(self):

        cfgs = {"@is_even": deepcopy(IS_EVEN_CFG)}

        self.energy_estimator.apply(cfgs)

        modified_cfg = cfgs["@is_even"]
        self.assertEqual(len(modified_cfg.nodes), 1)
        self.assertEqual(len(modified_cfg.edges), 0)
        modified_bb = list(modified_cfg.nodes)[0]
        self.assertEqual(modified_bb.cost_all_nvm, 14)
