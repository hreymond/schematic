import json
import re
import unittest
import pygraphviz as pgv

ANALYSIS_RESULTS_PATH = "analysis_results/program/"


def get_llvm_label(cfg, node):
    label = cfg.get_node(node).attr["label"]
    search = re.search(".*<B>%.*\((.*)\)</B>.*", label)
    return search.group(1)


class TestCFG(unittest.TestCase):
    """
    Test the execution of ScEpTIC with application of our checkpoint placement/memory allocation algorithm on a program
    with 3 functions and one condition
    """

    def setUp(self):
        file_path = "dot_cfgs/@main/before.dot"
        with open(file_path, "r") as f:
            self.main_cfg_before = pgv.AGraph(f.read())
        for node in list(self.main_cfg_before.nodes()):
            if not (self.main_cfg_before.successors(node) or self.main_cfg_before.predecessors(node)):
                self.main_cfg_before.remove_node(node)

        file_path = "dot_cfgs/after_analysis/" + "@main.dot"
        with open(file_path, "r") as f:
            self.main_cfg_after = pgv.AGraph(f.read())
        for node in list(self.main_cfg_after.nodes()):
            if not (self.main_cfg_after.successors(node) or self.main_cfg_after.predecessors(node)):
                self.main_cfg_after.remove_node(node)

        with open(ANALYSIS_RESULTS_PATH + "energy/results.json", "r") as f:
            self.energy_results = json.loads(f.read())
        with open(ANALYSIS_RESULTS_PATH + "states/base.txt", "r") as f:
            self.state_base = f.read()

    def test_parse_main(self):
        nodes = self.main_cfg_before.nodes()
        # 3 originals bbs + 2*2 basic blocks representing function calls
        # + 2 bb for the splitted basic blocks (%0 and %5) + 2 for start/end bbs
        self.assertEqual(3 + 4 + 2 + 2, len(nodes))
        self.assertEqual(11, len(self.main_cfg_before.edges()))

        llvm_labels = [get_llvm_label(self.main_cfg_before, node) for node in nodes]
        # Check if original basic blocks are presents
        self.assertIn("%0", llvm_labels)
        self.assertIn("%5", llvm_labels)
        self.assertIn("%7", llvm_labels)
        # Check if the functions have been well extracted
        self.assertIn("%5_@g_entry", llvm_labels)
        self.assertIn("%5_@g_exit", llvm_labels)
        self.assertIn("%0_@f_entry", llvm_labels)
        self.assertIn("%0_@f_exit", llvm_labels)
        # Check if the basic block are well splitted
        self.assertIn("%0_2", llvm_labels)
        self.assertIn("%5_0", llvm_labels)

    def test_analyse_before_analysis_main(self):
        nb_active = 0
        nb_disabled = 0
        nb_virtual = 0
        nb_potential = 0
        for u, v in self.main_cfg_before.edges():
            edge = self.main_cfg_before.get_edge(u, v)
            checkpoint = edge.attr["arrowhead"]
            if checkpoint == "diamond":
                nb_active += 1
            elif checkpoint == "normal":
                nb_disabled += 1
            elif checkpoint == "dot":
                nb_virtual += 1
            elif checkpoint == "tee":
                nb_potential += 1
            else:
                raise ValueError("Unexpected checkpoint symbol: {}".format(checkpoint))
        self.assertEqual(0, nb_active)
        # g must have a checkpoint placed in it, translating in a virtual checkpoint in main
        self.assertEqual(0, nb_virtual)
        # f must be checkpoint free, translating in a disabled checkpoint in main
        self.assertEqual(2, nb_disabled)
        self.assertEqual(9, nb_potential)

    def test_analyse_after_analysis_main(self):
        nb_active = 0
        nb_disabled = 0
        nb_virtual = 0
        nb_potential = 0
        for u, v in self.main_cfg_after.edges():
            edge = self.main_cfg_after.get_edge(u, v)
            checkpoint = edge.attr["arrowhead"]
            if checkpoint == "diamond":
                nb_active += 1
            elif checkpoint == "normal":
                nb_disabled += 1
            elif checkpoint == "dot":
                nb_virtual += 1
            elif checkpoint == "tee":
                nb_potential += 1
            else:
                raise ValueError("Unexpected checkpoint symbol: {}".format(checkpoint))
        self.assertEqual(2, nb_active)
        self.assertEqual(0, nb_virtual)
        self.assertEqual(9, nb_disabled)
        self.assertEqual(0, nb_potential)

    def test_checkpoint_execution(self):
        analysis = self.energy_results[0]
        self.assertEqual(2, analysis["measures"]["summary"]["checkpoint"])

    def test_able_to_complete(self):
        self.assertTrue(self.energy_results[0]["measures"]["able_to_complete"], "The program isn't able to complete")

    def test_return_value(self):
        reg_val = None
        for l in self.state_base.splitlines():
            if "%8" in l:
                reg_val = int(l.split(":")[-1])
                break
        self.assertIsNotNone(reg_val, "Couldn't find register %8 in states/base.txt")
        self.assertEqual(6, reg_val)

    def test_energy_consumed(self):
        total_energy = self.energy_results[0]["measures"]["data"]["total"]
        self.assertAlmostEqual(0, total_energy["volatile_accesses"]["energy"], 10)
        self.assertAlmostEqual(1.584e-08, total_energy["non_volatile_accesses"]["energy"], 10)
        self.assertAlmostEqual(9.186e-9, total_energy["no_memory_accesses"]["energy"], 10)
        self.assertAlmostEqual(2.5e-8, total_energy["total"]["energy"], 10)
