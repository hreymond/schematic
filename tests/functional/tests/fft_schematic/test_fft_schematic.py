import json
import unittest

ANALYSIS_RESULTS_PATH = "analysis_results/program/"


class TestEnergy(unittest.TestCase):
    """
    Test the execution of ScEpTIC with Schematic pass to ensure that we don't break Alfred/ScEpTIC
    """

    def setUp(self):
        energy_results_file = ANALYSIS_RESULTS_PATH + "energy/results.json"
        with open(energy_results_file, "r") as f:
            self.energy_results = json.load(f)

    def test_json_result(self):
        self.assertNotEqual(0, self.energy_results)

    def test_able_to_complete(self):
        self.assertTrue(self.energy_results[0]["measures"]["able_to_complete"], "The fft program isn't able to complete")

    # def test_constant_summary(self):
    #     expected_summary = {"checkpoint": 1215, "trigger_call": 0, "restore": 1215, "power_failure": 1215}
    #     self.assertDictEqual(
    #         expected_summary,
    #         self.energy_results[0]["measures"]["summary"],
    #         "The summary generated and the expected summary don't match",
    #     )

    def test_energy_consumed(self):
        total_energy = self.energy_results[0]["measures"]["data"]["total"]
        self.assertAlmostEqual(0.00017152, total_energy["total"]["energy"], 6)
        self.assertAlmostEqual(3.09e-05, total_energy["volatile_accesses"]["energy"], 6)
        self.assertAlmostEqual(9.93e-05, total_energy["non_volatile_accesses"]["energy"], 6)
        self.assertAlmostEqual(4.12e-05, total_energy["no_memory_accesses"]["energy"], 6)
