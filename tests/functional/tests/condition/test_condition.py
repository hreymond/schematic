import json
import os
import re
import unittest
import pygraphviz as pgv

ANALYSIS_RESULTS_PATH = "analysis_results/program/"
DOT_FILES_PATH = os.getenv("DOT_FILES_PATH", "dot_cfgs/before_analysis/")


def get_llvm_label(cfg, node):
    label = cfg.get_node(node).attr["label"]
    search = re.search(".*<B>%.*\((.*)\)</B>.*", label)
    return search.group(1)


def nodes_to_llvm_labels(cfg, nodes):
    return [get_llvm_label(cfg, node) for node in nodes]


class TestCFG(unittest.TestCase):
    """
    Test the execution of ScEpTIC with application of our checkpoint placement/memory allocation algorithm on a
    program with a simple condition

    The only feature tested yet is the generation of the .dot graph
    """

    def setUp(self):
        self.file_path = DOT_FILES_PATH + "@main.dot"
        with open(self.file_path, "r") as f:
            self.cfg = pgv.AGraph(f.read())
        with open(ANALYSIS_RESULTS_PATH + "energy/results.json", "r") as f:
            self.energy_results = json.loads(f.read())
        with open(ANALYSIS_RESULTS_PATH + "states/base.txt", "r") as f:
            self.state_base = f.read()

    def test_nodes_edges(self):
        edges = self.cfg.edges()
        nodes = self.cfg.nodes()
        self.assertEqual(6, len(edges))
        self.assertEqual(6, len(nodes))

        llvm_nodes = nodes_to_llvm_labels(self.cfg, nodes)
        # Check if LLVM labels are well parsed
        self.assertIn("%0", llvm_nodes)
        self.assertIn("%8", llvm_nodes)
        self.assertIn("%10", llvm_nodes)
        self.assertIn("%12", llvm_nodes)

    def test_able_to_complete(self):
        self.assertTrue(self.energy_results[0]["measures"]["able_to_complete"], "The program isn't able to complete")

    def test_result_(self):
        for l in self.state_base.splitlines():
            if "%13" in l:
                reg_val = int(l.split(":")[-1])
                self.assertEqual(1, reg_val)
                return
