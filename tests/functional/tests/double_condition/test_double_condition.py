import json
import os
import re
import unittest
import pygraphviz as pgv

ANALYSIS_RESULTS_PATH = "analysis_results/program/"
DOT_FILES_PATH = os.getenv("DOT_FILES_PATH", "dot_cfgs/")


def get_llvm_label(cfg, node):
    label = cfg.get_node(node).attr["label"]
    search = re.search(".*<B>%.*\((.*)\)</B>.*", label)
    return search.group(1)


def nodes_to_llvm_labels(cfg, nodes):
    return [get_llvm_label(cfg, node) for node in nodes]


class TestCFG(unittest.TestCase):
    """
    Test the execution of ScEpTIC with application of our checkpoint placement/memory allocation algorithm on a program
    with only one path

    The only feature tested yet is the generation of the .dot graph and the checkpoint placement + ability
    to complete.

    TODO:
    - modify the main.c to return a value, verify that the value is the one expected -> main.c modification done,
        I still need to work on how to get the return code smartly
    - modify the config.py so that the energy model is the MSP430WorstCase, test the program with it
    """

    def setUp(self):
        file_path = DOT_FILES_PATH + "before_analysis/@main.dot"
        with open(file_path, "r") as f:
            self.cfg = pgv.AGraph(f.read())
        with open(ANALYSIS_RESULTS_PATH + "energy/results.json", "r") as f:
            self.energy_results = json.loads(f.read())
        with open(ANALYSIS_RESULTS_PATH + "states/base.txt", "r") as f:
            self.state_base = f.read()

    def test_cfg_before_transformation(self):
        edges = self.cfg.edges()
        nodes = self.cfg.nodes()
        self.assertEqual(8, len(nodes))
        self.assertEqual(9, len(edges))
        llvm_labels = nodes_to_llvm_labels(self.cfg, nodes)
        # Check if LLVM labels are well parsed
        self.assertIn("%0", llvm_labels)
        self.assertIn("%8", llvm_labels)
        self.assertIn("%22", llvm_labels)
        self.assertIn("%25", llvm_labels)
        self.assertIn("%26", llvm_labels)

    def test_able_to_complete(self):
        self.assertTrue(self.energy_results[0]["measures"]["able_to_complete"], "The program isn't able to complete")

    def test_return_value(self):
        for l in self.state_base.splitlines():
            if "%29" in l:
                reg_val = int(l.split(":")[-1])
                self.assertEqual(3, reg_val)
