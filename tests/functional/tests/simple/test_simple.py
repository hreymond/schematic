import json
import os
import re
import unittest
import pygraphviz as pgv

ANALYSIS_RESULTS_PATH = "analysis_results/program/"
DOT_FILES_PATH = os.getenv("DOT_FILES_PATH", "dot_cfgs/before_analysis/")


def get_llvm_label(cfg, node):
    label = cfg.get_node(node).attr["label"]
    search = re.search(".*<B>%.*\((.*)\)</B>.*", label)
    return search.group(1)


class TestCFG(unittest.TestCase):
    """
    Test the execution of ScEpTIC with application of our checkpoint placement/memory allocation algorithm on a program
    with only one path

    The only feature tested yet is the generation of the .dot graph and the checkpoint placement + ability
    to complete.

    TODO:
    - modify the main.c to return a value, verify that the value is the one expected
    - modify the config.py so that the energy model is the MSP430WorstCase, test the program with it
    """

    def setUp(self):
        self.file_path = DOT_FILES_PATH + "@main.dot"
        with open(self.file_path, "r") as f:
            self.cfg = pgv.AGraph(f.read())
        with open(ANALYSIS_RESULTS_PATH + "energy/results.json", "r") as f:
            self.energy_results = json.loads(f.read())

    def test_nodes_edges(self):
        edges = self.cfg.edges()
        nodes = self.cfg.nodes()
        self.assertEqual(5, len(nodes))
        self.assertEqual(4, len(edges))
        llvm_labels = [get_llvm_label(self.cfg, node) for node in nodes]
        # Check if LLVM labels are well parsed
        self.assertIn("%0", llvm_labels)
        self.assertIn("%4", llvm_labels)
        self.assertIn("%13", llvm_labels)

    def test_checkpoint_placement(self):
        analysis = self.energy_results[0]
        self.assertEqual(1, analysis["measures"]["summary"]["checkpoint"])

    def test_able_to_complete(self):
        self.assertTrue(self.energy_results[0]["measures"]["able_to_complete"], "The program isn't able to complete")
