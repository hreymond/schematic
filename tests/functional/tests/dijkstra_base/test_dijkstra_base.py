import json
import os
import re
import unittest

ANALYSIS_RESULTS_PATH = "analysis_results/program/"
LOG_PATH = os.getenv("LOG_PATH", "logs")
DOT_FILES_PATH = os.getenv("DOT_FILES_PATH", "dot_cfgs/")


expected_logs = """[PRINTF]: Shortest path is 8 in cost.
[PRINTF]: Path is:
[PRINTF]:  15
[PRINTF]:  18
[PRINTF]:  16
[PRINTF]:  0
[PRINTF]:

[PRINTF]: Shortest path is 8 in cost.
[PRINTF]: Path is:
[PRINTF]:  16
[PRINTF]:  0
[PRINTF]:  6
[PRINTF]:  1
[PRINTF]:

[PRINTF]: Shortest path is 22 in cost.
[PRINTF]: Path is:
[PRINTF]:  17
[PRINTF]:  9
[PRINTF]:  20
[PRINTF]:  10
[PRINTF]:  2
[PRINTF]:

[PRINTF]: Shortest path is 9 in cost.
[PRINTF]: Path is:
[PRINTF]:  18
[PRINTF]:  23
[PRINTF]:  26
[PRINTF]:  8
[PRINTF]:  21
[PRINTF]:  3
[PRINTF]:

[PRINTF]: Shortest path is 5 in cost.
[PRINTF]: Path is:
[PRINTF]:  19
[PRINTF]:  4
[PRINTF]:

[PRINTF]: Shortest path is 9 in cost.
[PRINTF]: Path is:
[PRINTF]:  20
[PRINTF]:  28
[PRINTF]:  23
[PRINTF]:  26
[PRINTF]:  5
[PRINTF]:

[PRINTF]: Shortest path is 14 in cost.
[PRINTF]: Path is:
[PRINTF]:  21
[PRINTF]:  7
[PRINTF]:  17
[PRINTF]:  0
[PRINTF]:  6
[PRINTF]:

[PRINTF]: Shortest path is 7 in cost.
[PRINTF]: Path is:
[PRINTF]:  22
[PRINTF]:  11
[PRINTF]:  24
[PRINTF]:  17
[PRINTF]:  7
[PRINTF]:

[PRINTF]: Shortest path is 1 in cost.
[PRINTF]: Path is:
[PRINTF]:  23
[PRINTF]:  26
[PRINTF]:  8
[PRINTF]:

[PRINTF]: Shortest path is 4 in cost.
[PRINTF]: Path is:
[PRINTF]:  24
[PRINTF]:  19
[PRINTF]:  13
[PRINTF]:  15
[PRINTF]:  9
[PRINTF]:
"""


def get_llvm_label(cfg, node):
    label = cfg.get_node(node).attr["label"]
    search = re.search(".*<B>%.*\((.*)\)</B>.*", label)
    return search.group(1)


def nodes_to_llvm_labels(cfg, nodes):
    return [get_llvm_label(cfg, node) for node in nodes]


class TestCFG(unittest.TestCase):
    """
    Test the execution of ScEpTIC with application of our checkpoint placement/memory allocation algorithm on a program
    with only one path

    The only feature tested yet is the generation of the .dot graph and the checkpoint placement + ability
    to complete.

    TODO:
    - modify the main.c to return a value, verify that the value is the one expected -> main.c modification done,
        I still need to work on how to get the return code smartly
    - modify the config.py so that the energy model is the MSP430WorstCase, test the program with it
    """

    def test_result(self):
        with open(os.path.join(LOG_PATH, "dijkstra_base"), "r") as f:
            text = f.read().replace(" ", "").strip()
            print(text)
            print(expected_logs.replace(" ", "").strip())
            self.assertIn(expected_logs.replace(" ", "").strip(), text)
