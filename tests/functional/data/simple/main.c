// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll -target msp430

int main(){
    int a = 0;
    int b = 1;
    goto bb2;
bb2:
    a = 8;
    a--;
    a--;
    a--;
    b++;
    goto bb3;
bb3:
    a = 3;
    return a;
}