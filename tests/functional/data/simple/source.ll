; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

; Function Attrs: noinline nounwind optnone
define dso_local i16 @main() #0 !dbg !7 {
  %1 = alloca i16, align 2
  %2 = alloca i16, align 2
  %3 = alloca i16, align 2
  store i16 0, i16* %1, align 2
  call void @llvm.dbg.declare(metadata i16* %2, metadata !11, metadata !DIExpression()), !dbg !12
  store i16 0, i16* %2, align 2, !dbg !12
  call void @llvm.dbg.declare(metadata i16* %3, metadata !13, metadata !DIExpression()), !dbg !14
  store i16 1, i16* %3, align 2, !dbg !14
  br label %4, !dbg !15

; <label>:4:                                      ; preds = %0
  store i16 8, i16* %2, align 2, !dbg !16
  %5 = load i16, i16* %2, align 2, !dbg !17
  %6 = add nsw i16 %5, -1, !dbg !17
  store i16 %6, i16* %2, align 2, !dbg !17
  %7 = load i16, i16* %2, align 2, !dbg !18
  %8 = add nsw i16 %7, -1, !dbg !18
  store i16 %8, i16* %2, align 2, !dbg !18
  %9 = load i16, i16* %2, align 2, !dbg !19
  %10 = add nsw i16 %9, -1, !dbg !19
  store i16 %10, i16* %2, align 2, !dbg !19
  %11 = load i16, i16* %3, align 2, !dbg !20
  %12 = add nsw i16 %11, 1, !dbg !20
  store i16 %12, i16* %3, align 2, !dbg !20
  br label %13, !dbg !21

; <label>:13:                                     ; preds = %4
  store i16 3, i16* %2, align 2, !dbg !22
  %14 = load i16, i16* %2, align 2, !dbg !23
  ret i16 %14, !dbg !24
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!3, !4, !5}
!llvm.ident = !{!6}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2)
!1 = !DIFile(filename: "main.c", directory: "/home/user/Desktop/alfred/tests/data/simple")
!2 = !{}
!3 = !{i32 2, !"Dwarf Version", i32 4}
!4 = !{i32 2, !"Debug Info Version", i32 3}
!5 = !{i32 1, !"wchar_size", i32 2}
!6 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
!7 = distinct !DISubprogram(name: "main", scope: !1, file: !1, line: 3, type: !8, isLocal: false, isDefinition: true, scopeLine: 3, isOptimized: false, unit: !0, retainedNodes: !2)
!8 = !DISubroutineType(types: !9)
!9 = !{!10}
!10 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!11 = !DILocalVariable(name: "a", scope: !7, file: !1, line: 4, type: !10)
!12 = !DILocation(line: 4, column: 9, scope: !7)
!13 = !DILocalVariable(name: "b", scope: !7, file: !1, line: 5, type: !10)
!14 = !DILocation(line: 5, column: 9, scope: !7)
!15 = !DILocation(line: 6, column: 5, scope: !7)
!16 = !DILocation(line: 8, column: 7, scope: !7)
!17 = !DILocation(line: 9, column: 6, scope: !7)
!18 = !DILocation(line: 10, column: 6, scope: !7)
!19 = !DILocation(line: 11, column: 6, scope: !7)
!20 = !DILocation(line: 12, column: 6, scope: !7)
!21 = !DILocation(line: 13, column: 5, scope: !7)
!22 = !DILocation(line: 15, column: 7, scope: !7)
!23 = !DILocation(line: 16, column: 12, scope: !7)
!24 = !DILocation(line: 16, column: 5, scope: !7)
