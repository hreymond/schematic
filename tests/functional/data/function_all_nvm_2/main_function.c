// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll -target msp430
int a = 0;

int f() {
    return a+3;
}

int g(){
   return f();
}

int main(){
     a = f();
     if(a > 2) {
        a = g();
     }
    return a;
}