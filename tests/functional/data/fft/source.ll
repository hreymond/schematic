; ModuleID = 'llvm-link'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@.str = private unnamed_addr constant [73 x i8] c">>> Error in fftmisc.c: argument %d to NumberOfBitsNeeded is too small.\0A\00", align 1
@.str.1 = private unnamed_addr constant [7 x i8] c"RealIn\00", align 1
@.str.1.2 = private unnamed_addr constant [8 x i8] c"RealOut\00", align 1
@.str.2 = private unnamed_addr constant [8 x i8] c"ImagOut\00", align 1
@invfft = dso_local global i32 0, align 4, !dbg !0
@MAXWAVES = dso_local global i32 4, align 4, !dbg !6
@MAXSIZE = common dso_local global i32 0, align 4, !dbg !9
@realin = internal global [1024 x float] zeroinitializer, align 16, !dbg !11
@imagin = internal global [1024 x float] zeroinitializer, align 16, !dbg !17
@realout = internal global [1024 x float] zeroinitializer, align 16, !dbg !19
@imagout = internal global [1024 x float] zeroinitializer, align 16, !dbg !21
@Coeff = internal global [16 x float] zeroinitializer, align 16, !dbg !23
@Amp = internal global [16 x float] zeroinitializer, align 16, !dbg !28

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @IsPowerOfTwo(i32) #0 !dbg !41 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %3, align 4
  call void @llvm.dbg.declare(metadata i32* %3, metadata !44, metadata !DIExpression()), !dbg !45
  %4 = load i32, i32* %3, align 4, !dbg !46
  %5 = icmp ult i32 %4, 2, !dbg !48
  br i1 %5, label %6, label %7, !dbg !49

; <label>:6:                                      ; preds = %1
  store i32 0, i32* %2, align 4, !dbg !50
  br label %16, !dbg !50

; <label>:7:                                      ; preds = %1
  %8 = load i32, i32* %3, align 4, !dbg !51
  %9 = load i32, i32* %3, align 4, !dbg !53
  %10 = sub i32 %9, 1, !dbg !54
  %11 = and i32 %8, %10, !dbg !55
  %12 = icmp ne i32 %11, 0, !dbg !55
  br i1 %12, label %13, label %14, !dbg !56

; <label>:13:                                     ; preds = %7
  store i32 0, i32* %2, align 4, !dbg !57
  br label %16, !dbg !57

; <label>:14:                                     ; preds = %7
  %15 = call i32 (...) @checkpoint(), !dbg !58
  store i32 1, i32* %2, align 4, !dbg !59
  br label %16, !dbg !59

; <label>:16:                                     ; preds = %14, %13, %6
  %17 = load i32, i32* %2, align 4, !dbg !60
  ret i32 %17, !dbg !60
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

declare dso_local i32 @checkpoint(...) #2

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @NumberOfBitsNeeded(i32) #0 !dbg !61 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !64, metadata !DIExpression()), !dbg !65
  call void @llvm.dbg.declare(metadata i32* %3, metadata !66, metadata !DIExpression()), !dbg !67
  %4 = load i32, i32* %2, align 4, !dbg !68
  %5 = icmp ult i32 %4, 2, !dbg !70
  br i1 %5, label %6, label %9, !dbg !71

; <label>:6:                                      ; preds = %1
  %7 = load i32, i32* %2, align 4, !dbg !72
  %8 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([73 x i8], [73 x i8]* @.str, i32 0, i32 0), i32 %7), !dbg !74
  call void @exit(i32 1) #5, !dbg !75
  unreachable, !dbg !75

; <label>:9:                                      ; preds = %1
  store i32 0, i32* %3, align 4, !dbg !76
  br label %10, !dbg !78

; <label>:10:                                     ; preds = %20, %9
  %11 = load i32, i32* %2, align 4, !dbg !79
  %12 = load i32, i32* %3, align 4, !dbg !83
  %13 = shl i32 1, %12, !dbg !84
  %14 = and i32 %11, %13, !dbg !85
  %15 = icmp ne i32 %14, 0, !dbg !85
  br i1 %15, label %16, label %18, !dbg !86

; <label>:16:                                     ; preds = %10
  %17 = load i32, i32* %3, align 4, !dbg !87
  ret i32 %17, !dbg !89

; <label>:18:                                     ; preds = %10
  %19 = call i32 (...) @checkpoint(), !dbg !90
  br label %20, !dbg !91

; <label>:20:                                     ; preds = %18
  %21 = load i32, i32* %3, align 4, !dbg !92
  %22 = add i32 %21, 1, !dbg !92
  store i32 %22, i32* %3, align 4, !dbg !92
  br label %10, !dbg !93, !llvm.loop !94
}

declare dso_local i32 @printf(i8*, ...) #2

; Function Attrs: noreturn
declare dso_local void @exit(i32) #3

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @ReverseBits(i32, i32) #0 !dbg !97 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  store i32 %0, i32* %3, align 4
  call void @llvm.dbg.declare(metadata i32* %3, metadata !100, metadata !DIExpression()), !dbg !101
  store i32 %1, i32* %4, align 4
  call void @llvm.dbg.declare(metadata i32* %4, metadata !102, metadata !DIExpression()), !dbg !103
  call void @llvm.dbg.declare(metadata i32* %5, metadata !104, metadata !DIExpression()), !dbg !105
  call void @llvm.dbg.declare(metadata i32* %6, metadata !106, metadata !DIExpression()), !dbg !107
  store i32 0, i32* %6, align 4, !dbg !108
  store i32 0, i32* %5, align 4, !dbg !110
  br label %7, !dbg !111

; <label>:7:                                      ; preds = %20, %2
  %8 = load i32, i32* %5, align 4, !dbg !112
  %9 = load i32, i32* %4, align 4, !dbg !114
  %10 = icmp ult i32 %8, %9, !dbg !115
  br i1 %10, label %11, label %23, !dbg !116

; <label>:11:                                     ; preds = %7
  %12 = load i32, i32* %6, align 4, !dbg !117
  %13 = shl i32 %12, 1, !dbg !119
  %14 = load i32, i32* %3, align 4, !dbg !120
  %15 = and i32 %14, 1, !dbg !121
  %16 = or i32 %13, %15, !dbg !122
  store i32 %16, i32* %6, align 4, !dbg !123
  %17 = load i32, i32* %3, align 4, !dbg !124
  %18 = lshr i32 %17, 1, !dbg !124
  store i32 %18, i32* %3, align 4, !dbg !124
  %19 = call i32 (...) @checkpoint(), !dbg !125
  br label %20, !dbg !126

; <label>:20:                                     ; preds = %11
  %21 = load i32, i32* %5, align 4, !dbg !127
  %22 = add i32 %21, 1, !dbg !127
  store i32 %22, i32* %5, align 4, !dbg !127
  br label %7, !dbg !128, !llvm.loop !129

; <label>:23:                                     ; preds = %7
  %24 = load i32, i32* %6, align 4, !dbg !131
  ret i32 %24, !dbg !132
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local double @Index_to_frequency(i32, i32) #0 !dbg !133 {
  %3 = alloca double, align 8
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  store i32 %0, i32* %4, align 4
  call void @llvm.dbg.declare(metadata i32* %4, metadata !136, metadata !DIExpression()), !dbg !137
  store i32 %1, i32* %5, align 4
  call void @llvm.dbg.declare(metadata i32* %5, metadata !138, metadata !DIExpression()), !dbg !139
  %6 = load i32, i32* %5, align 4, !dbg !140
  %7 = load i32, i32* %4, align 4, !dbg !142
  %8 = icmp uge i32 %6, %7, !dbg !143
  br i1 %8, label %9, label %10, !dbg !144

; <label>:9:                                      ; preds = %2
  store double 0.000000e+00, double* %3, align 8, !dbg !145
  br label %31, !dbg !145

; <label>:10:                                     ; preds = %2
  %11 = load i32, i32* %5, align 4, !dbg !147
  %12 = load i32, i32* %4, align 4, !dbg !149
  %13 = udiv i32 %12, 2, !dbg !150
  %14 = icmp ule i32 %11, %13, !dbg !151
  br i1 %14, label %15, label %21, !dbg !152

; <label>:15:                                     ; preds = %10
  %16 = load i32, i32* %5, align 4, !dbg !153
  %17 = uitofp i32 %16 to double, !dbg !155
  %18 = load i32, i32* %4, align 4, !dbg !156
  %19 = uitofp i32 %18 to double, !dbg !157
  %20 = fdiv double %17, %19, !dbg !158
  store double %20, double* %3, align 8, !dbg !159
  br label %31, !dbg !159

; <label>:21:                                     ; preds = %10
  br label %22

; <label>:22:                                     ; preds = %21
  %23 = load i32, i32* %4, align 4, !dbg !160
  %24 = load i32, i32* %5, align 4, !dbg !161
  %25 = sub i32 %23, %24, !dbg !162
  %26 = uitofp i32 %25 to double, !dbg !163
  %27 = fsub double -0.000000e+00, %26, !dbg !164
  %28 = load i32, i32* %4, align 4, !dbg !165
  %29 = uitofp i32 %28 to double, !dbg !166
  %30 = fdiv double %27, %29, !dbg !167
  store double %30, double* %3, align 8, !dbg !168
  br label %31, !dbg !168

; <label>:31:                                     ; preds = %22, %15, %9
  %32 = load double, double* %3, align 8, !dbg !169
  ret double %32, !dbg !169
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @fft_float(i32, i32, float*, float*, float*, float*) #0 !dbg !170 {
  %7 = alloca i32, align 4
  %8 = alloca i32, align 4
  %9 = alloca float*, align 8
  %10 = alloca float*, align 8
  %11 = alloca float*, align 8
  %12 = alloca float*, align 8
  %13 = alloca i32, align 4
  %14 = alloca i32, align 4
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca double, align 8
  %21 = alloca double, align 8
  %22 = alloca double, align 8
  %23 = alloca double, align 8
  %24 = alloca double, align 8
  %25 = alloca double, align 8
  %26 = alloca double, align 8
  %27 = alloca double, align 8
  %28 = alloca double, align 8
  %29 = alloca [3 x double], align 16
  %30 = alloca [3 x double], align 16
  %31 = alloca double, align 8
  store i32 %0, i32* %7, align 4
  call void @llvm.dbg.declare(metadata i32* %7, metadata !174, metadata !DIExpression()), !dbg !175
  store i32 %1, i32* %8, align 4
  call void @llvm.dbg.declare(metadata i32* %8, metadata !176, metadata !DIExpression()), !dbg !177
  store float* %2, float** %9, align 8
  call void @llvm.dbg.declare(metadata float** %9, metadata !178, metadata !DIExpression()), !dbg !179
  store float* %3, float** %10, align 8
  call void @llvm.dbg.declare(metadata float** %10, metadata !180, metadata !DIExpression()), !dbg !181
  store float* %4, float** %11, align 8
  call void @llvm.dbg.declare(metadata float** %11, metadata !182, metadata !DIExpression()), !dbg !183
  store float* %5, float** %12, align 8
  call void @llvm.dbg.declare(metadata float** %12, metadata !184, metadata !DIExpression()), !dbg !185
  call void @llvm.dbg.declare(metadata i32* %13, metadata !186, metadata !DIExpression()), !dbg !187
  call void @llvm.dbg.declare(metadata i32* %14, metadata !188, metadata !DIExpression()), !dbg !189
  call void @llvm.dbg.declare(metadata i32* %15, metadata !190, metadata !DIExpression()), !dbg !191
  call void @llvm.dbg.declare(metadata i32* %16, metadata !192, metadata !DIExpression()), !dbg !193
  call void @llvm.dbg.declare(metadata i32* %17, metadata !194, metadata !DIExpression()), !dbg !195
  call void @llvm.dbg.declare(metadata i32* %18, metadata !196, metadata !DIExpression()), !dbg !197
  call void @llvm.dbg.declare(metadata i32* %19, metadata !198, metadata !DIExpression()), !dbg !199
  call void @llvm.dbg.declare(metadata double* %20, metadata !200, metadata !DIExpression()), !dbg !201
  store double 0x401921FB54442D18, double* %20, align 8, !dbg !201
  call void @llvm.dbg.declare(metadata double* %21, metadata !202, metadata !DIExpression()), !dbg !203
  call void @llvm.dbg.declare(metadata double* %22, metadata !204, metadata !DIExpression()), !dbg !205
  %32 = load i32, i32* %7, align 4, !dbg !206
  %33 = call i32 @IsPowerOfTwo(i32 %32), !dbg !208
  %34 = icmp ne i32 %33, 0, !dbg !208
  br i1 %34, label %36, label %35, !dbg !209

; <label>:35:                                     ; preds = %6
  call void @exit(i32 1) #5, !dbg !210
  unreachable, !dbg !210

; <label>:36:                                     ; preds = %6
  %37 = load i32, i32* %8, align 4, !dbg !212
  %38 = icmp ne i32 %37, 0, !dbg !212
  br i1 %38, label %39, label %42, !dbg !214

; <label>:39:                                     ; preds = %36
  %40 = load double, double* %20, align 8, !dbg !215
  %41 = fsub double -0.000000e+00, %40, !dbg !216
  store double %41, double* %20, align 8, !dbg !217
  br label %42, !dbg !218

; <label>:42:                                     ; preds = %39, %36
  %43 = load float*, float** %9, align 8, !dbg !219
  %44 = bitcast float* %43 to i8*, !dbg !219
  call void @CheckPointer(i8* %44, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i32 0, i32 0)), !dbg !219
  %45 = load float*, float** %11, align 8, !dbg !220
  %46 = bitcast float* %45 to i8*, !dbg !220
  call void @CheckPointer(i8* %46, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.1.2, i32 0, i32 0)), !dbg !220
  %47 = load float*, float** %12, align 8, !dbg !221
  %48 = bitcast float* %47 to i8*, !dbg !221
  call void @CheckPointer(i8* %48, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.2, i32 0, i32 0)), !dbg !221
  %49 = load i32, i32* %7, align 4, !dbg !222
  %50 = call i32 @NumberOfBitsNeeded(i32 %49), !dbg !223
  store i32 %50, i32* %13, align 4, !dbg !224
  store i32 0, i32* %14, align 4, !dbg !225
  br label %51, !dbg !227

; <label>:51:                                     ; preds = %86, %42
  %52 = load i32, i32* %14, align 4, !dbg !228
  %53 = load i32, i32* %7, align 4, !dbg !230
  %54 = icmp ult i32 %52, %53, !dbg !231
  br i1 %54, label %55, label %89, !dbg !232

; <label>:55:                                     ; preds = %51
  %56 = load i32, i32* %14, align 4, !dbg !233
  %57 = load i32, i32* %13, align 4, !dbg !235
  %58 = call i32 @ReverseBits(i32 %56, i32 %57), !dbg !236
  store i32 %58, i32* %15, align 4, !dbg !237
  %59 = load float*, float** %9, align 8, !dbg !238
  %60 = load i32, i32* %14, align 4, !dbg !239
  %61 = zext i32 %60 to i64, !dbg !238
  %62 = getelementptr inbounds float, float* %59, i64 %61, !dbg !238
  %63 = load float, float* %62, align 4, !dbg !238
  %64 = load float*, float** %11, align 8, !dbg !240
  %65 = load i32, i32* %15, align 4, !dbg !241
  %66 = zext i32 %65 to i64, !dbg !240
  %67 = getelementptr inbounds float, float* %64, i64 %66, !dbg !240
  store float %63, float* %67, align 4, !dbg !242
  %68 = load float*, float** %10, align 8, !dbg !243
  %69 = icmp eq float* %68, null, !dbg !244
  br i1 %69, label %70, label %71, !dbg !245

; <label>:70:                                     ; preds = %55
  br label %78, !dbg !245

; <label>:71:                                     ; preds = %55
  %72 = load float*, float** %10, align 8, !dbg !246
  %73 = load i32, i32* %14, align 4, !dbg !247
  %74 = zext i32 %73 to i64, !dbg !246
  %75 = getelementptr inbounds float, float* %72, i64 %74, !dbg !246
  %76 = load float, float* %75, align 4, !dbg !246
  %77 = fpext float %76 to double, !dbg !246
  br label %78, !dbg !245

; <label>:78:                                     ; preds = %71, %70
  %79 = phi double [ 0.000000e+00, %70 ], [ %77, %71 ], !dbg !245
  %80 = fptrunc double %79 to float, !dbg !245
  %81 = load float*, float** %12, align 8, !dbg !248
  %82 = load i32, i32* %15, align 4, !dbg !249
  %83 = zext i32 %82 to i64, !dbg !248
  %84 = getelementptr inbounds float, float* %81, i64 %83, !dbg !248
  store float %80, float* %84, align 4, !dbg !250
  %85 = call i32 (...) @checkpoint(), !dbg !251
  br label %86, !dbg !252

; <label>:86:                                     ; preds = %78
  %87 = load i32, i32* %14, align 4, !dbg !253
  %88 = add i32 %87, 1, !dbg !253
  store i32 %88, i32* %14, align 4, !dbg !253
  br label %51, !dbg !254, !llvm.loop !255

; <label>:89:                                     ; preds = %51
  store i32 1, i32* %19, align 4, !dbg !257
  store i32 2, i32* %18, align 4, !dbg !258
  br label %90, !dbg !260

; <label>:90:                                     ; preds = %260, %89
  %91 = load i32, i32* %18, align 4, !dbg !261
  %92 = load i32, i32* %7, align 4, !dbg !263
  %93 = icmp ule i32 %91, %92, !dbg !264
  br i1 %93, label %94, label %263, !dbg !265

; <label>:94:                                     ; preds = %90
  call void @llvm.dbg.declare(metadata double* %23, metadata !266, metadata !DIExpression()), !dbg !268
  %95 = load double, double* %20, align 8, !dbg !269
  %96 = load i32, i32* %18, align 4, !dbg !270
  %97 = uitofp i32 %96 to double, !dbg !271
  %98 = fdiv double %95, %97, !dbg !272
  store double %98, double* %23, align 8, !dbg !268
  call void @llvm.dbg.declare(metadata double* %24, metadata !273, metadata !DIExpression()), !dbg !274
  %99 = load double, double* %23, align 8, !dbg !275
  %100 = fmul double -2.000000e+00, %99, !dbg !276
  %101 = call double @sin(double %100) #6, !dbg !277
  store double %101, double* %24, align 8, !dbg !274
  call void @llvm.dbg.declare(metadata double* %25, metadata !278, metadata !DIExpression()), !dbg !279
  %102 = load double, double* %23, align 8, !dbg !280
  %103 = fsub double -0.000000e+00, %102, !dbg !281
  %104 = call double @sin(double %103) #6, !dbg !282
  store double %104, double* %25, align 8, !dbg !279
  call void @llvm.dbg.declare(metadata double* %26, metadata !283, metadata !DIExpression()), !dbg !284
  %105 = load double, double* %23, align 8, !dbg !285
  %106 = fmul double -2.000000e+00, %105, !dbg !286
  %107 = call double @cos(double %106) #6, !dbg !287
  store double %107, double* %26, align 8, !dbg !284
  call void @llvm.dbg.declare(metadata double* %27, metadata !288, metadata !DIExpression()), !dbg !289
  %108 = load double, double* %23, align 8, !dbg !290
  %109 = fsub double -0.000000e+00, %108, !dbg !291
  %110 = call double @cos(double %109) #6, !dbg !292
  store double %110, double* %27, align 8, !dbg !289
  call void @llvm.dbg.declare(metadata double* %28, metadata !293, metadata !DIExpression()), !dbg !294
  %111 = load double, double* %27, align 8, !dbg !295
  %112 = fmul double 2.000000e+00, %111, !dbg !296
  store double %112, double* %28, align 8, !dbg !294
  call void @llvm.dbg.declare(metadata [3 x double]* %29, metadata !297, metadata !DIExpression()), !dbg !301
  call void @llvm.dbg.declare(metadata [3 x double]* %30, metadata !302, metadata !DIExpression()), !dbg !303
  store i32 0, i32* %14, align 4, !dbg !304
  br label %113, !dbg !306

; <label>:113:                                    ; preds = %253, %94
  %114 = load i32, i32* %14, align 4, !dbg !307
  %115 = load i32, i32* %7, align 4, !dbg !309
  %116 = icmp ult i32 %114, %115, !dbg !310
  br i1 %116, label %117, label %257, !dbg !311

; <label>:117:                                    ; preds = %113
  %118 = load double, double* %26, align 8, !dbg !312
  %119 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 2, !dbg !314
  store double %118, double* %119, align 16, !dbg !315
  %120 = load double, double* %27, align 8, !dbg !316
  %121 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 1, !dbg !317
  store double %120, double* %121, align 8, !dbg !318
  %122 = load double, double* %24, align 8, !dbg !319
  %123 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 2, !dbg !320
  store double %122, double* %123, align 16, !dbg !321
  %124 = load double, double* %25, align 8, !dbg !322
  %125 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 1, !dbg !323
  store double %124, double* %125, align 8, !dbg !324
  %126 = load i32, i32* %14, align 4, !dbg !325
  store i32 %126, i32* %15, align 4, !dbg !327
  store i32 0, i32* %17, align 4, !dbg !328
  br label %127, !dbg !329

; <label>:127:                                    ; preds = %246, %117
  %128 = load i32, i32* %17, align 4, !dbg !330
  %129 = load i32, i32* %19, align 4, !dbg !332
  %130 = icmp ult i32 %128, %129, !dbg !333
  br i1 %130, label %131, label %251, !dbg !334

; <label>:131:                                    ; preds = %127
  %132 = load double, double* %28, align 8, !dbg !335
  %133 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 1, !dbg !337
  %134 = load double, double* %133, align 8, !dbg !337
  %135 = fmul double %132, %134, !dbg !338
  %136 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 2, !dbg !339
  %137 = load double, double* %136, align 16, !dbg !339
  %138 = fsub double %135, %137, !dbg !340
  %139 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 0, !dbg !341
  store double %138, double* %139, align 16, !dbg !342
  %140 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 1, !dbg !343
  %141 = load double, double* %140, align 8, !dbg !343
  %142 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 2, !dbg !344
  store double %141, double* %142, align 16, !dbg !345
  %143 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 0, !dbg !346
  %144 = load double, double* %143, align 16, !dbg !346
  %145 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 1, !dbg !347
  store double %144, double* %145, align 8, !dbg !348
  %146 = load double, double* %28, align 8, !dbg !349
  %147 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 1, !dbg !350
  %148 = load double, double* %147, align 8, !dbg !350
  %149 = fmul double %146, %148, !dbg !351
  %150 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 2, !dbg !352
  %151 = load double, double* %150, align 16, !dbg !352
  %152 = fsub double %149, %151, !dbg !353
  %153 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 0, !dbg !354
  store double %152, double* %153, align 16, !dbg !355
  %154 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 1, !dbg !356
  %155 = load double, double* %154, align 8, !dbg !356
  %156 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 2, !dbg !357
  store double %155, double* %156, align 16, !dbg !358
  %157 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 0, !dbg !359
  %158 = load double, double* %157, align 16, !dbg !359
  %159 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 1, !dbg !360
  store double %158, double* %159, align 8, !dbg !361
  %160 = load i32, i32* %15, align 4, !dbg !362
  %161 = load i32, i32* %19, align 4, !dbg !363
  %162 = add i32 %160, %161, !dbg !364
  store i32 %162, i32* %16, align 4, !dbg !365
  %163 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 0, !dbg !366
  %164 = load double, double* %163, align 16, !dbg !366
  %165 = load float*, float** %11, align 8, !dbg !367
  %166 = load i32, i32* %16, align 4, !dbg !368
  %167 = zext i32 %166 to i64, !dbg !367
  %168 = getelementptr inbounds float, float* %165, i64 %167, !dbg !367
  %169 = load float, float* %168, align 4, !dbg !367
  %170 = fpext float %169 to double, !dbg !367
  %171 = fmul double %164, %170, !dbg !369
  %172 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 0, !dbg !370
  %173 = load double, double* %172, align 16, !dbg !370
  %174 = load float*, float** %12, align 8, !dbg !371
  %175 = load i32, i32* %16, align 4, !dbg !372
  %176 = zext i32 %175 to i64, !dbg !371
  %177 = getelementptr inbounds float, float* %174, i64 %176, !dbg !371
  %178 = load float, float* %177, align 4, !dbg !371
  %179 = fpext float %178 to double, !dbg !371
  %180 = fmul double %173, %179, !dbg !373
  %181 = fsub double %171, %180, !dbg !374
  store double %181, double* %21, align 8, !dbg !375
  %182 = getelementptr inbounds [3 x double], [3 x double]* %29, i64 0, i64 0, !dbg !376
  %183 = load double, double* %182, align 16, !dbg !376
  %184 = load float*, float** %12, align 8, !dbg !377
  %185 = load i32, i32* %16, align 4, !dbg !378
  %186 = zext i32 %185 to i64, !dbg !377
  %187 = getelementptr inbounds float, float* %184, i64 %186, !dbg !377
  %188 = load float, float* %187, align 4, !dbg !377
  %189 = fpext float %188 to double, !dbg !377
  %190 = fmul double %183, %189, !dbg !379
  %191 = getelementptr inbounds [3 x double], [3 x double]* %30, i64 0, i64 0, !dbg !380
  %192 = load double, double* %191, align 16, !dbg !380
  %193 = load float*, float** %11, align 8, !dbg !381
  %194 = load i32, i32* %16, align 4, !dbg !382
  %195 = zext i32 %194 to i64, !dbg !381
  %196 = getelementptr inbounds float, float* %193, i64 %195, !dbg !381
  %197 = load float, float* %196, align 4, !dbg !381
  %198 = fpext float %197 to double, !dbg !381
  %199 = fmul double %192, %198, !dbg !383
  %200 = fadd double %190, %199, !dbg !384
  store double %200, double* %22, align 8, !dbg !385
  %201 = load float*, float** %11, align 8, !dbg !386
  %202 = load i32, i32* %15, align 4, !dbg !387
  %203 = zext i32 %202 to i64, !dbg !386
  %204 = getelementptr inbounds float, float* %201, i64 %203, !dbg !386
  %205 = load float, float* %204, align 4, !dbg !386
  %206 = fpext float %205 to double, !dbg !386
  %207 = load double, double* %21, align 8, !dbg !388
  %208 = fsub double %206, %207, !dbg !389
  %209 = fptrunc double %208 to float, !dbg !386
  %210 = load float*, float** %11, align 8, !dbg !390
  %211 = load i32, i32* %16, align 4, !dbg !391
  %212 = zext i32 %211 to i64, !dbg !390
  %213 = getelementptr inbounds float, float* %210, i64 %212, !dbg !390
  store float %209, float* %213, align 4, !dbg !392
  %214 = load float*, float** %12, align 8, !dbg !393
  %215 = load i32, i32* %15, align 4, !dbg !394
  %216 = zext i32 %215 to i64, !dbg !393
  %217 = getelementptr inbounds float, float* %214, i64 %216, !dbg !393
  %218 = load float, float* %217, align 4, !dbg !393
  %219 = fpext float %218 to double, !dbg !393
  %220 = load double, double* %22, align 8, !dbg !395
  %221 = fsub double %219, %220, !dbg !396
  %222 = fptrunc double %221 to float, !dbg !393
  %223 = load float*, float** %12, align 8, !dbg !397
  %224 = load i32, i32* %16, align 4, !dbg !398
  %225 = zext i32 %224 to i64, !dbg !397
  %226 = getelementptr inbounds float, float* %223, i64 %225, !dbg !397
  store float %222, float* %226, align 4, !dbg !399
  %227 = load double, double* %21, align 8, !dbg !400
  %228 = load float*, float** %11, align 8, !dbg !401
  %229 = load i32, i32* %15, align 4, !dbg !402
  %230 = zext i32 %229 to i64, !dbg !401
  %231 = getelementptr inbounds float, float* %228, i64 %230, !dbg !401
  %232 = load float, float* %231, align 4, !dbg !403
  %233 = fpext float %232 to double, !dbg !403
  %234 = fadd double %233, %227, !dbg !403
  %235 = fptrunc double %234 to float, !dbg !403
  store float %235, float* %231, align 4, !dbg !403
  %236 = load double, double* %22, align 8, !dbg !404
  %237 = load float*, float** %12, align 8, !dbg !405
  %238 = load i32, i32* %15, align 4, !dbg !406
  %239 = zext i32 %238 to i64, !dbg !405
  %240 = getelementptr inbounds float, float* %237, i64 %239, !dbg !405
  %241 = load float, float* %240, align 4, !dbg !407
  %242 = fpext float %241 to double, !dbg !407
  %243 = fadd double %242, %236, !dbg !407
  %244 = fptrunc double %243 to float, !dbg !407
  store float %244, float* %240, align 4, !dbg !407
  %245 = call i32 (...) @checkpoint(), !dbg !408
  br label %246, !dbg !409

; <label>:246:                                    ; preds = %131
  %247 = load i32, i32* %15, align 4, !dbg !410
  %248 = add i32 %247, 1, !dbg !410
  store i32 %248, i32* %15, align 4, !dbg !410
  %249 = load i32, i32* %17, align 4, !dbg !411
  %250 = add i32 %249, 1, !dbg !411
  store i32 %250, i32* %17, align 4, !dbg !411
  br label %127, !dbg !412, !llvm.loop !413

; <label>:251:                                    ; preds = %127
  %252 = call i32 (...) @checkpoint(), !dbg !415
  br label %253, !dbg !416

; <label>:253:                                    ; preds = %251
  %254 = load i32, i32* %18, align 4, !dbg !417
  %255 = load i32, i32* %14, align 4, !dbg !418
  %256 = add i32 %255, %254, !dbg !418
  store i32 %256, i32* %14, align 4, !dbg !418
  br label %113, !dbg !419, !llvm.loop !420

; <label>:257:                                    ; preds = %113
  %258 = load i32, i32* %18, align 4, !dbg !422
  store i32 %258, i32* %19, align 4, !dbg !423
  %259 = call i32 (...) @checkpoint(), !dbg !424
  br label %260, !dbg !425

; <label>:260:                                    ; preds = %257
  %261 = load i32, i32* %18, align 4, !dbg !426
  %262 = shl i32 %261, 1, !dbg !426
  store i32 %262, i32* %18, align 4, !dbg !426
  br label %90, !dbg !427, !llvm.loop !428

; <label>:263:                                    ; preds = %90
  %264 = load i32, i32* %8, align 4, !dbg !430
  %265 = icmp ne i32 %264, 0, !dbg !430
  br i1 %265, label %266, label %297, !dbg !432

; <label>:266:                                    ; preds = %263
  call void @llvm.dbg.declare(metadata double* %31, metadata !433, metadata !DIExpression()), !dbg !435
  %267 = load i32, i32* %7, align 4, !dbg !436
  %268 = uitofp i32 %267 to double, !dbg !437
  store double %268, double* %31, align 8, !dbg !435
  store i32 0, i32* %14, align 4, !dbg !438
  br label %269, !dbg !440

; <label>:269:                                    ; preds = %293, %266
  %270 = load i32, i32* %14, align 4, !dbg !441
  %271 = load i32, i32* %7, align 4, !dbg !443
  %272 = icmp ult i32 %270, %271, !dbg !444
  br i1 %272, label %273, label %296, !dbg !445

; <label>:273:                                    ; preds = %269
  %274 = load double, double* %31, align 8, !dbg !446
  %275 = load float*, float** %11, align 8, !dbg !448
  %276 = load i32, i32* %14, align 4, !dbg !449
  %277 = zext i32 %276 to i64, !dbg !448
  %278 = getelementptr inbounds float, float* %275, i64 %277, !dbg !448
  %279 = load float, float* %278, align 4, !dbg !450
  %280 = fpext float %279 to double, !dbg !450
  %281 = fdiv double %280, %274, !dbg !450
  %282 = fptrunc double %281 to float, !dbg !450
  store float %282, float* %278, align 4, !dbg !450
  %283 = load double, double* %31, align 8, !dbg !451
  %284 = load float*, float** %12, align 8, !dbg !452
  %285 = load i32, i32* %14, align 4, !dbg !453
  %286 = zext i32 %285 to i64, !dbg !452
  %287 = getelementptr inbounds float, float* %284, i64 %286, !dbg !452
  %288 = load float, float* %287, align 4, !dbg !454
  %289 = fpext float %288 to double, !dbg !454
  %290 = fdiv double %289, %283, !dbg !454
  %291 = fptrunc double %290 to float, !dbg !454
  store float %291, float* %287, align 4, !dbg !454
  %292 = call i32 (...) @checkpoint(), !dbg !455
  br label %293, !dbg !456

; <label>:293:                                    ; preds = %273
  %294 = load i32, i32* %14, align 4, !dbg !457
  %295 = add i32 %294, 1, !dbg !457
  store i32 %295, i32* %14, align 4, !dbg !457
  br label %269, !dbg !458, !llvm.loop !459

; <label>:296:                                    ; preds = %269
  br label %297, !dbg !461

; <label>:297:                                    ; preds = %296, %263
  ret void, !dbg !462
}

; Function Attrs: noinline nounwind optnone uwtable
define internal void @CheckPointer(i8*, i8*) #0 !dbg !463 {
  %3 = alloca i8*, align 8
  %4 = alloca i8*, align 8
  store i8* %0, i8** %3, align 8
  call void @llvm.dbg.declare(metadata i8** %3, metadata !469, metadata !DIExpression()), !dbg !470
  store i8* %1, i8** %4, align 8
  call void @llvm.dbg.declare(metadata i8** %4, metadata !471, metadata !DIExpression()), !dbg !472
  %5 = load i8*, i8** %3, align 8, !dbg !473
  %6 = icmp eq i8* %5, null, !dbg !475
  br i1 %6, label %7, label %8, !dbg !476

; <label>:7:                                      ; preds = %2
  call void @exit(i32 1) #5, !dbg !477
  unreachable, !dbg !477

; <label>:8:                                      ; preds = %2
  ret void, !dbg !479
}

; Function Attrs: nounwind
declare dso_local double @sin(double) #4

; Function Attrs: nounwind
declare dso_local double @cos(double) #4

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @main() #0 !dbg !480 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  store i32 128, i32* @MAXSIZE, align 4, !dbg !483
  %2 = call i32 @old_main(), !dbg !484
  store i32 1, i32* @invfft, align 4, !dbg !485
  store i32 256, i32* @MAXSIZE, align 4, !dbg !486
  %3 = call i32 @old_main(), !dbg !487
  ret i32 0, !dbg !488
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @old_main() #0 !dbg !489 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca float*, align 8
  %4 = alloca float*, align 8
  %5 = alloca float*, align 8
  %6 = alloca float*, align 8
  %7 = alloca float*, align 8
  %8 = alloca float*, align 8
  call void @llvm.dbg.declare(metadata i32* %1, metadata !490, metadata !DIExpression()), !dbg !491
  call void @llvm.dbg.declare(metadata i32* %2, metadata !492, metadata !DIExpression()), !dbg !493
  call void @llvm.dbg.declare(metadata float** %3, metadata !494, metadata !DIExpression()), !dbg !495
  call void @llvm.dbg.declare(metadata float** %4, metadata !496, metadata !DIExpression()), !dbg !497
  call void @llvm.dbg.declare(metadata float** %5, metadata !498, metadata !DIExpression()), !dbg !499
  call void @llvm.dbg.declare(metadata float** %6, metadata !500, metadata !DIExpression()), !dbg !501
  call void @llvm.dbg.declare(metadata float** %7, metadata !502, metadata !DIExpression()), !dbg !503
  call void @llvm.dbg.declare(metadata float** %8, metadata !504, metadata !DIExpression()), !dbg !505
  %9 = call i32 (i32, ...) bitcast (i32 (...)* @srand to i32 (i32, ...)*)(i32 1), !dbg !506
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @realin, i32 0, i32 0), float** %3, align 8, !dbg !507
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @imagin, i32 0, i32 0), float** %4, align 8, !dbg !508
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @realout, i32 0, i32 0), float** %5, align 8, !dbg !509
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @imagout, i32 0, i32 0), float** %6, align 8, !dbg !510
  store float* getelementptr inbounds ([16 x float], [16 x float]* @Coeff, i32 0, i32 0), float** %7, align 8, !dbg !511
  store float* getelementptr inbounds ([16 x float], [16 x float]* @Amp, i32 0, i32 0), float** %8, align 8, !dbg !512
  store i32 0, i32* %1, align 4, !dbg !513
  br label %10, !dbg !515

; <label>:10:                                     ; preds = %30, %0
  %11 = load i32, i32* %1, align 4, !dbg !516
  %12 = load i32, i32* @MAXWAVES, align 4, !dbg !518
  %13 = icmp ult i32 %11, %12, !dbg !519
  br i1 %13, label %14, label %33, !dbg !520

; <label>:14:                                     ; preds = %10
  %15 = call i32 (...) @rand(), !dbg !521
  %16 = srem i32 %15, 1000, !dbg !523
  %17 = sitofp i32 %16 to float, !dbg !521
  %18 = load float*, float** %7, align 8, !dbg !524
  %19 = load i32, i32* %1, align 4, !dbg !525
  %20 = zext i32 %19 to i64, !dbg !524
  %21 = getelementptr inbounds float, float* %18, i64 %20, !dbg !524
  store float %17, float* %21, align 4, !dbg !526
  %22 = call i32 (...) @rand(), !dbg !527
  %23 = srem i32 %22, 1000, !dbg !528
  %24 = sitofp i32 %23 to float, !dbg !527
  %25 = load float*, float** %8, align 8, !dbg !529
  %26 = load i32, i32* %1, align 4, !dbg !530
  %27 = zext i32 %26 to i64, !dbg !529
  %28 = getelementptr inbounds float, float* %25, i64 %27, !dbg !529
  store float %24, float* %28, align 4, !dbg !531
  %29 = call i32 (...) @checkpoint(), !dbg !532
  br label %30, !dbg !533

; <label>:30:                                     ; preds = %14
  %31 = load i32, i32* %1, align 4, !dbg !534
  %32 = add i32 %31, 1, !dbg !534
  store i32 %32, i32* %1, align 4, !dbg !534
  br label %10, !dbg !535, !llvm.loop !536

; <label>:33:                                     ; preds = %10
  store i32 0, i32* %1, align 4, !dbg !538
  br label %34, !dbg !540

; <label>:34:                                     ; preds = %114, %33
  %35 = load i32, i32* %1, align 4, !dbg !541
  %36 = load i32, i32* @MAXSIZE, align 4, !dbg !543
  %37 = icmp ult i32 %35, %36, !dbg !544
  br i1 %37, label %38, label %117, !dbg !545

; <label>:38:                                     ; preds = %34
  %39 = load float*, float** %3, align 8, !dbg !546
  %40 = load i32, i32* %1, align 4, !dbg !548
  %41 = zext i32 %40 to i64, !dbg !546
  %42 = getelementptr inbounds float, float* %39, i64 %41, !dbg !546
  store float 0.000000e+00, float* %42, align 4, !dbg !549
  store i32 0, i32* %2, align 4, !dbg !550
  br label %43, !dbg !552

; <label>:43:                                     ; preds = %109, %38
  %44 = load i32, i32* %2, align 4, !dbg !553
  %45 = load i32, i32* @MAXWAVES, align 4, !dbg !555
  %46 = icmp ult i32 %44, %45, !dbg !556
  br i1 %46, label %47, label %112, !dbg !557

; <label>:47:                                     ; preds = %43
  %48 = call i32 (...) @rand(), !dbg !558
  %49 = srem i32 %48, 2, !dbg !561
  %50 = icmp ne i32 %49, 0, !dbg !561
  br i1 %50, label %51, label %77, !dbg !562

; <label>:51:                                     ; preds = %47
  %52 = load float*, float** %7, align 8, !dbg !563
  %53 = load i32, i32* %2, align 4, !dbg !565
  %54 = zext i32 %53 to i64, !dbg !563
  %55 = getelementptr inbounds float, float* %52, i64 %54, !dbg !563
  %56 = load float, float* %55, align 4, !dbg !563
  %57 = fpext float %56 to double, !dbg !563
  %58 = load float*, float** %8, align 8, !dbg !566
  %59 = load i32, i32* %2, align 4, !dbg !567
  %60 = zext i32 %59 to i64, !dbg !566
  %61 = getelementptr inbounds float, float* %58, i64 %60, !dbg !566
  %62 = load float, float* %61, align 4, !dbg !566
  %63 = load i32, i32* %1, align 4, !dbg !568
  %64 = uitofp i32 %63 to float, !dbg !568
  %65 = fmul float %62, %64, !dbg !569
  %66 = fpext float %65 to double, !dbg !566
  %67 = call double @cos(double %66) #6, !dbg !570
  %68 = fmul double %57, %67, !dbg !571
  %69 = load float*, float** %3, align 8, !dbg !572
  %70 = load i32, i32* %1, align 4, !dbg !573
  %71 = zext i32 %70 to i64, !dbg !572
  %72 = getelementptr inbounds float, float* %69, i64 %71, !dbg !572
  %73 = load float, float* %72, align 4, !dbg !574
  %74 = fpext float %73 to double, !dbg !574
  %75 = fadd double %74, %68, !dbg !574
  %76 = fptrunc double %75 to float, !dbg !574
  store float %76, float* %72, align 4, !dbg !574
  br label %103, !dbg !575

; <label>:77:                                     ; preds = %47
  %78 = load float*, float** %7, align 8, !dbg !576
  %79 = load i32, i32* %2, align 4, !dbg !578
  %80 = zext i32 %79 to i64, !dbg !576
  %81 = getelementptr inbounds float, float* %78, i64 %80, !dbg !576
  %82 = load float, float* %81, align 4, !dbg !576
  %83 = fpext float %82 to double, !dbg !576
  %84 = load float*, float** %8, align 8, !dbg !579
  %85 = load i32, i32* %2, align 4, !dbg !580
  %86 = zext i32 %85 to i64, !dbg !579
  %87 = getelementptr inbounds float, float* %84, i64 %86, !dbg !579
  %88 = load float, float* %87, align 4, !dbg !579
  %89 = load i32, i32* %1, align 4, !dbg !581
  %90 = uitofp i32 %89 to float, !dbg !581
  %91 = fmul float %88, %90, !dbg !582
  %92 = fpext float %91 to double, !dbg !579
  %93 = call double @sin(double %92) #6, !dbg !583
  %94 = fmul double %83, %93, !dbg !584
  %95 = load float*, float** %3, align 8, !dbg !585
  %96 = load i32, i32* %1, align 4, !dbg !586
  %97 = zext i32 %96 to i64, !dbg !585
  %98 = getelementptr inbounds float, float* %95, i64 %97, !dbg !585
  %99 = load float, float* %98, align 4, !dbg !587
  %100 = fpext float %99 to double, !dbg !587
  %101 = fadd double %100, %94, !dbg !587
  %102 = fptrunc double %101 to float, !dbg !587
  store float %102, float* %98, align 4, !dbg !587
  br label %103

; <label>:103:                                    ; preds = %77, %51
  %104 = load float*, float** %4, align 8, !dbg !588
  %105 = load i32, i32* %1, align 4, !dbg !589
  %106 = zext i32 %105 to i64, !dbg !588
  %107 = getelementptr inbounds float, float* %104, i64 %106, !dbg !588
  store float 0.000000e+00, float* %107, align 4, !dbg !590
  %108 = call i32 (...) @checkpoint(), !dbg !591
  br label %109, !dbg !592

; <label>:109:                                    ; preds = %103
  %110 = load i32, i32* %2, align 4, !dbg !593
  %111 = add i32 %110, 1, !dbg !593
  store i32 %111, i32* %2, align 4, !dbg !593
  br label %43, !dbg !594, !llvm.loop !595

; <label>:112:                                    ; preds = %43
  %113 = call i32 (...) @checkpoint(), !dbg !597
  br label %114, !dbg !598

; <label>:114:                                    ; preds = %112
  %115 = load i32, i32* %1, align 4, !dbg !599
  %116 = add i32 %115, 1, !dbg !599
  store i32 %116, i32* %1, align 4, !dbg !599
  br label %34, !dbg !600, !llvm.loop !601

; <label>:117:                                    ; preds = %34
  %118 = load i32, i32* @MAXSIZE, align 4, !dbg !603
  %119 = load i32, i32* @invfft, align 4, !dbg !604
  %120 = load float*, float** %3, align 8, !dbg !605
  %121 = load float*, float** %4, align 8, !dbg !606
  %122 = load float*, float** %5, align 8, !dbg !607
  %123 = load float*, float** %6, align 8, !dbg !608
  call void @fft_float(i32 %118, i32 %119, float* %120, float* %121, float* %122, float* %123), !dbg !609
  %124 = call i32 (...) @checkpoint(), !dbg !610
  ret i32 0, !dbg !611
}

declare dso_local i32 @srand(...) #2

declare dso_local i32 @rand(...) #2

attributes #0 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn }
attributes #6 = { nounwind }

!llvm.dbg.cu = !{!31, !35, !2}
!llvm.ident = !{!37, !37, !37}
!llvm.module.flags = !{!38, !39, !40}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "invfft", scope: !2, file: !3, line: 5, type: !30, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5)
!3 = !DIFile(filename: "main.c", directory: "/home/user/Desktop/alfred/samples/fft")
!4 = !{}
!5 = !{!0, !6, !9, !11, !17, !19, !21, !23, !28}
!6 = !DIGlobalVariableExpression(var: !7, expr: !DIExpression())
!7 = distinct !DIGlobalVariable(name: "MAXWAVES", scope: !2, file: !3, line: 7, type: !8, isLocal: false, isDefinition: true)
!8 = !DIBasicType(name: "unsigned int", size: 32, encoding: DW_ATE_unsigned)
!9 = !DIGlobalVariableExpression(var: !10, expr: !DIExpression())
!10 = distinct !DIGlobalVariable(name: "MAXSIZE", scope: !2, file: !3, line: 6, type: !8, isLocal: false, isDefinition: true)
!11 = !DIGlobalVariableExpression(var: !12, expr: !DIExpression())
!12 = distinct !DIGlobalVariable(name: "realin", scope: !2, file: !3, line: 8, type: !13, isLocal: true, isDefinition: true)
!13 = !DICompositeType(tag: DW_TAG_array_type, baseType: !14, size: 32768, elements: !15)
!14 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!15 = !{!16}
!16 = !DISubrange(count: 1024)
!17 = !DIGlobalVariableExpression(var: !18, expr: !DIExpression())
!18 = distinct !DIGlobalVariable(name: "imagin", scope: !2, file: !3, line: 9, type: !13, isLocal: true, isDefinition: true)
!19 = !DIGlobalVariableExpression(var: !20, expr: !DIExpression())
!20 = distinct !DIGlobalVariable(name: "realout", scope: !2, file: !3, line: 10, type: !13, isLocal: true, isDefinition: true)
!21 = !DIGlobalVariableExpression(var: !22, expr: !DIExpression())
!22 = distinct !DIGlobalVariable(name: "imagout", scope: !2, file: !3, line: 11, type: !13, isLocal: true, isDefinition: true)
!23 = !DIGlobalVariableExpression(var: !24, expr: !DIExpression())
!24 = distinct !DIGlobalVariable(name: "Coeff", scope: !2, file: !3, line: 12, type: !25, isLocal: true, isDefinition: true)
!25 = !DICompositeType(tag: DW_TAG_array_type, baseType: !14, size: 512, elements: !26)
!26 = !{!27}
!27 = !DISubrange(count: 16)
!28 = !DIGlobalVariableExpression(var: !29, expr: !DIExpression())
!29 = distinct !DIGlobalVariable(name: "Amp", scope: !2, file: !3, line: 13, type: !25, isLocal: true, isDefinition: true)
!30 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!31 = distinct !DICompileUnit(language: DW_LANG_C99, file: !32, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, retainedTypes: !33)
!32 = !DIFile(filename: "fftmisc.c", directory: "/home/user/Desktop/alfred/samples/fft")
!33 = !{!34}
!34 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!35 = distinct !DICompileUnit(language: DW_LANG_C99, file: !36, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, retainedTypes: !33)
!36 = !DIFile(filename: "fourierf.c", directory: "/home/user/Desktop/alfred/samples/fft")
!37 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
!38 = !{i32 2, !"Dwarf Version", i32 4}
!39 = !{i32 2, !"Debug Info Version", i32 3}
!40 = !{i32 1, !"wchar_size", i32 4}
!41 = distinct !DISubprogram(name: "IsPowerOfTwo", scope: !32, file: !32, line: 35, type: !42, isLocal: false, isDefinition: true, scopeLine: 36, flags: DIFlagPrototyped, isOptimized: false, unit: !31, retainedNodes: !4)
!42 = !DISubroutineType(types: !43)
!43 = !{!30, !8}
!44 = !DILocalVariable(name: "x", arg: 1, scope: !41, file: !32, line: 35, type: !8)
!45 = !DILocation(line: 35, column: 29, scope: !41)
!46 = !DILocation(line: 37, column: 10, scope: !47)
!47 = distinct !DILexicalBlock(scope: !41, file: !32, line: 37, column: 10)
!48 = !DILocation(line: 37, column: 12, scope: !47)
!49 = !DILocation(line: 37, column: 10, scope: !41)
!50 = !DILocation(line: 38, column: 9, scope: !47)
!51 = !DILocation(line: 40, column: 10, scope: !52)
!52 = distinct !DILexicalBlock(scope: !41, file: !32, line: 40, column: 10)
!53 = !DILocation(line: 40, column: 15, scope: !52)
!54 = !DILocation(line: 40, column: 16, scope: !52)
!55 = !DILocation(line: 40, column: 12, scope: !52)
!56 = !DILocation(line: 40, column: 10, scope: !41)
!57 = !DILocation(line: 41, column: 9, scope: !52)
!58 = !DILocation(line: 42, column: 5, scope: !41)
!59 = !DILocation(line: 43, column: 5, scope: !41)
!60 = !DILocation(line: 44, column: 1, scope: !41)
!61 = distinct !DISubprogram(name: "NumberOfBitsNeeded", scope: !32, file: !32, line: 47, type: !62, isLocal: false, isDefinition: true, scopeLine: 48, flags: DIFlagPrototyped, isOptimized: false, unit: !31, retainedNodes: !4)
!62 = !DISubroutineType(types: !63)
!63 = !{!8, !8}
!64 = !DILocalVariable(name: "PowerOfTwo", arg: 1, scope: !61, file: !32, line: 47, type: !8)
!65 = !DILocation(line: 47, column: 40, scope: !61)
!66 = !DILocalVariable(name: "i", scope: !61, file: !32, line: 49, type: !8)
!67 = !DILocation(line: 49, column: 14, scope: !61)
!68 = !DILocation(line: 51, column: 10, scope: !69)
!69 = distinct !DILexicalBlock(scope: !61, file: !32, line: 51, column: 10)
!70 = !DILocation(line: 51, column: 21, scope: !69)
!71 = !DILocation(line: 51, column: 10, scope: !61)
!72 = !DILocation(line: 54, column: 13, scope: !73)
!73 = distinct !DILexicalBlock(scope: !69, file: !32, line: 52, column: 5)
!74 = !DILocation(line: 53, column: 9, scope: !73)
!75 = !DILocation(line: 56, column: 9, scope: !73)
!76 = !DILocation(line: 59, column: 12, scope: !77)
!77 = distinct !DILexicalBlock(scope: !61, file: !32, line: 59, column: 5)
!78 = !DILocation(line: 59, column: 11, scope: !77)
!79 = !DILocation(line: 61, column: 14, scope: !80)
!80 = distinct !DILexicalBlock(scope: !81, file: !32, line: 61, column: 14)
!81 = distinct !DILexicalBlock(scope: !82, file: !32, line: 60, column: 5)
!82 = distinct !DILexicalBlock(scope: !77, file: !32, line: 59, column: 5)
!83 = !DILocation(line: 61, column: 33, scope: !80)
!84 = !DILocation(line: 61, column: 30, scope: !80)
!85 = !DILocation(line: 61, column: 25, scope: !80)
!86 = !DILocation(line: 61, column: 14, scope: !81)
!87 = !DILocation(line: 62, column: 20, scope: !88)
!88 = distinct !DILexicalBlock(scope: !80, file: !32, line: 61, column: 38)
!89 = !DILocation(line: 62, column: 13, scope: !88)
!90 = !DILocation(line: 64, column: 9, scope: !81)
!91 = !DILocation(line: 65, column: 5, scope: !81)
!92 = !DILocation(line: 59, column: 19, scope: !82)
!93 = !DILocation(line: 59, column: 5, scope: !82)
!94 = distinct !{!94, !95, !96}
!95 = !DILocation(line: 59, column: 5, scope: !77)
!96 = !DILocation(line: 65, column: 5, scope: !77)
!97 = distinct !DISubprogram(name: "ReverseBits", scope: !32, file: !32, line: 70, type: !98, isLocal: false, isDefinition: true, scopeLine: 71, flags: DIFlagPrototyped, isOptimized: false, unit: !31, retainedNodes: !4)
!98 = !DISubroutineType(types: !99)
!99 = !{!8, !8, !8}
!100 = !DILocalVariable(name: "index", arg: 1, scope: !97, file: !32, line: 70, type: !8)
!101 = !DILocation(line: 70, column: 33, scope: !97)
!102 = !DILocalVariable(name: "NumBits", arg: 2, scope: !97, file: !32, line: 70, type: !8)
!103 = !DILocation(line: 70, column: 49, scope: !97)
!104 = !DILocalVariable(name: "i", scope: !97, file: !32, line: 72, type: !8)
!105 = !DILocation(line: 72, column: 14, scope: !97)
!106 = !DILocalVariable(name: "rev", scope: !97, file: !32, line: 72, type: !8)
!107 = !DILocation(line: 72, column: 17, scope: !97)
!108 = !DILocation(line: 74, column: 16, scope: !109)
!109 = distinct !DILexicalBlock(scope: !97, file: !32, line: 74, column: 5)
!110 = !DILocation(line: 74, column: 12, scope: !109)
!111 = !DILocation(line: 74, column: 11, scope: !109)
!112 = !DILocation(line: 74, column: 20, scope: !113)
!113 = distinct !DILexicalBlock(scope: !109, file: !32, line: 74, column: 5)
!114 = !DILocation(line: 74, column: 24, scope: !113)
!115 = !DILocation(line: 74, column: 22, scope: !113)
!116 = !DILocation(line: 74, column: 5, scope: !109)
!117 = !DILocation(line: 76, column: 16, scope: !118)
!118 = distinct !DILexicalBlock(scope: !113, file: !32, line: 75, column: 5)
!119 = !DILocation(line: 76, column: 20, scope: !118)
!120 = !DILocation(line: 76, column: 29, scope: !118)
!121 = !DILocation(line: 76, column: 35, scope: !118)
!122 = !DILocation(line: 76, column: 26, scope: !118)
!123 = !DILocation(line: 76, column: 13, scope: !118)
!124 = !DILocation(line: 77, column: 15, scope: !118)
!125 = !DILocation(line: 78, column: 9, scope: !118)
!126 = !DILocation(line: 79, column: 5, scope: !118)
!127 = !DILocation(line: 74, column: 34, scope: !113)
!128 = !DILocation(line: 74, column: 5, scope: !113)
!129 = distinct !{!129, !116, !130}
!130 = !DILocation(line: 79, column: 5, scope: !109)
!131 = !DILocation(line: 81, column: 12, scope: !97)
!132 = !DILocation(line: 81, column: 5, scope: !97)
!133 = distinct !DISubprogram(name: "Index_to_frequency", scope: !32, file: !32, line: 85, type: !134, isLocal: false, isDefinition: true, scopeLine: 86, flags: DIFlagPrototyped, isOptimized: false, unit: !31, retainedNodes: !4)
!134 = !DISubroutineType(types: !135)
!135 = !{!34, !8, !8}
!136 = !DILocalVariable(name: "NumSamples", arg: 1, scope: !133, file: !32, line: 85, type: !8)
!137 = !DILocation(line: 85, column: 38, scope: !133)
!138 = !DILocalVariable(name: "Index", arg: 2, scope: !133, file: !32, line: 85, type: !8)
!139 = !DILocation(line: 85, column: 59, scope: !133)
!140 = !DILocation(line: 87, column: 10, scope: !141)
!141 = distinct !DILexicalBlock(scope: !133, file: !32, line: 87, column: 10)
!142 = !DILocation(line: 87, column: 19, scope: !141)
!143 = !DILocation(line: 87, column: 16, scope: !141)
!144 = !DILocation(line: 87, column: 10, scope: !133)
!145 = !DILocation(line: 88, column: 9, scope: !146)
!146 = distinct !DILexicalBlock(scope: !141, file: !32, line: 87, column: 32)
!147 = !DILocation(line: 90, column: 15, scope: !148)
!148 = distinct !DILexicalBlock(scope: !141, file: !32, line: 90, column: 15)
!149 = !DILocation(line: 90, column: 24, scope: !148)
!150 = !DILocation(line: 90, column: 34, scope: !148)
!151 = !DILocation(line: 90, column: 21, scope: !148)
!152 = !DILocation(line: 90, column: 15, scope: !141)
!153 = !DILocation(line: 91, column: 24, scope: !154)
!154 = distinct !DILexicalBlock(scope: !148, file: !32, line: 90, column: 39)
!155 = !DILocation(line: 91, column: 16, scope: !154)
!156 = !DILocation(line: 91, column: 40, scope: !154)
!157 = !DILocation(line: 91, column: 32, scope: !154)
!158 = !DILocation(line: 91, column: 30, scope: !154)
!159 = !DILocation(line: 91, column: 9, scope: !154)
!160 = !DILocation(line: 93, column: 22, scope: !133)
!161 = !DILocation(line: 93, column: 33, scope: !133)
!162 = !DILocation(line: 93, column: 32, scope: !133)
!163 = !DILocation(line: 93, column: 13, scope: !133)
!164 = !DILocation(line: 93, column: 12, scope: !133)
!165 = !DILocation(line: 93, column: 50, scope: !133)
!166 = !DILocation(line: 93, column: 42, scope: !133)
!167 = !DILocation(line: 93, column: 40, scope: !133)
!168 = !DILocation(line: 93, column: 5, scope: !133)
!169 = !DILocation(line: 94, column: 1, scope: !133)
!170 = distinct !DISubprogram(name: "fft_float", scope: !36, file: !36, line: 36, type: !171, isLocal: false, isDefinition: true, scopeLine: 43, flags: DIFlagPrototyped, isOptimized: false, unit: !35, retainedNodes: !4)
!171 = !DISubroutineType(types: !172)
!172 = !{null, !8, !30, !173, !173, !173, !173}
!173 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !14, size: 64)
!174 = !DILocalVariable(name: "NumSamples", arg: 1, scope: !170, file: !36, line: 37, type: !8)
!175 = !DILocation(line: 37, column: 15, scope: !170)
!176 = !DILocalVariable(name: "InverseTransform", arg: 2, scope: !170, file: !36, line: 38, type: !30)
!177 = !DILocation(line: 38, column: 15, scope: !170)
!178 = !DILocalVariable(name: "RealIn", arg: 3, scope: !170, file: !36, line: 39, type: !173)
!179 = !DILocation(line: 39, column: 15, scope: !170)
!180 = !DILocalVariable(name: "ImagIn", arg: 4, scope: !170, file: !36, line: 40, type: !173)
!181 = !DILocation(line: 40, column: 15, scope: !170)
!182 = !DILocalVariable(name: "RealOut", arg: 5, scope: !170, file: !36, line: 41, type: !173)
!183 = !DILocation(line: 41, column: 15, scope: !170)
!184 = !DILocalVariable(name: "ImagOut", arg: 6, scope: !170, file: !36, line: 42, type: !173)
!185 = !DILocation(line: 42, column: 15, scope: !170)
!186 = !DILocalVariable(name: "NumBits", scope: !170, file: !36, line: 44, type: !8)
!187 = !DILocation(line: 44, column: 14, scope: !170)
!188 = !DILocalVariable(name: "i", scope: !170, file: !36, line: 45, type: !8)
!189 = !DILocation(line: 45, column: 14, scope: !170)
!190 = !DILocalVariable(name: "j", scope: !170, file: !36, line: 45, type: !8)
!191 = !DILocation(line: 45, column: 17, scope: !170)
!192 = !DILocalVariable(name: "k", scope: !170, file: !36, line: 45, type: !8)
!193 = !DILocation(line: 45, column: 20, scope: !170)
!194 = !DILocalVariable(name: "n", scope: !170, file: !36, line: 45, type: !8)
!195 = !DILocation(line: 45, column: 23, scope: !170)
!196 = !DILocalVariable(name: "BlockSize", scope: !170, file: !36, line: 46, type: !8)
!197 = !DILocation(line: 46, column: 14, scope: !170)
!198 = !DILocalVariable(name: "BlockEnd", scope: !170, file: !36, line: 46, type: !8)
!199 = !DILocation(line: 46, column: 25, scope: !170)
!200 = !DILocalVariable(name: "angle_numerator", scope: !170, file: !36, line: 48, type: !34)
!201 = !DILocation(line: 48, column: 12, scope: !170)
!202 = !DILocalVariable(name: "tr", scope: !170, file: !36, line: 49, type: !34)
!203 = !DILocation(line: 49, column: 12, scope: !170)
!204 = !DILocalVariable(name: "ti", scope: !170, file: !36, line: 49, type: !34)
!205 = !DILocation(line: 49, column: 16, scope: !170)
!206 = !DILocation(line: 51, column: 24, scope: !207)
!207 = distinct !DILexicalBlock(scope: !170, file: !36, line: 51, column: 10)
!208 = !DILocation(line: 51, column: 11, scope: !207)
!209 = !DILocation(line: 51, column: 10, scope: !170)
!210 = !DILocation(line: 58, column: 9, scope: !211)
!211 = distinct !DILexicalBlock(scope: !207, file: !36, line: 52, column: 5)
!212 = !DILocation(line: 61, column: 10, scope: !213)
!213 = distinct !DILexicalBlock(scope: !170, file: !36, line: 61, column: 10)
!214 = !DILocation(line: 61, column: 10, scope: !170)
!215 = !DILocation(line: 62, column: 28, scope: !213)
!216 = !DILocation(line: 62, column: 27, scope: !213)
!217 = !DILocation(line: 62, column: 25, scope: !213)
!218 = !DILocation(line: 62, column: 9, scope: !213)
!219 = !DILocation(line: 64, column: 5, scope: !170)
!220 = !DILocation(line: 65, column: 5, scope: !170)
!221 = !DILocation(line: 66, column: 5, scope: !170)
!222 = !DILocation(line: 68, column: 36, scope: !170)
!223 = !DILocation(line: 68, column: 15, scope: !170)
!224 = !DILocation(line: 68, column: 13, scope: !170)
!225 = !DILocation(line: 74, column: 12, scope: !226)
!226 = distinct !DILexicalBlock(scope: !170, file: !36, line: 74, column: 5)
!227 = !DILocation(line: 74, column: 11, scope: !226)
!228 = !DILocation(line: 74, column: 16, scope: !229)
!229 = distinct !DILexicalBlock(scope: !226, file: !36, line: 74, column: 5)
!230 = !DILocation(line: 74, column: 20, scope: !229)
!231 = !DILocation(line: 74, column: 18, scope: !229)
!232 = !DILocation(line: 74, column: 5, scope: !226)
!233 = !DILocation(line: 76, column: 27, scope: !234)
!234 = distinct !DILexicalBlock(scope: !229, file: !36, line: 75, column: 5)
!235 = !DILocation(line: 76, column: 30, scope: !234)
!236 = !DILocation(line: 76, column: 13, scope: !234)
!237 = !DILocation(line: 76, column: 11, scope: !234)
!238 = !DILocation(line: 77, column: 22, scope: !234)
!239 = !DILocation(line: 77, column: 29, scope: !234)
!240 = !DILocation(line: 77, column: 9, scope: !234)
!241 = !DILocation(line: 77, column: 17, scope: !234)
!242 = !DILocation(line: 77, column: 20, scope: !234)
!243 = !DILocation(line: 78, column: 23, scope: !234)
!244 = !DILocation(line: 78, column: 30, scope: !234)
!245 = !DILocation(line: 78, column: 22, scope: !234)
!246 = !DILocation(line: 78, column: 47, scope: !234)
!247 = !DILocation(line: 78, column: 54, scope: !234)
!248 = !DILocation(line: 78, column: 9, scope: !234)
!249 = !DILocation(line: 78, column: 17, scope: !234)
!250 = !DILocation(line: 78, column: 20, scope: !234)
!251 = !DILocation(line: 79, column: 9, scope: !234)
!252 = !DILocation(line: 80, column: 5, scope: !234)
!253 = !DILocation(line: 74, column: 33, scope: !229)
!254 = !DILocation(line: 74, column: 5, scope: !229)
!255 = distinct !{!255, !232, !256}
!256 = !DILocation(line: 80, column: 5, scope: !226)
!257 = !DILocation(line: 86, column: 14, scope: !170)
!258 = !DILocation(line: 87, column: 21, scope: !259)
!259 = distinct !DILexicalBlock(scope: !170, file: !36, line: 87, column: 5)
!260 = !DILocation(line: 87, column: 11, scope: !259)
!261 = !DILocation(line: 87, column: 26, scope: !262)
!262 = distinct !DILexicalBlock(scope: !259, file: !36, line: 87, column: 5)
!263 = !DILocation(line: 87, column: 39, scope: !262)
!264 = !DILocation(line: 87, column: 36, scope: !262)
!265 = !DILocation(line: 87, column: 5, scope: !259)
!266 = !DILocalVariable(name: "delta_angle", scope: !267, file: !36, line: 89, type: !34)
!267 = distinct !DILexicalBlock(scope: !262, file: !36, line: 88, column: 5)
!268 = !DILocation(line: 89, column: 16, scope: !267)
!269 = !DILocation(line: 89, column: 30, scope: !267)
!270 = !DILocation(line: 89, column: 56, scope: !267)
!271 = !DILocation(line: 89, column: 48, scope: !267)
!272 = !DILocation(line: 89, column: 46, scope: !267)
!273 = !DILocalVariable(name: "sm2", scope: !267, file: !36, line: 90, type: !34)
!274 = !DILocation(line: 90, column: 16, scope: !267)
!275 = !DILocation(line: 90, column: 33, scope: !267)
!276 = !DILocation(line: 90, column: 31, scope: !267)
!277 = !DILocation(line: 90, column: 22, scope: !267)
!278 = !DILocalVariable(name: "sm1", scope: !267, file: !36, line: 91, type: !34)
!279 = !DILocation(line: 91, column: 16, scope: !267)
!280 = !DILocation(line: 91, column: 29, scope: !267)
!281 = !DILocation(line: 91, column: 28, scope: !267)
!282 = !DILocation(line: 91, column: 22, scope: !267)
!283 = !DILocalVariable(name: "cm2", scope: !267, file: !36, line: 92, type: !34)
!284 = !DILocation(line: 92, column: 16, scope: !267)
!285 = !DILocation(line: 92, column: 33, scope: !267)
!286 = !DILocation(line: 92, column: 31, scope: !267)
!287 = !DILocation(line: 92, column: 22, scope: !267)
!288 = !DILocalVariable(name: "cm1", scope: !267, file: !36, line: 93, type: !34)
!289 = !DILocation(line: 93, column: 16, scope: !267)
!290 = !DILocation(line: 93, column: 29, scope: !267)
!291 = !DILocation(line: 93, column: 28, scope: !267)
!292 = !DILocation(line: 93, column: 22, scope: !267)
!293 = !DILocalVariable(name: "w", scope: !267, file: !36, line: 94, type: !34)
!294 = !DILocation(line: 94, column: 16, scope: !267)
!295 = !DILocation(line: 94, column: 24, scope: !267)
!296 = !DILocation(line: 94, column: 22, scope: !267)
!297 = !DILocalVariable(name: "ar", scope: !267, file: !36, line: 95, type: !298)
!298 = !DICompositeType(tag: DW_TAG_array_type, baseType: !34, size: 192, elements: !299)
!299 = !{!300}
!300 = !DISubrange(count: 3)
!301 = !DILocation(line: 95, column: 16, scope: !267)
!302 = !DILocalVariable(name: "ai", scope: !267, file: !36, line: 95, type: !298)
!303 = !DILocation(line: 95, column: 23, scope: !267)
!304 = !DILocation(line: 97, column: 16, scope: !305)
!305 = distinct !DILexicalBlock(scope: !267, file: !36, line: 97, column: 9)
!306 = !DILocation(line: 97, column: 15, scope: !305)
!307 = !DILocation(line: 97, column: 20, scope: !308)
!308 = distinct !DILexicalBlock(scope: !305, file: !36, line: 97, column: 9)
!309 = !DILocation(line: 97, column: 24, scope: !308)
!310 = !DILocation(line: 97, column: 22, scope: !308)
!311 = !DILocation(line: 97, column: 9, scope: !305)
!312 = !DILocation(line: 99, column: 21, scope: !313)
!313 = distinct !DILexicalBlock(scope: !308, file: !36, line: 98, column: 9)
!314 = !DILocation(line: 99, column: 13, scope: !313)
!315 = !DILocation(line: 99, column: 19, scope: !313)
!316 = !DILocation(line: 100, column: 21, scope: !313)
!317 = !DILocation(line: 100, column: 13, scope: !313)
!318 = !DILocation(line: 100, column: 19, scope: !313)
!319 = !DILocation(line: 102, column: 21, scope: !313)
!320 = !DILocation(line: 102, column: 13, scope: !313)
!321 = !DILocation(line: 102, column: 19, scope: !313)
!322 = !DILocation(line: 103, column: 21, scope: !313)
!323 = !DILocation(line: 103, column: 13, scope: !313)
!324 = !DILocation(line: 103, column: 19, scope: !313)
!325 = !DILocation(line: 105, column: 21, scope: !326)
!326 = distinct !DILexicalBlock(scope: !313, file: !36, line: 105, column: 13)
!327 = !DILocation(line: 105, column: 20, scope: !326)
!328 = !DILocation(line: 105, column: 25, scope: !326)
!329 = !DILocation(line: 105, column: 19, scope: !326)
!330 = !DILocation(line: 105, column: 29, scope: !331)
!331 = distinct !DILexicalBlock(scope: !326, file: !36, line: 105, column: 13)
!332 = !DILocation(line: 105, column: 33, scope: !331)
!333 = !DILocation(line: 105, column: 31, scope: !331)
!334 = !DILocation(line: 105, column: 13, scope: !326)
!335 = !DILocation(line: 107, column: 25, scope: !336)
!336 = distinct !DILexicalBlock(scope: !331, file: !36, line: 106, column: 13)
!337 = !DILocation(line: 107, column: 27, scope: !336)
!338 = !DILocation(line: 107, column: 26, scope: !336)
!339 = !DILocation(line: 107, column: 35, scope: !336)
!340 = !DILocation(line: 107, column: 33, scope: !336)
!341 = !DILocation(line: 107, column: 17, scope: !336)
!342 = !DILocation(line: 107, column: 23, scope: !336)
!343 = !DILocation(line: 108, column: 25, scope: !336)
!344 = !DILocation(line: 108, column: 17, scope: !336)
!345 = !DILocation(line: 108, column: 23, scope: !336)
!346 = !DILocation(line: 109, column: 25, scope: !336)
!347 = !DILocation(line: 109, column: 17, scope: !336)
!348 = !DILocation(line: 109, column: 23, scope: !336)
!349 = !DILocation(line: 111, column: 25, scope: !336)
!350 = !DILocation(line: 111, column: 27, scope: !336)
!351 = !DILocation(line: 111, column: 26, scope: !336)
!352 = !DILocation(line: 111, column: 35, scope: !336)
!353 = !DILocation(line: 111, column: 33, scope: !336)
!354 = !DILocation(line: 111, column: 17, scope: !336)
!355 = !DILocation(line: 111, column: 23, scope: !336)
!356 = !DILocation(line: 112, column: 25, scope: !336)
!357 = !DILocation(line: 112, column: 17, scope: !336)
!358 = !DILocation(line: 112, column: 23, scope: !336)
!359 = !DILocation(line: 113, column: 25, scope: !336)
!360 = !DILocation(line: 113, column: 17, scope: !336)
!361 = !DILocation(line: 113, column: 23, scope: !336)
!362 = !DILocation(line: 115, column: 21, scope: !336)
!363 = !DILocation(line: 115, column: 25, scope: !336)
!364 = !DILocation(line: 115, column: 23, scope: !336)
!365 = !DILocation(line: 115, column: 19, scope: !336)
!366 = !DILocation(line: 116, column: 22, scope: !336)
!367 = !DILocation(line: 116, column: 28, scope: !336)
!368 = !DILocation(line: 116, column: 36, scope: !336)
!369 = !DILocation(line: 116, column: 27, scope: !336)
!370 = !DILocation(line: 116, column: 41, scope: !336)
!371 = !DILocation(line: 116, column: 47, scope: !336)
!372 = !DILocation(line: 116, column: 55, scope: !336)
!373 = !DILocation(line: 116, column: 46, scope: !336)
!374 = !DILocation(line: 116, column: 39, scope: !336)
!375 = !DILocation(line: 116, column: 20, scope: !336)
!376 = !DILocation(line: 117, column: 22, scope: !336)
!377 = !DILocation(line: 117, column: 28, scope: !336)
!378 = !DILocation(line: 117, column: 36, scope: !336)
!379 = !DILocation(line: 117, column: 27, scope: !336)
!380 = !DILocation(line: 117, column: 41, scope: !336)
!381 = !DILocation(line: 117, column: 47, scope: !336)
!382 = !DILocation(line: 117, column: 55, scope: !336)
!383 = !DILocation(line: 117, column: 46, scope: !336)
!384 = !DILocation(line: 117, column: 39, scope: !336)
!385 = !DILocation(line: 117, column: 20, scope: !336)
!386 = !DILocation(line: 119, column: 30, scope: !336)
!387 = !DILocation(line: 119, column: 38, scope: !336)
!388 = !DILocation(line: 119, column: 43, scope: !336)
!389 = !DILocation(line: 119, column: 41, scope: !336)
!390 = !DILocation(line: 119, column: 17, scope: !336)
!391 = !DILocation(line: 119, column: 25, scope: !336)
!392 = !DILocation(line: 119, column: 28, scope: !336)
!393 = !DILocation(line: 120, column: 30, scope: !336)
!394 = !DILocation(line: 120, column: 38, scope: !336)
!395 = !DILocation(line: 120, column: 43, scope: !336)
!396 = !DILocation(line: 120, column: 41, scope: !336)
!397 = !DILocation(line: 120, column: 17, scope: !336)
!398 = !DILocation(line: 120, column: 25, scope: !336)
!399 = !DILocation(line: 120, column: 28, scope: !336)
!400 = !DILocation(line: 122, column: 31, scope: !336)
!401 = !DILocation(line: 122, column: 17, scope: !336)
!402 = !DILocation(line: 122, column: 25, scope: !336)
!403 = !DILocation(line: 122, column: 28, scope: !336)
!404 = !DILocation(line: 123, column: 31, scope: !336)
!405 = !DILocation(line: 123, column: 17, scope: !336)
!406 = !DILocation(line: 123, column: 25, scope: !336)
!407 = !DILocation(line: 123, column: 28, scope: !336)
!408 = !DILocation(line: 124, column: 17, scope: !336)
!409 = !DILocation(line: 125, column: 13, scope: !336)
!410 = !DILocation(line: 105, column: 44, scope: !331)
!411 = !DILocation(line: 105, column: 49, scope: !331)
!412 = !DILocation(line: 105, column: 13, scope: !331)
!413 = distinct !{!413, !334, !414}
!414 = !DILocation(line: 125, column: 13, scope: !326)
!415 = !DILocation(line: 126, column: 13, scope: !313)
!416 = !DILocation(line: 127, column: 9, scope: !313)
!417 = !DILocation(line: 97, column: 41, scope: !308)
!418 = !DILocation(line: 97, column: 38, scope: !308)
!419 = !DILocation(line: 97, column: 9, scope: !308)
!420 = distinct !{!420, !311, !421}
!421 = !DILocation(line: 127, column: 9, scope: !305)
!422 = !DILocation(line: 129, column: 20, scope: !267)
!423 = !DILocation(line: 129, column: 18, scope: !267)
!424 = !DILocation(line: 130, column: 9, scope: !267)
!425 = !DILocation(line: 131, column: 5, scope: !267)
!426 = !DILocation(line: 87, column: 61, scope: !262)
!427 = !DILocation(line: 87, column: 5, scope: !262)
!428 = distinct !{!428, !265, !429}
!429 = !DILocation(line: 131, column: 5, scope: !259)
!430 = !DILocation(line: 137, column: 10, scope: !431)
!431 = distinct !DILexicalBlock(scope: !170, file: !36, line: 137, column: 10)
!432 = !DILocation(line: 137, column: 10, scope: !170)
!433 = !DILocalVariable(name: "denom", scope: !434, file: !36, line: 139, type: !34)
!434 = distinct !DILexicalBlock(scope: !431, file: !36, line: 138, column: 5)
!435 = !DILocation(line: 139, column: 16, scope: !434)
!436 = !DILocation(line: 139, column: 32, scope: !434)
!437 = !DILocation(line: 139, column: 24, scope: !434)
!438 = !DILocation(line: 141, column: 16, scope: !439)
!439 = distinct !DILexicalBlock(scope: !434, file: !36, line: 141, column: 9)
!440 = !DILocation(line: 141, column: 15, scope: !439)
!441 = !DILocation(line: 141, column: 20, scope: !442)
!442 = distinct !DILexicalBlock(scope: !439, file: !36, line: 141, column: 9)
!443 = !DILocation(line: 141, column: 24, scope: !442)
!444 = !DILocation(line: 141, column: 22, scope: !442)
!445 = !DILocation(line: 141, column: 9, scope: !439)
!446 = !DILocation(line: 143, column: 27, scope: !447)
!447 = distinct !DILexicalBlock(scope: !442, file: !36, line: 142, column: 9)
!448 = !DILocation(line: 143, column: 13, scope: !447)
!449 = !DILocation(line: 143, column: 21, scope: !447)
!450 = !DILocation(line: 143, column: 24, scope: !447)
!451 = !DILocation(line: 144, column: 27, scope: !447)
!452 = !DILocation(line: 144, column: 13, scope: !447)
!453 = !DILocation(line: 144, column: 21, scope: !447)
!454 = !DILocation(line: 144, column: 24, scope: !447)
!455 = !DILocation(line: 145, column: 13, scope: !447)
!456 = !DILocation(line: 146, column: 9, scope: !447)
!457 = !DILocation(line: 141, column: 37, scope: !442)
!458 = !DILocation(line: 141, column: 9, scope: !442)
!459 = distinct !{!459, !445, !460}
!460 = !DILocation(line: 146, column: 9, scope: !439)
!461 = !DILocation(line: 147, column: 5, scope: !434)
!462 = !DILocation(line: 148, column: 1, scope: !170)
!463 = distinct !DISubprogram(name: "CheckPointer", scope: !36, file: !36, line: 26, type: !464, isLocal: true, isDefinition: true, scopeLine: 27, flags: DIFlagPrototyped, isOptimized: false, unit: !35, retainedNodes: !4)
!464 = !DISubroutineType(types: !465)
!465 = !{null, !466, !467}
!466 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 64)
!467 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !468, size: 64)
!468 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!469 = !DILocalVariable(name: "p", arg: 1, scope: !463, file: !36, line: 26, type: !466)
!470 = !DILocation(line: 26, column: 34, scope: !463)
!471 = !DILocalVariable(name: "name", arg: 2, scope: !463, file: !36, line: 26, type: !467)
!472 = !DILocation(line: 26, column: 43, scope: !463)
!473 = !DILocation(line: 28, column: 10, scope: !474)
!474 = distinct !DILexicalBlock(scope: !463, file: !36, line: 28, column: 10)
!475 = !DILocation(line: 28, column: 12, scope: !474)
!476 = !DILocation(line: 28, column: 10, scope: !463)
!477 = !DILocation(line: 31, column: 9, scope: !478)
!478 = distinct !DILexicalBlock(scope: !474, file: !36, line: 29, column: 5)
!479 = !DILocation(line: 33, column: 1, scope: !463)
!480 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 18, type: !481, isLocal: false, isDefinition: true, scopeLine: 18, isOptimized: false, unit: !2, retainedNodes: !4)
!481 = !DISubroutineType(types: !482)
!482 = !{!30}
!483 = !DILocation(line: 19, column: 13, scope: !480)
!484 = !DILocation(line: 20, column: 5, scope: !480)
!485 = !DILocation(line: 21, column: 12, scope: !480)
!486 = !DILocation(line: 22, column: 13, scope: !480)
!487 = !DILocation(line: 23, column: 5, scope: !480)
!488 = !DILocation(line: 24, column: 5, scope: !480)
!489 = distinct !DISubprogram(name: "old_main", scope: !3, file: !3, line: 27, type: !481, isLocal: false, isDefinition: true, scopeLine: 27, isOptimized: false, unit: !2, retainedNodes: !4)
!490 = !DILocalVariable(name: "i", scope: !489, file: !3, line: 28, type: !8)
!491 = !DILocation(line: 28, column: 11, scope: !489)
!492 = !DILocalVariable(name: "j", scope: !489, file: !3, line: 28, type: !8)
!493 = !DILocation(line: 28, column: 13, scope: !489)
!494 = !DILocalVariable(name: "RealIn", scope: !489, file: !3, line: 29, type: !173)
!495 = !DILocation(line: 29, column: 9, scope: !489)
!496 = !DILocalVariable(name: "ImagIn", scope: !489, file: !3, line: 30, type: !173)
!497 = !DILocation(line: 30, column: 9, scope: !489)
!498 = !DILocalVariable(name: "RealOut", scope: !489, file: !3, line: 31, type: !173)
!499 = !DILocation(line: 31, column: 9, scope: !489)
!500 = !DILocalVariable(name: "ImagOut", scope: !489, file: !3, line: 32, type: !173)
!501 = !DILocation(line: 32, column: 9, scope: !489)
!502 = !DILocalVariable(name: "coeff", scope: !489, file: !3, line: 33, type: !173)
!503 = !DILocation(line: 33, column: 9, scope: !489)
!504 = !DILocalVariable(name: "amp", scope: !489, file: !3, line: 34, type: !173)
!505 = !DILocation(line: 34, column: 9, scope: !489)
!506 = !DILocation(line: 37, column: 2, scope: !489)
!507 = !DILocation(line: 48, column: 9, scope: !489)
!508 = !DILocation(line: 49, column: 9, scope: !489)
!509 = !DILocation(line: 50, column: 10, scope: !489)
!510 = !DILocation(line: 51, column: 10, scope: !489)
!511 = !DILocation(line: 52, column: 8, scope: !489)
!512 = !DILocation(line: 53, column: 6, scope: !489)
!513 = !DILocation(line: 56, column: 7, scope: !514)
!514 = distinct !DILexicalBlock(scope: !489, file: !3, line: 56, column: 2)
!515 = !DILocation(line: 56, column: 6, scope: !514)
!516 = !DILocation(line: 56, column: 10, scope: !517)
!517 = distinct !DILexicalBlock(scope: !514, file: !3, line: 56, column: 2)
!518 = !DILocation(line: 56, column: 12, scope: !517)
!519 = !DILocation(line: 56, column: 11, scope: !517)
!520 = !DILocation(line: 56, column: 2, scope: !514)
!521 = !DILocation(line: 58, column: 14, scope: !522)
!522 = distinct !DILexicalBlock(scope: !517, file: !3, line: 57, column: 2)
!523 = !DILocation(line: 58, column: 20, scope: !522)
!524 = !DILocation(line: 58, column: 3, scope: !522)
!525 = !DILocation(line: 58, column: 9, scope: !522)
!526 = !DILocation(line: 58, column: 12, scope: !522)
!527 = !DILocation(line: 59, column: 12, scope: !522)
!528 = !DILocation(line: 59, column: 18, scope: !522)
!529 = !DILocation(line: 59, column: 3, scope: !522)
!530 = !DILocation(line: 59, column: 7, scope: !522)
!531 = !DILocation(line: 59, column: 10, scope: !522)
!532 = !DILocation(line: 60, column: 3, scope: !522)
!533 = !DILocation(line: 61, column: 2, scope: !522)
!534 = !DILocation(line: 56, column: 22, scope: !517)
!535 = !DILocation(line: 56, column: 2, scope: !517)
!536 = distinct !{!536, !520, !537}
!537 = !DILocation(line: 61, column: 2, scope: !514)
!538 = !DILocation(line: 62, column: 7, scope: !539)
!539 = distinct !DILexicalBlock(scope: !489, file: !3, line: 62, column: 2)
!540 = !DILocation(line: 62, column: 6, scope: !539)
!541 = !DILocation(line: 62, column: 10, scope: !542)
!542 = distinct !DILexicalBlock(scope: !539, file: !3, line: 62, column: 2)
!543 = !DILocation(line: 62, column: 12, scope: !542)
!544 = !DILocation(line: 62, column: 11, scope: !542)
!545 = !DILocation(line: 62, column: 2, scope: !539)
!546 = !DILocation(line: 65, column: 3, scope: !547)
!547 = distinct !DILexicalBlock(scope: !542, file: !3, line: 63, column: 2)
!548 = !DILocation(line: 65, column: 10, scope: !547)
!549 = !DILocation(line: 65, column: 12, scope: !547)
!550 = !DILocation(line: 66, column: 8, scope: !551)
!551 = distinct !DILexicalBlock(scope: !547, file: !3, line: 66, column: 3)
!552 = !DILocation(line: 66, column: 7, scope: !551)
!553 = !DILocation(line: 66, column: 11, scope: !554)
!554 = distinct !DILexicalBlock(scope: !551, file: !3, line: 66, column: 3)
!555 = !DILocation(line: 66, column: 13, scope: !554)
!556 = !DILocation(line: 66, column: 12, scope: !554)
!557 = !DILocation(line: 66, column: 3, scope: !551)
!558 = !DILocation(line: 69, column: 8, scope: !559)
!559 = distinct !DILexicalBlock(scope: !560, file: !3, line: 69, column: 8)
!560 = distinct !DILexicalBlock(scope: !554, file: !3, line: 67, column: 3)
!561 = !DILocation(line: 69, column: 14, scope: !559)
!562 = !DILocation(line: 69, column: 8, scope: !560)
!563 = !DILocation(line: 71, column: 17, scope: !564)
!564 = distinct !DILexicalBlock(scope: !559, file: !3, line: 70, column: 4)
!565 = !DILocation(line: 71, column: 23, scope: !564)
!566 = !DILocation(line: 71, column: 30, scope: !564)
!567 = !DILocation(line: 71, column: 34, scope: !564)
!568 = !DILocation(line: 71, column: 37, scope: !564)
!569 = !DILocation(line: 71, column: 36, scope: !564)
!570 = !DILocation(line: 71, column: 26, scope: !564)
!571 = !DILocation(line: 71, column: 25, scope: !564)
!572 = !DILocation(line: 71, column: 6, scope: !564)
!573 = !DILocation(line: 71, column: 13, scope: !564)
!574 = !DILocation(line: 71, column: 15, scope: !564)
!575 = !DILocation(line: 72, column: 4, scope: !564)
!576 = !DILocation(line: 75, column: 16, scope: !577)
!577 = distinct !DILexicalBlock(scope: !559, file: !3, line: 74, column: 4)
!578 = !DILocation(line: 75, column: 22, scope: !577)
!579 = !DILocation(line: 75, column: 29, scope: !577)
!580 = !DILocation(line: 75, column: 33, scope: !577)
!581 = !DILocation(line: 75, column: 36, scope: !577)
!582 = !DILocation(line: 75, column: 35, scope: !577)
!583 = !DILocation(line: 75, column: 25, scope: !577)
!584 = !DILocation(line: 75, column: 24, scope: !577)
!585 = !DILocation(line: 75, column: 5, scope: !577)
!586 = !DILocation(line: 75, column: 12, scope: !577)
!587 = !DILocation(line: 75, column: 14, scope: !577)
!588 = !DILocation(line: 77, column: 5, scope: !560)
!589 = !DILocation(line: 77, column: 12, scope: !560)
!590 = !DILocation(line: 77, column: 14, scope: !560)
!591 = !DILocation(line: 78, column: 5, scope: !560)
!592 = !DILocation(line: 79, column: 3, scope: !560)
!593 = !DILocation(line: 66, column: 23, scope: !554)
!594 = !DILocation(line: 66, column: 3, scope: !554)
!595 = distinct !{!595, !557, !596}
!596 = !DILocation(line: 79, column: 3, scope: !551)
!597 = !DILocation(line: 80, column: 3, scope: !547)
!598 = !DILocation(line: 81, column: 2, scope: !547)
!599 = !DILocation(line: 62, column: 21, scope: !542)
!600 = !DILocation(line: 62, column: 2, scope: !542)
!601 = distinct !{!601, !545, !602}
!602 = !DILocation(line: 81, column: 2, scope: !539)
!603 = !DILocation(line: 84, column: 13, scope: !489)
!604 = !DILocation(line: 84, column: 21, scope: !489)
!605 = !DILocation(line: 84, column: 28, scope: !489)
!606 = !DILocation(line: 84, column: 35, scope: !489)
!607 = !DILocation(line: 84, column: 42, scope: !489)
!608 = !DILocation(line: 84, column: 50, scope: !489)
!609 = !DILocation(line: 84, column: 2, scope: !489)
!610 = !DILocation(line: 96, column: 2, scope: !489)
!611 = !DILocation(line: 97, column: 2, scope: !489)
