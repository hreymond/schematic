; ModuleID = 'main_function.c'
source_filename = "main_function.c"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

@a = dso_local global i16 0, align 2, !dbg !0

; Function Attrs: noinline nounwind optnone
define dso_local i16 @f() #0 !dbg !11 {
  %1 = load i16, i16* @a, align 2, !dbg !14
  %2 = add nsw i16 %1, 1, !dbg !14
  store i16 %2, i16* @a, align 2, !dbg !14
  %3 = load i16, i16* @a, align 2, !dbg !15
  %4 = add nsw i16 %3, 1, !dbg !15
  store i16 %4, i16* @a, align 2, !dbg !15
  %5 = load i16, i16* @a, align 2, !dbg !16
  %6 = add nsw i16 %5, 1, !dbg !16
  store i16 %6, i16* @a, align 2, !dbg !16
  %7 = load i16, i16* @a, align 2, !dbg !17
  %8 = add nsw i16 %7, 1, !dbg !17
  store i16 %8, i16* @a, align 2, !dbg !17
  %9 = load i16, i16* @a, align 2, !dbg !18
  %10 = add nsw i16 %9, 1, !dbg !18
  store i16 %10, i16* @a, align 2, !dbg !18
  %11 = load i16, i16* @a, align 2, !dbg !19
  ret i16 %11, !dbg !20
}

; Function Attrs: noinline nounwind optnone
define dso_local i16 @g() #0 !dbg !21 {
  %1 = load i16, i16* @a, align 2, !dbg !22
  %2 = add nsw i16 %1, 1, !dbg !22
  store i16 %2, i16* @a, align 2, !dbg !22
  %3 = call i16 @f(), !dbg !23
  ret i16 %3, !dbg !24
}

; Function Attrs: noinline nounwind optnone
define dso_local i16 @main() #0 !dbg !25 {
  %1 = alloca i16, align 2
  store i16 0, i16* %1, align 2
  %2 = call i16 @f(), !dbg !26
  store i16 %2, i16* @a, align 2, !dbg !27
  %3 = load i16, i16* @a, align 2, !dbg !28
  %4 = icmp sgt i16 %3, 2, !dbg !30
  br i1 %4, label %5, label %7, !dbg !31

; <label>:5:                                      ; preds = %0
  %6 = call i16 @g(), !dbg !32
  store i16 %6, i16* @a, align 2, !dbg !34
  br label %7, !dbg !35

; <label>:7:                                      ; preds = %5, %0
  %8 = load i16, i16* @a, align 2, !dbg !36
  ret i16 %8, !dbg !37
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.dbg.cu = !{!2}
!llvm.module.flags = !{!7, !8, !9}
!llvm.ident = !{!10}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "a", scope: !2, file: !3, line: 2, type: !6, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5)
!3 = !DIFile(filename: "main_function.c", directory: "/home/user/Desktop/alfred/tests/functional/data/function_different_allocations")
!4 = !{}
!5 = !{!0}
!6 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!7 = !{i32 2, !"Dwarf Version", i32 4}
!8 = !{i32 2, !"Debug Info Version", i32 3}
!9 = !{i32 1, !"wchar_size", i32 2}
!10 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
!11 = distinct !DISubprogram(name: "f", scope: !3, file: !3, line: 4, type: !12, isLocal: false, isDefinition: true, scopeLine: 4, isOptimized: false, unit: !2, retainedNodes: !4)
!12 = !DISubroutineType(types: !13)
!13 = !{!6}
!14 = !DILocation(line: 5, column: 6, scope: !11)
!15 = !DILocation(line: 6, column: 6, scope: !11)
!16 = !DILocation(line: 7, column: 6, scope: !11)
!17 = !DILocation(line: 8, column: 6, scope: !11)
!18 = !DILocation(line: 9, column: 6, scope: !11)
!19 = !DILocation(line: 10, column: 12, scope: !11)
!20 = !DILocation(line: 10, column: 5, scope: !11)
!21 = distinct !DISubprogram(name: "g", scope: !3, file: !3, line: 13, type: !12, isLocal: false, isDefinition: true, scopeLine: 13, isOptimized: false, unit: !2, retainedNodes: !4)
!22 = !DILocation(line: 14, column: 5, scope: !21)
!23 = !DILocation(line: 15, column: 11, scope: !21)
!24 = !DILocation(line: 15, column: 4, scope: !21)
!25 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 18, type: !12, isLocal: false, isDefinition: true, scopeLine: 18, isOptimized: false, unit: !2, retainedNodes: !4)
!26 = !DILocation(line: 19, column: 10, scope: !25)
!27 = !DILocation(line: 19, column: 8, scope: !25)
!28 = !DILocation(line: 20, column: 9, scope: !29)
!29 = distinct !DILexicalBlock(scope: !25, file: !3, line: 20, column: 9)
!30 = !DILocation(line: 20, column: 11, scope: !29)
!31 = !DILocation(line: 20, column: 9, scope: !25)
!32 = !DILocation(line: 21, column: 13, scope: !33)
!33 = distinct !DILexicalBlock(scope: !29, file: !3, line: 20, column: 16)
!34 = !DILocation(line: 21, column: 11, scope: !33)
!35 = !DILocation(line: 22, column: 6, scope: !33)
!36 = !DILocation(line: 23, column: 12, scope: !25)
!37 = !DILocation(line: 23, column: 5, scope: !25)
