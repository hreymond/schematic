// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll -target msp430
int nombre = 35;
int valeur;

int main(){
    valeur = 0;
    valeur++;
    valeur++;
    if(nombre > 20){
        valeur++;
        if(valeur > 10) {
            valeur--;
            valeur--;
            valeur--;
            valeur--;
        } else {
            valeur++;
        }
    }
    valeur--;
    return valeur;
}