; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

@nombre = dso_local global i16 35, align 2, !dbg !0
@valeur = common dso_local global i16 0, align 2, !dbg !6

; Function Attrs: noinline nounwind optnone
define dso_local i16 @main() #0 !dbg !13 {
  %1 = alloca i16, align 2
  store i16 0, i16* %1, align 2
  store i16 0, i16* @valeur, align 2, !dbg !16
  %2 = load i16, i16* @valeur, align 2, !dbg !17
  %3 = add nsw i16 %2, 1, !dbg !17
  store i16 %3, i16* @valeur, align 2, !dbg !17
  %4 = load i16, i16* @valeur, align 2, !dbg !18
  %5 = add nsw i16 %4, 1, !dbg !18
  store i16 %5, i16* @valeur, align 2, !dbg !18
  %6 = load i16, i16* @nombre, align 2, !dbg !19
  %7 = icmp sgt i16 %6, 20, !dbg !21
  br i1 %7, label %8, label %26, !dbg !22

; <label>:8:                                      ; preds = %0
  %9 = load i16, i16* @valeur, align 2, !dbg !23
  %10 = add nsw i16 %9, 1, !dbg !23
  store i16 %10, i16* @valeur, align 2, !dbg !23
  %11 = load i16, i16* @valeur, align 2, !dbg !25
  %12 = icmp sgt i16 %11, 10, !dbg !27
  br i1 %12, label %13, label %22, !dbg !28

; <label>:13:                                     ; preds = %8
  %14 = load i16, i16* @valeur, align 2, !dbg !29
  %15 = add nsw i16 %14, -1, !dbg !29
  store i16 %15, i16* @valeur, align 2, !dbg !29
  %16 = load i16, i16* @valeur, align 2, !dbg !31
  %17 = add nsw i16 %16, -1, !dbg !31
  store i16 %17, i16* @valeur, align 2, !dbg !31
  %18 = load i16, i16* @valeur, align 2, !dbg !32
  %19 = add nsw i16 %18, -1, !dbg !32
  store i16 %19, i16* @valeur, align 2, !dbg !32
  %20 = load i16, i16* @valeur, align 2, !dbg !33
  %21 = add nsw i16 %20, -1, !dbg !33
  store i16 %21, i16* @valeur, align 2, !dbg !33
  br label %25, !dbg !34

; <label>:22:                                     ; preds = %8
  %23 = load i16, i16* @valeur, align 2, !dbg !35
  %24 = add nsw i16 %23, 1, !dbg !35
  store i16 %24, i16* @valeur, align 2, !dbg !35
  br label %25

; <label>:25:                                     ; preds = %22, %13
  br label %26, !dbg !37

; <label>:26:                                     ; preds = %25, %0
  %27 = load i16, i16* @valeur, align 2, !dbg !38
  %28 = add nsw i16 %27, -1, !dbg !38
  store i16 %28, i16* @valeur, align 2, !dbg !38
  %29 = load i16, i16* @valeur, align 2, !dbg !39
  ret i16 %29, !dbg !40
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.dbg.cu = !{!2}
!llvm.module.flags = !{!9, !10, !11}
!llvm.ident = !{!12}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "nombre", scope: !2, file: !3, line: 2, type: !8, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5)
!3 = !DIFile(filename: "main.c", directory: "/home/user/Desktop/alfred/tests/data/double_condition")
!4 = !{}
!5 = !{!0, !6}
!6 = !DIGlobalVariableExpression(var: !7, expr: !DIExpression())
!7 = distinct !DIGlobalVariable(name: "valeur", scope: !2, file: !3, line: 3, type: !8, isLocal: false, isDefinition: true)
!8 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!9 = !{i32 2, !"Dwarf Version", i32 4}
!10 = !{i32 2, !"Debug Info Version", i32 3}
!11 = !{i32 1, !"wchar_size", i32 2}
!12 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
!13 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 5, type: !14, isLocal: false, isDefinition: true, scopeLine: 5, isOptimized: false, unit: !2, retainedNodes: !4)
!14 = !DISubroutineType(types: !15)
!15 = !{!8}
!16 = !DILocation(line: 6, column: 12, scope: !13)
!17 = !DILocation(line: 7, column: 11, scope: !13)
!18 = !DILocation(line: 8, column: 11, scope: !13)
!19 = !DILocation(line: 9, column: 8, scope: !20)
!20 = distinct !DILexicalBlock(scope: !13, file: !3, line: 9, column: 8)
!21 = !DILocation(line: 9, column: 15, scope: !20)
!22 = !DILocation(line: 9, column: 8, scope: !13)
!23 = !DILocation(line: 10, column: 15, scope: !24)
!24 = distinct !DILexicalBlock(scope: !20, file: !3, line: 9, column: 20)
!25 = !DILocation(line: 11, column: 12, scope: !26)
!26 = distinct !DILexicalBlock(scope: !24, file: !3, line: 11, column: 12)
!27 = !DILocation(line: 11, column: 19, scope: !26)
!28 = !DILocation(line: 11, column: 12, scope: !24)
!29 = !DILocation(line: 12, column: 19, scope: !30)
!30 = distinct !DILexicalBlock(scope: !26, file: !3, line: 11, column: 25)
!31 = !DILocation(line: 13, column: 19, scope: !30)
!32 = !DILocation(line: 14, column: 19, scope: !30)
!33 = !DILocation(line: 15, column: 19, scope: !30)
!34 = !DILocation(line: 16, column: 9, scope: !30)
!35 = !DILocation(line: 17, column: 19, scope: !36)
!36 = distinct !DILexicalBlock(scope: !26, file: !3, line: 16, column: 16)
!37 = !DILocation(line: 19, column: 5, scope: !24)
!38 = !DILocation(line: 20, column: 11, scope: !13)
!39 = !DILocation(line: 21, column: 12, scope: !13)
!40 = !DILocation(line: 21, column: 5, scope: !13)
