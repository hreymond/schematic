; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

; Function Attrs: noinline nounwind optnone
define dso_local i16 @main() #0 !dbg !7 {
  %1 = alloca i16, align 2
  %2 = alloca i16, align 2
  %3 = alloca i16, align 2
  %4 = alloca i16, align 2
  store i16 0, i16* %1, align 2
  call void @llvm.dbg.declare(metadata i16* %2, metadata !11, metadata !DIExpression()), !dbg !12
  store i16 0, i16* %2, align 2, !dbg !12
  call void @llvm.dbg.declare(metadata i16* %3, metadata !13, metadata !DIExpression()), !dbg !14
  store i16 1, i16* %3, align 2, !dbg !14
  call void @llvm.dbg.declare(metadata i16* %4, metadata !15, metadata !DIExpression()), !dbg !16
  store i16 0, i16* %4, align 2, !dbg !16
  %5 = load i16, i16* %2, align 2, !dbg !17
  %6 = load i16, i16* %3, align 2, !dbg !19
  %7 = icmp sgt i16 %5, %6, !dbg !20
  br i1 %7, label %8, label %10, !dbg !21

; <label>:8:                                      ; preds = %0
  %9 = load i16, i16* %2, align 2, !dbg !22
  store i16 %9, i16* %4, align 2, !dbg !24
  br label %12, !dbg !25

; <label>:10:                                     ; preds = %0
  %11 = load i16, i16* %3, align 2, !dbg !26
  store i16 %11, i16* %4, align 2, !dbg !28
  br label %12

; <label>:12:                                     ; preds = %10, %8
  %13 = load i16, i16* %4, align 2, !dbg !29
  ret i16 %13, !dbg !30
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!3, !4, !5}
!llvm.ident = !{!6}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2)
!1 = !DIFile(filename: "main.c", directory: "/home/user/Desktop/alfred/tests/functional/data/condition")
!2 = !{}
!3 = !{i32 2, !"Dwarf Version", i32 4}
!4 = !{i32 2, !"Debug Info Version", i32 3}
!5 = !{i32 1, !"wchar_size", i32 2}
!6 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
!7 = distinct !DISubprogram(name: "main", scope: !1, file: !1, line: 3, type: !8, isLocal: false, isDefinition: true, scopeLine: 3, isOptimized: false, unit: !0, retainedNodes: !2)
!8 = !DISubroutineType(types: !9)
!9 = !{!10}
!10 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!11 = !DILocalVariable(name: "a", scope: !7, file: !1, line: 4, type: !10)
!12 = !DILocation(line: 4, column: 9, scope: !7)
!13 = !DILocalVariable(name: "b", scope: !7, file: !1, line: 5, type: !10)
!14 = !DILocation(line: 5, column: 9, scope: !7)
!15 = !DILocalVariable(name: "res", scope: !7, file: !1, line: 6, type: !10)
!16 = !DILocation(line: 6, column: 9, scope: !7)
!17 = !DILocation(line: 7, column: 8, scope: !18)
!18 = distinct !DILexicalBlock(scope: !7, file: !1, line: 7, column: 8)
!19 = !DILocation(line: 7, column: 12, scope: !18)
!20 = !DILocation(line: 7, column: 10, scope: !18)
!21 = !DILocation(line: 7, column: 8, scope: !7)
!22 = !DILocation(line: 8, column: 15, scope: !23)
!23 = distinct !DILexicalBlock(scope: !18, file: !1, line: 7, column: 15)
!24 = !DILocation(line: 8, column: 13, scope: !23)
!25 = !DILocation(line: 9, column: 5, scope: !23)
!26 = !DILocation(line: 10, column: 15, scope: !27)
!27 = distinct !DILexicalBlock(scope: !18, file: !1, line: 9, column: 12)
!28 = !DILocation(line: 10, column: 13, scope: !27)
!29 = !DILocation(line: 12, column: 12, scope: !7)
!30 = !DILocation(line: 12, column: 5, scope: !7)
