// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll -target msp430

int main(){
    int a = 0;
    int b = 1;
    int res = 0;
    if(a > b) {
        res = a;
    } else {
        res = b;
    }
    return res;
}