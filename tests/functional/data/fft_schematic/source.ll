; ModuleID = 'llvm-link'
source_filename = "llvm-link"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

@invfft = dso_local global i16 0, align 2, !dbg !0
@MAXSIZE = common dso_local global i16 0, align 2, !dbg !6
@realin = internal global [1024 x float] zeroinitializer, align 4, !dbg !9
@imagin = internal global [1024 x float] zeroinitializer, align 4, !dbg !15
@realout = internal global [1024 x float] zeroinitializer, align 4, !dbg !17
@imagout = internal global [1024 x float] zeroinitializer, align 4, !dbg !19
@Coeff = internal global [16 x float] zeroinitializer, align 4, !dbg !21
@Amp = internal global [16 x float] zeroinitializer, align 4, !dbg !26
@.str = private unnamed_addr constant [7 x i8] c"RealIn\00", align 1
@.str.1 = private unnamed_addr constant [8 x i8] c"RealOut\00", align 1
@.str.2 = private unnamed_addr constant [8 x i8] c"ImagOut\00", align 1
@.str.7 = private unnamed_addr constant [73 x i8] c">>> Error in fftmisc.c: argument %d to NumberOfBitsNeeded is too small.\0A\00", align 1

; Function Attrs: noinline nounwind optnone
define dso_local i16 @main() #0 !dbg !39 {
  %1 = alloca i16, align 2
  store i16 0, i16* %1, align 2
  %2 = call i16 bitcast (i16 (...)* @restore to i16 ()*)(), !dbg !42
  store i16 128, i16* @MAXSIZE, align 2, !dbg !43
  %3 = call i16 @old_main(), !dbg !44
  store i16 1, i16* @invfft, align 2, !dbg !45
  store i16 256, i16* @MAXSIZE, align 2, !dbg !46
  %4 = call i16 @old_main(), !dbg !47
  ret i16 0, !dbg !48
}

declare dso_local i16 @restore(...) #1

; Function Attrs: noinline nounwind optnone
define dso_local i16 @old_main() #0 !dbg !49 {
  %1 = alloca i16, align 2
  %2 = alloca i16, align 2
  %3 = alloca float*, align 2
  %4 = alloca float*, align 2
  %5 = alloca float*, align 2
  %6 = alloca float*, align 2
  %7 = alloca float*, align 2
  %8 = alloca float*, align 2
  call void @llvm.dbg.declare(metadata i16* %1, metadata !50, metadata !DIExpression()), !dbg !51
  call void @llvm.dbg.declare(metadata i16* %2, metadata !52, metadata !DIExpression()), !dbg !53
  call void @llvm.dbg.declare(metadata float** %3, metadata !54, metadata !DIExpression()), !dbg !56
  call void @llvm.dbg.declare(metadata float** %4, metadata !57, metadata !DIExpression()), !dbg !58
  call void @llvm.dbg.declare(metadata float** %5, metadata !59, metadata !DIExpression()), !dbg !60
  call void @llvm.dbg.declare(metadata float** %6, metadata !61, metadata !DIExpression()), !dbg !62
  call void @llvm.dbg.declare(metadata float** %7, metadata !63, metadata !DIExpression()), !dbg !64
  call void @llvm.dbg.declare(metadata float** %8, metadata !65, metadata !DIExpression()), !dbg !66
  %9 = call i16 bitcast (i16 (...)* @srand to i16 (i16)*)(i16 1), !dbg !67
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @realin, i32 0, i32 0), float** %3, align 2, !dbg !68
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @imagin, i32 0, i32 0), float** %4, align 2, !dbg !69
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @realout, i32 0, i32 0), float** %5, align 2, !dbg !70
  store float* getelementptr inbounds ([1024 x float], [1024 x float]* @imagout, i32 0, i32 0), float** %6, align 2, !dbg !71
  store float* getelementptr inbounds ([16 x float], [16 x float]* @Coeff, i32 0, i32 0), float** %7, align 2, !dbg !72
  store float* getelementptr inbounds ([16 x float], [16 x float]* @Amp, i32 0, i32 0), float** %8, align 2, !dbg !73
  store i16 0, i16* %1, align 2, !dbg !74
  br label %10, !dbg !76

; <label>:10:                                     ; preds = %27, %0
  %11 = load i16, i16* %1, align 2, !dbg !77
  %12 = icmp ult i16 %11, 4, !dbg !79
  br i1 %12, label %13, label %30, !dbg !80

; <label>:13:                                     ; preds = %10
  %14 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 4), !dbg !81
  %15 = call i16 bitcast (i16 (...)* @rand to i16 ()*)(), !dbg !83
  %16 = srem i16 %15, 1000, !dbg !84
  %17 = sitofp i16 %16 to float, !dbg !83
  %18 = load float*, float** %7, align 2, !dbg !85
  %19 = load i16, i16* %1, align 2, !dbg !86
  %20 = getelementptr inbounds float, float* %18, i16 %19, !dbg !85
  store float %17, float* %20, align 4, !dbg !87
  %21 = call i16 bitcast (i16 (...)* @rand to i16 ()*)(), !dbg !88
  %22 = srem i16 %21, 1000, !dbg !89
  %23 = sitofp i16 %22 to float, !dbg !88
  %24 = load float*, float** %8, align 2, !dbg !90
  %25 = load i16, i16* %1, align 2, !dbg !91
  %26 = getelementptr inbounds float, float* %24, i16 %25, !dbg !90
  store float %23, float* %26, align 4, !dbg !92
  br label %27, !dbg !93

; <label>:27:                                     ; preds = %13
  %28 = load i16, i16* %1, align 2, !dbg !94
  %29 = add i16 %28, 1, !dbg !94
  store i16 %29, i16* %1, align 2, !dbg !94
  br label %10, !dbg !95, !llvm.loop !96

; <label>:30:                                     ; preds = %10
  store i16 0, i16* %1, align 2, !dbg !98
  br label %31, !dbg !100

; <label>:31:                                     ; preds = %102, %30
  %32 = load i16, i16* %1, align 2, !dbg !101
  %33 = load i16, i16* @MAXSIZE, align 2, !dbg !103
  %34 = icmp ult i16 %32, %33, !dbg !104
  br i1 %34, label %35, label %105, !dbg !105

; <label>:35:                                     ; preds = %31
  %36 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 256), !dbg !106
  %37 = load float*, float** %3, align 2, !dbg !108
  %38 = load i16, i16* %1, align 2, !dbg !109
  %39 = getelementptr inbounds float, float* %37, i16 %38, !dbg !108
  store float 0.000000e+00, float* %39, align 4, !dbg !110
  store i16 0, i16* %2, align 2, !dbg !111
  br label %40, !dbg !113

; <label>:40:                                     ; preds = %98, %35
  %41 = load i16, i16* %2, align 2, !dbg !114
  %42 = icmp ult i16 %41, 4, !dbg !116
  br i1 %42, label %43, label %101, !dbg !117

; <label>:43:                                     ; preds = %40
  %44 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 4), !dbg !118
  %45 = call i16 bitcast (i16 (...)* @rand to i16 ()*)(), !dbg !120
  %46 = srem i16 %45, 2, !dbg !122
  %47 = icmp ne i16 %46, 0, !dbg !122
  br i1 %47, label %48, label %71, !dbg !123

; <label>:48:                                     ; preds = %43
  %49 = load float*, float** %7, align 2, !dbg !124
  %50 = load i16, i16* %2, align 2, !dbg !126
  %51 = getelementptr inbounds float, float* %49, i16 %50, !dbg !124
  %52 = load float, float* %51, align 4, !dbg !124
  %53 = fpext float %52 to double, !dbg !124
  %54 = load float*, float** %8, align 2, !dbg !127
  %55 = load i16, i16* %2, align 2, !dbg !128
  %56 = getelementptr inbounds float, float* %54, i16 %55, !dbg !127
  %57 = load float, float* %56, align 4, !dbg !127
  %58 = load i16, i16* %1, align 2, !dbg !129
  %59 = uitofp i16 %58 to float, !dbg !129
  %60 = fmul float %57, %59, !dbg !130
  %61 = fpext float %60 to double, !dbg !127
  %62 = call double @cos(double %61) #5, !dbg !131
  %63 = fmul double %53, %62, !dbg !132
  %64 = load float*, float** %3, align 2, !dbg !133
  %65 = load i16, i16* %1, align 2, !dbg !134
  %66 = getelementptr inbounds float, float* %64, i16 %65, !dbg !133
  %67 = load float, float* %66, align 4, !dbg !135
  %68 = fpext float %67 to double, !dbg !135
  %69 = fadd double %68, %63, !dbg !135
  %70 = fptrunc double %69 to float, !dbg !135
  store float %70, float* %66, align 4, !dbg !135
  br label %94, !dbg !136

; <label>:71:                                     ; preds = %43
  %72 = load float*, float** %7, align 2, !dbg !137
  %73 = load i16, i16* %2, align 2, !dbg !139
  %74 = getelementptr inbounds float, float* %72, i16 %73, !dbg !137
  %75 = load float, float* %74, align 4, !dbg !137
  %76 = fpext float %75 to double, !dbg !137
  %77 = load float*, float** %8, align 2, !dbg !140
  %78 = load i16, i16* %2, align 2, !dbg !141
  %79 = getelementptr inbounds float, float* %77, i16 %78, !dbg !140
  %80 = load float, float* %79, align 4, !dbg !140
  %81 = load i16, i16* %1, align 2, !dbg !142
  %82 = uitofp i16 %81 to float, !dbg !142
  %83 = fmul float %80, %82, !dbg !143
  %84 = fpext float %83 to double, !dbg !140
  %85 = call double @sin(double %84) #5, !dbg !144
  %86 = fmul double %76, %85, !dbg !145
  %87 = load float*, float** %3, align 2, !dbg !146
  %88 = load i16, i16* %1, align 2, !dbg !147
  %89 = getelementptr inbounds float, float* %87, i16 %88, !dbg !146
  %90 = load float, float* %89, align 4, !dbg !148
  %91 = fpext float %90 to double, !dbg !148
  %92 = fadd double %91, %86, !dbg !148
  %93 = fptrunc double %92 to float, !dbg !148
  store float %93, float* %89, align 4, !dbg !148
  br label %94

; <label>:94:                                     ; preds = %71, %48
  %95 = load float*, float** %4, align 2, !dbg !149
  %96 = load i16, i16* %1, align 2, !dbg !150
  %97 = getelementptr inbounds float, float* %95, i16 %96, !dbg !149
  store float 0.000000e+00, float* %97, align 4, !dbg !151
  br label %98, !dbg !152

; <label>:98:                                     ; preds = %94
  %99 = load i16, i16* %2, align 2, !dbg !153
  %100 = add i16 %99, 1, !dbg !153
  store i16 %100, i16* %2, align 2, !dbg !153
  br label %40, !dbg !154, !llvm.loop !155

; <label>:101:                                    ; preds = %40
  br label %102, !dbg !157

; <label>:102:                                    ; preds = %101
  %103 = load i16, i16* %1, align 2, !dbg !158
  %104 = add i16 %103, 1, !dbg !158
  store i16 %104, i16* %1, align 2, !dbg !158
  br label %31, !dbg !159, !llvm.loop !160

; <label>:105:                                    ; preds = %31
  %106 = load i16, i16* @MAXSIZE, align 2, !dbg !162
  %107 = load i16, i16* @invfft, align 2, !dbg !163
  %108 = load float*, float** %3, align 2, !dbg !164
  %109 = load float*, float** %4, align 2, !dbg !165
  %110 = load float*, float** %5, align 2, !dbg !166
  %111 = load float*, float** %6, align 2, !dbg !167
  call void @fft_float(i16 %106, i16 %107, float* %108, float* %109, float* %110, float* %111), !dbg !168
  %112 = call i16 bitcast (i16 (...)* @checkpoint to i16 ()*)(), !dbg !169
  ret i16 0, !dbg !170
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #2

declare dso_local i16 @srand(...) #1

declare dso_local i16 @__max_iter(...) #1

declare dso_local i16 @rand(...) #1

; Function Attrs: nounwind
declare dso_local double @cos(double) #3

; Function Attrs: nounwind
declare dso_local double @sin(double) #3

declare dso_local i16 @checkpoint(...) #1

; Function Attrs: noinline nounwind optnone
define dso_local void @fft_float(i16, i16, float*, float*, float*, float*) #0 !dbg !171 {
  %7 = alloca i16, align 2
  %8 = alloca i16, align 2
  %9 = alloca float*, align 2
  %10 = alloca float*, align 2
  %11 = alloca float*, align 2
  %12 = alloca float*, align 2
  %13 = alloca i16, align 2
  %14 = alloca i16, align 2
  %15 = alloca i16, align 2
  %16 = alloca i16, align 2
  %17 = alloca i16, align 2
  %18 = alloca i16, align 2
  %19 = alloca i16, align 2
  %20 = alloca double, align 8
  %21 = alloca double, align 8
  %22 = alloca double, align 8
  %23 = alloca double, align 8
  %24 = alloca double, align 8
  %25 = alloca double, align 8
  %26 = alloca double, align 8
  %27 = alloca double, align 8
  %28 = alloca double, align 8
  %29 = alloca [3 x double], align 8
  %30 = alloca [3 x double], align 8
  %31 = alloca double, align 8
  store i16 %0, i16* %7, align 2
  call void @llvm.dbg.declare(metadata i16* %7, metadata !174, metadata !DIExpression()), !dbg !175
  store i16 %1, i16* %8, align 2
  call void @llvm.dbg.declare(metadata i16* %8, metadata !176, metadata !DIExpression()), !dbg !177
  store float* %2, float** %9, align 2
  call void @llvm.dbg.declare(metadata float** %9, metadata !178, metadata !DIExpression()), !dbg !179
  store float* %3, float** %10, align 2
  call void @llvm.dbg.declare(metadata float** %10, metadata !180, metadata !DIExpression()), !dbg !181
  store float* %4, float** %11, align 2
  call void @llvm.dbg.declare(metadata float** %11, metadata !182, metadata !DIExpression()), !dbg !183
  store float* %5, float** %12, align 2
  call void @llvm.dbg.declare(metadata float** %12, metadata !184, metadata !DIExpression()), !dbg !185
  call void @llvm.dbg.declare(metadata i16* %13, metadata !186, metadata !DIExpression()), !dbg !187
  call void @llvm.dbg.declare(metadata i16* %14, metadata !188, metadata !DIExpression()), !dbg !189
  call void @llvm.dbg.declare(metadata i16* %15, metadata !190, metadata !DIExpression()), !dbg !191
  call void @llvm.dbg.declare(metadata i16* %16, metadata !192, metadata !DIExpression()), !dbg !193
  call void @llvm.dbg.declare(metadata i16* %17, metadata !194, metadata !DIExpression()), !dbg !195
  call void @llvm.dbg.declare(metadata i16* %18, metadata !196, metadata !DIExpression()), !dbg !197
  call void @llvm.dbg.declare(metadata i16* %19, metadata !198, metadata !DIExpression()), !dbg !199
  call void @llvm.dbg.declare(metadata double* %20, metadata !200, metadata !DIExpression()), !dbg !201
  store double 0x401921FB54442D18, double* %20, align 8, !dbg !201
  call void @llvm.dbg.declare(metadata double* %21, metadata !202, metadata !DIExpression()), !dbg !203
  call void @llvm.dbg.declare(metadata double* %22, metadata !204, metadata !DIExpression()), !dbg !205
  %32 = load i16, i16* %7, align 2, !dbg !206
  %33 = call i16 @IsPowerOfTwo(i16 %32), !dbg !208
  %34 = icmp ne i16 %33, 0, !dbg !208
  br i1 %34, label %36, label %35, !dbg !209

; <label>:35:                                     ; preds = %6
  call void @exit(i16 1) #6, !dbg !210
  unreachable, !dbg !210

; <label>:36:                                     ; preds = %6
  %37 = load i16, i16* %8, align 2, !dbg !212
  %38 = icmp ne i16 %37, 0, !dbg !212
  br i1 %38, label %39, label %42, !dbg !214

; <label>:39:                                     ; preds = %36
  %40 = load double, double* %20, align 8, !dbg !215
  %41 = fsub double -0.000000e+00, %40, !dbg !216
  store double %41, double* %20, align 8, !dbg !217
  br label %42, !dbg !218

; <label>:42:                                     ; preds = %39, %36
  %43 = load float*, float** %9, align 2, !dbg !219
  %44 = bitcast float* %43 to i8*, !dbg !219
  call void @CheckPointer(i8* %44, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0)), !dbg !219
  %45 = load float*, float** %11, align 2, !dbg !220
  %46 = bitcast float* %45 to i8*, !dbg !220
  call void @CheckPointer(i8* %46, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.1, i32 0, i32 0)), !dbg !220
  %47 = load float*, float** %12, align 2, !dbg !221
  %48 = bitcast float* %47 to i8*, !dbg !221
  call void @CheckPointer(i8* %48, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.2, i32 0, i32 0)), !dbg !221
  %49 = load i16, i16* %7, align 2, !dbg !222
  %50 = call i16 @NumberOfBitsNeeded(i16 %49), !dbg !223
  store i16 %50, i16* %13, align 2, !dbg !224
  store i16 0, i16* %14, align 2, !dbg !225
  br label %51, !dbg !227

; <label>:51:                                     ; preds = %82, %42
  %52 = load i16, i16* %14, align 2, !dbg !228
  %53 = load i16, i16* %7, align 2, !dbg !230
  %54 = icmp ult i16 %52, %53, !dbg !231
  br i1 %54, label %55, label %85, !dbg !232

; <label>:55:                                     ; preds = %51
  %56 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 256), !dbg !233
  %57 = load i16, i16* %14, align 2, !dbg !235
  %58 = load i16, i16* %13, align 2, !dbg !236
  %59 = call i16 @ReverseBits(i16 %57, i16 %58), !dbg !237
  store i16 %59, i16* %15, align 2, !dbg !238
  %60 = load float*, float** %9, align 2, !dbg !239
  %61 = load i16, i16* %14, align 2, !dbg !240
  %62 = getelementptr inbounds float, float* %60, i16 %61, !dbg !239
  %63 = load float, float* %62, align 4, !dbg !239
  %64 = load float*, float** %11, align 2, !dbg !241
  %65 = load i16, i16* %15, align 2, !dbg !242
  %66 = getelementptr inbounds float, float* %64, i16 %65, !dbg !241
  store float %63, float* %66, align 4, !dbg !243
  %67 = load float*, float** %10, align 2, !dbg !244
  %68 = icmp eq float* %67, null, !dbg !245
  br i1 %68, label %69, label %70, !dbg !246

; <label>:69:                                     ; preds = %55
  br label %76, !dbg !246

; <label>:70:                                     ; preds = %55
  %71 = load float*, float** %10, align 2, !dbg !247
  %72 = load i16, i16* %14, align 2, !dbg !248
  %73 = getelementptr inbounds float, float* %71, i16 %72, !dbg !247
  %74 = load float, float* %73, align 4, !dbg !247
  %75 = fpext float %74 to double, !dbg !247
  br label %76, !dbg !246

; <label>:76:                                     ; preds = %70, %69
  %77 = phi double [ 0.000000e+00, %69 ], [ %75, %70 ], !dbg !246
  %78 = fptrunc double %77 to float, !dbg !246
  %79 = load float*, float** %12, align 2, !dbg !249
  %80 = load i16, i16* %15, align 2, !dbg !250
  %81 = getelementptr inbounds float, float* %79, i16 %80, !dbg !249
  store float %78, float* %81, align 4, !dbg !251
  br label %82, !dbg !252

; <label>:82:                                     ; preds = %76
  %83 = load i16, i16* %14, align 2, !dbg !253
  %84 = add i16 %83, 1, !dbg !253
  store i16 %84, i16* %14, align 2, !dbg !253
  br label %51, !dbg !254, !llvm.loop !255

; <label>:85:                                     ; preds = %51
  store i16 1, i16* %19, align 2, !dbg !257
  store i16 2, i16* %18, align 2, !dbg !258
  br label %86, !dbg !260

; <label>:86:                                     ; preds = %246, %85
  %87 = load i16, i16* %18, align 2, !dbg !261
  %88 = load i16, i16* %7, align 2, !dbg !263
  %89 = icmp ule i16 %87, %88, !dbg !264
  br i1 %89, label %90, label %249, !dbg !265

; <label>:90:                                     ; preds = %86
  %91 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 8), !dbg !266
  call void @llvm.dbg.declare(metadata double* %23, metadata !268, metadata !DIExpression()), !dbg !269
  %92 = load double, double* %20, align 8, !dbg !270
  %93 = load i16, i16* %18, align 2, !dbg !271
  %94 = uitofp i16 %93 to double, !dbg !272
  %95 = fdiv double %92, %94, !dbg !273
  store double %95, double* %23, align 8, !dbg !269
  call void @llvm.dbg.declare(metadata double* %24, metadata !274, metadata !DIExpression()), !dbg !275
  %96 = load double, double* %23, align 8, !dbg !276
  %97 = fmul double -2.000000e+00, %96, !dbg !277
  %98 = call double @sin(double %97) #5, !dbg !278
  store double %98, double* %24, align 8, !dbg !275
  call void @llvm.dbg.declare(metadata double* %25, metadata !279, metadata !DIExpression()), !dbg !280
  %99 = load double, double* %23, align 8, !dbg !281
  %100 = fsub double -0.000000e+00, %99, !dbg !282
  %101 = call double @sin(double %100) #5, !dbg !283
  store double %101, double* %25, align 8, !dbg !280
  call void @llvm.dbg.declare(metadata double* %26, metadata !284, metadata !DIExpression()), !dbg !285
  %102 = load double, double* %23, align 8, !dbg !286
  %103 = fmul double -2.000000e+00, %102, !dbg !287
  %104 = call double @cos(double %103) #5, !dbg !288
  store double %104, double* %26, align 8, !dbg !285
  call void @llvm.dbg.declare(metadata double* %27, metadata !289, metadata !DIExpression()), !dbg !290
  %105 = load double, double* %23, align 8, !dbg !291
  %106 = fsub double -0.000000e+00, %105, !dbg !292
  %107 = call double @cos(double %106) #5, !dbg !293
  store double %107, double* %27, align 8, !dbg !290
  call void @llvm.dbg.declare(metadata double* %28, metadata !294, metadata !DIExpression()), !dbg !295
  %108 = load double, double* %27, align 8, !dbg !296
  %109 = fmul double 2.000000e+00, %108, !dbg !297
  store double %109, double* %28, align 8, !dbg !295
  call void @llvm.dbg.declare(metadata [3 x double]* %29, metadata !298, metadata !DIExpression()), !dbg !302
  call void @llvm.dbg.declare(metadata [3 x double]* %30, metadata !303, metadata !DIExpression()), !dbg !304
  store i16 0, i16* %14, align 2, !dbg !305
  br label %110, !dbg !307

; <label>:110:                                    ; preds = %240, %90
  %111 = load i16, i16* %14, align 2, !dbg !308
  %112 = load i16, i16* %7, align 2, !dbg !310
  %113 = icmp ult i16 %111, %112, !dbg !311
  br i1 %113, label %114, label %244, !dbg !312

; <label>:114:                                    ; preds = %110
  %115 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 128), !dbg !313
  %116 = load double, double* %26, align 8, !dbg !315
  %117 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 2, !dbg !316
  store double %116, double* %117, align 8, !dbg !317
  %118 = load double, double* %27, align 8, !dbg !318
  %119 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !319
  store double %118, double* %119, align 8, !dbg !320
  %120 = load double, double* %24, align 8, !dbg !321
  %121 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 2, !dbg !322
  store double %120, double* %121, align 8, !dbg !323
  %122 = load double, double* %25, align 8, !dbg !324
  %123 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !325
  store double %122, double* %123, align 8, !dbg !326
  %124 = load i16, i16* %14, align 2, !dbg !327
  store i16 %124, i16* %15, align 2, !dbg !329
  store i16 0, i16* %17, align 2, !dbg !330
  br label %125, !dbg !331

; <label>:125:                                    ; preds = %234, %114
  %126 = load i16, i16* %17, align 2, !dbg !332
  %127 = load i16, i16* %19, align 2, !dbg !334
  %128 = icmp ult i16 %126, %127, !dbg !335
  br i1 %128, label %129, label %239, !dbg !336

; <label>:129:                                    ; preds = %125
  %130 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 128), !dbg !337
  %131 = load double, double* %28, align 8, !dbg !339
  %132 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !340
  %133 = load double, double* %132, align 8, !dbg !340
  %134 = fmul double %131, %133, !dbg !341
  %135 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 2, !dbg !342
  %136 = load double, double* %135, align 8, !dbg !342
  %137 = fsub double %134, %136, !dbg !343
  %138 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !344
  store double %137, double* %138, align 8, !dbg !345
  %139 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !346
  %140 = load double, double* %139, align 8, !dbg !346
  %141 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 2, !dbg !347
  store double %140, double* %141, align 8, !dbg !348
  %142 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !349
  %143 = load double, double* %142, align 8, !dbg !349
  %144 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 1, !dbg !350
  store double %143, double* %144, align 8, !dbg !351
  %145 = load double, double* %28, align 8, !dbg !352
  %146 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !353
  %147 = load double, double* %146, align 8, !dbg !353
  %148 = fmul double %145, %147, !dbg !354
  %149 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 2, !dbg !355
  %150 = load double, double* %149, align 8, !dbg !355
  %151 = fsub double %148, %150, !dbg !356
  %152 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !357
  store double %151, double* %152, align 8, !dbg !358
  %153 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !359
  %154 = load double, double* %153, align 8, !dbg !359
  %155 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 2, !dbg !360
  store double %154, double* %155, align 8, !dbg !361
  %156 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !362
  %157 = load double, double* %156, align 8, !dbg !362
  %158 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 1, !dbg !363
  store double %157, double* %158, align 8, !dbg !364
  %159 = load i16, i16* %15, align 2, !dbg !365
  %160 = load i16, i16* %19, align 2, !dbg !366
  %161 = add i16 %159, %160, !dbg !367
  store i16 %161, i16* %16, align 2, !dbg !368
  %162 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !369
  %163 = load double, double* %162, align 8, !dbg !369
  %164 = load float*, float** %11, align 2, !dbg !370
  %165 = load i16, i16* %16, align 2, !dbg !371
  %166 = getelementptr inbounds float, float* %164, i16 %165, !dbg !370
  %167 = load float, float* %166, align 4, !dbg !370
  %168 = fpext float %167 to double, !dbg !370
  %169 = fmul double %163, %168, !dbg !372
  %170 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !373
  %171 = load double, double* %170, align 8, !dbg !373
  %172 = load float*, float** %12, align 2, !dbg !374
  %173 = load i16, i16* %16, align 2, !dbg !375
  %174 = getelementptr inbounds float, float* %172, i16 %173, !dbg !374
  %175 = load float, float* %174, align 4, !dbg !374
  %176 = fpext float %175 to double, !dbg !374
  %177 = fmul double %171, %176, !dbg !376
  %178 = fsub double %169, %177, !dbg !377
  store double %178, double* %21, align 8, !dbg !378
  %179 = getelementptr inbounds [3 x double], [3 x double]* %29, i16 0, i16 0, !dbg !379
  %180 = load double, double* %179, align 8, !dbg !379
  %181 = load float*, float** %12, align 2, !dbg !380
  %182 = load i16, i16* %16, align 2, !dbg !381
  %183 = getelementptr inbounds float, float* %181, i16 %182, !dbg !380
  %184 = load float, float* %183, align 4, !dbg !380
  %185 = fpext float %184 to double, !dbg !380
  %186 = fmul double %180, %185, !dbg !382
  %187 = getelementptr inbounds [3 x double], [3 x double]* %30, i16 0, i16 0, !dbg !383
  %188 = load double, double* %187, align 8, !dbg !383
  %189 = load float*, float** %11, align 2, !dbg !384
  %190 = load i16, i16* %16, align 2, !dbg !385
  %191 = getelementptr inbounds float, float* %189, i16 %190, !dbg !384
  %192 = load float, float* %191, align 4, !dbg !384
  %193 = fpext float %192 to double, !dbg !384
  %194 = fmul double %188, %193, !dbg !386
  %195 = fadd double %186, %194, !dbg !387
  store double %195, double* %22, align 8, !dbg !388
  %196 = load float*, float** %11, align 2, !dbg !389
  %197 = load i16, i16* %15, align 2, !dbg !390
  %198 = getelementptr inbounds float, float* %196, i16 %197, !dbg !389
  %199 = load float, float* %198, align 4, !dbg !389
  %200 = fpext float %199 to double, !dbg !389
  %201 = load double, double* %21, align 8, !dbg !391
  %202 = fsub double %200, %201, !dbg !392
  %203 = fptrunc double %202 to float, !dbg !389
  %204 = load float*, float** %11, align 2, !dbg !393
  %205 = load i16, i16* %16, align 2, !dbg !394
  %206 = getelementptr inbounds float, float* %204, i16 %205, !dbg !393
  store float %203, float* %206, align 4, !dbg !395
  %207 = load float*, float** %12, align 2, !dbg !396
  %208 = load i16, i16* %15, align 2, !dbg !397
  %209 = getelementptr inbounds float, float* %207, i16 %208, !dbg !396
  %210 = load float, float* %209, align 4, !dbg !396
  %211 = fpext float %210 to double, !dbg !396
  %212 = load double, double* %22, align 8, !dbg !398
  %213 = fsub double %211, %212, !dbg !399
  %214 = fptrunc double %213 to float, !dbg !396
  %215 = load float*, float** %12, align 2, !dbg !400
  %216 = load i16, i16* %16, align 2, !dbg !401
  %217 = getelementptr inbounds float, float* %215, i16 %216, !dbg !400
  store float %214, float* %217, align 4, !dbg !402
  %218 = load double, double* %21, align 8, !dbg !403
  %219 = load float*, float** %11, align 2, !dbg !404
  %220 = load i16, i16* %15, align 2, !dbg !405
  %221 = getelementptr inbounds float, float* %219, i16 %220, !dbg !404
  %222 = load float, float* %221, align 4, !dbg !406
  %223 = fpext float %222 to double, !dbg !406
  %224 = fadd double %223, %218, !dbg !406
  %225 = fptrunc double %224 to float, !dbg !406
  store float %225, float* %221, align 4, !dbg !406
  %226 = load double, double* %22, align 8, !dbg !407
  %227 = load float*, float** %12, align 2, !dbg !408
  %228 = load i16, i16* %15, align 2, !dbg !409
  %229 = getelementptr inbounds float, float* %227, i16 %228, !dbg !408
  %230 = load float, float* %229, align 4, !dbg !410
  %231 = fpext float %230 to double, !dbg !410
  %232 = fadd double %231, %226, !dbg !410
  %233 = fptrunc double %232 to float, !dbg !410
  store float %233, float* %229, align 4, !dbg !410
  br label %234, !dbg !411

; <label>:234:                                    ; preds = %129
  %235 = load i16, i16* %15, align 2, !dbg !412
  %236 = add i16 %235, 1, !dbg !412
  store i16 %236, i16* %15, align 2, !dbg !412
  %237 = load i16, i16* %17, align 2, !dbg !413
  %238 = add i16 %237, 1, !dbg !413
  store i16 %238, i16* %17, align 2, !dbg !413
  br label %125, !dbg !414, !llvm.loop !415

; <label>:239:                                    ; preds = %125
  br label %240, !dbg !417

; <label>:240:                                    ; preds = %239
  %241 = load i16, i16* %18, align 2, !dbg !418
  %242 = load i16, i16* %14, align 2, !dbg !419
  %243 = add i16 %242, %241, !dbg !419
  store i16 %243, i16* %14, align 2, !dbg !419
  br label %110, !dbg !420, !llvm.loop !421

; <label>:244:                                    ; preds = %110
  %245 = load i16, i16* %18, align 2, !dbg !423
  store i16 %245, i16* %19, align 2, !dbg !424
  br label %246, !dbg !425

; <label>:246:                                    ; preds = %244
  %247 = load i16, i16* %18, align 2, !dbg !426
  %248 = shl i16 %247, 1, !dbg !426
  store i16 %248, i16* %18, align 2, !dbg !426
  br label %86, !dbg !427, !llvm.loop !428

; <label>:249:                                    ; preds = %86
  %250 = load i16, i16* %8, align 2, !dbg !430
  %251 = icmp ne i16 %250, 0, !dbg !430
  br i1 %251, label %252, label %281, !dbg !432

; <label>:252:                                    ; preds = %249
  call void @llvm.dbg.declare(metadata double* %31, metadata !433, metadata !DIExpression()), !dbg !435
  %253 = load i16, i16* %7, align 2, !dbg !436
  %254 = uitofp i16 %253 to double, !dbg !437
  store double %254, double* %31, align 8, !dbg !435
  store i16 0, i16* %14, align 2, !dbg !438
  br label %255, !dbg !440

; <label>:255:                                    ; preds = %277, %252
  %256 = load i16, i16* %14, align 2, !dbg !441
  %257 = load i16, i16* %7, align 2, !dbg !443
  %258 = icmp ult i16 %256, %257, !dbg !444
  br i1 %258, label %259, label %280, !dbg !445

; <label>:259:                                    ; preds = %255
  %260 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 256), !dbg !446
  %261 = load double, double* %31, align 8, !dbg !448
  %262 = load float*, float** %11, align 2, !dbg !449
  %263 = load i16, i16* %14, align 2, !dbg !450
  %264 = getelementptr inbounds float, float* %262, i16 %263, !dbg !449
  %265 = load float, float* %264, align 4, !dbg !451
  %266 = fpext float %265 to double, !dbg !451
  %267 = fdiv double %266, %261, !dbg !451
  %268 = fptrunc double %267 to float, !dbg !451
  store float %268, float* %264, align 4, !dbg !451
  %269 = load double, double* %31, align 8, !dbg !452
  %270 = load float*, float** %12, align 2, !dbg !453
  %271 = load i16, i16* %14, align 2, !dbg !454
  %272 = getelementptr inbounds float, float* %270, i16 %271, !dbg !453
  %273 = load float, float* %272, align 4, !dbg !455
  %274 = fpext float %273 to double, !dbg !455
  %275 = fdiv double %274, %269, !dbg !455
  %276 = fptrunc double %275 to float, !dbg !455
  store float %276, float* %272, align 4, !dbg !455
  br label %277, !dbg !456

; <label>:277:                                    ; preds = %259
  %278 = load i16, i16* %14, align 2, !dbg !457
  %279 = add i16 %278, 1, !dbg !457
  store i16 %279, i16* %14, align 2, !dbg !457
  br label %255, !dbg !458, !llvm.loop !459

; <label>:280:                                    ; preds = %255
  br label %281, !dbg !461

; <label>:281:                                    ; preds = %280, %249
  ret void, !dbg !462
}

; Function Attrs: noreturn
declare dso_local void @exit(i16) #4

; Function Attrs: noinline nounwind optnone
define internal void @CheckPointer(i8*, i8*) #0 !dbg !463 {
  %3 = alloca i8*, align 2
  %4 = alloca i8*, align 2
  store i8* %0, i8** %3, align 2
  call void @llvm.dbg.declare(metadata i8** %3, metadata !469, metadata !DIExpression()), !dbg !470
  store i8* %1, i8** %4, align 2
  call void @llvm.dbg.declare(metadata i8** %4, metadata !471, metadata !DIExpression()), !dbg !472
  %5 = load i8*, i8** %3, align 2, !dbg !473
  %6 = icmp eq i8* %5, null, !dbg !475
  br i1 %6, label %7, label %8, !dbg !476

; <label>:7:                                      ; preds = %2
  call void @exit(i16 1) #6, !dbg !477
  unreachable, !dbg !477

; <label>:8:                                      ; preds = %2
  ret void, !dbg !479
}

; Function Attrs: noinline nounwind optnone
define dso_local i16 @IsPowerOfTwo(i16) #0 !dbg !480 {
  %2 = alloca i16, align 2
  %3 = alloca i16, align 2
  store i16 %0, i16* %3, align 2
  call void @llvm.dbg.declare(metadata i16* %3, metadata !483, metadata !DIExpression()), !dbg !484
  %4 = load i16, i16* %3, align 2, !dbg !485
  %5 = icmp ult i16 %4, 2, !dbg !487
  br i1 %5, label %6, label %7, !dbg !488

; <label>:6:                                      ; preds = %1
  store i16 0, i16* %2, align 2, !dbg !489
  br label %15, !dbg !489

; <label>:7:                                      ; preds = %1
  %8 = load i16, i16* %3, align 2, !dbg !490
  %9 = load i16, i16* %3, align 2, !dbg !492
  %10 = sub i16 %9, 1, !dbg !493
  %11 = and i16 %8, %10, !dbg !494
  %12 = icmp ne i16 %11, 0, !dbg !494
  br i1 %12, label %13, label %14, !dbg !495

; <label>:13:                                     ; preds = %7
  store i16 0, i16* %2, align 2, !dbg !496
  br label %15, !dbg !496

; <label>:14:                                     ; preds = %7
  store i16 1, i16* %2, align 2, !dbg !497
  br label %15, !dbg !497

; <label>:15:                                     ; preds = %14, %13, %6
  %16 = load i16, i16* %2, align 2, !dbg !498
  ret i16 %16, !dbg !498
}

; Function Attrs: noinline nounwind optnone
define dso_local i16 @NumberOfBitsNeeded(i16) #0 !dbg !499 {
  %2 = alloca i16, align 2
  %3 = alloca i16, align 2
  store i16 %0, i16* %2, align 2
  call void @llvm.dbg.declare(metadata i16* %2, metadata !502, metadata !DIExpression()), !dbg !503
  call void @llvm.dbg.declare(metadata i16* %3, metadata !504, metadata !DIExpression()), !dbg !505
  %4 = load i16, i16* %2, align 2, !dbg !506
  %5 = icmp ult i16 %4, 2, !dbg !508
  br i1 %5, label %6, label %9, !dbg !509

; <label>:6:                                      ; preds = %1
  %7 = load i16, i16* %2, align 2, !dbg !510
  %8 = call i16 (i8*, ...) @printf(i8* getelementptr inbounds ([73 x i8], [73 x i8]* @.str.7, i32 0, i32 0), i16 %7), !dbg !512
  call void @exit(i16 1) #6, !dbg !513
  unreachable, !dbg !513

; <label>:9:                                      ; preds = %1
  store i16 0, i16* %3, align 2, !dbg !514
  br label %10, !dbg !516

; <label>:10:                                     ; preds = %20, %9
  %11 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 16), !dbg !517
  %12 = load i16, i16* %2, align 2, !dbg !520
  %13 = load i16, i16* %3, align 2, !dbg !522
  %14 = shl i16 1, %13, !dbg !523
  %15 = and i16 %12, %14, !dbg !524
  %16 = icmp ne i16 %15, 0, !dbg !524
  br i1 %16, label %17, label %19, !dbg !525

; <label>:17:                                     ; preds = %10
  %18 = load i16, i16* %3, align 2, !dbg !526
  ret i16 %18, !dbg !528

; <label>:19:                                     ; preds = %10
  br label %20, !dbg !529

; <label>:20:                                     ; preds = %19
  %21 = load i16, i16* %3, align 2, !dbg !530
  %22 = add i16 %21, 1, !dbg !530
  store i16 %22, i16* %3, align 2, !dbg !530
  br label %10, !dbg !531, !llvm.loop !532
}

declare dso_local i16 @printf(i8*, ...) #1

; Function Attrs: noinline nounwind optnone
define dso_local i16 @ReverseBits(i16, i16) #0 !dbg !535 {
  %3 = alloca i16, align 2
  %4 = alloca i16, align 2
  %5 = alloca i16, align 2
  %6 = alloca i16, align 2
  store i16 %0, i16* %3, align 2
  call void @llvm.dbg.declare(metadata i16* %3, metadata !538, metadata !DIExpression()), !dbg !539
  store i16 %1, i16* %4, align 2
  call void @llvm.dbg.declare(metadata i16* %4, metadata !540, metadata !DIExpression()), !dbg !541
  call void @llvm.dbg.declare(metadata i16* %5, metadata !542, metadata !DIExpression()), !dbg !543
  call void @llvm.dbg.declare(metadata i16* %6, metadata !544, metadata !DIExpression()), !dbg !545
  store i16 0, i16* %6, align 2, !dbg !546
  store i16 0, i16* %5, align 2, !dbg !548
  br label %7, !dbg !549

; <label>:7:                                      ; preds = %20, %2
  %8 = load i16, i16* %5, align 2, !dbg !550
  %9 = load i16, i16* %4, align 2, !dbg !552
  %10 = icmp ult i16 %8, %9, !dbg !553
  br i1 %10, label %11, label %23, !dbg !554

; <label>:11:                                     ; preds = %7
  %12 = call i16 bitcast (i16 (...)* @__max_iter to i16 (i16)*)(i16 16), !dbg !555
  %13 = load i16, i16* %6, align 2, !dbg !557
  %14 = shl i16 %13, 1, !dbg !558
  %15 = load i16, i16* %3, align 2, !dbg !559
  %16 = and i16 %15, 1, !dbg !560
  %17 = or i16 %14, %16, !dbg !561
  store i16 %17, i16* %6, align 2, !dbg !562
  %18 = load i16, i16* %3, align 2, !dbg !563
  %19 = lshr i16 %18, 1, !dbg !563
  store i16 %19, i16* %3, align 2, !dbg !563
  br label %20, !dbg !564

; <label>:20:                                     ; preds = %11
  %21 = load i16, i16* %5, align 2, !dbg !565
  %22 = add i16 %21, 1, !dbg !565
  store i16 %22, i16* %5, align 2, !dbg !565
  br label %7, !dbg !566, !llvm.loop !567

; <label>:23:                                     ; preds = %7
  %24 = load i16, i16* %6, align 2, !dbg !569
  ret i16 %24, !dbg !570
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind readnone speculatable }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }
attributes #6 = { noreturn }

!llvm.dbg.cu = !{!2, !29, !33}
!llvm.ident = !{!35, !35, !35}
!llvm.module.flags = !{!36, !37, !38}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "invfft", scope: !2, file: !3, line: 5, type: !28, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 7.0.0 (tags/RELEASE_700/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5)
!3 = !DIFile(filename: "main.c", directory: "/home/hreymond/code/alfred/samples/mibench/fft/fft_eloise")
!4 = !{}
!5 = !{!0, !6, !9, !15, !17, !19, !21, !26}
!6 = !DIGlobalVariableExpression(var: !7, expr: !DIExpression())
!7 = distinct !DIGlobalVariable(name: "MAXSIZE", scope: !2, file: !3, line: 6, type: !8, isLocal: false, isDefinition: true)
!8 = !DIBasicType(name: "unsigned int", size: 16, encoding: DW_ATE_unsigned)
!9 = !DIGlobalVariableExpression(var: !10, expr: !DIExpression())
!10 = distinct !DIGlobalVariable(name: "realin", scope: !2, file: !3, line: 8, type: !11, isLocal: true, isDefinition: true)
!11 = !DICompositeType(tag: DW_TAG_array_type, baseType: !12, size: 32768, elements: !13)
!12 = !DIBasicType(name: "float", size: 32, encoding: DW_ATE_float)
!13 = !{!14}
!14 = !DISubrange(count: 1024)
!15 = !DIGlobalVariableExpression(var: !16, expr: !DIExpression())
!16 = distinct !DIGlobalVariable(name: "imagin", scope: !2, file: !3, line: 9, type: !11, isLocal: true, isDefinition: true)
!17 = !DIGlobalVariableExpression(var: !18, expr: !DIExpression())
!18 = distinct !DIGlobalVariable(name: "realout", scope: !2, file: !3, line: 10, type: !11, isLocal: true, isDefinition: true)
!19 = !DIGlobalVariableExpression(var: !20, expr: !DIExpression())
!20 = distinct !DIGlobalVariable(name: "imagout", scope: !2, file: !3, line: 11, type: !11, isLocal: true, isDefinition: true)
!21 = !DIGlobalVariableExpression(var: !22, expr: !DIExpression())
!22 = distinct !DIGlobalVariable(name: "Coeff", scope: !2, file: !3, line: 12, type: !23, isLocal: true, isDefinition: true)
!23 = !DICompositeType(tag: DW_TAG_array_type, baseType: !12, size: 512, elements: !24)
!24 = !{!25}
!25 = !DISubrange(count: 16)
!26 = !DIGlobalVariableExpression(var: !27, expr: !DIExpression())
!27 = distinct !DIGlobalVariable(name: "Amp", scope: !2, file: !3, line: 13, type: !23, isLocal: true, isDefinition: true)
!28 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!29 = distinct !DICompileUnit(language: DW_LANG_C99, file: !30, producer: "clang version 7.0.0 (tags/RELEASE_700/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, retainedTypes: !31)
!30 = !DIFile(filename: "fourierf.c", directory: "/home/hreymond/code/alfred/samples/mibench/fft/fft_eloise")
!31 = !{!32}
!32 = !DIBasicType(name: "double", size: 64, encoding: DW_ATE_float)
!33 = distinct !DICompileUnit(language: DW_LANG_C99, file: !34, producer: "clang version 7.0.0 (tags/RELEASE_700/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4)
!34 = !DIFile(filename: "fftmisc.c", directory: "/home/hreymond/code/alfred/samples/mibench/fft/fft_eloise")
!35 = !{!"clang version 7.0.0 (tags/RELEASE_700/final)"}
!36 = !{i32 2, !"Dwarf Version", i32 4}
!37 = !{i32 2, !"Debug Info Version", i32 3}
!38 = !{i32 1, !"wchar_size", i32 2}
!39 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 18, type: !40, isLocal: false, isDefinition: true, scopeLine: 18, isOptimized: false, unit: !2, retainedNodes: !4)
!40 = !DISubroutineType(types: !41)
!41 = !{!28}
!42 = !DILocation(line: 19, column: 5, scope: !39)
!43 = !DILocation(line: 20, column: 13, scope: !39)
!44 = !DILocation(line: 21, column: 5, scope: !39)
!45 = !DILocation(line: 22, column: 12, scope: !39)
!46 = !DILocation(line: 23, column: 13, scope: !39)
!47 = !DILocation(line: 24, column: 5, scope: !39)
!48 = !DILocation(line: 25, column: 5, scope: !39)
!49 = distinct !DISubprogram(name: "old_main", scope: !3, file: !3, line: 28, type: !40, isLocal: false, isDefinition: true, scopeLine: 28, isOptimized: false, unit: !2, retainedNodes: !4)
!50 = !DILocalVariable(name: "i", scope: !49, file: !3, line: 29, type: !8)
!51 = !DILocation(line: 29, column: 11, scope: !49)
!52 = !DILocalVariable(name: "j", scope: !49, file: !3, line: 29, type: !8)
!53 = !DILocation(line: 29, column: 13, scope: !49)
!54 = !DILocalVariable(name: "RealIn", scope: !49, file: !3, line: 30, type: !55)
!55 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !12, size: 16)
!56 = !DILocation(line: 30, column: 9, scope: !49)
!57 = !DILocalVariable(name: "ImagIn", scope: !49, file: !3, line: 31, type: !55)
!58 = !DILocation(line: 31, column: 9, scope: !49)
!59 = !DILocalVariable(name: "RealOut", scope: !49, file: !3, line: 32, type: !55)
!60 = !DILocation(line: 32, column: 9, scope: !49)
!61 = !DILocalVariable(name: "ImagOut", scope: !49, file: !3, line: 33, type: !55)
!62 = !DILocation(line: 33, column: 9, scope: !49)
!63 = !DILocalVariable(name: "coeff", scope: !49, file: !3, line: 34, type: !55)
!64 = !DILocation(line: 34, column: 9, scope: !49)
!65 = !DILocalVariable(name: "amp", scope: !49, file: !3, line: 35, type: !55)
!66 = !DILocation(line: 35, column: 9, scope: !49)
!67 = !DILocation(line: 38, column: 2, scope: !49)
!68 = !DILocation(line: 49, column: 9, scope: !49)
!69 = !DILocation(line: 50, column: 9, scope: !49)
!70 = !DILocation(line: 51, column: 10, scope: !49)
!71 = !DILocation(line: 52, column: 10, scope: !49)
!72 = !DILocation(line: 53, column: 8, scope: !49)
!73 = !DILocation(line: 54, column: 6, scope: !49)
!74 = !DILocation(line: 57, column: 10, scope: !75)
!75 = distinct !DILexicalBlock(scope: !49, file: !3, line: 57, column: 5)
!76 = !DILocation(line: 57, column: 9, scope: !75)
!77 = !DILocation(line: 57, column: 13, scope: !78)
!78 = distinct !DILexicalBlock(scope: !75, file: !3, line: 57, column: 5)
!79 = !DILocation(line: 57, column: 14, scope: !78)
!80 = !DILocation(line: 57, column: 5, scope: !75)
!81 = !DILocation(line: 59, column: 9, scope: !82)
!82 = distinct !DILexicalBlock(scope: !78, file: !3, line: 58, column: 5)
!83 = !DILocation(line: 60, column: 20, scope: !82)
!84 = !DILocation(line: 60, column: 26, scope: !82)
!85 = !DILocation(line: 60, column: 9, scope: !82)
!86 = !DILocation(line: 60, column: 15, scope: !82)
!87 = !DILocation(line: 60, column: 18, scope: !82)
!88 = !DILocation(line: 61, column: 18, scope: !82)
!89 = !DILocation(line: 61, column: 24, scope: !82)
!90 = !DILocation(line: 61, column: 9, scope: !82)
!91 = !DILocation(line: 61, column: 13, scope: !82)
!92 = !DILocation(line: 61, column: 16, scope: !82)
!93 = !DILocation(line: 63, column: 5, scope: !82)
!94 = !DILocation(line: 57, column: 25, scope: !78)
!95 = !DILocation(line: 57, column: 5, scope: !78)
!96 = distinct !{!96, !80, !97}
!97 = !DILocation(line: 63, column: 5, scope: !75)
!98 = !DILocation(line: 64, column: 7, scope: !99)
!99 = distinct !DILexicalBlock(scope: !49, file: !3, line: 64, column: 2)
!100 = !DILocation(line: 64, column: 6, scope: !99)
!101 = !DILocation(line: 64, column: 10, scope: !102)
!102 = distinct !DILexicalBlock(scope: !99, file: !3, line: 64, column: 2)
!103 = !DILocation(line: 64, column: 12, scope: !102)
!104 = !DILocation(line: 64, column: 11, scope: !102)
!105 = !DILocation(line: 64, column: 2, scope: !99)
!106 = !DILocation(line: 66, column: 5, scope: !107)
!107 = distinct !DILexicalBlock(scope: !102, file: !3, line: 65, column: 2)
!108 = !DILocation(line: 68, column: 3, scope: !107)
!109 = !DILocation(line: 68, column: 10, scope: !107)
!110 = !DILocation(line: 68, column: 12, scope: !107)
!111 = !DILocation(line: 69, column: 8, scope: !112)
!112 = distinct !DILexicalBlock(scope: !107, file: !3, line: 69, column: 3)
!113 = !DILocation(line: 69, column: 7, scope: !112)
!114 = !DILocation(line: 69, column: 11, scope: !115)
!115 = distinct !DILexicalBlock(scope: !112, file: !3, line: 69, column: 3)
!116 = !DILocation(line: 69, column: 12, scope: !115)
!117 = !DILocation(line: 69, column: 3, scope: !112)
!118 = !DILocation(line: 71, column: 7, scope: !119)
!119 = distinct !DILexicalBlock(scope: !115, file: !3, line: 70, column: 3)
!120 = !DILocation(line: 73, column: 8, scope: !121)
!121 = distinct !DILexicalBlock(scope: !119, file: !3, line: 73, column: 8)
!122 = !DILocation(line: 73, column: 14, scope: !121)
!123 = !DILocation(line: 73, column: 8, scope: !119)
!124 = !DILocation(line: 75, column: 17, scope: !125)
!125 = distinct !DILexicalBlock(scope: !121, file: !3, line: 74, column: 4)
!126 = !DILocation(line: 75, column: 23, scope: !125)
!127 = !DILocation(line: 75, column: 30, scope: !125)
!128 = !DILocation(line: 75, column: 34, scope: !125)
!129 = !DILocation(line: 75, column: 37, scope: !125)
!130 = !DILocation(line: 75, column: 36, scope: !125)
!131 = !DILocation(line: 75, column: 26, scope: !125)
!132 = !DILocation(line: 75, column: 25, scope: !125)
!133 = !DILocation(line: 75, column: 6, scope: !125)
!134 = !DILocation(line: 75, column: 13, scope: !125)
!135 = !DILocation(line: 75, column: 15, scope: !125)
!136 = !DILocation(line: 76, column: 4, scope: !125)
!137 = !DILocation(line: 79, column: 16, scope: !138)
!138 = distinct !DILexicalBlock(scope: !121, file: !3, line: 78, column: 4)
!139 = !DILocation(line: 79, column: 22, scope: !138)
!140 = !DILocation(line: 79, column: 29, scope: !138)
!141 = !DILocation(line: 79, column: 33, scope: !138)
!142 = !DILocation(line: 79, column: 36, scope: !138)
!143 = !DILocation(line: 79, column: 35, scope: !138)
!144 = !DILocation(line: 79, column: 25, scope: !138)
!145 = !DILocation(line: 79, column: 24, scope: !138)
!146 = !DILocation(line: 79, column: 5, scope: !138)
!147 = !DILocation(line: 79, column: 12, scope: !138)
!148 = !DILocation(line: 79, column: 14, scope: !138)
!149 = !DILocation(line: 81, column: 5, scope: !119)
!150 = !DILocation(line: 81, column: 12, scope: !119)
!151 = !DILocation(line: 81, column: 14, scope: !119)
!152 = !DILocation(line: 83, column: 3, scope: !119)
!153 = !DILocation(line: 69, column: 23, scope: !115)
!154 = !DILocation(line: 69, column: 3, scope: !115)
!155 = distinct !{!155, !117, !156}
!156 = !DILocation(line: 83, column: 3, scope: !112)
!157 = !DILocation(line: 85, column: 2, scope: !107)
!158 = !DILocation(line: 64, column: 21, scope: !102)
!159 = !DILocation(line: 64, column: 2, scope: !102)
!160 = distinct !{!160, !105, !161}
!161 = !DILocation(line: 85, column: 2, scope: !99)
!162 = !DILocation(line: 88, column: 13, scope: !49)
!163 = !DILocation(line: 88, column: 21, scope: !49)
!164 = !DILocation(line: 88, column: 28, scope: !49)
!165 = !DILocation(line: 88, column: 35, scope: !49)
!166 = !DILocation(line: 88, column: 42, scope: !49)
!167 = !DILocation(line: 88, column: 50, scope: !49)
!168 = !DILocation(line: 88, column: 2, scope: !49)
!169 = !DILocation(line: 100, column: 2, scope: !49)
!170 = !DILocation(line: 101, column: 2, scope: !49)
!171 = distinct !DISubprogram(name: "fft_float", scope: !30, file: !30, line: 36, type: !172, isLocal: false, isDefinition: true, scopeLine: 43, flags: DIFlagPrototyped, isOptimized: false, unit: !29, retainedNodes: !4)
!172 = !DISubroutineType(types: !173)
!173 = !{null, !8, !28, !55, !55, !55, !55}
!174 = !DILocalVariable(name: "NumSamples", arg: 1, scope: !171, file: !30, line: 37, type: !8)
!175 = !DILocation(line: 37, column: 15, scope: !171)
!176 = !DILocalVariable(name: "InverseTransform", arg: 2, scope: !171, file: !30, line: 38, type: !28)
!177 = !DILocation(line: 38, column: 15, scope: !171)
!178 = !DILocalVariable(name: "RealIn", arg: 3, scope: !171, file: !30, line: 39, type: !55)
!179 = !DILocation(line: 39, column: 15, scope: !171)
!180 = !DILocalVariable(name: "ImagIn", arg: 4, scope: !171, file: !30, line: 40, type: !55)
!181 = !DILocation(line: 40, column: 15, scope: !171)
!182 = !DILocalVariable(name: "RealOut", arg: 5, scope: !171, file: !30, line: 41, type: !55)
!183 = !DILocation(line: 41, column: 15, scope: !171)
!184 = !DILocalVariable(name: "ImagOut", arg: 6, scope: !171, file: !30, line: 42, type: !55)
!185 = !DILocation(line: 42, column: 15, scope: !171)
!186 = !DILocalVariable(name: "NumBits", scope: !171, file: !30, line: 44, type: !8)
!187 = !DILocation(line: 44, column: 14, scope: !171)
!188 = !DILocalVariable(name: "i", scope: !171, file: !30, line: 45, type: !8)
!189 = !DILocation(line: 45, column: 14, scope: !171)
!190 = !DILocalVariable(name: "j", scope: !171, file: !30, line: 45, type: !8)
!191 = !DILocation(line: 45, column: 17, scope: !171)
!192 = !DILocalVariable(name: "k", scope: !171, file: !30, line: 45, type: !8)
!193 = !DILocation(line: 45, column: 20, scope: !171)
!194 = !DILocalVariable(name: "n", scope: !171, file: !30, line: 45, type: !8)
!195 = !DILocation(line: 45, column: 23, scope: !171)
!196 = !DILocalVariable(name: "BlockSize", scope: !171, file: !30, line: 46, type: !8)
!197 = !DILocation(line: 46, column: 14, scope: !171)
!198 = !DILocalVariable(name: "BlockEnd", scope: !171, file: !30, line: 46, type: !8)
!199 = !DILocation(line: 46, column: 25, scope: !171)
!200 = !DILocalVariable(name: "angle_numerator", scope: !171, file: !30, line: 48, type: !32)
!201 = !DILocation(line: 48, column: 12, scope: !171)
!202 = !DILocalVariable(name: "tr", scope: !171, file: !30, line: 49, type: !32)
!203 = !DILocation(line: 49, column: 12, scope: !171)
!204 = !DILocalVariable(name: "ti", scope: !171, file: !30, line: 49, type: !32)
!205 = !DILocation(line: 49, column: 16, scope: !171)
!206 = !DILocation(line: 51, column: 24, scope: !207)
!207 = distinct !DILexicalBlock(scope: !171, file: !30, line: 51, column: 10)
!208 = !DILocation(line: 51, column: 11, scope: !207)
!209 = !DILocation(line: 51, column: 10, scope: !171)
!210 = !DILocation(line: 58, column: 9, scope: !211)
!211 = distinct !DILexicalBlock(scope: !207, file: !30, line: 52, column: 5)
!212 = !DILocation(line: 61, column: 10, scope: !213)
!213 = distinct !DILexicalBlock(scope: !171, file: !30, line: 61, column: 10)
!214 = !DILocation(line: 61, column: 10, scope: !171)
!215 = !DILocation(line: 62, column: 28, scope: !213)
!216 = !DILocation(line: 62, column: 27, scope: !213)
!217 = !DILocation(line: 62, column: 25, scope: !213)
!218 = !DILocation(line: 62, column: 9, scope: !213)
!219 = !DILocation(line: 64, column: 5, scope: !171)
!220 = !DILocation(line: 65, column: 5, scope: !171)
!221 = !DILocation(line: 66, column: 5, scope: !171)
!222 = !DILocation(line: 68, column: 36, scope: !171)
!223 = !DILocation(line: 68, column: 15, scope: !171)
!224 = !DILocation(line: 68, column: 13, scope: !171)
!225 = !DILocation(line: 74, column: 12, scope: !226)
!226 = distinct !DILexicalBlock(scope: !171, file: !30, line: 74, column: 5)
!227 = !DILocation(line: 74, column: 11, scope: !226)
!228 = !DILocation(line: 74, column: 16, scope: !229)
!229 = distinct !DILexicalBlock(scope: !226, file: !30, line: 74, column: 5)
!230 = !DILocation(line: 74, column: 20, scope: !229)
!231 = !DILocation(line: 74, column: 18, scope: !229)
!232 = !DILocation(line: 74, column: 5, scope: !226)
!233 = !DILocation(line: 76, column: 9, scope: !234)
!234 = distinct !DILexicalBlock(scope: !229, file: !30, line: 75, column: 5)
!235 = !DILocation(line: 77, column: 27, scope: !234)
!236 = !DILocation(line: 77, column: 30, scope: !234)
!237 = !DILocation(line: 77, column: 13, scope: !234)
!238 = !DILocation(line: 77, column: 11, scope: !234)
!239 = !DILocation(line: 78, column: 22, scope: !234)
!240 = !DILocation(line: 78, column: 29, scope: !234)
!241 = !DILocation(line: 78, column: 9, scope: !234)
!242 = !DILocation(line: 78, column: 17, scope: !234)
!243 = !DILocation(line: 78, column: 20, scope: !234)
!244 = !DILocation(line: 79, column: 23, scope: !234)
!245 = !DILocation(line: 79, column: 30, scope: !234)
!246 = !DILocation(line: 79, column: 22, scope: !234)
!247 = !DILocation(line: 79, column: 47, scope: !234)
!248 = !DILocation(line: 79, column: 54, scope: !234)
!249 = !DILocation(line: 79, column: 9, scope: !234)
!250 = !DILocation(line: 79, column: 17, scope: !234)
!251 = !DILocation(line: 79, column: 20, scope: !234)
!252 = !DILocation(line: 81, column: 5, scope: !234)
!253 = !DILocation(line: 74, column: 33, scope: !229)
!254 = !DILocation(line: 74, column: 5, scope: !229)
!255 = distinct !{!255, !232, !256}
!256 = !DILocation(line: 81, column: 5, scope: !226)
!257 = !DILocation(line: 87, column: 14, scope: !171)
!258 = !DILocation(line: 88, column: 21, scope: !259)
!259 = distinct !DILexicalBlock(scope: !171, file: !30, line: 88, column: 5)
!260 = !DILocation(line: 88, column: 11, scope: !259)
!261 = !DILocation(line: 88, column: 26, scope: !262)
!262 = distinct !DILexicalBlock(scope: !259, file: !30, line: 88, column: 5)
!263 = !DILocation(line: 88, column: 39, scope: !262)
!264 = !DILocation(line: 88, column: 36, scope: !262)
!265 = !DILocation(line: 88, column: 5, scope: !259)
!266 = !DILocation(line: 90, column: 9, scope: !267)
!267 = distinct !DILexicalBlock(scope: !262, file: !30, line: 89, column: 5)
!268 = !DILocalVariable(name: "delta_angle", scope: !267, file: !30, line: 91, type: !32)
!269 = !DILocation(line: 91, column: 16, scope: !267)
!270 = !DILocation(line: 91, column: 30, scope: !267)
!271 = !DILocation(line: 91, column: 56, scope: !267)
!272 = !DILocation(line: 91, column: 48, scope: !267)
!273 = !DILocation(line: 91, column: 46, scope: !267)
!274 = !DILocalVariable(name: "sm2", scope: !267, file: !30, line: 92, type: !32)
!275 = !DILocation(line: 92, column: 16, scope: !267)
!276 = !DILocation(line: 92, column: 33, scope: !267)
!277 = !DILocation(line: 92, column: 31, scope: !267)
!278 = !DILocation(line: 92, column: 22, scope: !267)
!279 = !DILocalVariable(name: "sm1", scope: !267, file: !30, line: 93, type: !32)
!280 = !DILocation(line: 93, column: 16, scope: !267)
!281 = !DILocation(line: 93, column: 29, scope: !267)
!282 = !DILocation(line: 93, column: 28, scope: !267)
!283 = !DILocation(line: 93, column: 22, scope: !267)
!284 = !DILocalVariable(name: "cm2", scope: !267, file: !30, line: 94, type: !32)
!285 = !DILocation(line: 94, column: 16, scope: !267)
!286 = !DILocation(line: 94, column: 33, scope: !267)
!287 = !DILocation(line: 94, column: 31, scope: !267)
!288 = !DILocation(line: 94, column: 22, scope: !267)
!289 = !DILocalVariable(name: "cm1", scope: !267, file: !30, line: 95, type: !32)
!290 = !DILocation(line: 95, column: 16, scope: !267)
!291 = !DILocation(line: 95, column: 29, scope: !267)
!292 = !DILocation(line: 95, column: 28, scope: !267)
!293 = !DILocation(line: 95, column: 22, scope: !267)
!294 = !DILocalVariable(name: "w", scope: !267, file: !30, line: 96, type: !32)
!295 = !DILocation(line: 96, column: 16, scope: !267)
!296 = !DILocation(line: 96, column: 24, scope: !267)
!297 = !DILocation(line: 96, column: 22, scope: !267)
!298 = !DILocalVariable(name: "ar", scope: !267, file: !30, line: 97, type: !299)
!299 = !DICompositeType(tag: DW_TAG_array_type, baseType: !32, size: 192, elements: !300)
!300 = !{!301}
!301 = !DISubrange(count: 3)
!302 = !DILocation(line: 97, column: 16, scope: !267)
!303 = !DILocalVariable(name: "ai", scope: !267, file: !30, line: 97, type: !299)
!304 = !DILocation(line: 97, column: 23, scope: !267)
!305 = !DILocation(line: 99, column: 16, scope: !306)
!306 = distinct !DILexicalBlock(scope: !267, file: !30, line: 99, column: 9)
!307 = !DILocation(line: 99, column: 15, scope: !306)
!308 = !DILocation(line: 99, column: 20, scope: !309)
!309 = distinct !DILexicalBlock(scope: !306, file: !30, line: 99, column: 9)
!310 = !DILocation(line: 99, column: 24, scope: !309)
!311 = !DILocation(line: 99, column: 22, scope: !309)
!312 = !DILocation(line: 99, column: 9, scope: !306)
!313 = !DILocation(line: 101, column: 13, scope: !314)
!314 = distinct !DILexicalBlock(scope: !309, file: !30, line: 100, column: 9)
!315 = !DILocation(line: 102, column: 21, scope: !314)
!316 = !DILocation(line: 102, column: 13, scope: !314)
!317 = !DILocation(line: 102, column: 19, scope: !314)
!318 = !DILocation(line: 103, column: 21, scope: !314)
!319 = !DILocation(line: 103, column: 13, scope: !314)
!320 = !DILocation(line: 103, column: 19, scope: !314)
!321 = !DILocation(line: 105, column: 21, scope: !314)
!322 = !DILocation(line: 105, column: 13, scope: !314)
!323 = !DILocation(line: 105, column: 19, scope: !314)
!324 = !DILocation(line: 106, column: 21, scope: !314)
!325 = !DILocation(line: 106, column: 13, scope: !314)
!326 = !DILocation(line: 106, column: 19, scope: !314)
!327 = !DILocation(line: 108, column: 21, scope: !328)
!328 = distinct !DILexicalBlock(scope: !314, file: !30, line: 108, column: 13)
!329 = !DILocation(line: 108, column: 20, scope: !328)
!330 = !DILocation(line: 108, column: 25, scope: !328)
!331 = !DILocation(line: 108, column: 19, scope: !328)
!332 = !DILocation(line: 108, column: 29, scope: !333)
!333 = distinct !DILexicalBlock(scope: !328, file: !30, line: 108, column: 13)
!334 = !DILocation(line: 108, column: 33, scope: !333)
!335 = !DILocation(line: 108, column: 31, scope: !333)
!336 = !DILocation(line: 108, column: 13, scope: !328)
!337 = !DILocation(line: 110, column: 17, scope: !338)
!338 = distinct !DILexicalBlock(scope: !333, file: !30, line: 109, column: 13)
!339 = !DILocation(line: 111, column: 25, scope: !338)
!340 = !DILocation(line: 111, column: 27, scope: !338)
!341 = !DILocation(line: 111, column: 26, scope: !338)
!342 = !DILocation(line: 111, column: 35, scope: !338)
!343 = !DILocation(line: 111, column: 33, scope: !338)
!344 = !DILocation(line: 111, column: 17, scope: !338)
!345 = !DILocation(line: 111, column: 23, scope: !338)
!346 = !DILocation(line: 112, column: 25, scope: !338)
!347 = !DILocation(line: 112, column: 17, scope: !338)
!348 = !DILocation(line: 112, column: 23, scope: !338)
!349 = !DILocation(line: 113, column: 25, scope: !338)
!350 = !DILocation(line: 113, column: 17, scope: !338)
!351 = !DILocation(line: 113, column: 23, scope: !338)
!352 = !DILocation(line: 115, column: 25, scope: !338)
!353 = !DILocation(line: 115, column: 27, scope: !338)
!354 = !DILocation(line: 115, column: 26, scope: !338)
!355 = !DILocation(line: 115, column: 35, scope: !338)
!356 = !DILocation(line: 115, column: 33, scope: !338)
!357 = !DILocation(line: 115, column: 17, scope: !338)
!358 = !DILocation(line: 115, column: 23, scope: !338)
!359 = !DILocation(line: 116, column: 25, scope: !338)
!360 = !DILocation(line: 116, column: 17, scope: !338)
!361 = !DILocation(line: 116, column: 23, scope: !338)
!362 = !DILocation(line: 117, column: 25, scope: !338)
!363 = !DILocation(line: 117, column: 17, scope: !338)
!364 = !DILocation(line: 117, column: 23, scope: !338)
!365 = !DILocation(line: 119, column: 21, scope: !338)
!366 = !DILocation(line: 119, column: 25, scope: !338)
!367 = !DILocation(line: 119, column: 23, scope: !338)
!368 = !DILocation(line: 119, column: 19, scope: !338)
!369 = !DILocation(line: 120, column: 22, scope: !338)
!370 = !DILocation(line: 120, column: 28, scope: !338)
!371 = !DILocation(line: 120, column: 36, scope: !338)
!372 = !DILocation(line: 120, column: 27, scope: !338)
!373 = !DILocation(line: 120, column: 41, scope: !338)
!374 = !DILocation(line: 120, column: 47, scope: !338)
!375 = !DILocation(line: 120, column: 55, scope: !338)
!376 = !DILocation(line: 120, column: 46, scope: !338)
!377 = !DILocation(line: 120, column: 39, scope: !338)
!378 = !DILocation(line: 120, column: 20, scope: !338)
!379 = !DILocation(line: 121, column: 22, scope: !338)
!380 = !DILocation(line: 121, column: 28, scope: !338)
!381 = !DILocation(line: 121, column: 36, scope: !338)
!382 = !DILocation(line: 121, column: 27, scope: !338)
!383 = !DILocation(line: 121, column: 41, scope: !338)
!384 = !DILocation(line: 121, column: 47, scope: !338)
!385 = !DILocation(line: 121, column: 55, scope: !338)
!386 = !DILocation(line: 121, column: 46, scope: !338)
!387 = !DILocation(line: 121, column: 39, scope: !338)
!388 = !DILocation(line: 121, column: 20, scope: !338)
!389 = !DILocation(line: 123, column: 30, scope: !338)
!390 = !DILocation(line: 123, column: 38, scope: !338)
!391 = !DILocation(line: 123, column: 43, scope: !338)
!392 = !DILocation(line: 123, column: 41, scope: !338)
!393 = !DILocation(line: 123, column: 17, scope: !338)
!394 = !DILocation(line: 123, column: 25, scope: !338)
!395 = !DILocation(line: 123, column: 28, scope: !338)
!396 = !DILocation(line: 124, column: 30, scope: !338)
!397 = !DILocation(line: 124, column: 38, scope: !338)
!398 = !DILocation(line: 124, column: 43, scope: !338)
!399 = !DILocation(line: 124, column: 41, scope: !338)
!400 = !DILocation(line: 124, column: 17, scope: !338)
!401 = !DILocation(line: 124, column: 25, scope: !338)
!402 = !DILocation(line: 124, column: 28, scope: !338)
!403 = !DILocation(line: 126, column: 31, scope: !338)
!404 = !DILocation(line: 126, column: 17, scope: !338)
!405 = !DILocation(line: 126, column: 25, scope: !338)
!406 = !DILocation(line: 126, column: 28, scope: !338)
!407 = !DILocation(line: 127, column: 31, scope: !338)
!408 = !DILocation(line: 127, column: 17, scope: !338)
!409 = !DILocation(line: 127, column: 25, scope: !338)
!410 = !DILocation(line: 127, column: 28, scope: !338)
!411 = !DILocation(line: 128, column: 13, scope: !338)
!412 = !DILocation(line: 108, column: 44, scope: !333)
!413 = !DILocation(line: 108, column: 49, scope: !333)
!414 = !DILocation(line: 108, column: 13, scope: !333)
!415 = distinct !{!415, !336, !416}
!416 = !DILocation(line: 128, column: 13, scope: !328)
!417 = !DILocation(line: 129, column: 9, scope: !314)
!418 = !DILocation(line: 99, column: 41, scope: !309)
!419 = !DILocation(line: 99, column: 38, scope: !309)
!420 = !DILocation(line: 99, column: 9, scope: !309)
!421 = distinct !{!421, !312, !422}
!422 = !DILocation(line: 129, column: 9, scope: !306)
!423 = !DILocation(line: 131, column: 20, scope: !267)
!424 = !DILocation(line: 131, column: 18, scope: !267)
!425 = !DILocation(line: 132, column: 5, scope: !267)
!426 = !DILocation(line: 88, column: 61, scope: !262)
!427 = !DILocation(line: 88, column: 5, scope: !262)
!428 = distinct !{!428, !265, !429}
!429 = !DILocation(line: 132, column: 5, scope: !259)
!430 = !DILocation(line: 138, column: 10, scope: !431)
!431 = distinct !DILexicalBlock(scope: !171, file: !30, line: 138, column: 10)
!432 = !DILocation(line: 138, column: 10, scope: !171)
!433 = !DILocalVariable(name: "denom", scope: !434, file: !30, line: 140, type: !32)
!434 = distinct !DILexicalBlock(scope: !431, file: !30, line: 139, column: 5)
!435 = !DILocation(line: 140, column: 16, scope: !434)
!436 = !DILocation(line: 140, column: 32, scope: !434)
!437 = !DILocation(line: 140, column: 24, scope: !434)
!438 = !DILocation(line: 142, column: 16, scope: !439)
!439 = distinct !DILexicalBlock(scope: !434, file: !30, line: 142, column: 9)
!440 = !DILocation(line: 142, column: 15, scope: !439)
!441 = !DILocation(line: 142, column: 20, scope: !442)
!442 = distinct !DILexicalBlock(scope: !439, file: !30, line: 142, column: 9)
!443 = !DILocation(line: 142, column: 24, scope: !442)
!444 = !DILocation(line: 142, column: 22, scope: !442)
!445 = !DILocation(line: 142, column: 9, scope: !439)
!446 = !DILocation(line: 144, column: 13, scope: !447)
!447 = distinct !DILexicalBlock(scope: !442, file: !30, line: 143, column: 9)
!448 = !DILocation(line: 145, column: 27, scope: !447)
!449 = !DILocation(line: 145, column: 13, scope: !447)
!450 = !DILocation(line: 145, column: 21, scope: !447)
!451 = !DILocation(line: 145, column: 24, scope: !447)
!452 = !DILocation(line: 146, column: 27, scope: !447)
!453 = !DILocation(line: 146, column: 13, scope: !447)
!454 = !DILocation(line: 146, column: 21, scope: !447)
!455 = !DILocation(line: 146, column: 24, scope: !447)
!456 = !DILocation(line: 148, column: 9, scope: !447)
!457 = !DILocation(line: 142, column: 37, scope: !442)
!458 = !DILocation(line: 142, column: 9, scope: !442)
!459 = distinct !{!459, !445, !460}
!460 = !DILocation(line: 148, column: 9, scope: !439)
!461 = !DILocation(line: 149, column: 5, scope: !434)
!462 = !DILocation(line: 150, column: 1, scope: !171)
!463 = distinct !DISubprogram(name: "CheckPointer", scope: !30, file: !30, line: 26, type: !464, isLocal: true, isDefinition: true, scopeLine: 27, flags: DIFlagPrototyped, isOptimized: false, unit: !29, retainedNodes: !4)
!464 = !DISubroutineType(types: !465)
!465 = !{null, !466, !467}
!466 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: null, size: 16)
!467 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !468, size: 16)
!468 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_signed_char)
!469 = !DILocalVariable(name: "p", arg: 1, scope: !463, file: !30, line: 26, type: !466)
!470 = !DILocation(line: 26, column: 34, scope: !463)
!471 = !DILocalVariable(name: "name", arg: 2, scope: !463, file: !30, line: 26, type: !467)
!472 = !DILocation(line: 26, column: 43, scope: !463)
!473 = !DILocation(line: 28, column: 10, scope: !474)
!474 = distinct !DILexicalBlock(scope: !463, file: !30, line: 28, column: 10)
!475 = !DILocation(line: 28, column: 12, scope: !474)
!476 = !DILocation(line: 28, column: 10, scope: !463)
!477 = !DILocation(line: 31, column: 9, scope: !478)
!478 = distinct !DILexicalBlock(scope: !474, file: !30, line: 29, column: 5)
!479 = !DILocation(line: 33, column: 1, scope: !463)
!480 = distinct !DISubprogram(name: "IsPowerOfTwo", scope: !34, file: !34, line: 35, type: !481, isLocal: false, isDefinition: true, scopeLine: 36, flags: DIFlagPrototyped, isOptimized: false, unit: !33, retainedNodes: !4)
!481 = !DISubroutineType(types: !482)
!482 = !{!28, !8}
!483 = !DILocalVariable(name: "x", arg: 1, scope: !480, file: !34, line: 35, type: !8)
!484 = !DILocation(line: 35, column: 29, scope: !480)
!485 = !DILocation(line: 37, column: 10, scope: !486)
!486 = distinct !DILexicalBlock(scope: !480, file: !34, line: 37, column: 10)
!487 = !DILocation(line: 37, column: 12, scope: !486)
!488 = !DILocation(line: 37, column: 10, scope: !480)
!489 = !DILocation(line: 38, column: 9, scope: !486)
!490 = !DILocation(line: 40, column: 10, scope: !491)
!491 = distinct !DILexicalBlock(scope: !480, file: !34, line: 40, column: 10)
!492 = !DILocation(line: 40, column: 15, scope: !491)
!493 = !DILocation(line: 40, column: 16, scope: !491)
!494 = !DILocation(line: 40, column: 12, scope: !491)
!495 = !DILocation(line: 40, column: 10, scope: !480)
!496 = !DILocation(line: 41, column: 9, scope: !491)
!497 = !DILocation(line: 43, column: 5, scope: !480)
!498 = !DILocation(line: 44, column: 1, scope: !480)
!499 = distinct !DISubprogram(name: "NumberOfBitsNeeded", scope: !34, file: !34, line: 47, type: !500, isLocal: false, isDefinition: true, scopeLine: 48, flags: DIFlagPrototyped, isOptimized: false, unit: !33, retainedNodes: !4)
!500 = !DISubroutineType(types: !501)
!501 = !{!8, !8}
!502 = !DILocalVariable(name: "PowerOfTwo", arg: 1, scope: !499, file: !34, line: 47, type: !8)
!503 = !DILocation(line: 47, column: 40, scope: !499)
!504 = !DILocalVariable(name: "i", scope: !499, file: !34, line: 49, type: !8)
!505 = !DILocation(line: 49, column: 14, scope: !499)
!506 = !DILocation(line: 51, column: 10, scope: !507)
!507 = distinct !DILexicalBlock(scope: !499, file: !34, line: 51, column: 10)
!508 = !DILocation(line: 51, column: 21, scope: !507)
!509 = !DILocation(line: 51, column: 10, scope: !499)
!510 = !DILocation(line: 54, column: 13, scope: !511)
!511 = distinct !DILexicalBlock(scope: !507, file: !34, line: 52, column: 5)
!512 = !DILocation(line: 53, column: 9, scope: !511)
!513 = !DILocation(line: 56, column: 9, scope: !511)
!514 = !DILocation(line: 59, column: 12, scope: !515)
!515 = distinct !DILexicalBlock(scope: !499, file: !34, line: 59, column: 5)
!516 = !DILocation(line: 59, column: 11, scope: !515)
!517 = !DILocation(line: 61, column: 9, scope: !518)
!518 = distinct !DILexicalBlock(scope: !519, file: !34, line: 60, column: 5)
!519 = distinct !DILexicalBlock(scope: !515, file: !34, line: 59, column: 5)
!520 = !DILocation(line: 62, column: 14, scope: !521)
!521 = distinct !DILexicalBlock(scope: !518, file: !34, line: 62, column: 14)
!522 = !DILocation(line: 62, column: 33, scope: !521)
!523 = !DILocation(line: 62, column: 30, scope: !521)
!524 = !DILocation(line: 62, column: 25, scope: !521)
!525 = !DILocation(line: 62, column: 14, scope: !518)
!526 = !DILocation(line: 63, column: 20, scope: !527)
!527 = distinct !DILexicalBlock(scope: !521, file: !34, line: 62, column: 38)
!528 = !DILocation(line: 63, column: 13, scope: !527)
!529 = !DILocation(line: 65, column: 5, scope: !518)
!530 = !DILocation(line: 59, column: 19, scope: !519)
!531 = !DILocation(line: 59, column: 5, scope: !519)
!532 = distinct !{!532, !533, !534}
!533 = !DILocation(line: 59, column: 5, scope: !515)
!534 = !DILocation(line: 65, column: 5, scope: !515)
!535 = distinct !DISubprogram(name: "ReverseBits", scope: !34, file: !34, line: 70, type: !536, isLocal: false, isDefinition: true, scopeLine: 71, flags: DIFlagPrototyped, isOptimized: false, unit: !33, retainedNodes: !4)
!536 = !DISubroutineType(types: !537)
!537 = !{!8, !8, !8}
!538 = !DILocalVariable(name: "index", arg: 1, scope: !535, file: !34, line: 70, type: !8)
!539 = !DILocation(line: 70, column: 33, scope: !535)
!540 = !DILocalVariable(name: "NumBits", arg: 2, scope: !535, file: !34, line: 70, type: !8)
!541 = !DILocation(line: 70, column: 49, scope: !535)
!542 = !DILocalVariable(name: "i", scope: !535, file: !34, line: 72, type: !8)
!543 = !DILocation(line: 72, column: 14, scope: !535)
!544 = !DILocalVariable(name: "rev", scope: !535, file: !34, line: 72, type: !8)
!545 = !DILocation(line: 72, column: 17, scope: !535)
!546 = !DILocation(line: 74, column: 16, scope: !547)
!547 = distinct !DILexicalBlock(scope: !535, file: !34, line: 74, column: 5)
!548 = !DILocation(line: 74, column: 12, scope: !547)
!549 = !DILocation(line: 74, column: 11, scope: !547)
!550 = !DILocation(line: 74, column: 20, scope: !551)
!551 = distinct !DILexicalBlock(scope: !547, file: !34, line: 74, column: 5)
!552 = !DILocation(line: 74, column: 24, scope: !551)
!553 = !DILocation(line: 74, column: 22, scope: !551)
!554 = !DILocation(line: 74, column: 5, scope: !547)
!555 = !DILocation(line: 76, column: 9, scope: !556)
!556 = distinct !DILexicalBlock(scope: !551, file: !34, line: 75, column: 5)
!557 = !DILocation(line: 77, column: 16, scope: !556)
!558 = !DILocation(line: 77, column: 20, scope: !556)
!559 = !DILocation(line: 77, column: 29, scope: !556)
!560 = !DILocation(line: 77, column: 35, scope: !556)
!561 = !DILocation(line: 77, column: 26, scope: !556)
!562 = !DILocation(line: 77, column: 13, scope: !556)
!563 = !DILocation(line: 78, column: 15, scope: !556)
!564 = !DILocation(line: 80, column: 5, scope: !556)
!565 = !DILocation(line: 74, column: 34, scope: !551)
!566 = !DILocation(line: 74, column: 5, scope: !551)
!567 = distinct !{!567, !554, !568}
!568 = !DILocation(line: 80, column: 5, scope: !547)
!569 = !DILocation(line: 82, column: 12, scope: !535)
!570 = !DILocation(line: 82, column: 5, scope: !535)
