; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

@a = dso_local global i16 0, align 2, !dbg !0

; Function Attrs: noinline nounwind optnone
define dso_local i16 @f() #0 !dbg !11 {
  %1 = load i16, i16* @a, align 2, !dbg !14
  %2 = add nsw i16 %1, 3, !dbg !15
  ret i16 %2, !dbg !16
}

; Function Attrs: noinline nounwind optnone
define dso_local i16 @g() #0 !dbg !17 {
  %1 = call i16 @f(), !dbg !18
  ret i16 %1, !dbg !19
}

; Function Attrs: noinline nounwind optnone
define dso_local i16 @main() #0 !dbg !20 {
  %1 = alloca i16, align 2
  store i16 0, i16* %1, align 2
  %2 = call i16 @f(), !dbg !21
  store i16 %2, i16* @a, align 2, !dbg !22
  %3 = load i16, i16* @a, align 2, !dbg !23
  %4 = icmp sgt i16 %3, 2, !dbg !25
  br i1 %4, label %5, label %7, !dbg !26

; <label>:5:                                      ; preds = %0
  %6 = call i16 @g(), !dbg !27
  store i16 %6, i16* @a, align 2, !dbg !29
  br label %7, !dbg !30

; <label>:7:                                      ; preds = %5, %0
  %8 = load i16, i16* @a, align 2, !dbg !31
  ret i16 %8, !dbg !32
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.dbg.cu = !{!2}
!llvm.module.flags = !{!7, !8, !9}
!llvm.ident = !{!10}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "a", scope: !2, file: !3, line: 3, type: !6, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5)
!3 = !DIFile(filename: "main.c", directory: "/home/user/Desktop/alfred/samples/simple")
!4 = !{}
!5 = !{!0}
!6 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!7 = !{i32 2, !"Dwarf Version", i32 4}
!8 = !{i32 2, !"Debug Info Version", i32 3}
!9 = !{i32 1, !"wchar_size", i32 2}
!10 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
!11 = distinct !DISubprogram(name: "f", scope: !3, file: !3, line: 5, type: !12, isLocal: false, isDefinition: true, scopeLine: 5, isOptimized: false, unit: !2, retainedNodes: !4)
!12 = !DISubroutineType(types: !13)
!13 = !{!6}
!14 = !DILocation(line: 6, column: 12, scope: !11)
!15 = !DILocation(line: 6, column: 13, scope: !11)
!16 = !DILocation(line: 6, column: 5, scope: !11)
!17 = distinct !DISubprogram(name: "g", scope: !3, file: !3, line: 9, type: !12, isLocal: false, isDefinition: true, scopeLine: 9, isOptimized: false, unit: !2, retainedNodes: !4)
!18 = !DILocation(line: 10, column: 11, scope: !17)
!19 = !DILocation(line: 10, column: 4, scope: !17)
!20 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 13, type: !12, isLocal: false, isDefinition: true, scopeLine: 13, isOptimized: false, unit: !2, retainedNodes: !4)
!21 = !DILocation(line: 14, column: 10, scope: !20)
!22 = !DILocation(line: 14, column: 8, scope: !20)
!23 = !DILocation(line: 15, column: 9, scope: !24)
!24 = distinct !DILexicalBlock(scope: !20, file: !3, line: 15, column: 9)
!25 = !DILocation(line: 15, column: 11, scope: !24)
!26 = !DILocation(line: 15, column: 9, scope: !20)
!27 = !DILocation(line: 16, column: 13, scope: !28)
!28 = distinct !DILexicalBlock(scope: !24, file: !3, line: 15, column: 16)
!29 = !DILocation(line: 16, column: 11, scope: !28)
!30 = !DILocation(line: 17, column: 6, scope: !28)
!31 = !DILocation(line: 19, column: 12, scope: !20)
!32 = !DILocation(line: 19, column: 5, scope: !20)
