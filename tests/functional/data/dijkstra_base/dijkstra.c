#include "../bareBench.h"
#include "input.h"

#define NULL 0
#define NUM_QITEM 1000
#define NUM_NODES 30
#define NONE 9999

struct _NODE
{
  int iDist;
  int iPrev;
};
typedef struct _NODE NODE;

struct _QITEM
{
  int iNode;
  int iDist;
  int iPrev;
  struct _QITEM *qNext;
};
typedef struct _QITEM QITEM;

unsigned alloc_indx = 0;
QITEM allocated[NUM_QITEM];
QITEM *qHead = NULL;

volatile int g_qCount = 0;
NODE rgnNodes[NUM_NODES];
int ch;
int iPrev, iNode;
int i, iCost, iDist;

void print_path(NODE *rgnNodes, int chNode)
{
  printf(" %d", chNode);
  while(rgnNodes[chNode].iPrev != NONE) {
      chNode = rgnNodes[chNode].iPrev;
      printf(" %d", chNode);
  }
}

void enqueue(int iNode, int iDist, int iPrev)
{
  QITEM *qNew = &allocated[alloc_indx];
  QITEM *qLast = qHead;

  if (alloc_indx >= NUM_QITEM)
  {
    printf("Out of memory.\n");
    // exit(1);
  }
  alloc_indx++;

  qNew->iNode = iNode;
  qNew->iDist = iDist;
  qNew->iPrev = iPrev;
  qNew->qNext = NULL;

  if (!qLast)
  {
    qHead = qNew;
  }
  else
  {
    qLast = &allocated[alloc_indx - 2];
    qLast->qNext = qNew;
  }
  g_qCount++;
}

void dequeue(int *piNode, int *piDist, int *piPrev)
{
  QITEM *qKill = qHead;
  if (qHead)
  {
    *piNode = qHead->iNode;
    *piDist = qHead->iDist;
    *piPrev = qHead->iPrev;
    qHead = qHead->qNext;
    // free(qKill);
    g_qCount--;
  }
}

int qcount(void)
{
  return (g_qCount);
}

int dijkstra(int chStart, int chEnd)
{

  for (ch = 0; ch < NUM_NODES; ch++)
  {
    rgnNodes[ch].iDist = NONE;
    rgnNodes[ch].iPrev = NONE;
  }
  if (chStart == chEnd)
  {
    printf("Shortest path is 0 in cost. Just stay where you are.\n");
  }
  else
  {
    rgnNodes[chStart].iDist = 0;
    rgnNodes[chStart].iPrev = NONE;
    enqueue(chStart, 0, NONE);

    while (qcount() > 0)
    {
      dequeue(&iNode, &iDist, &iPrev);
      for (i = 0; i < NUM_NODES; i++)
      {
        iCost = AdjMatrix[iNode][i];
        if (iCost != NONE)
        {
          int node_dist = rgnNodes[i].iDist;
          int cond = (node_dist == NONE || node_dist > (iCost + iDist));
          if (cond)
          {
            rgnNodes[i].iDist = iDist + iCost;
            rgnNodes[i].iPrev = iNode;
            enqueue(i, iDist + iCost, iNode);
          }
        }
      }
    }

    printf("Shortest path is %d in cost. ", rgnNodes[chEnd].iDist);
    printf("Path is: ");
    print_path(rgnNodes, chEnd);
    printf("\n");
  }
  return 0;
}

int main()
{
  int i, j;
  /* make a fully connected matrix */
  // see input.h

  /* finds 10 shortest paths between nodes */
  for (i = 0, j = NUM_NODES / 2; i < 10; i++, j++)
  {
    j = j % NUM_NODES;
    dijkstra(i, j);
    alloc_indx = 0;
  }
  return 0;
}
