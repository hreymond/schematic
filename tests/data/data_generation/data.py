import os
import dill
from ScEpTIC.emulator.energy.msp430_worst_case import MSP430WorstCaseEnergyCalculator
from tests.data.data_generation.simple_cfgs import load_pickle, generate_is_even_cfg

DATA_PATH = os.path.dirname(__file__)
IS_EVEN_CFG = generate_is_even_cfg()

with open(os.path.join(DATA_PATH, "conditional_cfg/conditional_cfg.dump"), "rb") as f:
    CONDITIONAL_CFG = dill.load(f)
CONDITIONAL_CFG_MEMORY_TAGS = load_pickle("/conditional_cfg/memory_tag_chain.dump")

with open(os.path.join(DATA_PATH, "simple_cfg/cfg.dump"), "rb") as f:
    SIMPLE_CFG = dill.load(f)

SIMPLE_CFG_MEMORY_TAGS = load_pickle("/simple_cfg/memory_tag_chain.dump")
SIMPLE_CFG_CHECKPOINT_PLACED = load_pickle("/simple_cfg/cfg_checkpoint_placed.dump")

# Not used since energy per cycle is redefined
wc_current_consumption = {
    "frequency": "8M",  # Clock Frequency
    "voltage": "3.6",  # Voltage
    "n_waits": "0",  # Number of wait cycles for FRAM accesses
    "I_am_fram": "2510u",
    # Current consumption for one clock cycle when program resides in FRAM and data in SRAM (cache hit 0%)
    "I_am_ram": "585u",  # Current consumption for one clock cycle when program and data resides in SRAM
    "fram_access_overhead": "1.3",  # Overhead per cycle for memory access to data in FRAM compared to SRAM
}
ENERGY_CALCULATOR = MSP430WorstCaseEnergyCalculator(wc_current_consumption)
# Parameters for testing purpose
ENERGY_CALCULATOR.energy_clock_cycle = 1
ENERGY_CALCULATOR.nvm_extra_cycles = 1
ENERGY_CALCULATOR.energy_non_volatile_memory_access = 0
ENERGY_CALCULATOR.energy_volatile_memory_access = 0
ENERGY_CALCULATOR.non_volatile_increase = 1
ENERGY_CALCULATOR.energy_vm_access = 1
ENERGY_CALCULATOR.energy_nvm_access = 2
