import os
import pickle
from pathlib import Path

import networkx as nx

import ScEpTIC
from ScEpTIC.AST.elements.instructions.binary_operation import BinaryOperation
from ScEpTIC.AST.elements.instructions.memory_operations import AllocaOperation, StoreOperation, LoadOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import ReturnOperation
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.llvmir_parser import parse_file


def generate_is_even_cfg():
    """
    Generate a simple cfg corresponding to the c function is_even(). The cfg contains 1 basic bloc and no edges

    The c function:
    ```c
    int is_even(nb) {
     return nb%2;
    }
    ```
    """
    bb = BasicBlock(0, "@is_even")
    bb.instructions.append(AllocaOperation(None, None, 0, 0))  # 1 cycle
    bb.instructions.append(StoreOperation("%1", "0", 0, False))  # 2 cycles
    bb.instructions.append(LoadOperation("%1", None, "0", 0, False))  # 2 cycles
    bb.instructions.append(BinaryOperation("srem", None, None, None, None, {}))  # 1 cycle
    bb.instructions.append(ReturnOperation(None))  # 8 cycles (4 classic + 4 memory_cycles)

    cfg = nx.DiGraph()
    cfg.add_node(bb)

    return cfg


def load_pickle(path):
    file_path = str(Path(__file__).resolve().parent) + path
    with open(file_path, "rb") as f:
        return pickle.load(f)
