; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@old_sum = dso_local global i32 4, align 4, !dbg !0
@buff = dso_local global i32 0, align 4, !dbg !6
@variation = common dso_local global i32 0, align 4, !dbg !9

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @main() #0 !dbg !15 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !18, metadata !DIExpression()), !dbg !19
  store i32 0, i32* %2, align 4, !dbg !19
  %4 = load i32, i32* %2, align 4, !dbg !20
  %5 = add nsw i32 %4, 1, !dbg !20
  store i32 %5, i32* %2, align 4, !dbg !20
  %6 = load i32, i32* %2, align 4, !dbg !21
  %7 = add nsw i32 %6, 1, !dbg !21
  store i32 %7, i32* %2, align 4, !dbg !21
  %8 = load i32, i32* %2, align 4, !dbg !22
  %9 = add nsw i32 %8, 1, !dbg !22
  store i32 %9, i32* %2, align 4, !dbg !22
  %10 = load i32, i32* %2, align 4, !dbg !23
  %11 = add nsw i32 %10, 1, !dbg !23
  store i32 %11, i32* %2, align 4, !dbg !23
  %12 = load i32, i32* %2, align 4, !dbg !24
  %13 = add nsw i32 %12, 1, !dbg !24
  store i32 %13, i32* %2, align 4, !dbg !24
  br label %14, !dbg !25

; <label>:14:                                     ; preds = %0
  %15 = load i32, i32* %2, align 4, !dbg !26
  %16 = load i32, i32* @old_sum, align 4, !dbg !27
  %17 = sub nsw i32 %15, %16, !dbg !28
  store i32 %17, i32* @variation, align 4, !dbg !29
  call void @llvm.dbg.declare(metadata i32* %3, metadata !30, metadata !DIExpression()), !dbg !31
  store i32 0, i32* %3, align 4, !dbg !31
  br label %18, !dbg !32

; <label>:18:                                     ; preds = %14
  %19 = load i32, i32* @variation, align 4, !dbg !33
  %20 = shl i32 %19, 2, !dbg !34
  store i32 %20, i32* @variation, align 4, !dbg !35
  %21 = load i32, i32* %3, align 4, !dbg !36
  %22 = add nsw i32 %21, 1, !dbg !36
  store i32 %22, i32* %3, align 4, !dbg !36
  %23 = load i32, i32* @variation, align 4, !dbg !37
  %24 = shl i32 %23, 2, !dbg !38
  store i32 %24, i32* @variation, align 4, !dbg !39
  %25 = load i32, i32* %3, align 4, !dbg !40
  %26 = add nsw i32 %25, 1, !dbg !40
  store i32 %26, i32* %3, align 4, !dbg !40
  %27 = load i32, i32* @variation, align 4, !dbg !41
  %28 = shl i32 %27, 2, !dbg !42
  store i32 %28, i32* @variation, align 4, !dbg !43
  %29 = load i32, i32* %3, align 4, !dbg !44
  %30 = add nsw i32 %29, 1, !dbg !44
  store i32 %30, i32* %3, align 4, !dbg !44
  %31 = load i32, i32* @variation, align 4, !dbg !45
  ret i32 %31, !dbg !46
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

attributes #0 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }

!llvm.dbg.cu = !{!2}
!llvm.module.flags = !{!11, !12, !13}
!llvm.ident = !{!14}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "old_sum", scope: !2, file: !3, line: 21, type: !8, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5)
!3 = !DIFile(filename: "main.c", directory: "/home/user/Desktop/alfred/tests/data/simple2")
!4 = !{}
!5 = !{!0, !6, !9}
!6 = !DIGlobalVariableExpression(var: !7, expr: !DIExpression())
!7 = distinct !DIGlobalVariable(name: "buff", scope: !2, file: !3, line: 23, type: !8, isLocal: false, isDefinition: true)
!8 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!9 = !DIGlobalVariableExpression(var: !10, expr: !DIExpression())
!10 = distinct !DIGlobalVariable(name: "variation", scope: !2, file: !3, line: 22, type: !8, isLocal: false, isDefinition: true)
!11 = !{i32 2, !"Dwarf Version", i32 4}
!12 = !{i32 2, !"Debug Info Version", i32 3}
!13 = !{i32 1, !"wchar_size", i32 4}
!14 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
!15 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 25, type: !16, isLocal: false, isDefinition: true, scopeLine: 25, isOptimized: false, unit: !2, retainedNodes: !4)
!16 = !DISubroutineType(types: !17)
!17 = !{!8}
!18 = !DILocalVariable(name: "sum", scope: !15, file: !3, line: 26, type: !8)
!19 = !DILocation(line: 26, column: 9, scope: !15)
!20 = !DILocation(line: 27, column: 8, scope: !15)
!21 = !DILocation(line: 28, column: 8, scope: !15)
!22 = !DILocation(line: 29, column: 8, scope: !15)
!23 = !DILocation(line: 30, column: 8, scope: !15)
!24 = !DILocation(line: 31, column: 8, scope: !15)
!25 = !DILocation(line: 32, column: 5, scope: !15)
!26 = !DILocation(line: 34, column: 17, scope: !15)
!27 = !DILocation(line: 34, column: 23, scope: !15)
!28 = !DILocation(line: 34, column: 21, scope: !15)
!29 = !DILocation(line: 34, column: 15, scope: !15)
!30 = !DILocalVariable(name: "nb_packet", scope: !15, file: !3, line: 35, type: !8)
!31 = !DILocation(line: 35, column: 9, scope: !15)
!32 = !DILocation(line: 36, column: 5, scope: !15)
!33 = !DILocation(line: 38, column: 17, scope: !15)
!34 = !DILocation(line: 38, column: 27, scope: !15)
!35 = !DILocation(line: 38, column: 15, scope: !15)
!36 = !DILocation(line: 39, column: 14, scope: !15)
!37 = !DILocation(line: 40, column: 17, scope: !15)
!38 = !DILocation(line: 40, column: 27, scope: !15)
!39 = !DILocation(line: 40, column: 15, scope: !15)
!40 = !DILocation(line: 41, column: 14, scope: !15)
!41 = !DILocation(line: 42, column: 17, scope: !15)
!42 = !DILocation(line: 42, column: 27, scope: !15)
!43 = !DILocation(line: 42, column: 15, scope: !15)
!44 = !DILocation(line: 43, column: 14, scope: !15)
!45 = !DILocation(line: 44, column: 12, scope: !15)
!46 = !DILocation(line: 44, column: 5, scope: !15)
