// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll -target msp430
/*** This example has been inspired by the presentation done during NOP Seminar

save_state();
int sum = 0;
for(int i=0; i<SIZE; i++)
  sum += tab[i];

save_state() ?

int pkt_sent = 0;
int variation = var(sum);

save_state() ?

pkt_sent = tx_send(variation);
save_state();

*
*/
int old_sum = 5;
int variation;

int main(){
    // Cost all NVM = 2 * 1 [alloca (sum + ret)] + 2 [store 0, sum] + 5 * 5 [read sum + add 1 + store sum]  + 0  [branch] = 29
    // Cost sum in VM = 2 * 1 [alloca (sum + ret)] + 2 [store 0, sum] + 5 * 3 [read sum + add 1 + store sum]  + 0  [branch] = 18
    int sum = 0;
    sum++;
    sum++;
    sum++;
    sum++;
    sum++;
    goto bb2;
bb2:
    // Cost all NVM = 2 * 2 [Load sum, old_sum] + 1 [sum - old_sum] + 2 * 2 [store variation, nb_packet] = 9
    // Cost variation/nb_packet in VM = 7
    variation = sum - old_sum;
    int nb_packet = 0;
    goto bb3;
bb3:
    // Cost all NVM = 3 * (2 [load] + 1 [shift] + 2 [store]) + 3 * (2 [load] + 1 [add] + 2 [store]) + 8 [ret] = 38
    // Cost all VM = 3 * 3 + 3 * 3 + 8 = 26
    variation = variation << 2;
    nb_packet++;
    variation = variation << 2;
    nb_packet++;
    variation = variation << 2;
    nb_packet++;
}