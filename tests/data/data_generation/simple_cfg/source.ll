; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-p:16:16-i32:16-i64:16-f32:16-f64:16-a:8-n8:16-S16"
target triple = "msp430"

@old_sum = dso_local global i16 5, align 2, !dbg !0
@variation = common dso_local global i16 0, align 2, !dbg !6

; Function Attrs: noinline nounwind optnone
define dso_local i16 @main() #0 !dbg !13 {
  %1 = alloca i16, align 2
  %2 = alloca i16, align 2
  call void @llvm.dbg.declare(metadata i16* %1, metadata !16, metadata !DIExpression()), !dbg !17
  store i16 0, i16* %1, align 2, !dbg !17
  %3 = load i16, i16* %1, align 2, !dbg !18
  %4 = add nsw i16 %3, 1, !dbg !18
  store i16 %4, i16* %1, align 2, !dbg !18
  %5 = load i16, i16* %1, align 2, !dbg !19
  %6 = add nsw i16 %5, 1, !dbg !19
  store i16 %6, i16* %1, align 2, !dbg !19
  %7 = load i16, i16* %1, align 2, !dbg !20
  %8 = add nsw i16 %7, 1, !dbg !20
  store i16 %8, i16* %1, align 2, !dbg !20
  %9 = load i16, i16* %1, align 2, !dbg !21
  %10 = add nsw i16 %9, 1, !dbg !21
  store i16 %10, i16* %1, align 2, !dbg !21
  %11 = load i16, i16* %1, align 2, !dbg !22
  %12 = add nsw i16 %11, 1, !dbg !22
  store i16 %12, i16* %1, align 2, !dbg !22
  br label %13, !dbg !23

; <label>:13:                                     ; preds = %0
  %14 = load i16, i16* %1, align 2, !dbg !24
  %15 = load i16, i16* @old_sum, align 2, !dbg !25
  %16 = sub nsw i16 %14, %15, !dbg !26
  store i16 %16, i16* @variation, align 2, !dbg !27
  call void @llvm.dbg.declare(metadata i16* %2, metadata !28, metadata !DIExpression()), !dbg !29
  store i16 0, i16* %2, align 2, !dbg !29
  br label %17, !dbg !30

; <label>:17:                                     ; preds = %13
  %18 = load i16, i16* @variation, align 2, !dbg !31
  %19 = shl i16 %18, 2, !dbg !32
  store i16 %19, i16* @variation, align 2, !dbg !33
  %20 = load i16, i16* %2, align 2, !dbg !34
  %21 = add nsw i16 %20, 1, !dbg !34
  store i16 %21, i16* %2, align 2, !dbg !34
  %22 = load i16, i16* @variation, align 2, !dbg !35
  %23 = shl i16 %22, 2, !dbg !36
  store i16 %23, i16* @variation, align 2, !dbg !37
  %24 = load i16, i16* %2, align 2, !dbg !38
  %25 = add nsw i16 %24, 1, !dbg !38
  store i16 %25, i16* %2, align 2, !dbg !38
  %26 = load i16, i16* @variation, align 2, !dbg !39
  %27 = shl i16 %26, 2, !dbg !40
  store i16 %27, i16* @variation, align 2, !dbg !41
  %28 = load i16, i16* %2, align 2, !dbg !42
  %29 = add nsw i16 %28, 1, !dbg !42
  store i16 %29, i16* %2, align 2, !dbg !42
  ret i16 0, !dbg !43
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }

!llvm.dbg.cu = !{!2}
!llvm.module.flags = !{!9, !10, !11}
!llvm.ident = !{!12}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "old_sum", scope: !2, file: !3, line: 21, type: !8, isLocal: false, isDefinition: true)
!2 = distinct !DICompileUnit(language: DW_LANG_C99, file: !3, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !4, globals: !5)
!3 = !DIFile(filename: "main.c", directory: "/home/user/Desktop/alfred/tests/data/data_generation/simple_cfg")
!4 = !{}
!5 = !{!0, !6}
!6 = !DIGlobalVariableExpression(var: !7, expr: !DIExpression())
!7 = distinct !DIGlobalVariable(name: "variation", scope: !2, file: !3, line: 22, type: !8, isLocal: false, isDefinition: true)
!8 = !DIBasicType(name: "int", size: 16, encoding: DW_ATE_signed)
!9 = !{i32 2, !"Dwarf Version", i32 4}
!10 = !{i32 2, !"Debug Info Version", i32 3}
!11 = !{i32 1, !"wchar_size", i32 2}
!12 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
!13 = distinct !DISubprogram(name: "main", scope: !3, file: !3, line: 24, type: !14, isLocal: false, isDefinition: true, scopeLine: 24, isOptimized: false, unit: !2, retainedNodes: !4)
!14 = !DISubroutineType(types: !15)
!15 = !{!8}
!16 = !DILocalVariable(name: "sum", scope: !13, file: !3, line: 27, type: !8)
!17 = !DILocation(line: 27, column: 9, scope: !13)
!18 = !DILocation(line: 28, column: 8, scope: !13)
!19 = !DILocation(line: 29, column: 8, scope: !13)
!20 = !DILocation(line: 30, column: 8, scope: !13)
!21 = !DILocation(line: 31, column: 8, scope: !13)
!22 = !DILocation(line: 32, column: 8, scope: !13)
!23 = !DILocation(line: 33, column: 5, scope: !13)
!24 = !DILocation(line: 37, column: 17, scope: !13)
!25 = !DILocation(line: 37, column: 23, scope: !13)
!26 = !DILocation(line: 37, column: 21, scope: !13)
!27 = !DILocation(line: 37, column: 15, scope: !13)
!28 = !DILocalVariable(name: "nb_packet", scope: !13, file: !3, line: 38, type: !8)
!29 = !DILocation(line: 38, column: 9, scope: !13)
!30 = !DILocation(line: 39, column: 5, scope: !13)
!31 = !DILocation(line: 43, column: 17, scope: !13)
!32 = !DILocation(line: 43, column: 27, scope: !13)
!33 = !DILocation(line: 43, column: 15, scope: !13)
!34 = !DILocation(line: 44, column: 14, scope: !13)
!35 = !DILocation(line: 45, column: 17, scope: !13)
!36 = !DILocation(line: 45, column: 27, scope: !13)
!37 = !DILocation(line: 45, column: 15, scope: !13)
!38 = !DILocation(line: 46, column: 14, scope: !13)
!39 = !DILocation(line: 47, column: 17, scope: !13)
!40 = !DILocation(line: 47, column: 27, scope: !13)
!41 = !DILocation(line: 47, column: 15, scope: !13)
!42 = !DILocation(line: 48, column: 14, scope: !13)
!43 = !DILocation(line: 49, column: 1, scope: !13)
