; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @main() #0 !dbg !7 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  call void @llvm.dbg.declare(metadata i32* %2, metadata !11, metadata !DIExpression()), !dbg !12
  store i32 0, i32* %2, align 4, !dbg !12
  call void @llvm.dbg.declare(metadata i32* %3, metadata !13, metadata !DIExpression()), !dbg !14
  store i32 1, i32* %3, align 4, !dbg !14
  call void @llvm.dbg.declare(metadata i32* %4, metadata !15, metadata !DIExpression()), !dbg !16
  store i32 0, i32* %4, align 4, !dbg !16
  %5 = load i32, i32* %2, align 4, !dbg !17
  %6 = load i32, i32* %3, align 4, !dbg !19
  %7 = icmp sgt i32 %5, %6, !dbg !20
  br i1 %7, label %8, label %12, !dbg !21

; <label>:8:                                      ; preds = %0
  %9 = load i32, i32* %2, align 4, !dbg !22
  %10 = add nsw i32 %9, 1, !dbg !24
  store i32 %10, i32* %2, align 4, !dbg !25
  %11 = load i32, i32* %2, align 4, !dbg !26
  store i32 %11, i32* %4, align 4, !dbg !27
  br label %14, !dbg !28

; <label>:12:                                     ; preds = %0
  %13 = load i32, i32* %3, align 4, !dbg !29
  store i32 %13, i32* %4, align 4, !dbg !31
  br label %14

; <label>:14:                                     ; preds = %12, %8
  %15 = load i32, i32* %4, align 4, !dbg !32
  ret i32 %15, !dbg !33
}

; Function Attrs: nounwind readnone speculatable
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

attributes #0 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!3, !4, !5}
!llvm.ident = !{!6}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 7.0.1-12 (tags/RELEASE_701/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2)
!1 = !DIFile(filename: "main.c", directory: "/home/user/Desktop/alfred/samples/simple")
!2 = !{}
!3 = !{i32 2, !"Dwarf Version", i32 4}
!4 = !{i32 2, !"Debug Info Version", i32 3}
!5 = !{i32 1, !"wchar_size", i32 4}
!6 = !{!"clang version 7.0.1-12 (tags/RELEASE_701/final)"}
!7 = distinct !DISubprogram(name: "main", scope: !1, file: !1, line: 3, type: !8, isLocal: false, isDefinition: true, scopeLine: 3, isOptimized: false, unit: !0, retainedNodes: !2)
!8 = !DISubroutineType(types: !9)
!9 = !{!10}
!10 = !DIBasicType(name: "int", size: 32, encoding: DW_ATE_signed)
!11 = !DILocalVariable(name: "a", scope: !7, file: !1, line: 5, type: !10)
!12 = !DILocation(line: 5, column: 9, scope: !7)
!13 = !DILocalVariable(name: "b", scope: !7, file: !1, line: 6, type: !10)
!14 = !DILocation(line: 6, column: 9, scope: !7)
!15 = !DILocalVariable(name: "res", scope: !7, file: !1, line: 7, type: !10)
!16 = !DILocation(line: 7, column: 9, scope: !7)
!17 = !DILocation(line: 8, column: 8, scope: !18)
!18 = distinct !DILexicalBlock(scope: !7, file: !1, line: 8, column: 8)
!19 = !DILocation(line: 8, column: 12, scope: !18)
!20 = !DILocation(line: 8, column: 10, scope: !18)
!21 = !DILocation(line: 8, column: 8, scope: !7)
!22 = !DILocation(line: 9, column: 13, scope: !23)
!23 = distinct !DILexicalBlock(scope: !18, file: !1, line: 8, column: 15)
!24 = !DILocation(line: 9, column: 15, scope: !23)
!25 = !DILocation(line: 9, column: 11, scope: !23)
!26 = !DILocation(line: 10, column: 15, scope: !23)
!27 = !DILocation(line: 10, column: 13, scope: !23)
!28 = !DILocation(line: 11, column: 5, scope: !23)
!29 = !DILocation(line: 12, column: 15, scope: !30)
!30 = distinct !DILexicalBlock(scope: !18, file: !1, line: 11, column: 12)
!31 = !DILocation(line: 12, column: 13, scope: !30)
!32 = !DILocation(line: 15, column: 12, scope: !7)
!33 = !DILocation(line: 15, column: 5, scope: !7)
