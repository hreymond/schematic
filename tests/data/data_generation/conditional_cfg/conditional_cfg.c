// Compile with clang-7 -emit-llvm -O0 -g -S main.c -o source.ll

int main(){
    checkpoint();
    int a = 0;
    int b = 1;
    int res = 0;
    if(a > b) {
        a++;
        res = a;
    } else {
        res = b;
    }
    checkpoint();
    return res;
}