import logging


def apply_transformation(transformation, functions_ast, vmstate):
    """
    Applies a program transformation.
    Note that any transformation must be specified as a sub-module of the transformation module.
    The name of the module represents the name of the transformation, which is passed as argument to this method.
    Each transformation sub-module need a apply_transformation() function that takes as argument the functions AST.
    """
    # Import custom module
    module_name = "ScEpTIC.AST.transformations.{}".format(transformation)
    function_name = "apply_transformation"
    module = __import__(module_name, fromlist=[function_name])
    transformation_function = getattr(module, function_name)

    # Apply transformation
    transformation_function(functions_ast, vmstate)
