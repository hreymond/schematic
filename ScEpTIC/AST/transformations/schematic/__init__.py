import logging
import time
from typing import Dict

import networkx as nx

from ScEpTIC.AST.elements.types import Type, BaseType
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.schematic.analysis.energy_consumption import EnergyConsumptionEstimationPass
from ScEpTIC.AST.transformations.schematic.analysis.variable_access_count import VariableAccessCountPass
from ScEpTIC.AST.transformations.schematic.cfg_modification import isolate_function_calls
from ScEpTIC.AST.transformations.schematic.code_transformation import CodeTransformation
from ScEpTIC.AST.transformations.schematic.parsers.trace_parser import parse_trace_file
from ScEpTIC.AST.transformations.schematic.utils.cfg_utils import export_cfg_pickle, export_memtagchain, save_cfgs
from ScEpTIC.AST.transformations.schematic.schematic import MyTransformation
from ScEpTIC.AST.transformations.schematic.parsers.ast_parser import ASTParser
from ScEpTIC.AST.transformations.schematic.parsers.cfg_parser import asts_to_cfgs, synchronise_cfg_to_ast
from ScEpTIC.AST.transformations.schematic.parsers.memory_tags_parser import MemoryTagsParser
from ScEpTIC.emulator.energy.msp430_worst_case import MSP430WorstCaseEnergyCalculator
from ScEpTIC.exceptions import ConfigurationException
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation


def relocate_access_to_nvm(functions: Dict):
    """
    Relocated each target of memory accesses (Store, Load, Call, Return, ...) to NVM
    """
    for function in functions.values():
        for inst in function.body:
            if hasattr(inst, "virtual_memory_target"):
                inst.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE


def apply_transformation(functions, vmstate):
    """
    Apply the schematic joint checkpoint placement and memory allocation algorithm to the program.

    1. Parse the program and create an AST via Alfred ASTParser
    2. Convert the parsed AST to a CFG
    3. Analyse and modify the CFG for schematic checkpoint placement and memory allocation algorithm
        1. Isolate function calls into new basic blocks
        2. Estimate energy consumption of each basic block
        3. Compute the variable linked to stored/load (using alfred MemoryTagsParser)
        4. Compute the number of access to each variable for each basic block
    4. Apply the schematic algorithm to the CFG (see schematic.py)
    5. Modify the CFG instructions to insert checkpoints and apply memory allocation
    6. Convert the modified CFG to an AST usable by the ScEpTIC emulator

    # Configuration options:

    schematic transformation is activated by adding the "schematic" key to the transformation_options dict in
    config.py.

    To configure the transformation, the following options are available:
    - `trace_file`: The path to the file containing the traces of the program.
    - `energy_budget`: The energy budget to use for the transformation (in clock cycles).
    - `vm_size`: The size of the virtual memory (in bytes).
    - `worst_case_current_consumption`: The worst case current consumption datasheet of the MSP430.
    - `authorize_conditional_checkpoints`: If true, checkpoints placed on a loop latch can be executed conditionally,
    given the number of iteration performed by the loop.
    - `joint_allocation_and_checkpoint`: If true, checkpoint placement and memory allocation are performed jointly.
    Otherwise, checkpoint placement is performed first, then memory allocation.
    - Optional: 'checkpoint_save`: The energy cost of saving a checkpoint (in clock cycles).
    - Optional: 'checkpoint_restore`: The energy cost of restoring a checkpoint (in clock cycles).
    - Optional: 'call_cost`: The energy cost of calling a function (in clock cycles).
    """
    start_t = time.time()
    config = vmstate.transformation_options["schematic"]
    worst_case_datasheet = config["worst_case_current_consumption"]
    if "trace_file" not in config:
        raise ConfigurationException("Please specifiy the filename containing the traces of the program: `trace_file`")
    trace_file = config["trace_file"]
    energy_budget = int(config["energy_budget"])

    # Word size (in bits) and memory size (in bytes)
    word_type = BaseType("int", 16)
    memory_size = 2000
    if "vm_size" in config:
        memory_size = int(config["vm_size"])
    else:
        logging.warning("VM_size not set, using default value of 2kb")

    # Relocate all memory accesses to target NVM
    relocate_access_to_nvm(functions)

    # Parse the functions of the program and create an AST usable by Alfred
    parser = ASTParser(functions)
    parser.parse()

    # Parse debug metadata to locate memory accesses
    mem_parser = MemoryTagsParser(parser)
    mem_parser.resolve_memory_tags()

    # Replace "conditional" and "Loops" basic blocs in the AST by classic BB
    parser.reparse()

    # Creates the CFGs from the asts
    cfgs = asts_to_cfgs(parser)

    save_cfgs(cfgs, "dot_cfgs/parsed_cfgs")

    # Get builtins functions name
    builtins_functions = set([f.name for f in filter(lambda f: f.is_builtin, functions.values())])
    # Isolate the function calls into new basic blocks
    function_depgraph: nx.DiGraph = isolate_function_calls(cfgs, builtins_functions)
    CallOperation.ignore.append([name[1:] for name in builtins_functions])

    try:
        dot_cfg = nx.nx_agraph.to_agraph(function_depgraph)
        dot_cfg.write("function_dep.dot")
    except ImportError:
        pass

    # Check for cycle in function dependency graph, i.e recursion
    contains_recursive_fn = next(nx.simple_cycles(function_depgraph), False)
    if contains_recursive_fn:
        raise Exception("[SCHEMATIC] SCHEMATIC does not support recursion yet, cannot handle function {}")

    # Uncomment the following to dump the cfg state (pickle format)
    # export_cfg_pickle(cfgs["@main"], "cfg.dump")
    # export_memtagchain(mem_parser.memory_tag_chains)

    msp430_energy_calculator = MSP430WorstCaseEnergyCalculator(worst_case_datasheet)
    msp430_energy_calculator.set_test_config()

    # Estimate energy consumption of the basic blocks
    energy_estimation_pass = EnergyConsumptionEstimationPass(msp430_energy_calculator, vmstate, builtins_functions)
    energy_estimation_pass.apply(cfgs)

    # Estimate variable accesses of the basic blocks
    VariableAccessCountPass.apply(cfgs, mem_parser.memory_tag_chains)
    save_cfgs(cfgs, "dot_cfgs/before_analysis")

    # Load trace file and create a dict containing the traces, ordered by number of execution
    # For example, a final dict can be:
    # { "@main": [[BB0, BB7, BB12], [BB0, BB12]], "@f": [[BB0]] }
    traces = parse_trace_file(trace_file, cfgs)

    # Setup checkpoint placement/memory allocation parameters
    config["vm_size"] = memory_size
    save_restore_1_reg_cost = msp430_energy_calculator.energy_nvm_access
    config["chkpt_restore"] = config.get("chkpt_restore", 16 * save_restore_1_reg_cost)
    config["chkpt_save"] = config.get("chkpt_save", 16 * save_restore_1_reg_cost)

    if "call_cost" not in config:
        call_cost_cycles = (
            CallOperation.tick_count + CallOperation.memory_tick_count * msp430_energy_calculator.nvm_extra_cycles
        )
        call_cost = call_cost_cycles * (
            msp430_energy_calculator.energy_clock_cycle + msp430_energy_calculator.energy_non_volatile_memory_access
        )

        config["call_cost"] = call_cost

    schematic = MyTransformation(msp430_energy_calculator, config)

    logging.info("[SCHEMATIC] Computing checkpoint placement and memory allocation")
    # Do the checkpoint placement and memory allocation of the basic blocks
    schematic.apply(cfgs, traces, function_depgraph)

    # Save cfg to dot format
    save_cfgs(cfgs, "dot_cfgs/after_analysis")
    # Uncomment the following to dump the cfg state (pickle format)
    # export_cfg_pickle(cfgs["@main"], "cfg_checkpoint_placed.dump")

    # Now, we need to create the global variable that will be located at the beginning of the SRAM
    # This variable will indicate to ScEpTic that the memory is accessible

    # First, we get the global symbol table located in SRAM
    # Since the default global symbol table should be in FRAM, we take the other gst

    gst = vmstate.memory.gst.other_gst

    # Creation of the global variable type: int16 []
    sram_needed = schematic.memory_allocator.top_address
    if sram_needed > 0:
        sram_type = Type(False, 0, True, [sram_needed], False, 0, True, None, False, None, word_type)
        # Creation of the global variable and allocation of the variable
        gst.allocate("@SRAM", sram_type.get_memory_composition(True), [0] * sram_needed)
        # Make the gst as the base state in order for our variable @SRAM to be in SRAM after a reboot
        gst.set_state_as_base_state()

    # Apply the code transformation to relocate some memory accesses from FRAM to SRAM
    # Places checkpoint and variable restoration
    code_transformation_pass = CodeTransformation(mem_parser.memory_tag_chains, memory_size)
    code_transformation_pass.apply(cfgs)

    # Save cfg to dot format
    save_cfgs(cfgs, "dot_cfgs/after_code_transformation")
    # Synchronize the modified CFG to the function llvm body
    synchronise_cfg_to_ast(cfgs, parser)
    parser.syncrhonize_ast_to_functions()
    print("SCHEMATIC applied in ", time.time() - start_t, " s")
