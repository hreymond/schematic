import logging
import re
import subprocess
from dataclasses import dataclass, asdict

from ScEpTIC.AST.misc.trace import LLVMNaturalLoop


def find_loops(source_file, opt_exe="opt-7"):
    """
    Uses llvm opt tool to find natural loops in a source file

    :param str source_file: The name of the source file
    :param str opt_exe: The opt executable to use

    :return: A dictionnary containing loops indexed by function name
    :rtype: Dict[str, LLVMNaturalLoop]

    More info on LoopPass output in LLVM source code: https://llvm.org/doxygen/classllvm_1_1LoopBase.html#a5b3fe20235340fb3bbf3ff86ec172d73
    """
    loops = {}
    curr_f = None
    # Check if the opt executable exists
    try:
        subprocess.check_output([opt_exe, "-version"])
    except FileNotFoundError:
        logging.critical("Couldn't find opt executable: '{}'".format(opt_exe))
        logging.critical("For now, SCHEMATIC rely on opt-7 to find loops in the LLVM IR")
        logging.critical("Please install LLVM 7 and make sure opt-7 is in your PATH")
        exit(1)

    sp = subprocess.Popen(
        "{} {} -loops -analyze".format(opt_exe, source_file),
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    subprocess_return = sp.stdout.readlines()
    sp.wait()
    if sp.returncode != 0:
        print("Return code {}".format(sp.returncode))
        output = ""
        for line in subprocess_return:
            output += line.decode("utf8")
        raise Exception("Couldn't analyse loops via {}: '{}'".format(opt_exe, output))

    for line in subprocess_return:
        line = line.decode("utf8")
        if line.startswith("Printing analysis"):
            # Going from Printing analysis 'Natural Loop Information' for function 'old_main':
            # To ['Printing analysis ', 'Natural Loop Information', ' for function ', 'old_main', ':\n']
            #              0                       1                         2               3        4
            f_name = line.split("'")[3]
            loops[f_name] = []
            curr_f = f_name
        elif line.strip().startswith("Loop at depth"):
            # Extract loop information: Loop at depth <depth> containing: <basic_block1>, <basic_block2>, ...
            search = re.search("[\t]*Loop at depth ([0-9]*) containing: (.*)", line)
            if search:
                depth = int(search.group(1).strip())
                parsed_bbs = search.group(2).split(",")
                header = []
                exiting = []
                latch = []
                bbs = []
                for bb in parsed_bbs:
                    bb_name = bb
                    if "<" in bb_name:
                        bb_name = bb_name[0 : bb.find("<")]
                    if "<header>" in bb:
                        header.append(bb_name)
                    if "<exiting>" in bb:
                        exiting.append(bb_name)
                    if "<latch>" in bb:
                        latch.append(bb_name)
                    bbs.append(bb_name)
                if len(header) != 1 or len(latch) != 1:
                    raise Exception("[SCHEMATIC] Doesn't handle loops with 2 headers or 2 latchs: {}".format(line))
                loops[curr_f].append(LLVMNaturalLoop(header[0], exiting, bbs, latch, depth))
            else:
                raise Exception(
                    "[SCHEMATIC] Error while parsing {} -loops -analyze output, "
                    "malformed output line: \n {}".format(opt_exe, line)
                )
        else:
            raise Exception(
                "[SCHEMATIC] Error while parsing {} -loops -analyze output, "
                "unexpected output line: \n {}".format(opt_exe, line)
            )
    return loops
