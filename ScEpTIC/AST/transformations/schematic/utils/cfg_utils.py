import os

import networkx as nx

from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import CheckpointTypeEnum

available_colors = [
    "darkgreen",
    "coral",
    "crimson",
    "blue",
    "darkorchid",
    "cadetblue3",
    "burlywood",
    "blueviolet",
    "brown",
    "darkcyan",
    "darkmagenta",
    "chocolate1",
    "cyan",
    "darkblue",
    "darkorange",
    "darkgoldenrod",
    "blue3",
    "darkorchid1",
    "darkred",
    "darksalmon",
    "darkseagreen",
    "darkslateblue",
    "darkturquoise",
    "darkviolet",
    "deeppink",
    "deepskyblue",
    "dimgray",
    "firebrick",
    "dodgerblue4",
    "deepskyblue3",
    "deeppink3",
]

""" More colors if needed
darkorchid1 	darkorchid2 	darkorchid3 	darkorchid4
darkred 	darksalmon 	darkseagreen 	darkseagreen1 	darkseagreen2
darkseagreen3 	darkseagreen4 	darkslateblue 	darkslategray 	darkslategray1
darkslategray2 	darkslategray3 	darkslategray4 	darkslategrey 	darkturquoise
darkviolet 	deeppink 	deeppink1 	deeppink2 	deeppink3
deeppink4 	deepskyblue 	deepskyblue1 	deepskyblue2 	deepskyblue3
deepskyblue4 	dimgray 	dimgrey 	dodgerblue 	dodgerblue1
dodgerblue2 	dodgerblue3 	dodgerblue4 	firebrick 	firebrick1
"""

mem_alloc_colors = {}
color_index = 0


def compute_basic_blocks_stats(cfg: CFG):
    """
    Return a dict containing the number of basic blocs fixed, and the total number of basic blocs
    """
    nb_fixed_bb = 0
    for bb in cfg.nodes:
        if bb.is_fixed:
            nb_fixed_bb += 1
    return {"nb_fixed_bb": nb_fixed_bb, "nb_bb": len(cfg.nodes)}


def _mem_alloc_to_color(allocation):
    global color_index
    global mem_alloc_colors
    if allocation not in mem_alloc_colors:
        mem_alloc_colors[allocation] = available_colors[color_index]
        color_index += 1
        if color_index >= len(available_colors):
            color_index = 0
            # raise Exception("No more colors available for memory allocations")

    return mem_alloc_colors[allocation]


def variable_to_str(var, memory_allocation=None):
    """
    Return a variable representation in a dot str format with it number of access and memory allocation.
    Example: "| { a | 12 }" or "| { a : 12 (NVM) }"
    """
    record = "| {" + var.name + "|" + str(var.nb_access)
    if memory_allocation and var.name in memory_allocation.vars:
        if memory_allocation.vars[var.name].allocation == VirtualMemoryEnum.VOLATILE:
            record += " (VM)"
        else:
            record += " (NVM)"
    record += "}"
    return record


def memory_alloc_to_dot(mem_alloc):
    """
    Return a string of the variables of a memory allocation separated by a pipe `|` (e.g `"| a: NVM | b: VM (0x02)"`)

    :param frozenset mem_alloc: The memory allocation variables
    """
    label = ""
    for var_alloc in mem_alloc:
        label += "|" + var_alloc.name + ": "
        if var_alloc.allocation == VirtualMemoryEnum.VOLATILE:
            label += " VM (" + str(hex(var_alloc.start_address)) + " "
            label += "R" if var_alloc.need_restore else ""
            label += "/" if var_alloc.need_restore and var_alloc.need_save else ""
            label += "S" if var_alloc.need_save else ""
            label += ")"
        else:
            label += " NVM"
    return label


def cfg_to_dot(cfg: nx.Graph, name="CFG", print_instr=True, print_cost=True, print_vars=True):
    """
    Convert a networkx graph to dot with the CFG formatting (square nodes) and labels (LLVM ir basic bloc labels)
    """
    if len(cfg.nodes) > 50:
        return

    # Initialize Dot graph
    cfg_printable = nx.relabel_nodes(cfg, lambda bb: bb.label)
    try:
        dot_cfg = nx.nx_agraph.to_agraph(cfg_printable)
    except ImportError:
        return
        # raise ImportError("Please install Pygraphviz to generate dot cfgs")

    dot_cfg.graph_attr["label"] = name
    dot_cfg.node_attr["shape"] = "record"
    dot_cfg.node_attr["fontsize"] = "7pt"
    dot_cfg.edge_attr["arrowhead"] = "none"

    mem_alloc_encountered = set()
    # Ad all basic bloc data into node label
    for bb in cfg.nodes:
        record = '<{ <FONT POINT-SIZE="15.0"><B>%' + str(bb.label) + "(%" + str(bb.llvm_label) + ")</B></FONT>"
        # Add the cost of the BB
        if print_cost:
            # record += " | {<TABLE>"
            # record += f"<TR><TD>Cost all NVM</TD><TD>{bb.cost_all_nvm}</TD></TR>"
            record += "| { Cost all NVM | " + str(bb.cost_all_nvm) + "}"
            if bb.final_cost is not None:
                # record += f"<TR><TD>Final cost</TD><TD>{bb.final_cost}</TD></TR>"
                record += "| { Final cost | " + str(bb.final_cost) + "}"
            if bb.energy_left is not None:
                # record += f"<TR><TD>Energy left</TD><TD>{bb.energy_left}</TD></TR>"
                record += "| { Energy left | " + str(bb.energy_left) + "}"
            if bb.energy_to_leave is not None:
                # record += f"<TR><TD>Energy to leave</TD><TD>{bb.energy_to_leave}</TD></TR>"
                record += "| { Energy to leave | " + str(bb.energy_to_leave) + "}"

            # record += "</TABLE> }"
        # Add the variables accessed in the BB and the number of accesses. Show the memory allocation if available.
        if print_vars:
            if bb.variables:
                variables = bb.variables
                record += "| { Variables and nb_access }"
                for var in variables.values():
                    record += variable_to_str(var, bb.memory_allocation)
            if bb.memory_allocation:
                mem_alloc_encountered.add(frozenset(bb.memory_allocation.vars.values()))
        # Print the instructions of the Basic Block
        inst_record = ""
        if print_instr:
            inst_record += "|<B> Instructions </B>"
            for inst in bb.instructions:
                inst_record += "|" + str(inst).replace("{", "(").replace("}", ")").replace("|", ",")
        if len(record) + 10 + len(inst_record) < 16384:
            record += inst_record
        else:
            record += "|<B> Instructions </B>| TOO MUCH INSTS"
        record += "}>"
        # Apply the computed label to the node of the dot graph
        node = dot_cfg.get_node(bb.label)
        node.attr["label"] = record
        if bb.memory_allocation:
            node.attr["color"] = _mem_alloc_to_color(frozenset(bb.memory_allocation.vars.values()))

    # Add labels and arrow-head for the edges of the CFG
    for edge in cfg.edges:
        checkpoint = cfg[edge[0]][edge[1]]["checkpoint"]
        dot_edge = dot_cfg.get_edge(edge[0].label, edge[1].label)
        dot_edge.attr["label"] = checkpoint.name
        if checkpoint.type == CheckpointTypeEnum.POTENTIAL:
            dot_edge.attr["arrowhead"] = "tee"
        elif checkpoint.type == CheckpointTypeEnum.DISABLED:
            dot_edge.attr["arrowhead"] = "normal"
        elif checkpoint.type == CheckpointTypeEnum.VIRTUAL:
            dot_edge.attr["arrowhead"] = "dot"
        elif checkpoint.type == CheckpointTypeEnum.ACTIVE:
            dot_edge.attr["arrowhead"] = "diamond"
        elif checkpoint.type == CheckpointTypeEnum.LOOP_LATCH:
            dot_edge.attr["arrowhead"] = "box"
            dot_edge.attr["headlabel"] = "{}   ".format(checkpoint.nb_iter)
        else:
            raise NotImplementedError("Unknown checkpoint type !")

    for mem_alloc in mem_alloc_encountered:
        color = _mem_alloc_to_color(mem_alloc)
        label = "{ Memory allocation " + color
        label += memory_alloc_to_dot(mem_alloc)
        label += "}"
        dot_cfg.add_node(color, label=label, type="record", color=color)
    # Set the layout to dot
    dot_cfg.layout(prog="dot")

    return dot_cfg


def save_cfg(name, cfg, folder="dot_cfgs", print_instr=True, print_cost=True, print_variables=True):
    os.makedirs(folder, exist_ok=True)
    dot_cfg = cfg_to_dot(
        cfg, name=("CFG " + name), print_instr=print_instr, print_cost=print_cost, print_vars=print_variables
    )
    if dot_cfg is not None:
        dot_cfg.write(folder + "/" + name + ".dot")


def save_cfgs(cfgs: dict, folder="dot_cfgs", print_instr=True, print_cost=True, print_variables=True):
    os.makedirs(folder, exist_ok=True)
    # Saves cfg in dot format
    for name, cfg in cfgs.items():
        dot_cfg = cfg_to_dot(
            cfg, name=("CFG " + name), print_instr=print_instr, print_cost=print_cost, print_vars=print_variables
        )
        if dot_cfg is not None:
            dot_cfg.write(folder + "/" + name + ".dot")


def export_cfg_pickle(cfg, filename="cfg.dump"):
    import dill

    with open(filename, "wb") as f:
        dill.dump(cfg, f)


def export_memtagchain(memtag_chain, filename="memory_tag_chain.dump"):
    import pickle

    with open(filename, "wb") as f:
        pickle.dump(memtag_chain, f)
