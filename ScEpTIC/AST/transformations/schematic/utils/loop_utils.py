import copy

import logging
from ScEpTIC.AST.misc.trace import *
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.elements.basic_block import *
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import *
from ScEpTIC.AST.transformations.schematic.parsers.cfg_parser import generate_unique_bb_id


def get_max_iter(loop, cfg):
    """
    Examine loop basic blocks to extract the maximum number of iterations given by the annotation `__max_iter(N)`

    A call to `__max_iter(N)` with N a constant value (e.g 100) must be placed in the beginning of the loop
    for it to work.  Double loops may not need 2 __max_iter calls since LLVM consider them as one loop. To avoid eventual
    problems, compile with --loop-simplify

    :param LLVMNaturalLoopBB loop: The loop we are examining
    :param CFG cfg: The cfg of the function/loop analysed

    :return: The maximal number of iteration given by the annotation
    :rtype: int

    :raise: Exception if the number of annotation for a loop is superior to one or not found
    """
    annotations_count = 0
    max_iter = None
    for bb in loop.basic_blocks:
        i = 0
        while i < len(bb.instructions):
            inst = bb.instructions[i]
            if isinstance(inst, CallOperation) and inst.name == "@__max_iter":
                annotations_count += 1
                max_iter = int(inst.function_args[0].value)
                bb.instructions.remove(inst)
                if i == 0:
                    bb.instructions[0].label = inst.label
            i += 1
    if annotations_count > 1:
        raise Exception(
            "[SCHEMATIC] {} __max_iter annotations found in loop {} from function {}: Cannot determine"
            " which one to take into account".format(annotations_count, loop.name, cfg.name)
        )
    if annotations_count == 0:
        logging.warning(
            "[SCHEMATIC] No __max_iter annotation found in loop {} from function {}" "".format(loop.name, cfg.name)
        )
        return 999999999
    return max_iter


def get_nb_iter(loop, cfg):
    """
    Examine loop basic blocks to extract the number of iterations given by the annotation `__nb_iter(N)`

    A call to `__nb_iter(N)` with N a constant value (e.g 100) must be placed in the beginning of the loop
    for it to work.  Double loops may not need 2 __nb_iter calls since LLVM consider them as one loop. To avoid eventual
    problems, compile with --loop-simplify

    :param LLVMNaturalLoopBB loop: The loop we are examining
    :param CFG cfg: The cfg of the function/loop analysed

    :return: The number of iteration given by the annotation
    :rtype: int

    :raise: Exception if the number of annotation for a loop is superior to one or not found
    """
    annotations_count = 0
    nb_iter = None
    for bb in loop.basic_blocks:
        i = 0
        while i < len(bb.instructions):
            inst = bb.instructions[i]
            if isinstance(inst, CallOperation) and inst.name == "@__nb_iter":
                annotations_count += 1
                nb_iter = int(inst.function_args[0].value)
                bb.instructions.remove(inst)
                if i == 0:
                    bb.instructions[0].label = inst.label
            i += 1
    if annotations_count > 1:
        raise Exception(
            "[PFI] {} __nb_iter annotations found in loop {} from function {}: Cannot determine"
            " which one to take into account".format(annotations_count, loop.name, cfg.name)
        )
    return nb_iter


def extract_loop_subgraph(loop, cfg):
    """
    Create a subgraph of the cfg given in parameter corresponding to the loop body
    (header, basic blocks of the loop and latch, with the arc between the latch and header removed)

    :param LLVMNaturalLoopBB loop: The loop we want to extract
    :param CFG cfg: The CFG we want to extract the loop from

    :return: A CFG of the loop
    :rtype: CFG
    """

    # Create a subgraph containing the basic blocks of the loop
    loop_cfg = cfg.subgraph(loop.basic_blocks).copy()
    # Since subgraph returns a nx.Digraph, change loop_cfg to CFG in order to use CFG methods add_edge and remove_edge
    loop_cfg.__class__ = CFG
    loop_cfg.name = "loop{}_f_{}".format(loop.header[0].llvm_label, cfg.name)

    # Remove the arc from the latch to the header
    loop_cfg.remove_edge(loop.latch[-1], loop.header[0])

    # Add start
    bb_start = BasicBlock(generate_unique_bb_id(), cfg.name)
    bb_start.llvm_label = "%START_Loop"
    bb_start.variables = {}
    bb_start.cost_all_nvm = 0
    loop_cfg.add_node(bb_start)
    loop_cfg.add_edge(bb_start, loop.header[0], checkpoint=Checkpoint(bb_start, loop.header[0]))
    loop_cfg.first_bb = bb_start

    # Add end
    bb_end = BasicBlock(generate_unique_bb_id(), cfg.name)
    bb_end.llvm_label = "%END_Loop"
    bb_end.variables = {}
    bb_end.cost_all_nvm = 0
    loop_cfg.add_node(bb_end)
    loop_cfg.last_bb = bb_end
    loop_cfg.add_edge(loop.latch[-1], bb_end, checkpoint=Checkpoint(loop.latch[-1], bb_end))
    return loop_cfg


def estimate_loop_variable_access(loop_traces, nb_iter: int):
    traces_freq = []
    total_exec = sum([trace.nb_executions for trace in loop_traces.traces.values()])
    for loop_trace in loop_traces.traces.values():
        freq = loop_trace.nb_executions / total_exec
        traces_freq.append((freq, loop_trace.trace))

    # Compute variables expectancy
    variables_accessed = {}
    for freq, trace in traces_freq:
        for bb in trace:
            for name, infos in bb.variables.items():
                if name in variables_accessed:
                    variables_accessed[name].nb_access += infos.nb_access * freq * nb_iter
                else:
                    variables_accessed[name] = copy.deepcopy(infos)
                    variables_accessed[name].nb_access += infos.nb_access * freq * nb_iter

    # Round the number of access to the variable, and make sure at it is at least 1 (otherwise there will be no memory allocation for the variable)
    for name, variable in variables_accessed.items():
        variable.nb_access = max(1, round(variable.nb_access))

    return variables_accessed
