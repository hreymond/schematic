import os

import networkx as nx


def rcg_to_dot(rcg: nx.Graph, name="Reachable checkpoint graph"):
    """
    Convert a networkx graph to dot with the reachable checkpoint graph formatting
    """
    rcg_printable = nx.relabel_nodes(rcg, lambda checkpoint: checkpoint.name)
    try:
        dot_rcg = nx.nx_agraph.to_agraph(rcg_printable)
    except ImportError:
        return
    dot_rcg.graph_attr["label"] = name
    for edge in rcg_printable.edges:
        dot_rcg.get_edge(edge[0], edge[1]).attr["label"] = rcg_printable[edge[0]][edge[1]]["weight"]

    return dot_rcg


def save_reachable_chkpt_graph(rcg, name="rcg"):
    os.makedirs("dot_rcgs", exist_ok=True)
    # Saves cfg in dot format
    dot_rcg = rcg_to_dot(rcg, name=name)
    if len(name) > 100:
        name = name[0:47] + "..." + name[-50:]
    if dot_rcg:
        dot_rcg.write("dot_rcgs/{}.dot".format(name))
