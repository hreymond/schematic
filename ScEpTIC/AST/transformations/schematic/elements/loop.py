from dataclasses import dataclass


@dataclass
class Loop:
    """
    A dataclass representing a loop

    They might be multiple basic block headers as one logical header can be splitted into several basic blocks during
    the function extraction process
    """

    loop_info: "LLVMNaturalLoopBB"
    nb_iter: int
    cost_one_it: float

    @property
    def name(self):
        return self.loop_info.header[0].llvm_label
