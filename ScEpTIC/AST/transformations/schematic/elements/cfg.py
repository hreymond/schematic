import logging

import networkx as nx

from ScEpTIC.AST.elements.instructions.termination_instructions import BranchOperation
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import CheckpointTypeEnum, Checkpoint


class CFG(nx.DiGraph):
    """
    Represents a CFG of a function

    :param str name: The name of the function
    :param BasicBlock first_bb: The first basic block of the function (labeled '@name_START')
    :param BasicBlock last_bb: The last basic block of the function (labeled '@name_END')
    """

    def __init__(self, **attr):
        super().__init__(**attr)
        self.name = ""
        self.first_bb = None
        self.last_bb = None

    def __str__(self):
        return "CFG<" + self.name + ">"

    def __repr__(self) -> str:
        return self.__str__()

    def add_edge(self, bb_start, bb_end, checkpoint=None, **attr):
        if checkpoint is None:
            checkpoint = Checkpoint(bb_start, bb_end)
        super(CFG, self).add_edge(bb_start, bb_end, checkpoint=checkpoint, **attr)
        if bb_start.label not in bb_end.prev:
            bb_end.prev.append(bb_start.label)
        if bb_end.label not in bb_start.next:
            bb_start.next.append(bb_end.label)

    def remove_edge(self, bb_start, bb_end):
        super(CFG, self).remove_edge(bb_start, bb_end)
        if bb_start.label in bb_end.prev:
            bb_end.prev.remove(bb_start.label)
        if bb_end.label in bb_start.next:
            bb_start.next.remove(bb_end.label)

    def has_only_disabled_checkpoints(self):
        """
        Returns True if the CFG only contains disabled checkpoint, False else

        :rtype: bool
        """
        checkpoints = nx.get_edge_attributes(self, "checkpoint")
        for chkpt in checkpoints.values():
            if chkpt.type != CheckpointTypeEnum.DISABLED:
                return False
        return True

    def has_potential_checkpoints(self):
        """
        Returns True if the cfg contain at least one potential checkpoint

        :rtype: bool
        """
        checkpoints = nx.get_edge_attributes(self, "checkpoint")
        for chkpt in checkpoints.values():
            if chkpt.type == CheckpointTypeEnum.POTENTIAL:
                return True
        return False

    def check_cfg_analysis(self):
        """
        Checks if the cfg has been well analysed
            - Are the bb all fixed ?
            - Are the bb energies all fixed ?
            - Have all the potential checkpoints been removed ?
            - Do all the bb have a memory allocation ?
            - Are the cfg edges and the branch labels coherent ?

        :raises Exception: if the cfg has not been well analysed
        """
        if self.name == "":
            logging.warning("A cfg does not have any function name defined")
        if self.has_potential_checkpoints():
            raise Exception(
                "[SCHEMATIC] The cfg of the function {} has still potential checkpoints at "
                "the end of its analyse ! Check the dot file generated".format(self.name)
            )
        self.check_all_bb_are_fixed()
        self.check_edges_label_coherency()

    def check_all_bb_are_fixed(self):
        """
        Checks if the cfg basic blocks has been well analysed
            - Are the bb all fixed
            - Are the bb energies all fixed
            - Do all the bb have a memory allocation ?

        :raises Exception: if a basic block is not correctly analysed
        """
        for bb in self.nodes:
            if not bb.is_fixed:
                raise Exception("[SCHEMATIC] Basic block {} in function {} is not fixed".format(bb.llvm_label, self.name))
            if bb.energy_to_leave is None:
                raise Exception(
                    "[SCHEMATIC] Basic block {} in function {} has no energy_to_leave".format(bb.llvm_label, self.name)
                )
            if bb.energy_left is None:
                raise Exception(
                    "[SCHEMATIC] Basic block {} in function {} has no energy_left".format(bb.llvm_label, self.name)
                )
            if bb.final_cost is None:
                raise Exception(
                    "[SCHEMATIC] Basic block {} in function {} has no final_cost".format(bb.llvm_label, self.name)
                )
            if bb.memory_allocation is None:
                raise Exception(
                    "[SCHEMATIC] Basic block {} in function {} has no memory_allocation".format(bb.llvm_label, self.name)
                )

    def check_edges_label_coherency(self):
        """
        Check that the labelling of the basic blocks and branch operation is coherent with the edges of the CFG

        :raises Exception: if a basic block is not correctly analysed
        """
        edge_graph = nx.DiGraph()
        for edge in self.edges:
            if edge[0] == self.first_bb or edge[1] == self.last_bb:
                continue
            edge_graph.add_edge(edge[0].label, edge[1].label)

        label_graph = nx.DiGraph()
        for bb in self.nodes:
            bb_label = bb.label_str
            # Do not consider the first and last bb since they are virtual
            if bb == self.first_bb or bb == self.last_bb:
                continue
            # Check if the bb has a predecessor. if not, there must be a label on the first instruction of the bb
            if len(list(self.predecessors(bb))) != 0:
                first_inst_label = bb.instructions[0].label
                if first_inst_label is None:
                    raise Exception(
                        "[SCHEMATIC] Basic block {} in function {} has no label on its first instruction".format(
                            bb_label, self.name
                        )
                    )
                if type(first_inst_label) != str:
                    raise Exception(
                        "[SCHEMATIC] Basic block {} in function {} has a label on its first instruction that is not a string".format(
                            bb_label, self.name
                        )
                    )

                if first_inst_label != bb_label:
                    raise Exception(
                        f"[SCHEMATIC] Basic block {bb_label} in function {self.name} has a label on its first instruction that is different from its label.\n"
                        f"\tBB label: {bb_label}, first_inst_label: {first_inst_label}"
                    )

            if len(bb.instructions) != 0 and type(bb.instructions[-1]) == BranchOperation:
                branch = bb.instructions[-1]
                if branch.target_true is None:
                    raise Exception("[SCHEMATIC] Basic block {} in function {} has no target oO".format(bb_label, self.name))
                label_graph.add_edge(bb_label, branch.target_true)
                if branch.target_false is not None:
                    label_graph.add_edge(bb_label, branch.target_false)

        # Convert the label graph labels to integers
        label_graph = nx.relabel_nodes(label_graph, lambda x: int(x[1:]))
        if edge_graph.adj != label_graph.adj or label_graph.nodes != edge_graph.nodes:
            msg = (
                f"[SCHEMATIC] The labelling of the basic blocks and branch operation is not coherent with"
                f" the edges of the CFG for function {self.name} \n"
            )

            try:
                dot_cfg = nx.nx_agraph.to_agraph(edge_graph)
                edge_graph = "edge_graph.dot"
                dot_cfg.write(edge_graph)
                dot_cfg = nx.nx_agraph.to_agraph(label_graph)
                label_graph = "label_graph.dot"
                dot_cfg.write(label_graph)
                msg += "The edge graph has been written in {} and the label graph in {} ".format(edge_graph, label_graph)
            except ImportError:
                msg += "Please install pygraphviz to get the dot files"
            raise Exception(msg)
