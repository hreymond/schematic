from enum import Enum


class CheckpointTypeEnum(Enum):
    POTENTIAL = 0
    DISABLED = 1
    ACTIVE = 2
    VIRTUAL = 3
    LOOP_LATCH = 4


class Checkpoint:
    """
    Representation of a checkpoint

    :param str name: The name of the checkpoint
    :param CheckpointTypeEnum type: The type of the checkpoint (POTENTIAL, DISABLED, ACTIVE, VIRTUAL)
    :param BasicBlock bb_before: The basic block before the checkpoint (origin of the cfg arc)
    :param BasicBlock bb_after: The basic block after the checkpoint (destination of the cfg arc)
    """

    index = 1

    def __init__(self, bb_before=None, bb_after=None, type=CheckpointTypeEnum.POTENTIAL):
        self.name = "C" + str(Checkpoint.index)
        Checkpoint.index += 1
        self.type = type
        self.bb_before = bb_before
        self.bb_after = bb_after
        self.nb_iter = None
        self.virtual = False

    def __str__(self):
        return self.name + "<" + self.type.name + ">"

    def __repr__(self) -> str:
        return self.__str__()
