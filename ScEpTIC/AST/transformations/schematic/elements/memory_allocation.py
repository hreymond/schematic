import typing

from copy import deepcopy
from dataclasses import dataclass

from typing import Dict

from ScEpTIC.AST.elements.instruction import Instruction
from ScEpTIC.AST.elements.instructions.memory_operations import LoadOperation
from ScEpTIC.AST.elements.types import Type
from ScEpTIC.AST.elements.value import Value
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum


class IncompatibleMemAllocException(Exception):
    pass


@dataclass
class Variable:
    """
    Represents a variable

    :param str name: The name of the variable
    :param Type type: The type of the variable
    :param Value element: The llvm element corresponding to the variable
    """

    name: str
    type: Type
    element: Value

    def __len__(self):
        return len(self.type)


@dataclass
class VariableAccesses(Variable):
    """
    Represents a variable in a basic bloc.

    :param str name: The name of the variable
    :param Type type: The type of the variable
    :param int nb_access: The number of access to the basic block
    """

    nb_access: int

    # TODO check what are the necessary informations to determine if a variable should be saved or not
    first_operation: typing.Type[Instruction]
    read_only: bool = False


class VariableAllocation(Variable):
    """
    Represents the allocation of a variable

    :param str name: The name of the variable
    :param Type type: The variable type
    :param start_address: The variable allocation start address
    :param end_address: The variable allocation end address
    :type start_address: int, optional
    :type end_address: int, optional
    :param allocation: The variable memory allocation type (VM:NVM)
    :type allocation: VirtualMemoryEnum
    """

    @staticmethod
    def from_variable(var):
        varalloc = VariableAllocation(var.name, var.type, var.element)
        if isinstance(var, VariableAccesses):
            varalloc._need_restore = var.first_operation == LoadOperation
            varalloc._need_save = True
        return varalloc

    def __init__(self, name, type, element, allocation=VirtualMemoryEnum.NON_VOLATILE):
        super().__init__(name, type, element)
        self.start_address = None
        self.end_address = None
        self.allocation = allocation
        self._need_restore = True
        self._need_save = True

    def __eq__(self, other):
        if isinstance(other, VariableAllocation):
            return (
                self.allocation == other.allocation
                and len(self.type) == len(other.type)
                and self.name == other.name
                and self.start_address == other.start_address
                and self.end_address == other.end_address
            )
        else:
            return False

    def __hash__(self):
        return hash((self.allocation, self.name, self.start_address, len(self.type)))

    @property
    def need_restore(self):
        return self._need_restore and self.allocation == VirtualMemoryEnum.VOLATILE

    @property
    def need_save(self):
        return self._need_save and self.allocation == VirtualMemoryEnum.VOLATILE

    def __str__(self):
        alloc_str = "NVM" if self.allocation == VirtualMemoryEnum.NON_VOLATILE else "VM"
        address = "" if self.allocation == VirtualMemoryEnum.NON_VOLATILE else "({})".format(hex(self.start_address))
        return "VariableAllocation({}: {} {})".format(self.name, alloc_str, address)


class MemoryAllocation:
    """
    Class which represent a Memory Allocation with its variables allocated

    :param Dict[str, VariableAllocation] vars: The allocation of the variables composing the memory allocation. Each VariableAllocation
        is indexed by the variable name.
    :param List[(int, int)] occupied_memory: Contains tuples which represent the occupied memory
    """

    def __init__(self):
        self.vars: Dict[str, VariableAllocation] = {}
        self.occupied_memory = []

    def equals(self, other):
        return set(self.vars.values()) == set(other.vars.values)

    def to_dict(self):
        """
        Return the memory allocation as a dict with variable names as key and the memory allocation type (VM/NVM)

        For exemple, the function could return
        { 'a': VirtualMemoryEnum.VM, 'b': VirtualMemoryEnum.NVM }
        """
        return {name: var.allocation for name, var in self.vars.items()}

    def interval_is_empty(self, start_address: int, end_address: int):
        """
        Returns true if the given memory interval is free in this memory allocation

        :param int start_address: start of the memory segment
        :param int end_address: end of the memory segment
        """
        # Check if interval are overlapping
        for interval in self.occupied_memory:
            if start_address < interval[1] and interval[0] < end_address:
                return False
        return True

    def _is_compatible_with(self, memory_allocation):
        for name, var_alloc in memory_allocation.vars.items():
            if name not in self.vars:
                if var_alloc.allocation == VirtualMemoryEnum.VOLATILE:
                    if not self.interval_is_empty(var_alloc.start_address, var_alloc.end_address):
                        return False
            else:
                if var_alloc != self.vars[name]:
                    return False
        return True

    def is_compatible_with(self, memory_allocation):
        """
        Returns True if the memory allocation is compatible with the memory allocation given in parameter
        """
        return self._is_compatible_with(memory_allocation) and memory_allocation._is_compatible_with(self)

    def extends(self, memory_allocation):
        """
        Extends the memory allocation with another memory allocation

        :param MemoryAllocation memory_allocation: The memory allocation containing the variable
            to add to the current memory allocation
        """
        if not self.is_compatible_with(memory_allocation):
            raise IncompatibleMemAllocException(
                "[SCHEMATIC] Trying to extend a memory allocation with "
                "an incompatible memory allocation:  {} != {}".format(memory_allocation.to_dict(), self.to_dict())
            )
        for name, var_alloc in memory_allocation.vars.items():
            if name not in self.vars:
                # Add the variable to the allocation
                self.vars[name] = deepcopy(var_alloc)
                # Update the occupied_memory if necessary
                if var_alloc.allocation == VirtualMemoryEnum.VOLATILE:
                    self.occupied_memory.append((var_alloc.start_address, var_alloc.end_address))

    def add_new_var_alloc(self, varalloc: VariableAllocation, checkpoint_increase_allowed=False):
        var_name = varalloc.name
        # If the variable is already in new_alloc, check the compatibility
        if var_name in self.vars and self.vars[var_name] != varalloc:
            raise IncompatibleMemAllocException(
                "[SCHEMATIC] Trying to add a variable allocation to "
                "a memory allocation containing the variable with "
                "incompatible attributes: {} != {}".format(varalloc, self.vars[var_name])
            )
        if var_name not in self.vars:
            if varalloc.allocation == VirtualMemoryEnum.VOLATILE:
                if self.interval_is_empty(varalloc.start_address, varalloc.end_address):
                    if (varalloc.need_restore or varalloc.need_save) and not checkpoint_increase_allowed:
                        raise IncompatibleMemAllocException(
                            "{} cannot be allocated in VM since it might increase checkpoint cost".format(varalloc.name)
                        )
                    else:
                        self.vars[varalloc.name] = varalloc
                        self.occupied_memory.append((varalloc.start_address, varalloc.end_address))
                else:
                    raise IncompatibleMemAllocException(
                        "{} cannot be allocated in VM, variables are already allocated in the address range".format(
                            varalloc.name
                        )
                    )
            else:
                self.vars[varalloc.name] = varalloc


def merge_allocations(allocations: typing.List[MemoryAllocation], checkpoint_increase_allowed=False):
    """
    Merge multiple memory allocation into a new memory allocation

    If there is no memory allocation, returns an empty MemoryAllocation

    :param List[MemoryAllocation] allocations: A list of the memory allocation to merge
    :param Boolean checkpoint_increase_allowed: If set to False, the function raises         IncompatibleMemAllocException if merging the memory allocations increase the checkpointing cost (i.e if alloc A is saving a variable that alloc B is not saving).
    """
    if len(allocations) == 0:
        return MemoryAllocation()

    new_alloc = deepcopy(allocations[0])
    # If there is only one memory allocation, return a copy of this allocation
    if len(allocations) == 1:
        return new_alloc

    for mem_alloc in allocations[1:]:
        # Check the variables in the memory allocation we want to merge into new_alloc
        for var_name, alloc in mem_alloc.vars.items():
            new_alloc.add_new_var_alloc(alloc, checkpoint_increase_allowed)
    return new_alloc
