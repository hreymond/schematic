from ScEpTIC.exceptions import NotImplementedException


class BaseASTElement:
    """
    Base component of the AST structure that we use for AST transformations
    """

    def __init__(self):
        self.conditional_entry_label = []
        self.conditional_exit_label = []
        self.is_conditional_entry_block = False
        self.is_conditional_exit_block = False

    def get_label(self):
        """
        Returns element label
        """
        raise NotImplementedException("get_label() must be implemented!")

    def get_labels(self):
        """
        Returns a list of all the labels covered by the object
        """
        return [self.get_label()]

    def get_next_labels(self):
        """
        Returns labels of the elements directly reachable from the current one
        """
        if self.is_conditional_entry_block:
            return self.conditional_entry_label

        return self._get_next_labels()

    def _get_next_labels(self):
        """
        Returns labels of the elements directly reachable from the current one
        """
        raise NotImplementedException("get_next_labels() must be implemented!")

    def get_previous_labels(self):
        """
        Returns the labels of the basic blocks that precede the current one
        """
        if self.is_conditional_exit_block:
            return self.conditional_exit_label

        return self._get_previous_labels()

    def _get_previous_labels(self):
        """
        Returns the labels of the basic blocks that precede the current one
        """
        raise NotImplementedException("get_previous_labels() must be implemented!")

    def jumps_after(self, label):
        """
        Returns if the element can jump after a certain label
        """

        for next_label in self.get_next_labels():
            if next_label >= label:
                return True

        return False

    def has_backward_jump(self):
        """
        Returns if the basic block performs a backward jump
        """

        label = self.get_label()

        for next_label in self.get_next_labels():
            if next_label < label:
                return True

        return False

    def flatten(self):
        """
        Returns a flatten representation of the element
        """
        return [self]

    @staticmethod
    def label_to_int(label):
        """
        Converts a LLVM IR label to an integer representation
        """
        if isinstance(label, str):
            return int(f"{label[1:]}")

        return int(label)

    def calls_to(self, function_names, is_checkpoint=True):
        """
        Returns if the basic block has a call to given functions
        """
        raise NotImplementedException(f"calls_to() not implemented for {self.__class__.__str__}")

    def calls_checkpoint(self, checkpoint_function_name, ignore_nested_checkpoint=False):
        """
        Returns if the basic block has a call to a checkpoint
        """
        raise NotImplementedException(f"calls_checkpoint() not implemented for {self.__class__.__str__}")

    def has_nested_loop(self):
        """
        Returns if the element has a nested loop
        """
        raise NotImplementedException(f"has_nested_loop() not implemented for {self.__class__.__str__}")

    def set_conditional_entry_label(self, label):
        """
        Sets the label of the entry block
        """
        self.is_conditional_entry_block = True
        self.conditional_entry_label.append(label)

    def set_conditional_exit_label(self, label):
        """
        Sets the label of the exit block
        """
        self.is_conditional_exit_block = True
        self.conditional_exit_label.append(label)

    def reset_conditional_structure(self):
        """
        Resets the book-keeping information for conditionals structures
        """
        self.is_conditional_entry_block = False
        self.is_conditional_exit_block = False
        self.conditional_entry_label = []
        self.conditional_exit_label = []

    def get_real_next_prev(self):
        """
        Returns the real next and prev (conditional entry/exit block have theirs overwritten)
        """
        retstr = ""

        if self.is_conditional_entry_block:
            retstr += f", real_next={self.next}"

        if self.is_conditional_exit_block:
            retstr += f", real_prev={self.prev}"

        return retstr
