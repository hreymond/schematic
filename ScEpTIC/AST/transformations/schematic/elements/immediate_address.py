class ImmediateAddress:
    def __init__(self, address, sram=True):
        self.address = address
        self.sram = sram

    def get_val(self):
        if self.sram:
            return "SGST-" + hex(self.address)
        else:
            return "FGST-" + hex(self.address)

    def __str__(self):
        return self.get_val()
