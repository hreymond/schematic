from copy import deepcopy
import logging

import networkx as nx

from ScEpTIC.AST.elements.instructions.memory_operations import (
    StoreOperation,
    LoadOperation,
    is_load_or_store,
    AllocaOperation,
)
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation, CompareOperation
from ScEpTIC.AST.elements.instructions.binary_operation import BinaryOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import ReturnOperation, BranchOperation
from ScEpTIC.AST.elements.types import BaseType, Type
from ScEpTIC.AST.elements.value import Value
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.schematic.cfg_modification import add_free_branch, add_branch
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import CheckpointTypeEnum, Checkpoint
from ScEpTIC.AST.transformations.schematic.elements.immediate_address import ImmediateAddress
from ScEpTIC.AST.transformations.schematic.parsers.cfg_parser import generate_unique_bb_id
from ScEpTIC.AST.transformations.schematic.elements.memory_allocation import VariableAllocation


def INT32_BASE_TYPE():
    """
    Return a new BaseType int32

    :rtype: BaseType
    """
    return BaseType("int", 32)


def INT16_BASE_TYPE():
    """
    Return a new BaseType int32

    :rtype: BaseType
    """
    return BaseType("int", 16)


def INT16_TYPE():
    """
    Return a new Type int16

    :rtype: Type
    """
    t = Type.empty()
    t.is_base_type = True
    t.base_type = INT16_BASE_TYPE()
    return t


def INT1_TYPE():
    """
    Return a new Type int1

    :rtype: Type
    """
    t = Type.empty()
    t.is_base_type = True
    t.base_type = BaseType("int", 1)
    return t


def INT32PTR_TYPE():
    """
    Return a new Type int32 *

    :rtype: Type
    """
    return Type(True, 1, False, [], False, 0, True, None, False, None, INT32_BASE_TYPE())


def INT16PTR_TYPE():
    """
    Return a new Type int32 *

    :rtype: Type
    """
    return Type(True, 1, False, [], False, 0, True, None, False, None, INT16_BASE_TYPE())


nb_reg = 0


def generate_tmp_register(type):
    """
    Generate a new temporary virtual_register

    The name convention for those register is %tmp0, %tmp1, ...

    :param type: The type of the register
    :type type: Type
    """
    global nb_reg
    name = "%tmp" + str(nb_reg)
    nb_reg += 1
    return Value("virtual_reg", name, type)


def insert_bb_on_cfg_arc(cfg: nx.DiGraph, bb_before: BasicBlock, bb_after: BasicBlock):
    """
    Insert a basic block on a cfg arc.
    """
    if bb_before.function_name != bb_after.function_name:
        raise ValueError("bb_before and bb_after must be from the same function !")

    # Get the checkpoint
    chkpt = cfg[bb_before][bb_after]["checkpoint"]
    # Create a new basic block
    bb_label = generate_unique_bb_id()
    bb = BasicBlock(bb_label, bb_after.function_name)
    bb.memory_allocation = bb_after.memory_allocation
    bb.is_fixed = True
    bb.cost_all_nvm = 0
    bb.final_cost = 0
    bb.llvm_label = "SAVE/RESTORE " + chkpt.name
    # Add a branch operation to the basic block
    branch = BranchOperation(None, "%" + str(bb_after.label), None)
    branch.metric_group = "checkpoint"
    branch.label = "%" + str(bb.label)
    bb.instructions.append(branch)
    # Update the basic block labels
    bb_before.next.remove(bb_after.label)
    bb_after.prev.remove(bb_before.label)

    bb_before.add_next_label(bb.label)
    bb.add_prev_label(bb_before.label)
    bb.add_next_label(bb_after.label)
    bb_after.add_next_label(bb.label)
    # Update bb_before jump
    branch_inst = bb_before.instructions[-1]
    if branch_inst.target_false == ("%" + str(bb_after.label)):
        branch_inst.target_false = "%" + str(bb.label)
    if branch_inst.target_true == ("%" + str(bb_after.label)):
        branch_inst.target_true = "%" + str(bb.label)
    # Update the cfg
    cfg.remove_edge(bb_before, bb_after)
    cfg.add_node(bb)
    cfg.add_edge(bb_before, bb, checkpoint=chkpt)
    disabled_chkpt = Checkpoint(bb, bb_after)
    disabled_chkpt.type = CheckpointTypeEnum.DISABLED
    cfg.add_edge(bb, bb_after, checkpoint=disabled_chkpt)
    return bb


class CodeTransformation:
    """
    Transformation pass which places checkpoint and apply memory allocation on the basic blocs of cfgs where the
    checkpoint and memory allocation are fixed

    - Relocates the target of the instructions accessing to variable promoted in VM
    - Adds instructions to save/restore the variable promoted to NVM before/after each checkpoint
    - Adds a call to the restore function before the program start

    :param memory_tag_chain; The memory tag chain of the program, used to gather information about variables types
    :type memory_tag_chain: dict

    :param sram_symbol: The symbol that represents the SRAM base_address. The variable must represented by the symbol
        must be an array of int32. Programs variable in SRAM will be acceded through this array (the variable address
        being its index in the array)
    :type sram_symbol: GlobalVariable
    """

    def __init__(self, memory_tag_chain, vm_size=2000):
        self.memory_tag_chain = memory_tag_chain
        self.vm_size = vm_size

    def _generate_immediate_address(self, address: int, type: Type):
        """
        Generate a value containing an immediate address

        :param int address: The memory location
        :param Type type: the type of the value
        """
        ptr_type = deepcopy(type)
        ptr_type.is_pointer = True
        ptr_type.pointer_level += 1
        return Value("address", ImmediateAddress(address), ptr_type)

    def _add_variable_save_instruction(self, bb, i, mem_alloc, fname):
        """
        Insert instructions to save the variables (i.e move the variables in VM to NVM) given a memory allocation in a
        basic block

        :param bb: BasicBlock to insert the instructions into
        :type bb: BasicBlock
        :param i: The index in the basic block where the instructions should be inserted (0 = start of the basic block,
            len(bb.instructions) = end of the instruction)
        :type i: int
        :param mem_alloc: The memory allocation containing the memory state to save (i.e the memory allocation before
            the checkpoint
        :type mem_alloc: MemoryAllocation

        TODO handle structs and arrays
        """
        for name, var_alloc in mem_alloc.vars.items():
            # Check if the variable is local to a function
            # If so,  only save the variables of the current function
            splitted_name = name.split(".")
            if len(splitted_name) > 1 and splitted_name[0] != fname[1:]:
                continue
            if var_alloc.need_save:
                # Determine VM and NVM addresses
                temp_reg = generate_tmp_register(var_alloc.type)

                # Add instructions to load from VM
                vm_val = self._generate_immediate_address(var_alloc.start_address, var_alloc.type)

                # vm_var = Value("global_var", var_alloc.start_address, ptr_type)
                load = LoadOperation(temp_reg, temp_reg.type, vm_val, 0, False)
                load.virtual_memory_target = VirtualMemoryEnum.VOLATILE
                load.metric_group = "checkpoint"
                # Add instruction to store in NVM
                nvm_address = var_alloc.element
                store = StoreOperation(nvm_address, temp_reg, 2, False)
                store.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE
                store.metric_group = "checkpoint"
                # Append the instructions
                bb.instructions.insert(i, store)
                bb.instructions.insert(i, load)

    def _add_variable_restore_instruction(self, bb, i, mem_alloc, fname):
        """
        Insert instructions to restore the variables (i.e move the variables from NVM to VM) given a memory allocation
        in a basic block

        :param bb: BasicBlock to insert the instructions into
        :type bb: BasicBlock
        :param i: The index in the basic block where the instructions should be inserted (0 = start of the basic block,
            len(bb.instructions) = end of the instruction)
        :type i: int
        :param mem_alloc: The memory allocation containing the memory state to restore (i.e the memory allocation after
            the checkpoint
        :type mem_alloc: MemoryAllocation

        """
        for name, var_alloc in mem_alloc.vars.items():
            # Check if the variable is local to a function
            # If so,  only save the variables of the current function
            splitted_name = name.split(".")
            if len(splitted_name) > 1 and splitted_name[0] != fname[1:]:
                continue
            if var_alloc.need_restore:
                # Determine VM and NVM addresses
                temp_reg = generate_tmp_register(var_alloc.type)

                # get SRAM and FRAM versions of the variable
                vm_val = self._generate_immediate_address(var_alloc.start_address, var_alloc.type)
                nvm_val = var_alloc.element  # Value("virtual_reg", var_tag["element"].value, INT16_TYPE())

                # Add instructions to load from NVM
                load = LoadOperation(temp_reg, temp_reg.type, nvm_val, 2, False)
                load.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE
                load.metric_group = "restore"
                # Add instruction to store in VM
                store = StoreOperation(vm_val, temp_reg, 0, False)
                store.virtual_memory_target = VirtualMemoryEnum.VOLATILE
                store.metric_group = "restore"
                # Append the instructions
                bb.instructions.insert(i, store)
                bb.instructions.insert(i, load)

    def place_checkpoint(self, bb_before, bb_after, checkpoint, fname):
        """
        Append instructions to basic blocks in order to perform checkpointing

        :param bb_before: The basic block before the checkpoint (origin of the CFG arc)
        :type bb_before: BasicBlock
        :param bb_after: The basic block after the checkpoint (destination of the CFG arc)
        :type bb_after: BasicBlock
        :param checkpoint: The checkpoint, unused argument for the moment
        :type checkpoint: Checkpoint
        :param str fname: The name of the function analysed

        WIP: Does not work yet (not handling cfgs with checkpoint between basic blocs with
        more than 1 arc out AND one arc out)
        """
        # Print checkpoint corresponding C code line and llvm ir basic blocks for debugging
        if logging.getLogger().getEffectiveLevel() >= logging.DEBUG:
            dgbelems = None

            scope = None
            file_name = "?"
            line_start = "?"
            line_end = "?"

            if len(bb_before.original_bb.instructions) > 0:
                # Parse the dwarf metadata of the block before the checkpoint
                metadata_obj = bb_before.original_bb.instructions[-1].metadata
                for i in range(1, len(bb_before.instructions) + 1):
                    if bb_before.instructions[-i].metadata is not None:
                        metadata_obj = bb_before.instructions[-i].metadata
                        break

                if metadata_obj is not None:
                    dgbelems = metadata_obj.elements
                    for dwarf_ref in metadata_obj.includes:
                        if dgbelems[dwarf_ref].type_name == "DILocation":
                            line_start = dgbelems[dwarf_ref].values['line']
                            scope = dgbelems[dwarf_ref].values['scope']

            if len(bb_after.instructions) > 0:
                # Parse the dwarf metadata of the block after the checkpoint
                metadata_obj = bb_after.instructions[0].metadata
                if metadata_obj is None and bb_after.original_bb is not None:
                    metadata_obj = bb_after.original_bb.instructions[-1].metadata
                if metadata_obj is not None:
                    dgbelems = metadata_obj.elements
                    for dwarf_ref in metadata_obj.includes:
                        if dgbelems[dwarf_ref].type_name == "DILocation":
                            line_end = dgbelems[dwarf_ref].values['line']
                            scope = dgbelems[dwarf_ref].values['scope']

            if scope is not None:
                file_ref = dgbelems[scope].values['file']
                file_name = dgbelems[file_ref].values['filename']
                logging.debug("Checkpoint in function",fname, " between %"  + str(bb_before.llvm_label), end="")
                logging.debug(" and %" + str(bb_after.llvm_label) + " : ", end="")
                logging.debug(file_name, " lines ", line_start, "-", line_end)

        # Create the checkpoint function call
        call = CallOperation(None, "@checkpoint", INT16_BASE_TYPE(), [], [], {})
        call.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE
        # If the basic bloc before the checkpoint as only one arc going out, we place the checkpoint at the end of this bb
        # We need to be sure that the basic block has at least one instruction before placing the checkpoint
        # (i.e that the basic block isn't a start_function)
        if len(bb_before.next) == 1 and len(bb_before.instructions) > 0:
            bb_before.instructions[0].label = None
            # Remove the branch instruction
            last_instr = bb_before.instructions.pop()
            # Add instructions to save the variables and registers
            self._add_variable_save_instruction(
                bb_before, len(bb_before.instructions), bb_before.memory_allocation, fname
            )
            bb_before.instructions.append(call)
            self._add_variable_restore_instruction(
                bb_before, len(bb_before.instructions), bb_after.memory_allocation, fname
            )
            # Replace the branch instruction at the end of the BB
            bb_before.instructions.append(last_instr)
            bb_before.instructions[0].label = "%" + str(bb_before.label)
        # Else, If the basic bloc after as only on arc going in, we place the checkpoint at the beginning of the bb
        elif len(bb_after.prev) == 1:
            i = 0
            if len(bb_after.instructions) == 0:
                raise NotImplementedError(
                    "[SCHEMATIC] does not handle checkpoint placement on a basic block with no "
                    "instructions: basic block {} from function {}".format(bb_after.llvm_label, bb_after.function_name)
                )
            first_inst = bb_after.instructions[0]
            # Skip the first allocas instructions, if any
            while i < len(bb_after.instructions) and isinstance(bb_after.instructions[i], AllocaOperation):
                i += 1
            self._add_variable_restore_instruction(bb_after, i, bb_after.memory_allocation, fname)
            bb_after.instructions.insert(i, call)
            self._add_variable_save_instruction(bb_after, i, bb_before.memory_allocation, fname)

            # Place the basic block label on the first non-alloca instruction
            new_first_inst = bb_after.instructions[i]
            new_first_inst.label = first_inst.label
            first_inst.label = None

    def modify_bb_accesses(self, bb):
        """
        Modify the accesses of a basic block for the variables that have been promoted to VM
        """
        if not bb.memory_allocation:
            raise ValueError(
                "Basic bloc {} from function {} " "has no memory allocation fixed".format(bb.llvm_label, bb.function_name)
            )
        mem_alloc = bb.memory_allocation.vars
        # Iterate over all the instructions
        for i in range(len(bb.instructions)):
            inst = bb.instructions[i]
            if isinstance(inst, ReturnOperation):
                if inst.memory_tag:
                    # Change the return value to point the variable in SRAM
                    var_alloc = mem_alloc[inst.memory_tag]
                    inst.element = self._generate_immediate_address(var_alloc.start_address, var_alloc.type)
                    inst.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE
            # TODO handle calls
            elif is_load_or_store(inst):
                if not inst.memory_tag:
                    raise Exception(
                        "A load/store operation in the basic block {} "
                        "does not have memory_tag: {}".format(bb.llvm_label, str(inst))
                    )
                if inst.memory_tag in mem_alloc:
                    var_alloc = mem_alloc[inst.memory_tag]
                    if var_alloc.allocation == VirtualMemoryEnum.VOLATILE:
                        # Change the store/load target to an index in the array located in SRAM (@SRAM)
                        if isinstance(inst, LoadOperation):
                            inst.element = self._generate_immediate_address(var_alloc.start_address, var_alloc.type)
                            inst.virtual_memory_target = VirtualMemoryEnum.VOLATILE
                            inst.align = 0
                        if isinstance(inst, StoreOperation):
                            inst.target = self._generate_immediate_address(var_alloc.start_address, var_alloc.type)
                            inst.virtual_memory_target = VirtualMemoryEnum.VOLATILE
                            inst.align = 0
                else:
                    raise Exception(
                        "The memory tag of the instruction "
                        + str(inst)
                        + " is not in the memory allocation of the basic block"
                    )

    def place_checkpoints(self, cfg):
        """
        Add instructions to perform checkpoint in a given cfg

        :param CFG cfg:
        """
        for bb_before, bb_after in list(cfg.edges):
            chkpt = cfg[bb_before][bb_after]["checkpoint"]
            if chkpt.type == CheckpointTypeEnum.ACTIVE:
                if len(bb_before.next) == 1 or len(bb_after.prev) == 1:
                    self.place_checkpoint(bb_before, bb_after, chkpt, cfg.name)
                else:
                    raise NotImplementedError(
                        "[SCHEMATIC] Cannot handle checkpoint placement between basic blocks "
                        "with more than one predecessor and ancestors. "
                        "The problem should have been handled before with add_bb_needed"
                    )
        for bb_before, bb_after in list(cfg.edges):
            chkpt = cfg[bb_before][bb_after]["checkpoint"]
            if chkpt.type == CheckpointTypeEnum.LOOP_LATCH:
                self.place_loop_latch_checkpoint(cfg, chkpt, cfg.name)

    def add_bb_needed(self, cfg):
        """
        Add basic blocks if checkpoints are placed between basic blocs with more than 1 arc flowing in/out
        of the basic block
        """
        for bb_before, bb_after in list(cfg.edges):
            chkpt = cfg[bb_before][bb_after]["checkpoint"]
            if chkpt.type == CheckpointTypeEnum.ACTIVE:
                if len(bb_before.next) > 1 and len(bb_after.prev) > 1:
                    insert_bb_on_cfg_arc(cfg, bb_before, bb_after)

    def apply_on_cfg(self, cfg):
        """
        Places checkpoint and apply memory allocation on the basic blocs of cfgs where the checkpoint and
        memory allocation are fixed

        - Relocates the target of the instructions accessing to variable promoted in VM
        - Adds instructions to save/restore the variable promoted to NVM before/after each checkpoint
        - Adds a call to the restore function before the program start

        See Also

        - `CodeTransformation.modify_bb_accesses` for the relocation of accesses
        - `CodeTransformation.place_checkpoint` for the checkpoint placement (call to checkpoint + variable restoration/save)
        """
        for bb in cfg.nodes:
            self.modify_bb_accesses(bb)
        self.add_bb_needed(cfg)
        self.place_checkpoints(cfg)

    def add_restore_for_main(self, cfg):
        first_bb = list(cfg.nodes)[0]
        i = 0
        # Skip the first alloca instructions
        while i < len(first_bb.instructions) and isinstance(first_bb.instructions[i], AllocaOperation):
            i += 1
        restore_call = CallOperation(None, "@restore", BaseType("int", 16), [], [], {})
        restore_call.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE
        first_bb.instructions.insert(i, restore_call)

    def apply(self, cfgs):
        """
        Places checkpoint and apply memory allocation on the basic blocs of cfgs where the checkpoint and
        memory allocation are fixed

        - Relocates the target of the instructions accessing to variable promoted in VM
        - Adds instructions to save/restore the variable promoted to NVM before/after each checkpoint
        - Adds a call to the restore function before the program start
        """
        for name, cfg in cfgs.items():
            if name == "@main":
                self.add_restore_for_main(cfg)
            self.func_tag_chain = self.memory_tag_chain[name]
            self.apply_on_cfg(cfg)

    def _generate_iter_variable(self, name, memory_allocation, memory_allocation2):
        """
        Create a new variable to count the iterations of a loop
        """
        # Check where the variable can be placed in SRAM
        max_mem_alloc_1 = max([bounds[1] for bounds in memory_allocation.occupied_memory] + [0])
        max_mem_alloc_2 = max([bounds[1] for bounds in memory_allocation2.occupied_memory] + [0])
        address = max(max_mem_alloc_1, max_mem_alloc_2, 0)
        allocation = VirtualMemoryEnum.VOLATILE
        value = Value("address", ImmediateAddress(address), INT16PTR_TYPE())
        if address + len(value.type) > self.vm_size:
            allocation = VirtualMemoryEnum.NON_VOLATILE
            value = Value("virtual_reg", "%" + str(name), INT16PTR_TYPE())
        # Create the variable
        var_alloc = VariableAllocation(name, INT16_TYPE(), value)
        var_alloc._need_restore = False
        var_alloc._need_save = False
        var_alloc.allocation = allocation
        var_alloc.start_address = address
        var_alloc.end_address = address + 2
        # Update the memory allocations
        memory_allocation.vars[name] = var_alloc
        memory_allocation.occupied_memory.append((var_alloc.start_address, var_alloc.end_address))
        memory_allocation2.vars[name] = var_alloc
        memory_allocation2.occupied_memory.append((var_alloc.start_address, var_alloc.end_address))

        return var_alloc

    def place_loop_latch_checkpoint(self, cfg, chkpt: Checkpoint, fname):
        """
        Places a loop_latch checkpoint (i.e a checkpoint triggered once every X iteration of the loop)
        """
        latch: BasicBlock = chkpt.bb_before
        latch_label = latch.instructions[0].label
        latch.instructions[0].label = None
        header = chkpt.bb_after

        name = "loop_" + str(header.llvm_label) + "_iter"
        var = self._generate_iter_variable(name, header.memory_allocation, latch.memory_allocation)
        if var.allocation == VirtualMemoryEnum.NON_VOLATILE:
            next(cfg.neighbors(cfg.first_bb)).instructions.insert(-1, AllocaOperation(var.element, var.type, 1, 0))

        # Initialize the iter variable before loop entry
        # TODO check if there is no checkpoint placed before
        for loop_entry in cfg.predecessors(header):
            if loop_entry != latch:
                store = StoreOperation(var.element, Value("immediate", 0, INT16_TYPE()), 2, False)
                store.metric_group = "checkpoint"
                store.virtual_memory_target = var.allocation
                if len(loop_entry.instructions) == 1:
                    store.label = loop_entry.instructions[0].label
                    loop_entry.instructions[0].label = None
                loop_entry.instructions.insert(-1, store)

        # Create the basic block which will do the checkpoint
        chkpt_bb = BasicBlock(generate_unique_bb_id(), cfg.name)
        chkpt_bb.llvm_label = "latch_checkpoint"
        chkpt_bb.vars = {}
        chkpt_bb.memory_allocation = header.memory_allocation

        # Create the basic block that will increment the iteration variable
        incr_bb = BasicBlock(generate_unique_bb_id(), cfg.name)
        incr_bb.llvm_label = "nb_iter++"
        incr_bb.vars = {}
        incr_bb.memory_allocation = latch.memory_allocation

        # In the latch, check if the iteration variable is superior or equal to the nb_max of iteration
        # If so, execute the checkpoint bb else increment the iter variable
        tmp_reg = generate_tmp_register(var.type)
        load = LoadOperation(tmp_reg, var.type, var.element, 0, False)
        load.metric_group = "checkpoint"
        load.virtual_memory_target = var.allocation
        latch.instructions.insert(-1, load)
        condition_reg = generate_tmp_register(INT1_TYPE())
        # Compare the number of iteration to chkpt.nb_iter (everything is minus 1 since the iter variable starts at 0)
        comp = CompareOperation(
            condition_reg, tmp_reg, Value("immediate", chkpt.nb_iter - 2, var.type), "sge", INT1_TYPE(), None
        )
        comp.metric_group = "checkpoint"
        latch.instructions.insert(-1, comp)

        latch.instructions[-1].condition = condition_reg
        latch.instructions[-1].target_true = "%" + str(chkpt_bb.label)
        latch.instructions[-1].target_false = "%" + str(incr_bb.label)
        latch.instructions[0].label = latch_label
        add_branch(incr_bb, header)
        add_free_branch(chkpt_bb, header)

        # Add instructions to increment the variable
        incr_reg = generate_tmp_register(var.type)
        incr_inst = BinaryOperation("add", tmp_reg, Value("immediate", 1, var.type), incr_reg, False, {})
        incr_inst.metric_group = "checkpoint"
        incr_bb.instructions.insert(-1, incr_inst)
        store_iter_var = StoreOperation(var.element, incr_reg, 0, False)
        store_iter_var.metric_group = "checkpoint"
        store_iter_var.virtual_memory_target = var.allocation
        incr_bb.instructions.insert(-1, store_iter_var)

        incr_bb.instructions[0].label = "%" + str(incr_bb.label)
        chkpt_bb.instructions[0].label = "%" + str(chkpt_bb.label)

        cfg.add_node(chkpt_bb)
        cfg.add_node(incr_bb)
        cfg.remove_edge(latch, header)
        cfg.add_edge(latch, chkpt_bb, checkpoint=Checkpoint(latch, chkpt_bb, CheckpointTypeEnum.DISABLED))
        cfg.add_edge(chkpt_bb, header, checkpoint=chkpt)
        cfg.add_edge(latch, incr_bb, checkpoint=Checkpoint(latch, incr_bb, CheckpointTypeEnum.DISABLED))
        cfg.add_edge(incr_bb, header, checkpoint=Checkpoint(incr_bb, header, CheckpointTypeEnum.DISABLED))

        # Place a checkpoint on the checkpoint bb and reinitialise the iter variable to 0
        self.place_checkpoint(chkpt_bb, header, cfg, fname)
        reset_iter_var_inst = StoreOperation(var.element, Value("immediate", 0, INT16_TYPE()), 0, False)
        reset_iter_var_inst.metric_group = "restore"
        reset_iter_var_inst.virtual_memory_target = var.allocation
        chkpt_bb.instructions.insert(-1, reset_iter_var_inst)
