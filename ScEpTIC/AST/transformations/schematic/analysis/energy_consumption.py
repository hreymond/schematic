from typing import List, Set

from ScEpTIC.AST.elements.instructions.memory_operations import is_load_or_store
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import ReturnOperation
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock


class EnergyConsumptionEstimationPass:
    """
    Pass to estimate the energy consumption of basic blocks.

    :param EnergyCalculator energy_calculator: An energy consumption model of the target platform
    :param Set[str] builtins_functions: A list of the builtins functions
    """

    def __init__(self, energy_calculator, vmstate, builtins_functions):
        self.energy_calculator = energy_calculator
        self.vmstate = vmstate
        self.builtins_functions = builtins_functions
        self.builtin_cost = {}

    def estimate_builtin_consumption(self, builtin_name):
        if builtin_name in self.builtin_cost:
            return self.builtin_cost[builtin_name]
        function = self.vmstate.functions[builtin_name]
        bb = BasicBlock(3, builtin_name)
        bb.instructions = function.body
        energy = self.estimate_energy_all_nvm(bb)
        self.builtin_cost[builtin_name] = energy
        return energy

    def estimate_energy_all_nvm(self, basic_bloc):
        """
        Estimate the energy consumption of a basic bloc with all variables in NVM
        """
        energy = 0
        for instruction in basic_bloc.instructions:
            # Do not take into account annotations functions
            if isinstance(instruction, CallOperation) and instruction.name in instruction.ignore:
                continue

            # Number of cycles required to execute the instruction
            n_cycles = instruction.tick_count
            # Energy consumed per cycle
            energy_per_cycle = self.energy_calculator.energy_clock_cycle

            # Handle return operation and load/store operations
            if isinstance(instruction, ReturnOperation):
                n_cycles += self.energy_calculator.nvm_extra_cycles * instruction.memory_tick_count
                energy_per_cycle += self.energy_calculator.energy_non_volatile_memory_access
            if is_load_or_store(instruction):
                n_cycles += self.energy_calculator.nvm_extra_cycles
                energy_per_cycle += self.energy_calculator.energy_non_volatile_memory_access
            # Account builtin cost since the function will not be analysed
            if isinstance(instruction, CallOperation) and instruction.name in self.builtins_functions:
                n_cycles += self.energy_calculator.nvm_extra_cycles * CallOperation.memory_tick_count
                energy_per_cycle += self.energy_calculator.energy_non_volatile_memory_access
                energy += self.estimate_builtin_consumption(instruction.name)
            # TODO handle function calls
            energy += n_cycles * energy_per_cycle
        return energy

    def apply(self, cfgs):
        """
        Update each basic block in the CFG with its cost with all memory access to NVM

        :param cfgs: A dict containing cfgs (nx.Digraph) indexed by function name (function_name => cfg)
        :type cfgs: dict
        """
        for name, function in cfgs.items():
            for bb in function:
                bb.cost_all_nvm = self.estimate_energy_all_nvm(bb)
