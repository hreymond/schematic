import copy

from ScEpTIC.AST.elements.instructions.memory_operations import LoadOperation, StoreOperation
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum


class MemoryUsage:
    @staticmethod
    def get_volatile_elements_before_calls(computation_intervals, order_function, checkpoint_function_name):
        """
        Returns for each function the memory tags whose reads can target volatile memory
        Does not support call depth > 1 (sufficient for evaluation)
        TODO: support call depth > 1
        """
        data = {}

        for computation_interval in computation_intervals:
            basic_blocks = order_function(computation_interval)

            volatile_map = set()

            for basic_block in basic_blocks:
                for instruction in basic_block.instructions:
                    if isinstance(instruction, StoreOperation):
                        memory_tag = MemoryUsage.get_base_memory_tag(instruction.memory_tag)

                        if instruction.schematic_target == VirtualMemoryEnum.VOLATILE:
                            volatile_map.add(memory_tag)
                        else:
                            if memory_tag in volatile_map:
                                volatile_map.remove(memory_tag)

                    if isinstance(instruction, CallOperation) and instruction.name != checkpoint_function_name:
                        name = instruction.name

                        if name not in data:
                            data[name] = []

                        data[name].append(copy.deepcopy(volatile_map))

        data = MemoryUsage.get_unique_volatile_elements(data)

        return data

    @staticmethod
    def get_volatile_elements_after_calls(computation_intervals, order_function, checkpoint_function_name):
        """
        Returns for each function the memory tags whose store can target volatile memory
        Does not support call depth > 1 (sufficient for evaluation)
        TODO: support call depth > 1
        """
        data = {}

        for computation_interval in reversed(computation_intervals):
            basic_blocks = reversed(order_function(computation_interval))

            volatile_map = set()

            for basic_block in basic_blocks:
                for instruction in reversed(basic_block.instructions):
                    if isinstance(instruction, StoreOperation):
                        memory_tag = MemoryUsage.get_base_memory_tag(instruction.memory_tag)
                        # if a store exists -> the previous one goes onto volatile memory
                        # regardeless of instruction.schematic_target, the store need to go into volatile memory
                        # -> if it targets volatile memory -> previous store volatile
                        # -> if it targets non-volatile memory -> previous store non-volatile
                        volatile_map.add(memory_tag)

                    if isinstance(instruction, CallOperation) and instruction.name != checkpoint_function_name:
                        name = instruction.name

                        if name not in data:
                            data[name] = []

                        data[name].append(copy.deepcopy(volatile_map))

        data = MemoryUsage.get_unique_volatile_elements(data)

        return data

    @staticmethod
    def remove_after_call_data_from_outside_frame_writes(after_call_data, outside_frame_writes):
        """
        Computes the elements that need to be preserved by the function call that has only one computation interval.
        The method returns a list of sets (one set per call to the function)
        """
        data = []

        for call_data in after_call_data:
            diff = set()

            # scan outside frame writes
            for memory_tag in outside_frame_writes:
                # if not in call data -> add to preserve set
                if memory_tag not in call_data:
                    diff.add(memory_tag)

            if diff not in data:
                data.append(diff)

        # normalization
        data = sorted(data, key=len)

        # highest set
        return data[-1]

    @staticmethod
    def get_outside_frame_read_volatile(before_call_data, outside_frame_reads):
        """
        Computes the elements that need to be retrieved from volatile memory
        """
        data = []

        for call_data in before_call_data:
            join = set()

            # scan outside frame reads
            for memory_tag in outside_frame_reads:
                # if in call data -> add to no-preserve set
                join.add(memory_tag)

            if join not in data:
                data.append(join)

        # normalization
        data = sorted(data, key=len)

        # lowest set
        return data[0]

    @staticmethod
    def get_unique_volatile_elements(volatile_elements, merge_data={}):
        """
        Merges volatile_elements and merge_data
        Parses retrieved volatile data and returns unique sets for each function
        """
        data = {}

        MemoryUsage._get_unique_volatile_elements(data, volatile_elements)
        MemoryUsage._get_unique_volatile_elements(data, merge_data)

        return data

    @staticmethod
    def _get_unique_volatile_elements(data, volatile_elements):
        """
        Parses retrieved volatile data and returns unique sets for each function
        """
        for name, elements in volatile_elements.items():
            if name not in data:
                data[name] = []

            for element in elements:
                if element not in data[name]:
                    data[name].append(element)

    @staticmethod
    def get_memory_usage_list(computation_intervals, order_function, initial_alive_tags):
        """
        Analyses the memory tags that need to be preserved across power failures

        Returns:
        - set of the tags that the function gets from outside its stack frame
        - set of the tags that the function writes outside its stack frame
        - list of the tags that need to be preserved, for each computation interval

        Outside-frame accesses supported only for global variables.
        TODO: pointers (not required for evaluation)
        """
        ci_tags_to_preserve = []

        alive_memory_tags = set()
        external_writes = set()

        # account for initial alive memory tags
        for element in initial_alive_tags:
            alive_memory_tags.add(element)

        for computation_interval in reversed(computation_intervals):
            to_preserve = set()

            # retrieve memory accesses infos
            basic_blocks = order_function(computation_interval)
            first_reads, writes = MemoryUsage._get_memory_tag_first_reads_writes(basic_blocks)

            # if a written memory location is in the alive set -> the computation interval needs to preserve it
            for memory_tag in writes:
                if memory_tag in alive_memory_tags:
                    to_preserve.add(memory_tag)

                    # remove memory location from alive set if it is a scalar variable
                    # NOTE: if it is not a scalar, all accesses but the last one need to save it
                    #       otherwise we may miss some updates to the non-scalar data structure
                    if "[]" not in memory_tag:
                        alive_memory_tags.remove(memory_tag)

                # global var write
                if memory_tag[0] == "@":
                    external_writes.add(memory_tag)

            # add memory tag to alive set
            for memory_tag in first_reads:
                alive_memory_tags.add(memory_tag)

            ci_tags_to_preserve.append(to_preserve)

        return alive_memory_tags, external_writes, list(reversed(ci_tags_to_preserve))

    @staticmethod
    def _get_memory_tag_first_reads_writes(basic_blocks):
        """
        Returns:
            - the memory tags that this computation interval reads from previous computation intervals
            - the memory tags that this computation interval writes
        """
        first_reads = set()
        writes = set()

        for basic_block in basic_blocks:
            for instruction in basic_block.instructions:
                if isinstance(instruction, LoadOperation):
                    memory_tag = MemoryUsage.get_base_memory_tag(instruction.memory_tag)

                    if memory_tag not in writes:
                        first_reads.add(memory_tag)

                elif isinstance(instruction, StoreOperation):
                    memory_tag = MemoryUsage.get_base_memory_tag(instruction.memory_tag)
                    writes.add(memory_tag)

        return first_reads, writes

    @staticmethod
    def get_base_memory_tag(memory_tag):
        """
        Returns the base memory tag, that is, a scalar-equivalent memory tag
        e.g. a[0] -> returns a[]
        """
        if "[" in memory_tag:
            return f"{memory_tag[:memory_tag.index('[')]}[]"

        return memory_tag
