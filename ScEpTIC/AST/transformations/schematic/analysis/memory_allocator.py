import math
from copy import deepcopy
from typing import Dict, List

from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.schematic.elements.memory_allocation import (
    VariableAllocation,
    MemoryAllocation,
    VariableAccesses,
    IncompatibleMemAllocException,
    merge_allocations,
    Variable,
)
from ScEpTIC.AST.elements.instructions.memory_operations import StoreOperation

from collections import Counter

# Word size in bits
WORD_SIZE = 16


def extends_memory_allocation(memory_allocation: MemoryAllocation, variables: Dict[str, Variable]):
    """
    Extends the memory allocation with the following variables.

    The extension of the allocation does not impact the placement of the variable already in the memory allocation
    nor the variable to save/restore.

    :param MemoryAllocation memory_allocation: The memory allocation to extends
    :param dict[str, Variable] variables: The variables to extend the memory allocation with
    """
    for name, var_infos in variables.items():
        if name not in memory_allocation.vars:
            memory_allocation.vars[name] = VariableAllocation.from_variable(var_infos)


class MemoryAllocator:
    """
    Used to determine the energy gain of different memory allocations and
    to choose the best memory allocation possible for a given path.

    :param EnergyCalculator energy_calculator: An energy calculator used to compute the energy gain of placing
        a variable to VM
    :param int,optional vm_size: The volatile memory, in bytes
    """

    def __init__(self, energy_calculator, vm_size=128, joint_alloc_placement=True):
        self.energy_calculator = energy_calculator
        # VM size in byte
        self.vm_size = vm_size
        self.joint_alloc_placement = joint_alloc_placement
        self.top_address = 0
        self.already_allocated_variables = {}
        # Costs of copying a word from VM to NVM = One access to VM to load the variable into register, and
        # one access to NVM to store the variable
        self.save_word_cost = self.energy_calculator.energy_nvm_access + self.energy_calculator.energy_vm_access
        self.restore_word_cost = self.energy_calculator.energy_nvm_access + self.energy_calculator.energy_vm_access

    def reset(self):
        """
        Resets the memory allocator:
        - Forget about the already allocated variables
        - Forget about the top address
        """
        self.top_address = 0
        self.already_allocated_variables = {}

    def compute_allocation_restore_cost(self, memory_allocation):
        """
        Compute the cost of restoring variables after boot

        :param memory_allocation: The memory allocation that will be used
        :type memory_allocation: MemoryAllocation
        """
        cost = 0
        for var_alloc in memory_allocation.vars.values():
            if var_alloc.allocation == VirtualMemoryEnum.VOLATILE:
                cost += self.restore_word_cost * int(len(var_alloc.type) / WORD_SIZE)
        return cost

    def compute_allocation_save_cost(self, memory_allocation):
        """
        Compute the cost of saving variables before powering off the platform

        :param MemoryAllocation memory_allocation: The memory allocation active before shutdown
        """
        cost = 0
        for var_alloc in memory_allocation.vars.values():
            if var_alloc.allocation == VirtualMemoryEnum.VOLATILE:
                cost += self.save_word_cost * int(len(var_alloc.type) / WORD_SIZE)
        return cost

    def estimate_energy_gain(self, variable_info, need_restore=True, need_save=True):
        """
        Compute the gain of placing the variable in SRAM

        :param VariableAccesses variable_info: The variable informations
        :param bool need_restore: Does the variable need to be restored ?
        :param bool need_save: Does the variable need to be saved ?
        """
        # Size in Word
        variable_size = math.ceil(len(variable_info.type) / WORD_SIZE)

        nb_access = variable_info.nb_access
        energy_gain = 0
        # Account checkpoint cost (temporary, without lifetimes)
        if need_restore:
            energy_gain -= self.restore_word_cost * variable_size
        if need_save:
            energy_gain -= self.save_word_cost * variable_size
        # Add energy gain
        energy_gain += nb_access * self.energy_calculator.non_volatile_increase
        return energy_gain

    def compute_memory_allocation_gain(self, variables, memory_allocation):
        gain = 0
        for name, var_infos in variables.items():
            if memory_allocation.vars[name].allocation == VirtualMemoryEnum.VOLATILE:
                # Remove from the BB cost the energy gain of doing the access to VM instead of NVM
                # (don't account for checkpointing cost)
                gain += self.estimate_energy_gain(var_infos)
        return gain

    def choose_memory_allocation(
        self,
        variables: Dict[str, VariableAccesses],
        start_alloc: MemoryAllocation or None,
        end_alloc: MemoryAllocation or None,
        memory_allocations: List[MemoryAllocation],
    ):
        """
        Chose the best memory allocation (VM or NVM) for the given variables

        Compute the energy gain for each variable of placing this variable in VM.
        Then, chooses the best memory allocation accordingly to the gain previously computed

        Implementation:
            For the moment, all variables are sorted by energy gain and
            we place variables in VM until their gain is > 0 or the VM size limit is reached

        :param dict[str, VariableAccesses] variables: A dict containing the variables accessed in the basic block
            considered, and their number of access
        :param MemoryAllocation,optional start_alloc: The allocation at the start of the path
        :param MemoryAllocation,optional end_alloc: The allocation at the end of the path
        :param List[MemoryAllocation] memory_allocations: The memory allocations encountered on the analysed path
        :return (MemoryAllocation, int): The best memory allocation satisfying the constraints
            and the energy gain associated
        """
        alloc = MemoryAllocation()
        total_gain = 0

        # Compute the memory allocation the variables must comply with
        try:
            # Merge the memory allocation found along the path
            if len(memory_allocations) > 0:
                alloc = merge_allocations(memory_allocations, checkpoint_increase_allowed=True)
            # If there is a start/end allocation, the variables must comply to the start/end allocation
            # and cannot increase the amount of data saved/restored.
            if start_alloc:
                alloc = merge_allocations([start_alloc, alloc])
            if end_alloc:
                alloc = merge_allocations([end_alloc, alloc])
        except IncompatibleMemAllocException:
            return MemoryAllocation(), -99999

        # We compute the gain of variables with a constraint on their allocation now, we don't consider them afterward
        for name, var_infos in alloc.vars.items():
            variable_size = math.ceil(len(alloc.vars[name]) / WORD_SIZE)
            total_gain -= self.restore_word_cost * variable_size if alloc.vars[name].need_restore else 0
            total_gain -= self.save_word_cost * variable_size if alloc.vars[name].need_save else 0

        # Compute the energy gain for each variable if placed in VM (except variable with an allocation constraint)
        energy_gains = {}
        for name, var_infos in variables.items():
            if name not in alloc.vars:
                need_restore = var_infos.first_operation != StoreOperation
                need_save = True  # not var_infos.read_only
                # If the variable is a pointer target, put in in NVM
                if name.startswith("*") or var_infos.type.is_pointer:
                    alloc.vars[name] = VariableAllocation.from_variable(var_infos)
                    continue
                # If there is no start_alloc or end_alloc, place the variable wherever you want
                if not start_alloc and not end_alloc:
                    energy_gains[name] = self.estimate_energy_gain(var_infos, need_restore, need_save)
                else:
                    # If the variable need to be restored/saved and is not restored/saved, put the variable in NVM
                    if (need_restore and start_alloc) or (need_save and end_alloc):
                        alloc.vars[name] = VariableAllocation.from_variable(var_infos)
                    else:
                        energy_gains[name] = self.estimate_energy_gain(var_infos, need_restore, need_save)

        var_by_gain = Counter(energy_gains).most_common()

        # Compute the memory allocation and the energy gain of this allocation
        # Size already allocated in bytes
        # TODO bin packing
        # top_address = max([interval[1] for interval in alloc.occupied_memory] + [0])
        top_address = self.top_address
        curr_address = top_address
        for var_name, gain in var_by_gain:
            # Var size in bytes
            var_size = math.ceil(len(variables[var_name].type) / WORD_SIZE) * WORD_SIZE // 8
            # Dy default, the variable is allocated in NVM
            var_alloc = VariableAllocation.from_variable(variables[var_name])
            # If the gain of placing the variable in VM is positive, we consider placing it in VM
            if gain > 0:
                # Does the variable already have an address in VM in other parts of the program ?
                if var_name in self.already_allocated_variables:
                    total_gain += gain
                    var_alloc.start_address = self.already_allocated_variables[var_name].start_address
                    var_alloc.end_address = self.already_allocated_variables[var_name].end_address
                    var_alloc.allocation = VirtualMemoryEnum.VOLATILE
                    alloc.occupied_memory.append((var_alloc.start_address, var_alloc.end_address))
                # Otherwise, is there enough space to fit the variable in VM ?
                elif curr_address + var_size <= self.vm_size:
                    total_gain += gain
                    var_alloc.start_address = curr_address
                    curr_address += var_size
                    var_alloc.end_address = curr_address
                    var_alloc.allocation = VirtualMemoryEnum.VOLATILE
                    self.already_allocated_variables[var_name] = var_alloc
            alloc.vars[var_name] = var_alloc
        # If a variable has been allocated to VM, update the occupied memory
        if top_address != curr_address:
            alloc.occupied_memory.append((top_address, curr_address))
            self.top_address = curr_address
        return alloc, total_gain

    def compute_cost(self, path, start_alloc=None, end_alloc=None):
        """
        Compute the cost of executing the basic blocks in path list with the best memory allocation available

        To do so, the cost and variable accessed of the basic bloc are concatenated and the function
        :func:`~MemoryAllocator.choose_memory_allocation` decide of the best allocation.

        :param list[BasicBlock] path: The list of basic block of which we want to compute the cost
        :param MemoryAllocation,optional start_alloc: The memory allocation at the beginning of the path
        :param MemoryAllocation,optional end_alloc: The memory allocation at the end of the path
        :return (MemoryAllocation, double): Return the best memory allocation and the energy consumed by the path
        """
        if len(path) == 0:
            return MemoryAllocation(), 0

        # Variable accessed in the path and their number of access
        variables_accessed = {}
        energy = 0
        memory_allocations = []
        for bb in path:
            # Add the cost of the basic bloc with all variables in NVM
            energy += bb.cost_all_nvm
            if bb.memory_allocation:
                memory_allocations.append(bb.memory_allocation)
            # Concatenate variable accesses for the path
            for name, infos in bb.variables.items():
                if name in variables_accessed:
                    variables_accessed[name].nb_access += infos.nb_access
                else:
                    variables_accessed[name] = deepcopy(infos)
        # Select the most energy-efficient memory allocation and compute the gain associated
        allocation, gain = self.choose_memory_allocation(variables_accessed, start_alloc, end_alloc, memory_allocations)
        # Account for the energy gain of placing variables in VM (If we are doing joint allocation and placement)
        if self.joint_alloc_placement:
            energy -= gain
        # If we are not doing joint allocation and placement, only count energy if the gain is negative (i.e when the allocation is impossible)
        elif gain < 0:
            energy -= gain

        return allocation, energy
