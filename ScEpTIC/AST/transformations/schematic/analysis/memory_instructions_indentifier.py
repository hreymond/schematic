from ScEpTIC.tools import flatten_multilevel_list


class MemoryInstructionsIdentifier:
    """
    This class contains the analysis required for identifying the first/last memory read/write instructions.
    """

    @staticmethod
    def resolve_first_reads_writes(basic_blocks, only_nvm=False, conditional_container=None, conditional_path=None):
        """
        Analyses the provided sequence of basic blocks and returns:
         - the list of all the read instructions that can execute before any write
         - the list of all the write instructions that can execute as first
        Each list is a python dictionary, whose key is the memory tag
        """
        writes = {}
        reads = {}
        final_writes = []

        for basic_block in basic_blocks:
            # get reads before first write
            first_reads, first_writes = basic_block.get_memory_tag_first_reads_writes(only_nvm)

            for memory_tag, metadata in first_reads.items():
                # skip
                if memory_tag in final_writes:
                    continue

                metadata = flatten_multilevel_list(metadata)

                if memory_tag not in reads:
                    reads[memory_tag] = []

                for element in metadata:
                    # set bookkeeping information for conditionals
                    element.set_conditional_container(conditional_container, conditional_path)
                    reads[memory_tag].append(element)

                # if reads not empty and previous writes are conditionals -> set writes to target nvm and account for normalization
                if len(metadata) > 1:
                    if memory_tag in writes:
                        for element in writes[memory_tag]:
                            if not element.write_covered:
                                element.to_normalize = True

            for memory_tag, metadata in first_writes.items():
                # skip
                if memory_tag in final_writes:
                    continue

                metadata = flatten_multilevel_list(metadata)

                # set bookkeeping information for conditionals
                for element in metadata:
                    element.set_conditional_container(conditional_container, conditional_path)

                # no prev_write -> add data and check
                if memory_tag not in writes:
                    writes[memory_tag] = metadata

                    last_write_reached = True

                    for element in metadata:
                        if element.missing and not element.write_covered:
                            last_write_reached = False
                            break

                    if last_write_reached:
                        final_writes.append(memory_tag)

                # prev_write data present
                # 1. If no write is missing -> stop (already certain)
                # 2. If missing writes all covered -> stop (already certain)
                # 3. If some missing write not covered
                #    3.1 If len(metadata) == 1 -> not a conditional -> set all previous writes as covered and stop
                #    3.2 If len(metadata) > 1  -> conditional -> check if metadata has missing writes
                #      3.2.1 If true -> continue
                #      3.2.2 If false -> set all previous writes as covered and stop
                else:
                    uncertain_write = False

                    for element in writes[memory_tag]:
                        # conditional write whose value is not overwritten by a subsequent write
                        if element.missing and not element.write_covered:
                            uncertain_write = True

                    if uncertain_write:
                        missing_write = False

                        # conditional block -> check for missing write
                        if len(metadata) > 1:
                            for element in metadata:
                                if element.missing:
                                    missing_write = True
                                    break

                        # No missing write -> cover all missing writes
                        if not missing_write:
                            for element in writes[memory_tag]:
                                element.write_covered = True

                            final_writes.append(memory_tag)

                        # add elements to writes
                        for element in metadata:
                            writes[memory_tag].append(element)

                    else:
                        final_writes.append(memory_tag)

        return reads, writes

    @staticmethod
    def resolve_last_reads_writes(basic_blocks, only_nvm=False, conditional_container=None, conditional_path=None):
        """
        Analyses the provided sequence of basic blocks and returns:
         - the list of all the read instructions that can execute after any write
         - the list of all the write instructions that can execute as last
        Each list is a python dictionary, whose key is the memory tag
        """
        reads = {}
        writes = {}

        for basic_block in basic_blocks:
            last_reads, last_writes = basic_block.get_memory_tag_last_reads_writes(only_nvm)

            for memory_tag, metadata in last_writes.items():
                uncertain_write = False
                metadata = flatten_multilevel_list(metadata)

                # set bookkeeping information for conditionals
                for element in metadata:
                    element.set_conditional_container(conditional_container, conditional_path)

                for element in metadata:
                    # conditional write
                    if element.missing:
                        uncertain_write = True
                        break

                if uncertain_write:
                    if memory_tag not in writes:
                        writes[memory_tag] = []

                    for element in metadata:
                        writes[memory_tag].append(element)

                else:
                    writes[memory_tag] = metadata
                    if memory_tag in reads:
                        del reads[memory_tag]

            for memory_tag, metadata in last_reads.items():
                metadata = flatten_multilevel_list(metadata)

                # set bookkeeping information for conditionals
                for element in metadata:
                    element.set_conditional_container(conditional_container, conditional_path)

                if memory_tag not in reads:
                    reads[memory_tag] = []

                for element in metadata:
                    reads[memory_tag].append(element)

        return reads, writes
