from ScEpTIC.AST.elements.instructions.memory_operations import StoreOperation, LoadOperation, AllocaOperation
from ScEpTIC.AST.transformations.schematic.elements.memory_allocation import VariableAccesses


def mem_tag_name_to_var_name(tag_name: str, function_name: str, pointer_access_lvl: int):
    """
    Extract the variable name from a memory tag

    :param str tag_name: The memory tag name
    :param str function_name: The name of the function where the variable is used
    :param int pointer_access_lvl: The level of pointer access: 0 = access to the variable, 1 = access to the address
        pointed by the variable

    Examples
    --------
    ```
    mem_tag_name_to_var_name("@a", "@main", 0) => "a"
    mem_tag_name_to_var_name("@a", "@main", 1) => "*a"
    mem_tag_name_to_var_name("a", "@main", 0) => "main.a"
    mem_tag_name_to_var_name("a", "@main", 2) => "**main.a"
    ```
    """
    if tag_name.startswith("@"):
        name = tag_name
    else:
        name = function_name[1:] + "." + tag_name
    pointer_access_lvl += name.count("[")
    if pointer_access_lvl >= 0:
        name = pointer_access_lvl * "*" + name.split("[")[0]
    else:
        raise Exception("[SCHEMATIC] Error pointer_access_lvl < 0")
    return name


class VariableAccessCountPass:
    """
    Pass to count the number of accesses to each variable in the different basic blocks
    """

    @staticmethod
    def update_variable_accesses_bb(bb, memory_tags, function_name):
        """
        Set the variables accessed in a basic block (bb.variables) and the number of accesses to those variables

        :param BasicBlock bb: The basic block we want to analyse
        :param memory_tags: The memory tag chain of the function containing the bb
        :param str function_name: The name of the function analysed
        """
        variables = {}
        for instruction in bb.instructions:
            if isinstance(instruction, StoreOperation) or isinstance(instruction, LoadOperation):
                # Get the variable type and element
                mem_tag_chain = memory_tags[instruction.memory_tag]
                var_type = mem_tag_chain[-1]["type"]
                element = mem_tag_chain[-1]["element"]
                # Get the element accessed through the memory operation (attribute target for a store, element for a load)
                mem_access_elem = instruction.target if isinstance(instruction, StoreOperation) else instruction.element
                # Compute the pointer level and get the variable name
                # (0 if the variable is accessed directly, 1 if the location pointed by the variable is accessed)
                pointer_access_lvl = element.type.pointer_level - mem_access_elem.type.pointer_level
                var_name = mem_tag_name_to_var_name(instruction.memory_tag, function_name, pointer_access_lvl)

                # If the variable has already been accessed, update the variable number of access
                if var_name in variables:
                    variables[var_name].nb_access += 1
                    if variables[var_name].read_only and isinstance(instruction, StoreOperation):
                        variables[var_name].read_only = False
                    instruction.memory_tag = var_name
                else:
                    # If the variable is an array or a pointer, not handled yet
                    if len(mem_tag_chain) > 1 or pointer_access_lvl:
                        # TODO check if working for struct and 2D tables
                        # raise NotImplementedError("Array and pointer accessed not handled yet")
                        pass
                    read_only = isinstance(instruction, LoadOperation)
                    variables[var_name] = VariableAccesses(var_name, var_type, element, 1, type(instruction), read_only)
                    instruction.memory_tag = var_name
        bb.variables = variables

    @staticmethod
    def apply(cfgs, memory_tag_chain):
        """
        Update the cfgs basic blocks with the name of the variable accessed and the number of accesses

        :param dict cfgs: A dict containing the cfgs (key: function_name (str), value: cfg (nx.Digraph))
        :param memory_tag_chain: The memory tag chain parsed with te memory_tag_parser
        """
        for name, function in cfgs.items():
            for bb in function:
                VariableAccessCountPass.update_variable_accesses_bb(bb, memory_tag_chain[name], name)
