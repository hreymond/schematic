from ScEpTIC.AST.elements.instructions.memory_operations import AllocaOperation
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation


class RegistersUsage:
    @staticmethod
    def set_checkpoints_regs(comp_int_manager):
        """
        Identifies the registers that each checkpoints need to save
        """

        for name in comp_int_manager.computation_intervals.keys():
            RegistersUsage.reset_register_info(comp_int_manager, name)
            RegistersUsage.set_checkpoints_general_purpose_regs(comp_int_manager, name)
            RegistersUsage.set_checkpoints_esp_reg(comp_int_manager, name)
            RegistersUsage.account_for_pc_save_only(comp_int_manager, name)

    @staticmethod
    def reset_register_info(comp_int_manager, name):
        """
        Resets the register-saving information associated to each checkpoint
        """
        computation_intervals = comp_int_manager.computation_intervals[name]

        for computation_interval in computation_intervals:
            basic_blocks = comp_int_manager.get_ordered_flatten_computation_interval(computation_interval)
            checkpoints = RegistersUsage._get_checkpoints(basic_blocks, comp_int_manager.checkpoint_function_name)

            for checkpoint in checkpoints:
                checkpoint.checkpoint_save_pc = True
                checkpoint.checkpoint_save_esp = False
                checkpoint.checkpoint_save_regs = set()

    @staticmethod
    def account_for_pc_save_only(comp_int_manager, name):
        """
        Adjust checkpoints that need to save the stack pointer (that is, if a checpoint need to save a register, except PC)
        If a checkpoint need to save only the PC (that is, checkpoint_save_esp = False) -> no 2-phase-commit needed as PC save is atomic
        """
        computation_intervals = comp_int_manager.computation_intervals[name]

        for computation_interval in computation_intervals:
            basic_blocks = comp_int_manager.get_ordered_flatten_computation_interval(computation_interval)

            # get all checkpoints
            checkpoints = RegistersUsage._get_checkpoints(basic_blocks, comp_int_manager.checkpoint_function_name)

            for checkpoint in checkpoints:
                # if a checkpoint need to save some register -> also need to save stack pointer
                if len(checkpoint.checkpoint_save_regs) > 0:
                    checkpoint.checkpoint_save_esp = True

    @staticmethod
    def set_checkpoints_esp_reg(comp_int_manager, name):
        """
        Sets the checkpoints in the computation interval that need to save the stack pointer
        """
        computation_intervals = comp_int_manager.computation_intervals[name]

        # first computation interval always need to save stack pointer
        save_esp = True

        for computation_interval in computation_intervals:
            basic_blocks = comp_int_manager.get_ordered_flatten_computation_interval(computation_interval)

            # set checkpoints to save stack pointer
            if save_esp:
                RegistersUsage._set_checkpoint_save_esp(basic_blocks, comp_int_manager.checkpoint_function_name)
                save_esp = False

            # if no checkpoint is inside the current computation interval, then:
            #  - it is the last of the function -> no next iteration
            #  - it has a call to a function that contains a checkpoint -> next iteration need to save ESP
            #    as it changes due to return instruction of the called function (that spans multiple computation intervals)
            checkpoints = RegistersUsage._get_checkpoints(basic_blocks, comp_int_manager.checkpoint_function_name)
            if len(checkpoints) == 0:
                save_esp = True

    @staticmethod
    def set_checkpoints_general_purpose_regs(comp_int_manager, name):
        """
        Sets the general purpose registers that each checkpoint in the computation interval need to save
        """
        computation_intervals = comp_int_manager.computation_intervals[name]

        # account for function's argument registers
        n_args = len(comp_int_manager.parser.functions[name].arguments)
        alloca_regs = RegistersUsage.get_alloca_regs(computation_intervals, n_args)

        alive_regs = set()

        # analyze computation intervals in reverse order to identify general purpose registers to save
        for computation_interval in reversed(computation_intervals):
            to_save = set()

            # retrieve regs infos
            basic_blocks = comp_int_manager.get_ordered_flatten_computation_interval(computation_interval)
            regs_external_uses, regs_defs = RegistersUsage.get_regs_first_use_def(basic_blocks, alloca_regs)

            # add live set to registers to save
            for reg in alive_regs:
                to_save.add(reg)

            # if a defined reg is in alive_regs -> save it and remove from alive_regs
            for reg in regs_defs:
                if reg in alive_regs:
                    alive_regs.remove(reg)

            # add to alive_regs the used regs
            for reg in regs_external_uses:
                alive_regs.add(reg)

            RegistersUsage._set_checkpoints_save_regs(basic_blocks, comp_int_manager.checkpoint_function_name, to_save)

    @staticmethod
    def _set_checkpoint_save_esp(basic_blocks, checkpoint_function_name):
        """
        Sets the checkpoints of a basic block to save the stack pointer
        """
        checkpoints = RegistersUsage._get_checkpoints(basic_blocks, checkpoint_function_name)

        for checkpoint in checkpoints:
            checkpoint.checkpoint_save_esp = True

    @staticmethod
    def _set_checkpoints_save_regs(basic_blocks, checkpoint_function_name, to_save):
        """
        Sets the checkpoints contained in the basic blocks to save the given registers

        Assumption (safe) -> no reg is used after a function call
        This is a safe assumption for the benchmarks we are considering, as we consider LLVM IR virtual regs
        """
        checkpoints = RegistersUsage._get_checkpoints(basic_blocks, checkpoint_function_name)

        for checkpoint in checkpoints:
            checkpoint.checkpoint_save_pc = True

            for reg in to_save:
                checkpoint.checkpoint_save_regs.add(reg)

    @staticmethod
    def _get_checkpoints(basic_blocks, checkpoint_function_name):
        """
        Returns a list of all the checkpoints included in the sequence of basic blocks
        """
        instructions = []

        for basic_block in basic_blocks:
            for instruction in basic_block.instructions:
                if isinstance(instruction, CallOperation) and instruction.name == checkpoint_function_name:
                    instructions.append(instruction)

        return instructions

    @staticmethod
    def get_alloca_regs(computation_intervals, n_args):
        """
        Returns the registers that are target of alloca instructions
        """
        alloca_regs = set()

        # accounts for registers that contain arguments
        for i in range(n_args):
            alloca_regs.add(f"%{i}")

        # order does not matter
        for computation_interval in computation_intervals:
            for basic_block in computation_interval.values():
                for instruction in basic_block.instructions:
                    if isinstance(instruction, AllocaOperation):
                        alloca_regs.add(instruction.target.value)

        return alloca_regs

    @staticmethod
    def get_regs_first_use_def(basic_blocks, alloca_regs):
        """
        Returns two lists:
            - registers whose use happens before their defs
            - registers defined
        """
        reg_first_uses = set()
        reg_defs = set()

        for basic_block in basic_blocks:
            for instruction in basic_block.instructions:

                for reg in instruction.get_uses():
                    # reg not defined + ignore alloca regs
                    if reg not in reg_defs and reg not in alloca_regs:
                        reg_first_uses.add(reg)

                for reg in instruction.get_defs():
                    # ignore alloca regs
                    if reg not in alloca_regs:
                        reg_defs.add(reg)

        return reg_first_uses, reg_defs
