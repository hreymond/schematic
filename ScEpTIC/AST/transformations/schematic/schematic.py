import logging
import sys
from collections import deque
from copy import deepcopy


import networkx as nx
from ScEpTIC.AST.transformations.schematic.elements.loop import Loop

from ScEpTIC.AST.transformations.schematic.analysis.memory_allocator import MemoryAllocator
from ScEpTIC.AST.transformations.schematic.cfg_modification import (
    update_checkpoint_type,
    propagate_energy_to_leave,
    propagate_energy_left,
)
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import Checkpoint, CheckpointTypeEnum
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.utils.cfg_utils import save_cfg, compute_basic_blocks_stats
from ScEpTIC.AST.transformations.schematic.utils.loop_utils import (
    get_max_iter,
    extract_loop_subgraph,
    estimate_loop_variable_access,
)
from ScEpTIC.AST.transformations.schematic.utils.reachable_chkpt_graph_utils import save_reachable_chkpt_graph
from ScEpTIC.emulator.energy import EnergyCalculator
from ScEpTIC.AST.elements.instructions.binary_operation import BinaryOperation
from ScEpTIC.AST.elements.instructions.memory_operations import LoadOperation, StoreOperation
from ScEpTIC.AST.elements.instructions.other_operations import CompareOperation


class MyTransformation:
    """
    This transformation places checkpoints and choose a memory allocation for the program.
    PS: We still need to find a name for the algorithm
    """

    def __init__(self, energy_calculator: EnergyCalculator, config=None):
        """
        Create a new MyTransformation given an energy calculator and a configuration.

        :param EnergyCalculator energy_calculator: A energy calculator used to determine the best memory allocation
        :param dict config: A dictionnary containing the configuration of the transformation.
            Must contains the fields
            - "energy_budget": The energy budget of the platform in J
            - "chkpt_restore": The energy (in J) for booting the platform and restoring the registers
            - "chkpt_save": The energy (in J) to save the registers and power off the platform
        """
        if not config:
            config = {
                "energy_budget": 50,
                "chkpt_restore": 2,
                "chkpt_save": 2,
                "call_cost": 5,
                "vm_size": 2000,
                "authorize_conditional_checkpoints": True,
                "joint_allocation_and_checkpoint": True,
            }
        try:
            self.energy_budget = config["energy_budget"]
            self.chkpt_restore = config["chkpt_restore"]
            self.chkpt_save = config["chkpt_save"]
            self.call_cost = config["call_cost"]
            self.vm_size = config["vm_size"]
            self.authorize_conditional_checkpoints = config.get("authorize_conditional_checkpoints", True)
            self.joint_allocation_and_placement = config.get("joint_allocation_and_checkpoint", True)
            # The instrumentation cost to execute a loop once every X iterations is the following
            # Load i, compare i, incr i and store i back
            loop_increment_computation_cost = (
                LoadOperation.tick_count
                + CompareOperation.tick_count
                + BinaryOperation.tick_count
                + StoreOperation.tick_count
            )
            loop_increment_computation_cost *= energy_calculator.energy_clock_cycle
            memory_cycles = LoadOperation.tick_count + StoreOperation.tick_count
            self.loop_increment_computation_cost_nvm = (
                loop_increment_computation_cost + memory_cycles * energy_calculator.energy_nvm_access
            )

            self.loop_increment_computation_cost_vm = (
                loop_increment_computation_cost + memory_cycles * energy_calculator.energy_vm_access
            )

        except KeyError as e:
            raise KeyError(
                "[SCHEMATIC] `{}` hasn't been found in the configuration given to SCHEMATIC transformation: ".format(
                    e.args[0], config
                )
            )

        self.memory_allocator = MemoryAllocator(energy_calculator, self.vm_size, self.joint_allocation_and_placement)
        self.function_analyzed = None
        self.traces = None

    def update_function_basic_blocks(self, cfg: CFG, callee_cfg: CFG, dependency, checkpoint_in_function):
        """
        Update function_entry and function_exit basic blocks with the cost and memory allocation of the function

        :param CFG cfg: The cfg of the caller
        :param CFG callee_cfg: The cfg of the callee
        :param (BasicBlock, BasicBlock) dependency: The basic blocks to update
        :param bool checkpoint_in_function: True if a checkpoint has been placed in the function called
        """
        f_entry = dependency[0]
        f_exit = dependency[1]
        f_start = callee_cfg.first_bb
        f_end = callee_cfg.last_bb

        # Set the basic blocks memory allocation
        f_entry.memory_allocation = deepcopy(f_start.memory_allocation)
        f_exit.memory_allocation = deepcopy(f_end.memory_allocation)

        chkpt = cfg[f_entry][f_exit]["checkpoint"]

        if checkpoint_in_function:
            chkpt.type = CheckpointTypeEnum.VIRTUAL
            f_entry.energy_to_leave = f_start.energy_to_leave + f_entry.cost_all_nvm
            f_entry.energy_left = f_start.energy_left
            f_entry.is_fixed = True
            f_entry.final_cost = f_start.energy_to_leave + f_entry.cost_all_nvm
            f_entry.cost_all_nvm = f_entry.final_cost
            f_exit.energy_to_leave = f_end.energy_to_leave
            f_exit.energy_left = f_end.energy_left
            f_exit.final_cost = self.energy_budget - f_end.energy_left + f_exit.cost_all_nvm
            f_exit.cost_all_nvm = f_exit.final_cost
            f_exit.is_fixed = True
        else:
            f_entry.final_cost = f_entry.cost_all_nvm + f_start.energy_to_leave - f_end.energy_to_leave
            f_entry.cost_all_nvm = f_entry.final_cost
            f_exit.final_cost = f_exit.cost_all_nvm
            chkpt.type = CheckpointTypeEnum.DISABLED

    def get_checkpoints_from_trace(self, cfg, trace):
        """
        Return the checkpoints encountered on the trace bounded by a "Start" and a "End" checkpoint

        :param CFG cfg: The cfg of the function considered
        :param list trace: A list of basic blocks
        :return list: A list of checkpoint, beginning with a checkpoint named "Start" and ending with a one named "End"
        """
        # Creates a "start" checkpoint with no basic bloc before it and the first bb after it
        start_node = Checkpoint(None, trace[0])
        start_node.name = "Start"
        start_node.virtual = True
        chkpt_list = [start_node]
        for i in range(0, len(trace) - 1):
            # Add the checkpoint to the list if it is not a checkpoint between a f_entry and f_exit basic block
            if not (trace[i].is_function_call and trace[i + 1].is_function_call):
                chkpt = cfg.get_edge_data(trace[i], trace[i + 1])["checkpoint"]
                chkpt_list.append(chkpt)
        end_node = Checkpoint(trace[len(trace) - 1], None)
        end_node.name = "End"
        end_node.virtual = True
        chkpt_list.append(end_node)
        return chkpt_list

    def create_reachable_checkpoint_graph(self, cfg, trace, checkpoints):
        """
        Creates a reachable checkpoint graph from the trace and cfg given

        :param CFG cfg: The CFG of the function analyzed. In order for the graph construction to work properly, the cfg must have been analysed with
            the EnergyEstimationPass and VariableCountPass
        :param list[BasicBlock] trace: A list of basic blocks that represent the trace executed
        :param list[Checkpoint] checkpoints: The checkpoints in the trace
        :return nx.DiGraph: The reachable checkpoint graph
            The graph start with a node "Start" and a node "End" and is composed of the checkpoint encountered
            when analysing the trace. An arc (i,j) present between 2 checkpoint indicates that the checkpoint i is
            reachable from checkpoint i

        The algorithm first fetch all the checkpoint crossed in the trace. Then it analyses the cost of
        reaching
        """

        if len(trace) < 3:
            raise Exception(
                "Malformed trace: A trace must be at least 3 basic bloc long ! "
                + str([bb.llvm_label for bb in trace])
                + " is not a valid trace"
            )

        reachable_chkpt_graph = nx.DiGraph()

        # Get the energy left at the beginning of the trace and the energy to leave at the end of the trace
        energy_left = trace[0].energy_left if trace[0].energy_left else (self.energy_budget - self.chkpt_restore)
        if cfg.first_bb == trace[0]:
            energy_left -= self.call_cost
        energy_to_leave = trace[-1].energy_to_leave if trace[-1].energy_to_leave else self.chkpt_save

        # Add checkpoints visible in trace to the reachable checkpoint graph + start/end nodes
        reachable_chkpt_graph.add_nodes_from(checkpoints)

        """
        The following code check if a checkpoint *chkpt* is reachable from another checkpoint *start_chkpt*
        (i, i+1) represent the checkpoint *start_chkpt* and (j, j+1) the checkpoint *chkpt*
        """

        # For each checkpoint seen in the trace except the last one
        # (since we want to check if A is reachable from B with A != B)
        for i in range(0, len(trace) - 2):
            cost = 0
            start_chkpt = cfg.get_edge_data(trace[i], trace[i + 1])["checkpoint"]
            if trace[i].is_function_call and trace[i + 1].is_function_call:
                continue
            j = i + 1
            # Check which checkpoints are reachable from start_chkpt
            while cost < self.energy_budget and j < len(trace) - 1:
                if trace[j].is_function_call and trace[j + 1].is_function_call:
                    j += 1
                    continue
                # Compute the best memory allocation and the cost associated for
                # the basic blocs between start_checkpoint and chkpt
                memory_allocation, cost = self.memory_allocator.compute_cost(trace[i + 1 : j + 1])
                cost += self.chkpt_restore + self.chkpt_save
                # Add an edge from start_chkpt to chkpt if chkpt is reachable from start_chkpt
                if cost < self.energy_budget:
                    chkpt = cfg.get_edge_data(trace[j], trace[j + 1])["checkpoint"]
                    reachable_chkpt_graph.add_edge(start_chkpt, chkpt, weight=cost, alloc=memory_allocation)
                j += 1

        """
        Handling of the specific cases of start node and end node
        """
        """
        NOTES:
        TODO Réfléchir à l'algo sur le tableau pour faire un truc propre

        Est ce que cela vaut le coup de gérer les noeuds start/end dans la boucle principale ?
        - Oui, plus simple à la lecture
        - Non, car séparer les boucles permet de gérer le cas où des allocations mémoires sont fixées séparément
        """

        """
        Start node to all other checkpoints
        """
        # Index of the basic bloc considered
        j = 0
        cost = 0
        while cost < energy_left and j < len(trace) - 1:
            if trace[j].is_function_call and trace[j + 1].is_function_call:
                j += 1
                continue
            chkpt = cfg.get_edge_data(trace[j], trace[j + 1])["checkpoint"]
            memory_allocation, cost = self.memory_allocator.compute_cost(
                trace[1 : j + 1], start_alloc=trace[0].memory_allocation
            )
            cost += self.chkpt_save
            if cost < energy_left:
                reachable_chkpt_graph.add_edge(checkpoints[0], chkpt, weight=cost, alloc=memory_allocation)
            j += 1

        """
        All checkpoints to end node
        """
        # Index of the basic bloc considered
        i = len(trace) - 1
        cost = 0
        while cost + energy_to_leave < self.energy_budget and i > 0:
            if trace[i - 1].is_function_call and trace[i].is_function_call:
                i -= 1
                continue
            memory_allocation, cost = self.memory_allocator.compute_cost(
                trace[i : len(trace) - 1], end_alloc=trace[len(trace) - 1].memory_allocation
            )
            cost += self.chkpt_restore
            chkpt = cfg.get_edge_data(trace[i - 1], trace[i])["checkpoint"]
            if cost + energy_to_leave < self.energy_budget:
                reachable_chkpt_graph.add_edge(chkpt, checkpoints[-1], weight=cost, alloc=memory_allocation)
            i -= 1

        memory_allocation, cost = self.memory_allocator.compute_cost(
            trace[1 : len(trace) - 1],
            start_alloc=trace[0].memory_allocation,
            end_alloc=trace[len(trace) - 1].memory_allocation,
        )
        if cost + energy_to_leave < energy_left:
            reachable_chkpt_graph.add_edge(checkpoints[0], checkpoints[-1], weight=cost, alloc=memory_allocation)

        return reachable_chkpt_graph

    def get_shortest_path_in_rcg(self, rcg):
        """
        Find the shortest path in the reachable checkpoint graph and returns it

        :param rcg: The reachable checkpoint graph (rcg)
        :type rcg: networkx.Digraph

        :return path: a list of nodes in a shortest path from the source to the target (including source & target).
        :rtype: list
        """
        chkpts = {checkpoint.name: checkpoint for checkpoint in rcg.nodes}
        path = nx.shortest_path(rcg, chkpts["Start"], chkpts["End"], weight="weight")

        return path

    def extract_not_fixed_bb_trace(self, start_bb, cfg):
        """
        Extract a path of not fixed basic block from a given bb not fixed

        :param BasicBlock start_bb: a basic block not fixed
        :param CFG cfg:
        """
        to_visit = deque([start_bb])
        # The path obtained start from a fixed basic block before start_bb
        trace = [next(cfg.predecessors(start_bb))]
        while to_visit:
            bb = to_visit.pop()
            trace.append(bb)
            for neighbor in cfg.neighbors(bb):
                if not neighbor.is_fixed:
                    to_visit.append(neighbor)
                    break
        trace.append(next(cfg.neighbors(bb)))
        return trace

    def extract_not_fixed_bb_paths(self, trace):
        """
        Extract the subpaths of contiguous basic blocs not fixed following a trace

        :param  List[BasicBlock] trace: the trace we want to extract the subpaths from
        """
        paths = []
        current_path = []
        registering_path = False
        last_fixed_bb = None
        # Traversing all the BB composing the trace
        for i in range(len(trace)):
            # If we are currently traversing a subtrace of not fixed bbs
            if registering_path:
                # Append the basic block to the subtrace
                current_path.append(trace[i])
                # If the next basic block is fixed, stop the trace
                if trace[i].is_fixed:
                    paths.append(current_path)
                    current_path = []
                    registering_path = False
            # If we are traversing fixed BBs, wait until we reach a basic block not fixed
            if not registering_path:
                if trace[i].is_fixed:
                    last_fixed_bb = trace[i]
                else:
                    registering_path = True
                    if last_fixed_bb:
                        current_path.append(last_fixed_bb)
                        last_fixed_bb = None
                    current_path.append(trace[i])

        if registering_path:
            paths.append(current_path)
        return paths

    def analyse_trace(self, cfg, trace):
        """
        Analyses an execution trace

        Does:

        1. Create reachable checkpoint graph
        2. Choose the best path in the reachable checkpoint graph to determine the checkpoint placement and memory alloc
        3. Apply the memory allocation/checkpoint placement to the CFG
        4. Compute the energyLeft/EnergyToLeave values

        :param CFG cfg: The cfg of the function analysed
        :param list[BasicBlock] trace: the list of basic bloc executed
        """
        sub_traces = self.extract_not_fixed_bb_paths(trace)
        for sub_path in sub_traces:
            checkpoints = self.get_checkpoints_from_trace(cfg, sub_path)
            rcg = self.create_reachable_checkpoint_graph(cfg, sub_path, checkpoints)
            save_reachable_chkpt_graph(rcg, "rcg_trace" + str([bb.llvm_label for bb in sub_path]))
            try:
                path = self.get_shortest_path_in_rcg(rcg)
            except nx.exception.NetworkXNoPath as e:
                raise nx.exception.NetworkXNoPath(
                    "No path has been found in the reachable checkpoint graph for function {} while examining the following trace:\n"
                    "{}\n"
                    "No safe checkpoint placement is available.".format(cfg.name, [bb.llvm_label for bb in sub_path])
                )
            update_checkpoint_type(checkpoints, path)
            self.apply_memory_allocation(sub_path, cfg, path, rcg)

        stats = compute_basic_blocks_stats(cfg)
        logging.info(f"Function {cfg.name}: {stats['nb_fixed_bb']}/{stats['nb_bb']} basic blocks fixed")

    def apply_memory_allocation(self, trace, cfg, path, rcg):
        """
        Apply the memory allocation determined by the best path in the reachable checkpoint graph to the basic blocs
        concerned

        :param trace: The trace analysed
        :type trace: list[BasicBlock]
        :param CFG cfg: The cfg of the function
        :param path: Nodes along shortest path in the reachable checkpoint graph
            (i.e checkpoints that need to be activated)
        :type path: list[Checkpoint]
        :param rcg: The reachable checkpoint graph (which contains the memory allocation between every checkpoint)
        """
        if len(trace) < 3:
            raise Exception("Trace should be at least 3 bb long (start, bb and end)")

        # If no checkpoint is placed after the first basic block of the trace
        # (i.e the first basic block shares the same allocation as the next ones), update the first bb allocation
        if trace[0].memory_allocation:
            if (path[1].bb_before and not path[1].bb_before == trace[0]) or path[1].bb_after is None:
                trace[0].memory_allocation.extends(rcg[path[0]][path[1]]["alloc"])
                rcg[path[0]][path[1]]["alloc"] = trace[0].memory_allocation
        # If no checkpoint is placed before the last basic block of the trace
        # (i.e the last basic block shares the same allocation as the previous ones), update the last bb allocation
        if trace[-1].memory_allocation:
            if (path[-2].bb_after and not path[-2].bb_before == trace[-1]) or path[-2].bb_before is None:
                trace[-1].memory_allocation.extends(rcg[path[-2]][path[-1]]["alloc"])
                rcg[path[-2]][path[-1]]["alloc"] = trace[-1].memory_allocation

        # If no checkpoint is activated on the path, update the first bb and last bb allocation so that they are the same
        if len(path) == 2:
            if trace[0].memory_allocation:
                trace[0].memory_allocation.extends(rcg[path[0]][path[1]]["alloc"])
            if trace[-1].memory_allocation:
                trace[-1].memory_allocation.extends(rcg[path[0]][path[1]]["alloc"])
            if trace[-1].memory_allocation and trace[0].memory_allocation:
                new_mem_alloc = trace[-1].memory_allocation
                old_mem_alloc = trace[0].memory_allocation
                for bb in cfg:
                    if bb.memory_allocation == old_mem_alloc:
                        bb.memory_allocation = new_mem_alloc
        # i = index of the current basic block in the trace
        # j = index of the current checkpoint in the list of checkpoints to activate (path)
        i = 0
        memory_alloc = "None"
        for j in range(len(path) - 1):
            # Step 1: Find the memory allocation between the last_checkpoint and the next checkpoint
            last_checkpoint = path[j]
            next_checkpoint = path[j + 1]
            memory_alloc = rcg[last_checkpoint][next_checkpoint]["alloc"]

            # Step 2. Apply the memory allocation to the basic blocks between the last_checkpoint and the next checkpoint
            checkpoint_reached = False
            while i < len(trace) - 1 and not checkpoint_reached:
                # Apply the current memory allocation to the basic block if not already fixed
                trace[i].set_memory_allocation(memory_alloc, self.memory_allocator)

                # if the checkpoint between i and i+1 is active, consider the next checkpoint interval
                if cfg[trace[i]][trace[i + 1]]["checkpoint"] == next_checkpoint:
                    checkpoint_reached = True
                i += 1

        # Handle the last basic bloc
        trace[i].set_memory_allocation(memory_alloc, self.memory_allocator)

        # Compute and propagate energy_left and energy_to_leave
        for checkpoint in path:
            if checkpoint.bb_after:
                bb = checkpoint.bb_after
                if checkpoint.virtual and bb.energy_left:
                    energy_left_start = bb.energy_left + bb.final_cost
                else:
                    energy_left_start = self.energy_budget - self.chkpt_restore
                    energy_left_start -= self.memory_allocator.compute_allocation_restore_cost(bb.memory_allocation)
                propagate_energy_left(checkpoint, energy_left_start, cfg)
            if checkpoint.bb_before:
                bb = checkpoint.bb_before
                if bb.energy_to_leave:
                    energy_to_leave = bb.energy_to_leave - bb.final_cost
                else:
                    energy_to_leave = self.chkpt_save
                    energy_to_leave += self.memory_allocator.compute_allocation_save_cost(bb.memory_allocation)
                propagate_energy_to_leave(checkpoint, energy_to_leave, cfg)

    def remove_potential_checkpoints_between_fixed_bbs(self, cfg):
        """
        Remove potential checkpoint between fixed basic blocks.

        If the 2 allocations of the basic blocks are different, place a checkpoint.
        Else,  check if the energy left at the end of the first bb is superior to the energy to_leave for the second bb.

        :param CFG cfg: The cfg we want te examine
        :raises NotImplementedError: if the function traces are not exhaustive
        """
        checkpoints = nx.get_edge_attributes(cfg, "checkpoint")
        for c in checkpoints.values():
            if c.type == CheckpointTypeEnum.POTENTIAL:
                # If the memory allocation before and after the checkpoint are not set, it means that the traces
                # did not cover all the program basic blocks
                if (
                    not c.bb_before
                    or not c.bb_after
                    or not c.bb_before.memory_allocation
                    or not c.bb_after.memory_allocation
                ):
                    raise NotImplementedError(
                        "[SCHEMATIC] The program does not yet handle non-exhaustive traces: function {}".format(cfg.name)
                    )

                # If the memory allocations are not the same, place a checkpoint
                if not c.bb_before.memory_allocation.is_compatible_with(c.bb_after.memory_allocation):
                    c.type = CheckpointTypeEnum.ACTIVE
                else:
                    if c.bb_before.energy_left > c.bb_after.energy_to_leave:
                        c.type = CheckpointTypeEnum.DISABLED
                        propagate_energy_left(c, c.bb_before.energy_left, cfg)
                        propagate_energy_to_leave(c, c.bb_after.energy_to_leave, cfg)
                    else:
                        c.type = CheckpointTypeEnum.ACTIVE

    def find_and_analyse_not_fixed_paths(self, cfg: CFG, name: str, iteration: int = 0):
        """
        Find all the basic block not analysed in a cfg and analyses it

        :param CFG cfg: The cfg to analyse
        :param str name: The name of the function analysed
        :param int, optional iteration: The number of analysis already made, only useful for debugging as it
            is appended to the dot graph generated.
        """
        for bb in cfg.nodes:
            if not bb.is_fixed:
                t = self.extract_not_fixed_bb_trace(bb, cfg)
                self.analyse_trace(cfg, t)
                save_cfg("it_" + str(iteration), cfg, folder="dot_cfgs/" + name)
                iteration += 1

    def analyse_loop(self, l_traces, cfg: CFG):
        """
        Analyse a loop

        :param LoopTraces l_traces: The trace and loop info of the loop we want to analyse
        :param CFG cfg: cfg of the function
        """

        logging.info("[SCHEMATIC] Analyse loop {}".format(l_traces.loop.name))
        loop_info = l_traces.loop
        max_iter = get_max_iter(loop_info, cfg)
        loop_cfg = extract_loop_subgraph(loop_info, cfg)
        # Extract memory allocations before analysis to keep track of the memory allocations to respect
        memory_allocations = []
        for bb in loop_cfg.nodes:
            if bb.memory_allocation and bb.memory_allocation not in memory_allocations:
                memory_allocations.append(bb.memory_allocation)
        map(lambda x: deepcopy(x), memory_allocations)
        # Analyse the loop traces (sorted by frequency)
        traces = list(l_traces.traces.values())
        traces.sort(key=lambda t: t.nb_executions, reverse=True)
        it = 0
        for trace in traces:
            # Add "Start_loop" and "END_Loop" basic blocks and analyse the trace
            trace.trace.insert(0, loop_cfg.first_bb)
            trace.trace.append(loop_cfg.last_bb)
            self.analyse_trace(loop_cfg, trace.trace)
            save_cfg(
                "it_{}.dot".format(it),
                loop_cfg,
                folder="dot_cfgs/{}/loop_{}".format(cfg.name, l_traces.loop.header[0].llvm_label),
            )
            it += 1

        self.find_and_analyse_not_fixed_paths(loop_cfg, loop_cfg.name)
        self.remove_potential_checkpoints_between_fixed_bbs(loop_cfg)

        chkpt = cfg[loop_info.latch[-1]][loop_info.header[0]]["checkpoint"]

        # If the memory allocation at the beginning of the loop header is different from the loop latch one,
        # place a checkpoint
        if loop_cfg.first_bb.memory_allocation != loop_cfg.last_bb.memory_allocation:
            chkpt.type = CheckpointTypeEnum.ACTIVE
            return

        # Otherwise, consider how many iteration can be done with the energy budget
        energy_one_it = (
            loop_cfg.first_bb.energy_to_leave
            - loop_cfg.last_bb.energy_to_leave
            + self.loop_increment_computation_cost_nvm
        )
        nb_it_with_budget = (self.energy_budget - loop_cfg.last_bb.energy_to_leave) // energy_one_it - 1
        variable_accessed = None
        # If the allocation is the same everywhere, reconsider allocation
        if loop_cfg.has_only_disabled_checkpoints() and nb_it_with_budget > 1:
            old_nb_it = None
            # Reconsider the memory allocation to increase the nb of iteration possible with the energy budget,
            # until it does not increase anymore
            i = 0
            while old_nb_it is None or nb_it_with_budget < old_nb_it:
                i += 1
                if i > 15:
                    raise Exception("La gestion des boucles ne converge pas ....")
                old_nb_it = nb_it_with_budget
                header = loop_info.header[0]
                save_cfg("loop_" + str(header.llvm_label) + "_before", loop_cfg)

                # Estimate the number of variable accesses if the loop is doing min(nb_it_with_budget, max_iter) iterations and update the memory allocation given the number of access
                variable_accessed = estimate_loop_variable_access(l_traces, min(nb_it_with_budget, max_iter))
                mem_alloc, _ = self.memory_allocator.choose_memory_allocation(
                    variable_accessed, None, None, memory_allocations
                )

                # Recompute basic block energy consumption given the new allocation
                for bb in loop_cfg.nodes:
                    bb.is_fixed = False
                    bb.set_memory_allocation(mem_alloc, self.memory_allocator)

                # Recompute the energy left and energy to leave for the basic blocks of the loop
                energy_left_start = self.energy_budget - self.chkpt_restore
                energy_left_start -= self.memory_allocator.compute_allocation_restore_cost(
                    loop_cfg.first_bb.memory_allocation
                )
                propagate_energy_left(Checkpoint(None, loop_cfg.first_bb), energy_left_start, loop_cfg)
                energy_to_leave = self.chkpt_save + self.loop_increment_computation_cost_nvm
                energy_to_leave += self.memory_allocator.compute_allocation_save_cost(loop_cfg.last_bb.memory_allocation)
                propagate_energy_to_leave(Checkpoint(loop_cfg.last_bb, None), energy_to_leave, loop_cfg)

                # Recompute the energy minimum to safely execute one iteration of the loop
                energy_one_it = (
                    loop_cfg.first_bb.energy_to_leave
                    - loop_cfg.last_bb.energy_to_leave
                    + self.loop_increment_computation_cost_nvm
                )
                nb_it_with_budget = (self.energy_budget - loop_cfg.first_bb.energy_to_leave) // energy_one_it
                nb_it_with_budget -= 1
                # print("Energy one it after:", energy_one_it, " nb it: ", nb_it_with_budget)
                save_cfg("loop_" + str(header.llvm_label) + "_after", loop_cfg)

        # If we can execute all loop iterations with the energy budget, no checkpoint is needed.
        if nb_it_with_budget > max_iter:
            # print("No checkpoint needed !")
            chkpt.type = CheckpointTypeEnum.DISABLED
            loop_info.header[0].variables = variable_accessed if variable_accessed else loop_info.header[0].variables
            loop: Loop = Loop(loop_info, max_iter, energy_one_it)
            for bb in loop_info.basic_blocks:
                bb.energy_to_leave = bb.energy_to_leave + (max_iter - 1) * energy_one_it
                bb.energy_left = bb.energy_left - (max_iter - 1) * energy_one_it
                bb.loop = loop

        # If conditional checkpoints are not enabled, activate the checkpoint
        elif nb_it_with_budget < 3 or not self.authorize_conditional_checkpoints:
            # print("Checkpoint needed every iteration")
            chkpt.type = CheckpointTypeEnum.ACTIVE
        else:
            # print("Checkpoint needed every {} iterations".format(nb_it_with_budget))
            chkpt.type = CheckpointTypeEnum.LOOP_LATCH

            loop_info.header[0].variables = variable_accessed if variable_accessed else loop_info.header[0].variables
            chkpt.nb_iter = nb_it_with_budget
            loop: Loop = Loop(loop_info, nb_it_with_budget, energy_one_it)
            for bb in loop_info.basic_blocks:
                bb.energy_to_leave += (nb_it_with_budget - 1) * energy_one_it
                bb.energy_left -= (nb_it_with_budget - 1) * energy_one_it
                bb.loop = loop

        print("Analysing loop")

        save_cfg("{}/loop_%{}".format(cfg.name, l_traces.loop.header[0].llvm_label), loop_cfg)

    def analyse_function(self, name: str, cfg: CFG, f_traces):
        self.function_analyzed = name
        iteration = 0
        save_cfg("before", cfg, folder="dot_cfgs/" + name)
        loop_traces = list(f_traces.loop_traces.values())
        try:
            loop_traces.sort(key=lambda val: val.loop.depth, reverse=True)
        except KeyError:
            logging.critical("[SCHEMATIC] Malformed trace: All loop traces must have a 'depth' attribute")
            sys.exit(1)
        logging.info("[SCHEMATIC] Analysing loop traces")
        # First, analyse loops
        for loop_trace in loop_traces:
            self.analyse_loop(loop_trace, cfg)
            save_cfg("it_" + str(iteration), cfg, folder="dot_cfgs/" + name)
            iteration += 1
        logging.info("[SCHEMATIC] Analysing function traces")
        # Then, analyse the function
        for trace in f_traces.traces.values():
            self.analyse_trace(cfg, trace.trace)
            save_cfg("it_" + str(iteration), cfg, folder="dot_cfgs/" + name)
            iteration += 1
        # Analyse path not present in the traces
        self.find_and_analyse_not_fixed_paths(cfg, name, iteration)
        self.remove_potential_checkpoints_between_fixed_bbs(cfg)

    def apply(self, cfgs, traces, function_depgraph):
        self.traces = traces
        self.function_depgraph = function_depgraph
        function_order = reversed(list(nx.topological_sort(function_depgraph)))
        for function in function_order:
            cfg: CFG = cfgs[function]
            logging.info(f"[SCHEMATIC] Analyzing function {function}")
            if function not in traces:
                logging.info(f"[SCHEMATIC] No trace found for function {function}, generating fake traces")
                from ScEpTIC.AST.misc.trace import FakeTraceGenerator

                traces[function] = FakeTraceGenerator(function, cfg).generate_fake_traces()
            self.analyse_function(function, cfg, traces[function])
            cfg.check_cfg_analysis()
            # Update basic blocks in CFGs calling the function analysed
            checkpoint_in_function = not cfg.has_only_disabled_checkpoints()
            for pred in function_depgraph.predecessors(function):
                dependencies = function_depgraph[pred][function]["dependencies"]
                for dependency in dependencies:
                    self.update_function_basic_blocks(cfgs[pred], cfg, dependency, checkpoint_in_function)
