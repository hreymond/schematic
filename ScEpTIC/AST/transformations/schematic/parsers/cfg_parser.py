from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import Checkpoint
from ScEpTIC.AST.transformations.schematic.normalizations.labels_normalizer import LabelsNormalizer


def asts_to_cfgs(parser):
    """
    Convert the ScEpTIC asts in nx.Digraph CFGs

    :param parser: The parser developed for the Alfred transformation
    :type parser: ASTParser

    :return: A dict containing the cfgs of the different functions indexed by function name
    :rtype: dict
    """
    cfgs = {}
    for name, function_ast in parser.basic_blocks.items():
        cfgs[name] = parse_function(name, function_ast)
    return cfgs


def synchronise_cfg_to_ast(cfgs, parser):
    """
    Update the ast in parser with the modified basic blocks from the cfg

    :param cfgs: A dict containing all the cfgs
    :param parser: The Alfred ast parser
    :type parser: ASTParser
    """
    for function, cfg in cfgs.items():
        parser.basic_blocks[function] = {}
        for bb in cfg.nodes:
            parser.basic_blocks[function][bb.label] = bb


bb_index = 1


def generate_unique_bb_id():
    """
    Generate an unique basic block id

    Based on the fact that the labelization of basic blocs done in LabelsNormalizer follow a predictable pattern
    which is:
    The first bb label is 0 and the next is old_label + LabelsNormalizer.LABELS_DISTANCE
    Thus, LabelsNormalizer creates the following sequence of ids [0, 20, 40, 60, ...]
    We use the same principle to create the sequence [1, 21, 31, 41]
    """
    global bb_index
    bb_index += LabelsNormalizer.LABELS_DISTANCE
    return bb_index


def parse_function(name, ast):
    """
    Transforms the ast of a function into a CFG

    :return CFG: returns the CFG corresponding to the AST
    """
    # Create the CFG and add the basic blocks to it
    cfg = CFG()
    cfg.name = name
    cfg.add_nodes_from(ast.values())
    # Add a start node with the label START_<funcname>
    start_node = BasicBlock(generate_unique_bb_id(), name)
    start_node.llvm_label = "START_" + name[1:]
    cfg.first_bb = start_node
    cfg.add_node(start_node)

    end_node = BasicBlock(generate_unique_bb_id(), name)
    end_node.llvm_label = "END_" + name[1:]
    cfg.last_bb = end_node
    cfg.add_node(end_node)

    for bb in ast.values():
        for successor_id in bb.next:
            cfg.add_edge(bb, ast[successor_id], checkpoint=Checkpoint(bb, ast[successor_id]))
        # Hacky way to determine start bb and end bb of a function
        if len(bb.prev) == 0:
            cfg.add_edge(start_node, bb, checkpoint=Checkpoint(start_node, bb))

        if len(bb.next) == 0:
            cfg.add_edge(bb, end_node, checkpoint=Checkpoint(bb, end_node))

    return cfg
