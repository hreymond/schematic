import copy

from ScEpTIC.AST.elements.value import Value
from ScEpTIC.AST.elements.instructions.memory_operations import AllocaOperation, StoreOperation, LoadOperation
from ScEpTIC.exceptions import NotImplementedException
from ScEpTIC.tools import flatten_multilevel_list


class MemoryTagsParser:
    """
    A memory tag is a virtual representation of an accessed memory location, which captures also the access pattern.
    A memory tag is associated to each LoadOperation and StoreOperation.
    This class contains a collection of methods for parsing the AST and identifying the memory tag of each operation.
    """

    # LLVM default data
    ALIGN = 4
    VOLATILE = False

    def __init__(self, ast_parser):
        self.parser = ast_parser
        self.memory_tag_chains = {}

    def resolve_memory_tags(self):
        """
        Populates the memory tags of the instructions in each function
        """
        for name in self.parser.get_function_names():
            self.memory_tag_chains[name] = {}
            self._resolve_memory_tags(name)

    def _resolve_memory_tags(self, name):
        """
        Populates the memory tags of the instructions of a given function
        :param name: name of the function / sub-routine to target
        """
        elements = {}
        first_alloca = True

        basic_blocks = self.parser.get_flatten_ast(name)

        for basic_block in basic_blocks:
            for instruction in basic_block.instructions:
                # If instruction has a target virtual register, keep track of it to resolve memory tags
                if "target" in dir(instruction) and not instruction.target is None and not instruction._omit_target:
                    target_reg = instruction.target.value
                    elements[target_reg] = instruction

                # Fix for LLVM 6/7/8 first alloca
                if isinstance(instruction, AllocaOperation):
                    # Set first Alloca
                    instruction.is_first = first_alloca
                    first_alloca = False

                # Resolve the memory tag of each LoadOperation and StoreOperation
                if isinstance(instruction, LoadOperation) or isinstance(instruction, StoreOperation):
                    tag = instruction.resolve_memory_tag(elements)
                    dependency = instruction.resolve_memory_tag_dependency(elements)

                    self._create_memory_tag_operation_chain(name, tag, instruction, elements)

    def _create_memory_tag_operation_chain(self, name, memory_tag, instruction, elements):
        """
        Retrieves the sequence of instructions required to create dummy writes.
        """
        if (
            not isinstance(instruction, StoreOperation) and not isinstance(instruction, LoadOperation)
        ) or memory_tag in self.memory_tag_chains[name]:
            return

        chain = instruction.resolve_memory_address_chain(elements)
        chain = copy.deepcopy(chain)
        flat_chain = flatten_multilevel_list(chain)
        # inverse list (correct order of execution)
        flat_chain.reverse()

        # the last element of the chain is only the target of a custom operation, not the operation itself
        last_op = flat_chain[-1]

        if isinstance(last_op, StoreOperation):
            flat_chain[-1] = last_op.target
            element_type = last_op.value.type

        else:
            flat_chain[-1] = last_op.element
            element_type = last_op.type

        register_names = set()
        alloca_regs = []

        # get used register names and isolate registers that target alloca operations, as they are the ones corresponding to a fixed address
        for element in flat_chain:
            # reset label
            if "label" in dir(element):
                element.label = None

            if isinstance(element, AllocaOperation):
                alloca_regs.append(element.target.value)

            else:
                for reg_name in element.get_uses():
                    register_names.add(reg_name)

                for reg_name in element.get_defs():
                    register_names.add(reg_name)

        # remove registers
        for alloca_reg in alloca_regs:
            register_names.remove(alloca_reg)

        # remove alloca operations from the chain
        flat_chain = [element for element in flat_chain if not isinstance(element, AllocaOperation)]

        # replace register names
        for element in flat_chain:
            for reg_name in register_names:
                new_reg_name = reg_name.replace("%", "%DUMMY-WRITE-REG-")
                element.replace_reg_name(reg_name, new_reg_name)

        flat_chain[-1] = {
            "element": flat_chain[-1],
            "type": element_type,
            "memory_tag": last_op.memory_tag,
            "memory_tag_dependency": last_op.memory_tag_dependency,
        }

        self.memory_tag_chains[name][memory_tag] = flat_chain

    def _get_chain(self, name, memory_tag):
        """
        Returns a copy of the instructions required to get the address of a given memory tag
        """
        if name not in self.memory_tag_chains or memory_tag not in self.memory_tag_chains[name]:
            raise NotImplementedException(f"No chain for {memory_tag} in function {name}")

        return copy.deepcopy(self.memory_tag_chains[name][memory_tag])

    def create_dummy_write(self, name, memory_tag):
        chain = self._get_chain(name, memory_tag)

        element_type = chain[-1]["type"]
        source_register = chain[-1]["element"]
        target_register = Value("virtual_reg", "%DUMMY-WRITE-MAIN-REG", None)

        load_instruction = LoadOperation(target_register, element_type, source_register, self.ALIGN, self.VOLATILE)
        load_instruction.memory_tag = chain[-1]["memory_tag"]
        load_instruction.memory_tag_dependency = chain[-1]["memory_tag_dependency"]

        source_register = copy.deepcopy(source_register)
        target_register = copy.deepcopy(target_register)
        target_register.type = copy.deepcopy(element_type)
        store_instruction = StoreOperation(source_register, target_register, self.ALIGN, self.VOLATILE)
        store_instruction.memory_tag = copy.deepcopy(load_instruction.memory_tag)
        store_instruction.memory_tag_dependency = copy.deepcopy(load_instruction.memory_tag_dependency)

        chain[-1] = load_instruction
        chain.append(store_instruction)

        return chain
