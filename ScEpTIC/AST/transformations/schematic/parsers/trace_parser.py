import json
import logging
import sys
from typing import List, Dict

from ScEpTIC.AST.misc.trace import *


def process_traces(traces: List[Dict], cfg, bbs, append_start=True):
    """
    Process traces of a given function

    :param List[Dict] traces: A dict containing a list of basic block llvm label for the key `trace` and the number
        of time the trace has been seen in the execution for the key `nb_execution`. An example:
        { "trace": ["%0", "%8", "%13"], nb_execution: 2 }
    :param CFG cfg: The cfg of the function
    :param bool append_start: Should start and end label be added to the input traces ?
    :return: A list of traces ordered by the most executed first
    :rtype List[List[BasicBlock]]:
    """
    try:
        traces.sort(key=lambda val: val["nb_execution"], reverse=True)
    except KeyError:
        logging.critical("[SCHEMATIC] Malformed trace: All traces must have a 'nb_execution' attribute")
        sys.exit(1)

    traces_bb = {}

    # Convert every llvm_label trace into BasicBlock trace, and automatically add the splitted basic blocks
    for trace in traces:
        trace_bb = label_array_to_basic_blocks(trace["trace"], cfg, bbs, append_start)
        h = hash(tuple(trace_bb))
        traces_bb[h] = Trace(h, trace_bb, trace["nb_execution"])
    return traces_bb


def process_loop(loop, cfg, bbs):
    """
    Parses a loop trace to create a LoopTrace object with basic block traces and LoopInfo

    :param Dict[str, Dict or List] loop: The loop trace dictionnary (e.g : { 'loop': { 'header': '%5', 'basic blocks': [ %5, %8, ....] ..},
        'traces' = [['%5', '%8'], [...], ... ]
    :param CFG cfg: cfg of the function containing the loop
    :param Dict[str, BasicBlock] bbs: Basic blocks indexed by their llvm_label

    :rtype LoopTraces:
    """
    l_trace = LoopTraces()

    label_loop = LLVMNaturalLoop(**loop["loop"])
    header = label_array_to_basic_blocks([label_loop.header], cfg, bbs, False)
    basic_blocks = label_array_to_basic_blocks(label_loop.basic_blocks, cfg, bbs, False)
    exiting = label_array_to_basic_blocks(label_loop.exiting, cfg, bbs, False)
    latch = label_array_to_basic_blocks(label_loop.latch, cfg, bbs, False)
    bb_loop = LLVMNaturalLoopBB(header, exiting, basic_blocks, latch, label_loop.depth, None)
    l_trace.loop = bb_loop
    l_trace.traces = process_traces(loop["traces"], cfg, bbs, False)
    return l_trace


def parse_trace_file(trace_file: str, cfgs):
    """
    Parses the trace file given in parameter and return the traces parsed
    """
    logging.info("[SCHEMATIC] Loading traces")

    # Read trace file
    f = open(trace_file, "r")
    traces_json = json.loads(f.read())

    for name in traces_json:
        if name not in cfgs:
            logging.critical(
                "[SCHEMATIC] Generated traces don't match the analysed program:"
                " function {} has not been found in the llvm source file".format(name)
            )
            sys.exit(1)
    # Dict containing the traces in basic block format, indexed by function name
    ouput_traces: Dict[str, FunctionTraces] = {}
    # Analyse each function trace found in the trace file
    for function, traces in traces_json.items():
        # The following line generate a dict containing all the bb indexed by their llvm block id
        # For example: { "%0": BB0, "%7": BB7, "%12": BB12 }
        bbs = {"%" + str(node.llvm_label): node for node in cfgs[function].nodes}

        # Create a FunctionTrace object to store the traces of the current function
        f_trace = FunctionTraces()
        # Parse function traces
        f_trace.traces = process_traces(traces["traces"], cfgs[function], bbs)
        # Parse loop traces
        f_trace.loop_traces = {}
        if "loop_traces" in traces:
            # Process each loop traces for each loop in the function
            for header, loop in traces["loop_traces"].items():
                f_trace.loop_traces[header] = process_loop(loop, cfgs[function], bbs)
        ouput_traces[function] = f_trace
    f.close()
    return ouput_traces
