from ScEpTIC.exceptions import ASTParsingException
from ScEpTIC.AST.transformations.schematic.elements.conditional_block import ConditionalBlock
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.elements.instructions.termination_instructions import BranchOperation


class ConditionalParser:
    """
    Module for parsing basic blocks into conditionals
    """

    force_path_fill = False

    def __init__(self, basic_blocks, function_name):
        self.basic_blocks = basic_blocks
        self.function_name = function_name

    def _identify_exit_block_label(self):
        """
        Returns the label of the first exit block from the root of the AST
        """

        labels = sorted(list(self.basic_blocks.keys()))

        for label in labels:
            basic_block = self.basic_blocks[label]
            previous_labels = basic_block.get_previous_labels()

            if len(previous_labels) > 1:
                for previous_label in previous_labels:
                    # jump from loop latch
                    if label < previous_label:
                        break

                # normal exit from loop -> no jump from loop latch
                else:
                    return label

        return None

    def _identify_entry_block_label(self, exit_block_label):
        """
        Returns the label of the last entry block in the path from the root node of the AST to specified exit block
        """
        labels = sorted(list(self.basic_blocks.keys()))
        exit_label_index = labels.index(exit_block_label)

        for label in reversed(labels[:exit_label_index]):
            # Entry block is the last block with a bifurcation
            if len(self.basic_blocks[label].get_next_labels()) > 1:
                return label

        return None

    def _get_basic_blocks_in_path(self, start_label, end_label):
        """
        Retrieves the basic block that belongs to a given path, which starts from start_label and ends in end_label
        """

        if start_label not in self.basic_blocks:
            from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock

            BasicBlock.str_print_instructions = True
            for element in self.basic_blocks.values():
                print(element)
            raise ASTParsingException(f"Start label {start_label} not found!")

        if end_label not in self.basic_blocks:
            raise ASTParsingException(f"End label {end_label} not found!")

        if start_label == end_label:
            return []

        basic_block = self.basic_blocks[start_label]
        path = [basic_block]

        while len(basic_block.get_next_labels()) == 1 and end_label not in basic_block.get_next_labels():
            next_labels = basic_block.get_next_labels()

            if len(next_labels) != 1:
                raise ASTParsingException(f"Basic block has multiple/empty next labels: {next_labels}")

            try:
                basic_block = self.basic_blocks[next_labels[0]]
                path.append(basic_block)
            except KeyError:
                raise ASTParsingException(f"Label {next_labels[0]} not found while parsing basic blocks AST!")

        return path

    def _identify_paths(self, entry_label, exit_label):
        """
        Identifies the paths of the conditional execution
        """
        entry_next_labels = self.basic_blocks[entry_label].get_next_labels()

        if len(entry_next_labels) != 2:
            raise ASTParsingException(
                f"Invalid bifurcation found for entry/exit blocks pair ({entry_label}, {exit_label})"
            )

        paths = []

        for label in entry_next_labels:
            paths.append(self._get_basic_blocks_in_path(label, exit_label))

        return paths

    def identify_conditionals(self):
        """
        Iterates over the basic blocks and builds all the conditionals objects.
        """

        while True:
            conditional = self.identify_conditional()

            # no conditional found -> exit
            if conditional is None:
                break

            self.adjust_conditional_basic_blocks(conditional)

    def identify_conditional(self):
        """
        Identifies and builds a conditional object
        """
        exit_label = self._identify_exit_block_label()

        if exit_label is None:
            return None

        entry_label = self._identify_entry_block_label(exit_label)

        if entry_label is None:
            return None

        entry_block = self.basic_blocks[entry_label]
        exit_block = self.basic_blocks[exit_label]
        paths = self._identify_paths(entry_label, exit_label)

        if self.force_path_fill:
            for i in range(2):
                if len(paths[i]) == 0:
                    custom_basic_block = self._generate_custom_path(entry_block, exit_block)
                    custom_basic_block.is_path_fill = True
                    paths[i].append(custom_basic_block)
                    entry_block.next.remove(exit_block.label)
                    entry_block.next.append(custom_basic_block.label)
                    exit_block.prev.remove(entry_block.label)
                    exit_block.prev.append(custom_basic_block.label)

        conditional_block = ConditionalBlock(entry_block, exit_block, paths, self.function_name)

        return conditional_block

    def _generate_custom_path(self, entry_block, exit_block):
        # generate labels
        entry_block_label = entry_block.instructions[0].label
        exit_block_label = exit_block.instructions[0].label
        custom_branch_label = f"{entry_block_label[0]}{int(entry_block_label[1:]) + 1}"

        # create custom basic block
        custom_basic_block = BasicBlock(BasicBlock.label_to_int(custom_branch_label), self.function_name)
        custom_basic_block.add_prev_label(entry_block_label)
        custom_basic_block.add_next_label(exit_block_label)

        # create custom branch operation
        custom_branch = BranchOperation(None, exit_block_label, None)
        custom_branch.tick_count = 0
        custom_branch.basic_block_id = custom_branch_label
        custom_branch.label = custom_branch_label

        # add to custom basic block
        custom_basic_block.instructions.append(custom_branch)

        # change target of the branch in the entry block (needs to target the custom basic block)
        entry_branch_instruction = entry_block.instructions[-1]

        if entry_branch_instruction.target_true == exit_block_label:
            entry_branch_instruction.target_true = custom_branch_label

        elif entry_branch_instruction.target_false == exit_block_label:
            entry_branch_instruction.target_false = custom_branch_label

        return custom_basic_block

    def adjust_conditional_basic_blocks(self, conditional):
        """
        Removes the loop basic blocks and inserts the conditional object in the list of basic blocks
        """

        # remove basic blocks
        for label in conditional.get_labels():
            if label in self.basic_blocks:
                del self.basic_blocks[label]

        # add loop as single basic block
        self.basic_blocks[conditional.get_label()] = conditional
