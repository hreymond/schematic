from .conditional_parser import ConditionalParser

from ScEpTIC.AST.transformations.schematic.elements.loop_block import LoopBlock


class LoopParser:
    """
    Module for parsing basic blocks into loops
    """

    def __init__(self, basic_blocks, function_name):
        self.basic_blocks = basic_blocks
        self.function_name = function_name

    def identify_loops(self):
        """
        Iterates over the basic blocks and builds all the loop objects.
        """

        while True:
            loop = self.identify_loop()

            # no loop found -> exit
            if loop is None:
                break

            for section, loop_section in loop.blocks.items():
                conditional_parser = ConditionalParser(loop_section, self.function_name)
                conditional_parser.identify_conditionals()

            self.adjust_loop_basic_blocks(loop)

    def identify_loop(self):
        """
        Identifies and builds a loop object
        """

        for label, basic_block in self.basic_blocks.items():

            # Identify loop from latch
            if basic_block.has_backward_jump():
                loop = self.extract_loop(label)

                if loop is not None:
                    return loop

        return None

    def extract_loop(self, latch_label):
        """
        Builds a loop object
        """
        labels = sorted(list(self.basic_blocks.keys()))

        loop = LoopBlock(self.function_name)

        latch_bb = self.basic_blocks[latch_label]
        loop.add_basic_block("latch", latch_bb)

        # natural loop form -> a basic block that jumps back, jumps to the loop header
        header_label = latch_bb.next[0]

        if header_label not in labels:
            LoopBlock.all_elements[self.function_name].remove(loop)
            return None

        header_label_start = labels.index(header_label)

        for header_label in labels[header_label_start:]:
            loop_header = self.basic_blocks[header_label]
            loop.add_basic_block("header", loop_header)

            if loop_header.jumps_after(latch_label):
                break

        body_label_start = labels.index(min(loop_header.get_next_labels()))

        for body_label in labels[body_label_start:]:
            if body_label == latch_label:
                break

            loop_body = self.basic_blocks[body_label]
            loop.add_basic_block("body", loop_body)

        return loop

    def adjust_loop_basic_blocks(self, loop):
        """
        Removes the loop basic blocks and inserts the loop object in the list of basic blocks
        """

        # remove basic blocks
        for label in loop.get_labels():
            if label in self.basic_blocks:
                del self.basic_blocks[label]

        # add loop as single basic block
        self.basic_blocks[loop.get_label()] = loop
