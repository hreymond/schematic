from ScEpTIC.AST.transformations.schematic.normalizations.labels_normalizer import LabelsNormalizer
from ScEpTIC.AST.transformations.schematic.parsers.elements.basic_blocks_parser import BasicBlocksParser
from ScEpTIC.AST.transformations.schematic.parsers.elements.loop_parser import LoopParser
from ScEpTIC.AST.transformations.schematic.parsers.elements.conditional_parser import ConditionalParser

from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock


class ASTParser:
    """
    Transforms the AST into a sequence of basic blocks, where control-flow statements are represented by dedicated objects.
    """

    def __init__(self, functions):
        self.functions = functions
        self.basic_blocks = {}

    def parse(self):
        """
        Parses the AST
        """

        for name, function in self.functions.items():

            # Skip builtins processing
            if function.is_builtin or function.is_input:
                continue

            # parse AST into basic blocks
            bb_parser = BasicBlocksParser(function, name)
            basic_blocks = bb_parser.parse_ast()

            self.parse_ast_structures(name, basic_blocks)

            self.basic_blocks[name] = basic_blocks

    def parse_ast_structures(self, name, basic_blocks, parse_loops=True):
        """
        Parses the basic blocks into ConditionalBlock and LoopBlock
        If reparse is set to True, the lists of ConditionalBlock and LoopBlock are reset
        """

        # parse loops
        if parse_loops:
            loop_parser = LoopParser(basic_blocks, name)
            loop_parser.identify_loops()

        # parse conditionals
        conditional_parser = ConditionalParser(basic_blocks, name)
        conditional_parser.identify_conditionals()

    def reparse(self):
        """
        Reparses the AST to merge basic blocks
        """
        for name in self.get_function_names():
            self._reparse(name)

    def _reparse(self, name):
        """
        Reparses the AST of the given function to merge basic blocks
        """
        prev_bb = None

        # flatten the entire AST into sequences of basic blocks
        basic_blocks = self.get_flatten_ast(name)
        to_remove = []

        for basic_block in basic_blocks:
            basic_block.reset_conditional_structure()

            if prev_bb is not None and prev_bb.merge_in_subsequent:
                # reposition label
                label = basic_block.instructions[0].label
                basic_block.instructions[0].label = None

                # id of the basic block
                basic_block_id = basic_block.instructions[0].basic_block_id

                for num, instruction in enumerate(prev_bb.instructions):
                    instruction.basic_block_id = basic_block_id
                    basic_block.instructions.insert(num, instruction)

                # reposition label
                basic_block.instructions[0].label = label

                to_remove.append(prev_bb)

                prev_bb = basic_block

            elif basic_block.merge_in_previous:
                # accounts for the last branch instruction of the basic block
                index = len(prev_bb.instructions) - 1

                # id of the basic block
                basic_block_id = prev_bb.instructions[0].basic_block_id

                # insert instructions in previous basic block
                for num, instruction in enumerate(basic_block.instructions):
                    instruction.basic_block_id = basic_block_id
                    instruction_index = index + num
                    prev_bb.instructions.insert(instruction_index, instruction)

                # remove merged basic block from basic blocks list
                to_remove.append(basic_block)

            else:
                prev_bb = basic_block

        if prev_bb is not None and prev_bb.merge_in_subsequent:
            print(name)
            print(prev_bb)
            print()
            print()
            self.print_ast(name, True)
            raise Exception("Unable to merge basic block in subsequent basic block: no subsequent bb found!")

        # remove merged basic blocks
        for basic_block in to_remove:
            BasicBlock.all_elements["custom"].remove(basic_block)
            basic_blocks.remove(basic_block)

        self.basic_blocks[name] = {bb.get_label(): bb for bb in basic_blocks}

    def normalize_ast_labels(self):
        """
        Normalizes all the labels in the AST
        """
        for name, basic_blocks in self.basic_blocks.items():
            data = LabelsNormalizer.normalize_labels([x for x in basic_blocks.values()])
            self.basic_blocks[name] = data

    def print_ast(self, name=None, print_instructions=False):
        """
        Prints the parsed AST
        :param name: name of the function to display. If empty, it prints out the AST of all the functions
        """

        old_value = BasicBlock.str_print_instructions
        BasicBlock.str_print_instructions = print_instructions

        if name is not None:
            self._print_ast(name)

        else:
            for name in self.basic_blocks:
                self._print_ast(name)

        BasicBlock.str_print_instructions = old_value

    def _print_ast(self, name):
        """
        Prints the parsed AST of a specific function
        """
        print(f"{name}")

        for _, basic_block in sorted(self.basic_blocks[name].items()):
            print(basic_block)

        print()

    def get_ast(self, name):
        """
        Returns the AST of the given function
        """

        return self.basic_blocks[name]

    def get_flatten_ast(self, name=None):
        """
        Returns the flatten AST of the program
        """

        if name is None:
            flatten = {}

            for name in self.basic_blocks:
                flatten[name] = self._flatten_ast(name)

            return flatten

        else:
            return self._flatten_ast(name)

    def _flatten_ast(self, name):
        """
        Returns the flatten AST of a specific function
        """

        data = []

        for _, basic_block in sorted(self.basic_blocks[name].items()):
            for element in basic_block.flatten():
                data.append(element)

        return data

    def get_function_names(self):
        """
        Returns the names of all the functions of the program
        """
        return [name for name, function in self.functions.items() if not function.is_input and not function.is_builtin]

    def syncrhonize_ast_to_functions(self, print_diff=False):
        """
        Synchronizes the AST of each function to its body
        """
        for name in self.basic_blocks.keys():
            self._synchronize_ast_to_function(name, print_diff)

    def _synchronize_ast_to_function(self, name, print_diff=False):
        """
        Synchronizes the AST to the function body
        """
        function = self.functions[name]

        # ignore builtins and input functions
        if function.is_builtin or function.is_input:
            return

        instructions = []

        for _, basic_block in sorted(self.basic_blocks[name].items()):
            for instruction in basic_block.instructions:
                instructions.append(instruction)

        # prints diff
        if print_diff:
            old_instructions = function.body

            old_i = 0

            print(f"{name} diff:")

            for i in range(len(instructions)):
                new_class = instructions[i].__class__.__name__
                try:
                    old_class = old_instructions[old_i].__class__.__name__
                except:
                    old_class = None

                if new_class != old_class:
                    print(f"  [+] {instructions[i]}")
                else:
                    old_i += 1

        # set function body
        function.update_body(instructions)
