"""
Handles modification on the CFG during the checkpoint placement and memory allocation pass
"""
import logging
from collections import deque

import networkx as nx

from ScEpTIC.AST.transformations.schematic.elements.checkpoint import CheckpointTypeEnum, Checkpoint
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import BranchOperation
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.parsers.cfg_parser import generate_unique_bb_id


def add_branch(bb1, bb2):
    """
    Add a branch always instruction at the end of the basic block bb1 targeting the basic block bb2.

    :param BasicBlock bb1: The basic block the instruction is added to
    :param BasicBlock bb2: The target of the branch instruction
    """
    branch = BranchOperation(None, "%" + str(bb2.label), None)
    if len(bb2.instructions) != 0:
        bb2.instructions[0].label = "%" + str(bb2.label)
    else:
        logging.warning("[SCHEMATIC] The basic block {} in {} has no instructions".format(bb1.llvm_label, bb2.function_name))
    bb1.instructions.append(branch)


def add_free_branch(bb1, bb2):
    """
    Add a branch always instruction at the end of the basic block bb1 targeting the basic block bb2.

    The branch instruction does not consume any energy (tick_count=0)

    :param BasicBlock bb1: The basic block the instruction is added to
    :param BasicBlock bb2: The target of the branch instruction
    """
    add_branch(bb1, bb2)
    bb1.instructions[-1].tick_count = 0


def create_bb_for_function(cfg: CFG, bb: BasicBlock, i: int):
    """
    Split a basic block in 2 basic blocs and insert a virtual basic block function_entry et function_exit.
    Used to isolate function call in a basic bloc.
    The call instruction (`CallOperation`) to isolate is located by its index
    in the instruction list of the basic block `i`

    :param CFG cfg: The cfg to modify to isolate the function call
    :param BasicBlock bb: The basic block containing the function call
    :param int i: The instruction index in the basic block instruction list
    :return: Returns a tuple with all the basic blocks created in the following order: [function_entry,
        function_exit and the second part of the split basic block
    :rtype: (BasicBlock, BasicBlock, BasicBlock)
    """
    call = bb.instructions[i]

    # Generate the basic block that will contain the instructions after the call instruction
    bb_label = generate_unique_bb_id()
    next_bb = BasicBlock(bb_label, bb.function_name)
    next_bb.instructions = bb.instructions[i + 1 :]
    next_bb.llvm_label = str(bb.llvm_label) + "_" + str(i)
    next_bb.original_bb = bb.original_bb if bb.original_bb else bb
    # Update the current basic block to only contain instructions before the call instruction
    bb.instructions[0].label = None
    bb.instructions = bb.instructions[0:i]
    # next_bb.next = list(bb.next)
    # bb.next = []

    # Generate function entry/exit basic blocks
    func_entry = BasicBlock(generate_unique_bb_id(), bb.function_name)
    func_entry.is_function_call = True
    func_entry.llvm_label = "{}_".format(bb.llvm_label) + call.name + "_entry"
    func_entry.instructions = [call]
    func_entry.original_bb = bb.original_bb if bb.original_bb else bb

    func_exit = BasicBlock(generate_unique_bb_id(), bb.function_name)
    func_exit.is_function_call = True
    func_exit.llvm_label = "{}_".format(bb.llvm_label) + call.name + "_exit"
    func_exit.original_bb = bb.original_bb if bb.original_bb else bb

    # Move the `bb` basic block neighbors to `next_bb` (second half of the original bb split)
    for neighbor in list(cfg.neighbors(bb)):
        chkpt = cfg[bb][neighbor]["checkpoint"]
        if not chkpt:
            raise Exception(
                "A CFG arc does not have a checkpoint: {} -> {} in {}".format(
                    bb.llvm_label, neighbor.llvm_label, bb.function_name
                )
            )
        cfg.remove_edge(bb, neighbor)
        cfg.add_edge(next_bb, neighbor, checkpoint=chkpt)
        chkpt.bb_before = next_bb

    cfg.add_node(func_entry)
    cfg.add_node(func_exit)
    cfg.add_node(next_bb)

    add_free_branch(func_exit, next_bb)
    cfg.add_edge(func_exit, next_bb, checkpoint=Checkpoint(func_exit, next_bb))

    add_free_branch(func_entry, func_exit)
    cfg.add_edge(func_entry, func_exit, checkpoint=Checkpoint(func_entry, func_exit))

    add_free_branch(bb, func_entry)
    cfg.add_edge(bb, func_entry, checkpoint=Checkpoint(bb, func_entry))

    bb.is_splitted = True
    bb.instructions[0].label = "%" + str(bb.label)
    return func_entry, func_exit, next_bb


def isolate_function_calls(cfgs, ignore_list=()):
    """
    Isolate functions call in basic blocks

    Iterate over the basic blocks in the given CFGs. Each time a function call is found in a basic block,
    the function call is isolated by creating two new basic blocks `function_entry` and `function_exit` and splitting
    the old basic block in two parts.

    :param Dict[str, CFG] cfgs: The cfgs to analyse
    :param List[str] ignore_list: List of functions to ignore
    :return nx.Digraph: Returns the function dependency graph. The attribute "dependencies" on each arc is list containing
        tuples representing the basic block to update after the function has been analysed

    See also: ..function: `~create_bb_for_function`
    """
    function_graph = nx.DiGraph()
    function_graph.add_nodes_from(list(cfgs.keys()))
    for function, cfg in cfgs.items():
        # Create a stack with all the basic block to analyse
        to_analyse = deque(list(cfg.nodes))
        while to_analyse:
            bb = to_analyse.pop()
            i = 0
            while i < len(bb.instructions):
                if (
                    isinstance(bb.instructions[i], CallOperation)
                    and bb.instructions[i].name not in bb.instructions[i].ignore
                    and bb.instructions[i].name not in ignore_list
                ):
                    callee = bb.instructions[i].name
                    func_entry, func_exit, next_bb = create_bb_for_function(cfg, bb, i)
                    if not function_graph.has_edge(function, callee):
                        function_graph.add_edge(function, callee, dependencies=[(func_entry, func_exit)])
                    else:
                        function_graph[function][callee]["dependencies"].append((func_entry, func_exit))
                    to_analyse.append(next_bb)
                i += 1
    return function_graph


def update_checkpoint_type(checkpoints, path):
    """
    Activate or disable checkpoints encountered on the trace, given the shortest path in
    the reachable checkpoint graph.

    :param checkpoints: The checkpoints on the trace analysed
    :param path: The checkpoints to be activated (i.e. the shortest path in the RCB)
    """
    for checkpoint in checkpoints:
        if checkpoint not in path:
            checkpoint.type = CheckpointTypeEnum.DISABLED
        else:
            checkpoint.type = CheckpointTypeEnum.ACTIVE


def propagate_energy_to_leave(chkpt_start: Checkpoint, energy_to_leave_start: float, cfg: CFG):
    """
    Propagate the energy to leave before basic bloc execution through the whole CFG

    Exploration of the CFG where the arcs are reversed.
    It starts from `bb_start` and stops at each non-disabled checkpoints (active or potential).
    Updates the energy to leave for a basic bloc execution if the new energy to leave is superior to the previous energy
    to leave.

    :param bb_start: The basic block from which the energy_to_leave propagation is done
    :type bb_start: BasicBlock
    :param energy_to_leave_start: The energy to leave before `bb_start`
    :type energy_to_leave_start: float
    :param CFG cfg: The CFG in which we want to propagate the energy
    """
    loops = set()
    loops.clear()
    max_energy_to_leave = {chkpt_start: energy_to_leave_start}
    visited = set()

    # If the start basic block is in a loop, the energy to leave already take into account the loop execution cost
    if chkpt_start.bb_before and chkpt_start.bb_before.loop:
        loops.add(chkpt_start.bb_before.loop.loop_info.header[0])

    # Creation of checkpoint graph
    # We first begin by creating a graph of disabled checkpoints reachable from chkpt_start
    # The graph is acyclic since we do not consider checkpoints already visited
    to_visit = deque([chkpt_start])
    chkpt_graph = nx.DiGraph()
    chkpt_graph.add_node(chkpt_start)
    visited.clear()
    while len(to_visit):
        chkpt = to_visit.pop()
        visited.add(chkpt)
        bb_before = chkpt.bb_before
        if bb_before is None:
            continue

        # Check for previous disabled checkpoint not already visited
        for child in cfg.predecessors(bb_before):
            chkpt_child = cfg[child][bb_before]["checkpoint"]
            if chkpt_child in visited or chkpt_child.type != CheckpointTypeEnum.DISABLED:
                continue
            chkpt_graph.add_node(chkpt_child)
            chkpt_graph.add_edge(chkpt, chkpt_child)
            if chkpt_child not in to_visit:
                to_visit.append(chkpt_child)

    # Once the graph built, we look for the longest path in this acyclic graph
    # https://en.wikipedia.org/wiki/Longest_path_problem
    visited.clear()
    for chkpt in nx.topological_sort(chkpt_graph):
        energy_to_leave = max_energy_to_leave[chkpt]
        del max_energy_to_leave[chkpt]

        # Check for previous basic blocks
        bb_before: BasicBlock = chkpt.bb_before
        if bb_before == None:
            continue

        # If the basic block is part of a loop, take into account loop cost
        if bb_before.loop and bb_before.loop.loop_info.header[0] not in loops:
            loops.add(bb_before.loop.loop_info.header[0])
            energy_to_leave += (bb_before.loop.nb_iter - 1) * bb_before.loop.cost_one_it

        if bb_before.final_cost is not None:
            energy_to_leave += bb_before.final_cost
            if bb_before.energy_to_leave is None or energy_to_leave > bb_before.energy_to_leave:
                bb_before.energy_to_leave = energy_to_leave
        else:
            raise Exception(
                f"The final_cost of basic block {bb_before.label} in {bb_before.function_name} is None,"
                f"maybe memory allocation has not been applied yet ?"
            )

        # Check for previous disabled checkpoint not already visited
        for child in cfg.predecessors(bb_before):
            chkpt_child = cfg[child][bb_before]["checkpoint"]
            if chkpt_child in visited or chkpt_child.type != CheckpointTypeEnum.DISABLED:
                continue
            # Update the checkpoint energy to leave if necessary
            if chkpt_child not in max_energy_to_leave or energy_to_leave > max_energy_to_leave[chkpt_child]:
                max_energy_to_leave[chkpt_child] = energy_to_leave


def propagate_energy_left(chkpt_start: Checkpoint, energy_left_start: float, cfg: CFG):
    """
    Propagate the minimal energy left after each basic bloc execution through the whole CFG

    Exploration of the CFG starting from `bb_start` and stopping at each non-disabled checkpoints (active or potential).
    Updates the energy left after a basic bloc execution if the new energy left is inferior to the previous energy
    left.

    :param bb_start: The basic block from which the energy_left propagation is done
    :type bb_start: BasicBlock
    :param energy_left_start: The energy left before `bb_start`
    :type energy_left_start: float
    :param cfg: The CFG in which we want to propagate the energy
    :type cfg: networkx.Digraph
    """
    loops = set()
    chkpt_cost = {chkpt_start: 0}
    visited = set()

    # If the start basic block is in a loop, the energy to leave already take into account the loop execution cost
    if chkpt_start.bb_after and chkpt_start.bb_after.loop:
        loops.add(chkpt_start.bb_after.loop.loop_info.header[0])

    while len(chkpt_cost):
        # Select the chkpt with the highest energy left
        chkpt = max(chkpt_cost, key=chkpt_cost.get)
        cost = chkpt_cost[chkpt]

        visited.add(chkpt)
        del chkpt_cost[chkpt]

        # Check for next basic blocks
        bb_after: BasicBlock = chkpt.bb_after
        if bb_after == None:
            continue

        # If the basic block is part of a loop, take into account loop cost
        if bb_after.loop and bb_after.loop.loop_info.header[0] not in loops:
            loops.add(bb_after.loop.loop_info.header[0])
            cost += (bb_after.loop.nb_iter - 1) * bb_after.loop.cost_one_it

        # Set the energy left of the basic block after the chkpt
        if bb_after.final_cost is not None:
            cost += bb_after.final_cost
            energy_left = energy_left_start - cost
            if bb_after.energy_left is None or energy_left < bb_after.energy_left:
                bb_after.energy_left = energy_left
        else:
            raise Exception(
                f"The final_cost of basic block {bb_after.label} in {bb_after.function_name} is None,"
                f"maybe memory allocation has not been applied yet ?"
            )

        # Update next basic blocks energy to left
        for child in cfg.neighbors(bb_after):
            chkpt_child = cfg[bb_after][child]["checkpoint"]
            if chkpt_child in visited or chkpt_child.type != CheckpointTypeEnum.DISABLED:
                continue
            # Check if that basic block has already an energy to leave
            if chkpt_child not in chkpt_cost or cost < chkpt_cost[chkpt_child]:
                chkpt_cost[chkpt_child] = cost
