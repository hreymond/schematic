from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.transformations.schematic.elements.conditional_block import ConditionalBlock
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock


class CheckpointConditionalNormalizer:
    """
    Normalization pass for normalizing checkpoint calls inside conditionally executed basic blocks
    """

    def __init__(self, computation_interval_splitter):
        self.splitter = computation_interval_splitter

    def normalize(self):
        """
        Normalizes all the conditionals containing a checkpoint
        """
        for name in ConditionalBlock.all_elements.keys():
            self._normalize(name)

    def _normalize(self, name):
        """
        Normalizes all the conditionals containing a checkpoint, whose execution happens inside the given function
        """
        conditionals = ConditionalBlock.all_elements[name]

        for conditional in conditionals:
            # conditional calls a checkpoint
            if conditional.calls_to(self.splitter.checkpoint_functions):
                self._normalize_conditional_checkpoints(conditional)

    def _normalize_conditional_checkpoints(self, conditional):
        """
        Normalizes a given conditional, which directly/indirectly contains a checkpoint.
        This methods applies the following transformations:
            (i)  if a branch has two or more checkpoints, or a branch has an indirect checkpoint call -> full normalization
            (ii) if both branches has one or less checkpoints, and no branch has an indirect checkpoint call -> lazy normalization
        """

        if not conditional.calls_to(self.splitter.checkpoint_functions):
            raise Exception("Error! No checkpoint found inside the conditional")

        full_normalization = self._full_normalization_required(conditional)

        if full_normalization:
            self._full_normalizaton(conditional)
        else:
            self._lazy_normalization(conditional)

    def _full_normalizaton(self, conditional):
        """
        Places a checkpoint at the end of the entry block and moves the last checkpoint of each branch at the end of the branch
        """
        for basic_blocks in conditional.branches.values():

            # skip empty branch
            if len(basic_blocks) == 0:
                continue

            last_checkpoint = self._get_branch_last_checkpoint(basic_blocks)

            if last_checkpoint is not None:
                target_basic_block = last_checkpoint["basic_block"]
                checkpoint = target_basic_block.instructions.pop(last_checkpoint["index"])

                # Adjust label
                if checkpoint.label is not None:
                    target_basic_block.instructions[0].label = checkpoint.label
                    checkpoint.label = None

            else:
                checkpoint = self.splitter.create_checkpoint()

            # get last basic block of the branch
            last_block = basic_blocks[-1]
            checkpoint.basic_block_id = last_block.instructions[0].basic_block_id
            # insert checkpoint before last instruction, that is, a jump
            last_instruction_index = len(last_block.instructions) - 1
            last_block.instructions.insert(last_instruction_index, checkpoint)

            # Adjust label
            if last_instruction_index == 0:
                last_block.instructions[0].label = last_block.instructions[1].label
                last_block.instructions[1].label = None

        # append checkpoint before header
        conditional.add_to_custom_basic_block("entry", self.splitter.create_checkpoint())

    def _lazy_normalization(self, conditional):
        """
        Removes checkpoints from the conditional branches and places a checkpoint at the beginning of the exit block
        """

        for basic_blocks in conditional.branches.values():
            checkpoint = self._get_branch_last_checkpoint(basic_blocks)

            if checkpoint is not None:
                target_basic_block = checkpoint["basic_block"]
                checkpoint_instruction = target_basic_block.instructions.pop(checkpoint["index"])

                # Adjust label
                if checkpoint_instruction.label is not None:
                    target_basic_block.instructions[0].label = checkpoint_instruction.label

        conditional.add_to_custom_basic_block("exit", self.splitter.create_checkpoint())

    def _full_normalization_required(self, conditional):
        """
        Returns if we need to fully normalize branches, that is, place a checkpoint at the end of the entry block and at the end of each branch
        Conditions:
            - a branch has more than 1 checkpoint
            - a branch contains a ConditionalBlock or a LoopBlock that executes a checkpoint
            - a branch contains a call to a function that executes a checkpoint
        """

        for basic_blocks in conditional.branches.values():
            n_direct_checkpoints = 0

            for basic_block in basic_blocks:
                # if a branch contains a ConditionalBlock or LoopBlock that executes a checkpoint
                # -> normalize both branches (cannot move the checkpoint at the exit block)
                if not isinstance(basic_block, BasicBlock) and basic_block.calls_to(self.splitter.checkpoint_functions):
                    return True

                elif isinstance(basic_block, BasicBlock):
                    for index, instruction in enumerate(basic_block.instructions):
                        if isinstance(instruction, CallOperation):
                            if instruction.name == self.splitter.checkpoint_function_name:
                                n_direct_checkpoints += 1

                                # if a branch contains 2+ checkpoints -> cannot move the checkpoint at the exit block
                                if n_direct_checkpoints > 1:
                                    return True

                            # if a branch has a basic block that calls a function that executes a checkpoint
                            elif instruction.name in self.splitter.checkpoint_functions:
                                return True

        return False

    def _get_branch_last_checkpoint(self, basic_blocks):
        """
        Returns the last checkpoint contained in a branch
        If no checkpoint found, returns a None value
        Otherwise, a dictionary {"basic_block", "index"}
        """
        last_checkpoint = None

        for basic_block in basic_blocks:
            # if the element is a LoopBlock or ConditionalBlock that will execute a checkpoint -> remove last checkpoint info
            # (current last checkpoint not directly contained in a basic block of the conditional)
            if not isinstance(basic_block, BasicBlock) and basic_block.calls_to(self.splitter.checkpoint_functions):
                last_checkpoint = None

            # if the basic block contains a direct call to a checkpoint (i.e. no nested loop / conditional with checkpoint)
            elif isinstance(basic_block, BasicBlock):
                for index, instruction in enumerate(basic_block.instructions):
                    if isinstance(instruction, CallOperation):

                        # if it calls a checkpoint -> update last info
                        if instruction.name == self.splitter.checkpoint_function_name:
                            last_checkpoint = {"basic_block": basic_block, "index": index}

                        # if it calls a function that calls a checkpoint -> remove last checkpoint info
                        # (current last checkpoint not directly contained in a basic block of the loop)
                        elif instruction.name in self.splitter.checkpoint_functions:
                            last_checkpoint = None

        return last_checkpoint
