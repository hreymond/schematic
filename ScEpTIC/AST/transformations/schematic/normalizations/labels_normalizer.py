from ScEpTIC.AST.elements.instructions.other_operations import PhiOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import BranchOperation
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock


class LabelsNormalizer:
    """
    Normalizes all the labels s.t. they are unique
    """

    LABELS_DISTANCE = 20

    @staticmethod
    def normalize_labels(basic_blocks):
        """
        Normalizes all the labels in the given sequence of basic blocks
        The method returns a dictionary {label:basic_block}
        """
        basic_blocks_dict = {}

        labels_map, labels_map_int = LabelsNormalizer._compute_label_maps(basic_blocks)

        label = None

        for basic_block in basic_blocks:

            for instruction in basic_block.instructions:
                # If the instruction has a label -> first instruction of the basic block
                if instruction.label is not None:
                    instruction.label = labels_map[instruction.label]
                    label = instruction.label

                instruction.basic_block_id = label

                if isinstance(instruction, BranchOperation):
                    if instruction.target_true is not None:
                        instruction.target_true = labels_map[instruction.target_true]

                    if instruction.target_false is not None:
                        instruction.target_false = labels_map[instruction.target_false]

                # adjust phi operations
                if isinstance(instruction, PhiOperation):
                    labels = list(instruction.values.keys())

                    for label in labels:
                        data = instruction.values[label]
                        del instruction.values[label]
                        instruction.values[labels_map[label]] = data

            # adjust basic block internal labels
            basic_block.label = BasicBlock.label_to_int(labels_map_int[basic_block.label])

            for index, label in enumerate(basic_block.prev):
                basic_block.prev[index] = BasicBlock.label_to_int(labels_map_int[label])

            for index, label in enumerate(basic_block.next):
                basic_block.next[index] = BasicBlock.label_to_int(labels_map_int[label])

            basic_blocks_dict[basic_block.label] = basic_block

        return {label: basic_blocks_dict[label] for label in sorted(basic_blocks_dict.keys())}

    @staticmethod
    def _compute_label_maps(basic_blocks):
        """
        Creates a map of current labels and new labels
        returns two dicts:
            - map for labels in string format
            - map for labels in integer format
        """
        labels = []
        labels_map = {}
        labels_map_int = {}

        first_label_id = 0

        for basic_block in basic_blocks:
            for instruction in basic_block.instructions:
                if instruction.label is not None:
                    label_int = BasicBlock.label_to_int(instruction.label)
                    labels.append(label_int)

        for label in sorted(labels):
            labels_map_int[label] = first_label_id
            label = f"%{label}"
            labels_map[label] = f"%{first_label_id}"
            first_label_id += LabelsNormalizer.LABELS_DISTANCE

        return labels_map, labels_map_int
