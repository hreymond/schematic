import copy

from ScEpTIC.AST.elements.instructions.memory_operations import StoreOperation, LoadOperation
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import BranchOperation, ReturnOperation
from ScEpTIC.AST.elements.types import BaseType
from ScEpTIC.AST.transformations.virtual_memory import MemoryUsage
from ScEpTIC.AST.transformations.virtual_memory.analysis.nvm_instructions_identifier import NVMInstructionsIdentifier

from ScEpTIC.AST.transformations.virtual_memory.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.virtual_memory.elements.conditional_block import ConditionalBlock
from ScEpTIC.AST.transformations.virtual_memory.elements.loop_block import LoopBlock
from ScEpTIC.AST.transformations.virtual_memory.normalizations.checkpoints.loops import CheckpointLoopNormalizer
from ScEpTIC.AST.transformations.virtual_memory.normalizations.checkpoints.conditionals import (
    CheckpointConditionalNormalizer,
)
from ScEpTIC.AST.transformations.virtual_memory.analysis.memory_instructions_indentifier import (
    MemoryInstructionsIdentifier,
)

from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.virtual_memory.parsers.elements.conditional_parser import ConditionalParser
from ScEpTIC.AST.transformations.virtual_memory.virtual_memory_transformations import VirtualMemoryTransformations


class ComputationIntervalsManager:
    def __init__(self, parser, memory_tag_parser, checkpoint_function_name, n_min_function, main_function_name):
        self.parser = parser
        self.memory_tag_parser = memory_tag_parser
        self.call_tree = {}
        self.functions_parsing_order = []
        self.functions_with_checkpoints = set()
        self.checkpoint_function_name = checkpoint_function_name
        self.n_min_function = n_min_function
        self.main_function_name = main_function_name

        self.populate_call_trees()
        self.populate_functions_parsing_order()

        self.checkpoint_functions = [x for x in self.functions_with_checkpoints] + [self.checkpoint_function_name]
        self.computation_intervals = {}

        self.boundaries_normalized = False

    def create_checkpoint(self):
        """
        Creates and returns a custom checkpoint function call
        """
        return CallOperation(None, self.checkpoint_function_name, BaseType("int", 32), [], [], {})

    def print_computation_intervals(self):
        """
        Prints the content of all the computation intervals
        """
        for name in self.computation_intervals:
            self.print_computation_interval(name)
            print()

    def print_computation_interval(self, name):
        """
        Prints the content of a given computation interval
        """
        tmp = BasicBlock.str_print_instructions
        BasicBlock.str_print_instructions = True

        num = len(self.computation_intervals[name]) - 1

        print(name)
        for idx, computation_interval in enumerate(self.computation_intervals[name]):
            basic_blocks = self.get_ordered_computation_interval(computation_interval)

            for basic_block in basic_blocks:
                print(basic_block)

            if idx < num:
                print("--- new computation interval ---")
                print()

        BasicBlock.str_print_instructions = tmp

    def synchronize_to_ast(self):
        """
        Synchronizes all the computation intervals with the AST structure
        """
        for name in self.parser.get_function_names():
            self._synchronize_to_ast(name)

    def _synchronize_to_ast(self, name):
        """
        Synchronizes the computation intervals of the provided function with its AST
        """
        elements = []

        for computation_interval in self.computation_intervals[name]:
            basic_blocks = self.get_ordered_computation_interval(computation_interval)

            for basic_block in basic_blocks:
                elements.append(basic_block)

        self.parser.basic_blocks[name] = {bb.get_label(): bb for bb in elements}

    def normalize_boundaries(self):
        """
        Normalizes checkpoints to fix computation interval boundaries
        """
        if self.boundaries_normalized:
            return

        conditionals_normalizer = CheckpointConditionalNormalizer(self)
        conditionals_normalizer.normalize()

        loop_normalizer = CheckpointLoopNormalizer(self)
        loop_normalizer.normalize()

        self.boundaries_normalized = True

    def split_computation_intervals(self):
        """
        Splits the functions into computation intervals
        Each computation interval contains a sequence of basic blocks
        """

        self.normalize_boundaries()

        self.parser.reparse()
        self.parser.normalize_ast_labels()

        for name in self.parser.get_function_names():
            computation_intervals = self._split(name)
            self.computation_intervals[name] = computation_intervals

        self._reset_bookkeeping_info()
        self.synchronize_to_ast()

    def parse_structures(self, parse_loops=True):
        """
        Parses the AST structures of computation intervals
        """
        for name, computation_intervals in self.computation_intervals.items():
            for idx, computation_interval in enumerate(computation_intervals):
                ci_name = f"{name}-{idx}"
                self.parser.parse_ast_structures(ci_name, computation_interval, parse_loops)

        self.synchronize_to_ast()
        return self.computation_intervals

    def get_ordered_computation_interval(self, computation_interval):
        """
        Returns the basic blocks of the computation interval, ordered by basic block id
        """
        basic_blocks = []

        labels = sorted(computation_interval.keys())

        for label in labels:
            basic_blocks.append(computation_interval[label])

        return basic_blocks

    def _split(self, name):
        """
        Splits the given subroutine into computation intervals
        """

        basic_blocks = self.parser.get_flatten_ast(name)

        computation_intervals = []
        computation_interval = {}

        labels_to_sobstitute = {}

        for basic_block in basic_blocks:
            basic_block.reset_conditional_structure()

            for label in basic_block.prev:
                if label in labels_to_sobstitute:
                    index = basic_block.prev.index(label)
                    basic_block.prev[index] = labels_to_sobstitute[label]

            if basic_block.calls_to(self.checkpoint_functions):
                split = self._split_basic_block(basic_block)

                # first element is part of the previous computation interval
                label = split[0].get_label()
                computation_interval[label] = split[0]

                if len(split) == 1:
                    computation_intervals.append(computation_interval)
                    computation_interval = {}

                else:
                    first_label = split[0].label
                    last_label = split[-1].label
                    labels_to_sobstitute[first_label] = last_label

                    # next elements start new computation intervals
                    for split_basic_block in split[1:]:
                        computation_intervals.append(computation_interval)
                        computation_interval = {}
                        label = split_basic_block.get_label()
                        computation_interval[label] = split_basic_block

            else:
                label = basic_block.get_label()
                computation_interval[label] = basic_block

        computation_intervals.append(computation_interval)

        return computation_intervals

    def _split_basic_block(self, basic_block):
        """
        Splits a given basic block into multiple basic blocks such that any basic block contains at most one call to a checkpoint (or to a function that calls a checkpoint)
        """
        basic_blocks = []

        instructions = basic_block.instructions
        basic_block.instructions = []

        set_next_label = None

        # split
        for index, instruction in enumerate(instructions):
            # Note: always split, otherwise I parse wrong structures and I wrongly transform the program
            # e.g. if the checkpoint is the penultimum instruction, but it is inside the latch, if I do not split, the parser identifies a loop
            # the subsequent normalization would treat the loop as within a single computation interval, but it is not.
            if isinstance(instruction, CallOperation) and instruction.name in self.checkpoint_functions:
                # basic block ids
                basic_block_id = basic_block.label
                # note: I can create a new basic block as I re-parse the labels and I have a distance sufficient to create a new one
                next_basic_block_id = basic_block_id + 1

                # append checkpoint to current basic block
                basic_block.instructions.append(instruction)

                # end the basic block with a branch operation
                next_label = f"%{next_basic_block_id}"
                useless_branch = BranchOperation(None, next_label, None)
                useless_branch.tick_count = 0
                useless_branch.basic_block_id = next_label
                basic_block.instructions.append(useless_branch)

                # append basic block to list of basic blocks
                old_next = basic_block.next
                basic_block.next = [next_basic_block_id]
                basic_blocks.append(basic_block)

                # generate new basic block
                basic_block = BasicBlock(next_basic_block_id, basic_block.function_name)
                basic_block.add_prev_label(BasicBlock.label_to_int(basic_block_id))
                basic_block.next = old_next

                # skip to next iteration -> current instruction parsed
                continue

            basic_block.instructions.append(instruction)

        basic_blocks.append(basic_block)

        # adjust labels
        for basic_block in basic_blocks:
            label = f"%{basic_block.get_label()}"
            basic_block.instructions[0].label = label

        return basic_blocks

    def address_uncertainty(self):
        """
        Addresses the uncertainty of loops
        """
        self._resolve_loop_uncertain_reads_writes()
        self._insert_loop_dummy_writes()
        self.synchronize_to_ast()

        # reparse data structures
        self.parser.reparse()
        self.split_computation_intervals()
        ConditionalParser.force_path_fill = True
        self.parse_structures(parse_loops=False)
        ConditionalParser.force_path_fill = False
        self.synchronize_to_ast()

    def populate_call_trees(self):
        """
        Creates a dictionary that maps to each function the functions it calls
        """
        for name in self.parser.get_function_names():
            self._populate_call_tree(name)

        self._propagate_checkpoint_calls()

    def _populate_call_tree(self, name):
        """
        Creates a dictionary that maps to a given function the functions it calls
        """

        basic_blocks = self.parser.get_flatten_ast(name)
        self.call_tree[name] = set()

        for basic_block in basic_blocks:
            for instruction in basic_block.instructions:
                if isinstance(instruction, CallOperation):
                    function_name = instruction.name

                    # if the analyzed function calls to a checkpoint, add to functions with checkpoints
                    if function_name == self.checkpoint_function_name:
                        self.functions_with_checkpoints.add(name)

                    else:
                        self.call_tree[name].add(instruction.name)

    def populate_functions_parsing_order(self):
        """
        Identifies the correct parsing order of functions
        We need to parse each caller before its callee.
        For example, we start from the main, then we parse the functions called in the main, then the functions called by such functions.
        """
        call_dependencies = {x: set() for x in self.call_tree.keys()}
        n_functions = len(call_dependencies)

        for function_name, called_functions in self.call_tree.items():
            for name in called_functions:
                # skip builtins
                if name not in call_dependencies:
                    continue
                call_dependencies[name].add(function_name)

        while len(self.functions_parsing_order) < n_functions:
            target = None

            for function_name, dependencies in call_dependencies.items():
                if len(dependencies) == 0 and function_name not in self.functions_parsing_order:
                    target = function_name
                    break

            if target == None:
                print(call_dependencies)
                raise Exception("Unable to identify function with no dependency!")

            self.functions_parsing_order.append(target)

            for dependency in call_dependencies.values():
                if target in dependency:
                    dependency.remove(target)

    def _propagate_checkpoint_calls(self):
        """
        Propagates the checkpoint calls that happen inside a function to its callers
        """
        for function_name, calls in self.call_tree.items():
            # skip if already present in functions with checkpoints
            if function_name in self.functions_with_checkpoints:
                continue

            for called_function_name in calls:
                # if calls to a function that contains a checkpoint -> add to functions with checkpoints
                if called_function_name in self.functions_with_checkpoints:
                    self.functions_with_checkpoints.add(function_name)
                    break

    def _reset_bookkeeping_info(self):
        """
        Resets the lists of conditional blocks and loop blocks
        """
        ConditionalBlock.reset_all_elements()
        LoopBlock.reset_all_elements()

    def _resolve_loop_uncertain_reads_writes(self):
        """
        Identifies the dummy writes required before and after each loop
        """
        for computation_intervals in LoopBlock.all_elements.values():
            for loop_block in computation_intervals:
                loop_block.identify_uncertain_instructions()

    def _insert_loop_dummy_writes(self):
        """
        Inserts the required dummy write in the loops
        """
        for ci_name, computation_intervals in LoopBlock.all_elements.items():
            name = self.get_name_from_computation_interval_id(ci_name)

            for loop_block in computation_intervals:
                loop_block.insert_dummy_writes(lambda x: self.memory_tag_parser.create_dummy_write(name, x))

    def adjust_dummy_writes(self):
        """
        Removes from each computation interval all the dummy write operations whose master instruction target volatile memory
        """
        data = self._extract_dummy_writes()

        for element in data:
            basic_block = element["basic_block"]

            # save label to restore after dummy write removal
            label = basic_block.instructions[0].label

            for instruction in element["dummy_writes"]:
                master = instruction.dummy_write_master

                # if master targets volatile memory -> useless dummy write -> remove
                if master.virtual_memory_target == VirtualMemoryEnum.VOLATILE:
                    basic_block.instructions.remove(instruction)

            # restore label
            basic_block.instructions[0].label = label

    def _extract_dummy_writes(self):
        """
        Returns the list of all the instructions in all the computation intervals that are involved in dummy writes
        """
        data = []

        for computation_intervals in self.computation_intervals.values():
            for computation_interval in computation_intervals:
                for basic_block in computation_interval.values():
                    dummy_writes = []

                    for instruction in basic_block.instructions:
                        if instruction.is_part_of_dummy_write:
                            dummy_writes.append(instruction)

                    if len(dummy_writes) > 0:
                        data.append({"basic_block": basic_block, "dummy_writes": dummy_writes})

        return data

    def apply_virtual_memory_transformations(self, memory_optimization_enabled):
        """
        Applies the implicit save and restore transformations.
        Then, it consolidates memory read instructions by evaluating multiple strategies for creating volatile copies of
        memory locations.
        It skips the transformation for single computation interval functions, as they need to target volatile memory
        """

        # parse all AST structures
        self.parse_structures()

        # fix compile-time uncertainty
        self.address_uncertainty()

        order_function = self.get_ordered_flatten_computation_interval
        flatten_function = self._get_flatten_computation_interval
        memory_tag_name_function = MemoryUsage.get_base_memory_tag

        before_call_volatile_data = {}
        after_call_volatile_data = {}

        # parse in correct order
        for name in self.functions_parsing_order:
            computation_intervals = self.computation_intervals[name]

            # get outside accesses and initial whitelist
            outside_frame_reads, outside_frame_writes, write_whitelists = MemoryUsage.get_memory_usage_list(
                computation_intervals, order_function, []
            )
            read_blacklist = []

            is_single_computation_interval_function = len(computation_intervals) == 1

            # before_call_volatile_data and after_call_volatile_data are popualted after parsing the main function
            # if element in before_call_volatile_data -> not the main function
            if name in before_call_volatile_data:
                # only function with single computation intervals need to retrieve data from volatile memory
                # all the other functions need to restore/save from/onto NVM
                if is_single_computation_interval_function:
                    write_initial_set = MemoryUsage.remove_after_call_data_from_outside_frame_writes(
                        after_call_volatile_data[name], outside_frame_writes
                    )
                    read_blacklist = MemoryUsage.get_outside_frame_read_volatile(
                        before_call_volatile_data[name], outside_frame_reads
                    )
                else:
                    write_initial_set = outside_frame_writes

                _, _, write_whitelists = MemoryUsage.get_memory_usage_list(
                    computation_intervals, order_function, write_initial_set
                )

            dummy_write_create = lambda x: self.memory_tag_parser.create_dummy_write(name, x)

            if not memory_optimization_enabled:
                read_blacklist = None

            # Note: single computation interval functions implicitly skips all the inside-frame accesses
            for index, computation_interval in enumerate(computation_intervals):
                # write whitelist enabled only if memory_optimization is enabled or if it is a single computation interval
                if memory_optimization_enabled or is_single_computation_interval_function:
                    write_whitelist = write_whitelists[index]
                else:
                    write_whitelist = None

                basic_blocks = self.get_ordered_computation_interval(computation_interval)
                VirtualMemoryTransformations.apply(
                    basic_blocks,
                    flatten_function,
                    write_whitelist,
                    read_blacklist,
                    memory_tag_name_function,
                    self.n_min_function,
                    dummy_write_create,
                )

            # retrieve volatile elements before/after function calls in computation intervals
            before_calls = MemoryUsage.get_volatile_elements_before_calls(
                computation_intervals, order_function, self.checkpoint_function_name
            )
            after_calls = MemoryUsage.get_volatile_elements_after_calls(
                computation_intervals, order_function, self.checkpoint_function_name
            )

            # update volatile data
            before_call_volatile_data = MemoryUsage.get_unique_volatile_elements(before_call_volatile_data, before_calls)
            after_call_volatile_data = MemoryUsage.get_unique_volatile_elements(after_call_volatile_data, after_calls)

        # reset mapping for builtins
        VirtualMemoryTransformations.reset_builtin_mapping(self.parser.functions)

        # synchronize computation intervals to AST
        self.synchronize_to_ast()

        # remove AST structures
        self.split_computation_intervals()

        # remove unnecessary dummy writes that we may add to address uncertainty
        self.adjust_dummy_writes()

        # adjust function stack target (volatile/non-volatile memory) for call and return instructions
        self.adjust_functions_stack()

    def get_ordered_flatten_computation_interval(self, computation_interval):
        """
        Flattens a computation interval, preserving the correct order of instructions
        """
        basic_blocks = self.get_ordered_computation_interval(computation_interval)

        return self._get_flatten_computation_interval(basic_blocks)

    def _get_flatten_computation_interval(self, basic_blocks):
        """
        Flattens a computation interval
        """
        data = []

        for basic_block in basic_blocks:
            for element in basic_block.flatten():
                data.append(element)

        return data

    def adjust_functions_stack(self):
        """
        Sets the memory target (volatile/non-volatile) of call and ret instructions
        """
        for function_name, computation_intervals in self.computation_intervals.items():

            current_function_stack_nvm = (
                function_name != self.main_function_name and function_name in self.functions_with_checkpoints
            )

            for computation_interval in computation_intervals:
                for basic_block in computation_interval.values():
                    for instruction in basic_block.instructions:
                        # the stack of functions containing a checkpoint needs to target NVM (stack non-volatile)
                        if isinstance(instruction, CallOperation) and instruction.name in self.functions_with_checkpoints:
                            instruction.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE

                        # return need to retrieve data from nvm if the function has a checkpoint, as its stack is non-volatile
                        if isinstance(instruction, ReturnOperation) and current_function_stack_nvm:
                            instruction.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE

    @staticmethod
    def get_name_from_computation_interval_id(name):
        """
        Returns the function name from the computation interval id
        """
        index = name.rindex("-")
        return name[:index]
