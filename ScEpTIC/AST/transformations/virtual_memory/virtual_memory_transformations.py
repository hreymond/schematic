import copy

from ScEpTIC.AST.elements.instructions.memory_operations import LoadOperation, StoreOperation
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.virtual_memory import MemoryTagsParser
from ScEpTIC.AST.transformations.virtual_memory.analysis.memory_instructions_indentifier import (
    MemoryInstructionsIdentifier,
)
from ScEpTIC.AST.transformations.virtual_memory.analysis.nvm_instructions_identifier import NVMInstructionsIdentifier


class VirtualMemoryTransformations:
    @staticmethod
    def apply(
        basic_blocks,
        flatten_function,
        write_whitelist,
        read_blacklist,
        memory_tags_name_function,
        n_min_function,
        dummy_write_create,
    ):
        """
        Applies all the virtual memory transformations: implicit save, implicit restore, and consolidate reads.
        :param write_whitelist a set of the memory tags that need to be preserved (if None -> all)
        :param read_blacklist a set of memory tags that need not to be read from NVM
        :param memory_tags_name_function the naming function for identifying a memory tag
        :param n_min_function function to calculate the n_min parameter of the consolidate reads transforamtion
        :param dummy_write_create function to create dummy writes
        """

        # set default mapping
        VirtualMemoryTransformations.reset_mapping(flatten_function(basic_blocks))

        reapply = True

        while reapply:
            VirtualMemoryTransformations.apply_implicit_save(basic_blocks, write_whitelist, memory_tags_name_function)
            VirtualMemoryTransformations.apply_implicit_restore(basic_blocks, read_blacklist, memory_tags_name_function)
            VirtualMemoryTransformations.consolidate_memory_operations(basic_blocks, n_min_function)
            reapply = VirtualMemoryTransformations.address_war_hazards(basic_blocks, flatten_function, dummy_write_create)

    @staticmethod
    def address_war_hazards(basic_blocks, flatten_function, dummy_write_create):
        reapply = False

        flatten_basic_blocks = flatten_function(basic_blocks)
        war_hazards = VirtualMemoryTransformations._get_tags_involved_in_war_hazards(flatten_basic_blocks)
        _, first_writes = MemoryInstructionsIdentifier.resolve_first_reads_writes(basic_blocks, True)

        elements_to_normalize = []

        # scan first writes and identify the ones that need normalization
        for memory_tag in war_hazards:
            # skip memory tags that do not have a war hazard
            if memory_tag not in first_writes:
                continue

            for element in first_writes[memory_tag]:
                if element.missing:
                    elements_to_normalize.append(element)

        # normalize first writes
        if len(elements_to_normalize) > 0:
            reapply = True

            VirtualMemoryTransformations.reset_mapping(flatten_basic_blocks)

            for element in elements_to_normalize:
                conditional_block = element.conditional
                path = element.path
                memory_tag = element.memory_tag
                target_basic_block = conditional_block.branches[path][-1]

                # create dummy write
                dummy_write = dummy_write_create(memory_tag)

                # get label of last instruction (if not None -> basic block has one instruction)
                label = target_basic_block.instructions[-1].label
                target_basic_block.instructions[-1].label = None

                # insert dummy write
                for instruction in dummy_write:
                    # propagate label to first instruction
                    instruction.label = label
                    label = None

                    target_basic_block.instructions.insert(-1, instruction)

        return reapply

    @staticmethod
    def reset_mapping(basic_blocks):
        """
        Reverts the effects of virtual memory transformations
        """
        for basic_block in basic_blocks:
            to_remove = []

            for instruction in basic_block.instructions:
                if isinstance(instruction, LoadOperation) or isinstance(instruction, StoreOperation):
                    if instruction.has_virtual_memory_copy:
                        to_remove.append(instruction.virtual_memory_copy)

                    instruction.virtual_memory_target = VirtualMemoryEnum.VOLATILE
                    instruction.virtual_memory_normalized = False
                    instruction.has_virtual_memory_copy = False
                    instruction.virtual_memory_copy = None

            for instruction in to_remove:
                basic_block.instructions.remove(instruction)

    @staticmethod
    def _get_tags_involved_in_war_hazards(basic_blocks):
        """
        Returns a set of tags that is involved in a WAR hazard
        """
        wars = set()
        first_reads = set()
        first_writes = set()

        for basic_block in basic_blocks:
            for instruction in basic_block.instructions:
                if isinstance(instruction, LoadOperation):
                    memory_tag = instruction.memory_tag

                    # add to read set only if no write happened
                    if memory_tag not in first_writes:
                        first_reads.add(memory_tag)

                elif isinstance(instruction, StoreOperation):
                    memory_tag = instruction.memory_tag
                    first_writes.add(memory_tag)

                    # verify if WAR found
                    if memory_tag in first_reads:
                        wars.add(memory_tag)

        return wars

    @staticmethod
    def apply_implicit_save(basic_blocks, write_whitelist, memory_tags_name_function):
        """
        Applies the transformation to implicitly save the memory content to a sequence of basic blocks
        :param write_whitelist a set of the memory tags that need to be preserved (if None -> all)
        :param memory_tags_name_function the naming function for identifying a memory tag
        """
        last_reads, last_writes = MemoryInstructionsIdentifier.resolve_last_reads_writes(basic_blocks)

        # All last memory writes must target NVM
        for memory_tag, metadata in last_writes.items():
            # skip tags
            if (
                isinstance(write_whitelist, set)
                and memory_tags_name_function(memory_tag) not in write_whitelist
                and "[]" not in memory_tags_name_function(memory_tag)
            ):
                continue

            for element in metadata:
                if element.instruction is not None:
                    element.instruction.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE

        # All memory reads that came after last writes must target NVM
        for memory_tag, metadata in last_reads.items():
            # skip tags
            if (
                isinstance(write_whitelist, set)
                and memory_tags_name_function(memory_tag) not in write_whitelist
                and "[]" not in memory_tags_name_function(memory_tag)
            ):
                continue

            for element in metadata:
                if element.instruction is not None:
                    element.instruction.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE

    @staticmethod
    def apply_implicit_restore(basic_blocks, read_blacklist, memory_tags_name_function):
        """
        Applies the transformation to implicitly restore the memory content
        :param read_blacklist a set of the memory tags that need not to be restored
        :param memory_tags_name_function the naming function for identifying a memory tag
        """
        first_reads, first_writes = MemoryInstructionsIdentifier.resolve_first_reads_writes(basic_blocks)

        # All memory reads that came before the first write must target NVM
        for memory_tag, metadata in first_reads.items():
            # skip tags
            if (
                isinstance(read_blacklist, set)
                and memory_tags_name_function(memory_tag) in read_blacklist
                and "[]" not in memory_tags_name_function(memory_tag)
            ):
                continue

            for element in metadata:
                if element.instruction is not None:
                    element.instruction.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE

        # Conditional normalization - writes that may execute as first need to target NVM for read consistency
        # if a memory read exists that may execute after such write (and, if write not executed, executes before any write)
        for memory_tag, metadata in first_writes.items():
            # skip tags
            if (
                isinstance(read_blacklist, set)
                and memory_tags_name_function(memory_tag) in read_blacklist
                and "[]" not in memory_tags_name_function(memory_tag)
            ):
                continue

            for element in metadata:
                if element.instruction is not None and element.to_normalize:
                    normalize_reads, normalize_writes = VirtualMemoryTransformations._get_last_write_elements(element)

                    for element in normalize_reads + normalize_writes:
                        # set normalized flag ONLY if it was targeting volatile memory
                        if element.instruction.virtual_memory_target != VirtualMemoryEnum.NON_VOLATILE:
                            element.instruction.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE
                            element.instruction.virtual_memory_normalized = True

    @staticmethod
    def _get_last_write_elements(element_metadata):
        """
        Returns the last read and write that happens with respect to a memory write that requires normalization.
        This method retrieves the surrounding block of the specified element and identifies the last read/write over the element's memory tag
        """
        if element_metadata.conditional is None:
            raise Exception("Unable to normalize a non-conditionally executed write instruction")

        memory_tag = element_metadata.memory_tag
        path = element_metadata.path
        basic_blocks = element_metadata.conditional.branches[path]

        last_reads, last_writes = MemoryInstructionsIdentifier.resolve_last_reads_writes(basic_blocks)

        reads = last_reads[memory_tag] if memory_tag in last_reads else []
        writes = last_writes[memory_tag] if memory_tag in last_writes else []

        return reads, writes

    @staticmethod
    def consolidate_memory_operations(basic_blocks, n_min_function):
        """
        Applies the transformation that creates volatile copies of frequently-accessed memory locations
        """
        nvm_instructions = NVMInstructionsIdentifier.identify_nvm_instruction(basic_blocks)

        for memory_tag, metadata in nvm_instructions.items():
            for element in metadata:
                # check if skip required
                if NVMInstructionsIdentifier.already_processed(element):
                    continue

                first_instructions, reads = NVMInstructionsIdentifier.identify_nvm_instructions_to_consolidate(
                    element, n_min_function
                )
                VirtualMemoryTransformations._create_volatile_copy(first_instructions)
                VirtualMemoryTransformations._consolidate_reads(reads)

    @staticmethod
    def _create_volatile_copy(instructions_metadata):
        """
        Creates a volatile copy of the memory location that each instruction in the instruction_metadata list targets
        """
        for metadata in instructions_metadata:
            basic_block = metadata["basic_block"]
            instruction = metadata["instruction"]
            index = basic_block.instructions.index(instruction) + 1

            if isinstance(instruction, StoreOperation):
                new_instruction = copy.deepcopy(instruction)
                # unset label
                new_instruction.label = None

            elif isinstance(instruction, LoadOperation):
                # load source register
                store_target_reg = copy.deepcopy(instruction.element)
                store_type = copy.deepcopy(instruction.type)

                # load target register
                store_value = copy.deepcopy(instruction.target)
                store_value.type = store_type

                align = MemoryTagsParser.ALIGN
                volatile = MemoryTagsParser.VOLATILE

                new_instruction = StoreOperation(store_target_reg, store_value, align, volatile)
            else:
                raise Exception("Bad instruction!")

            if instruction.has_virtual_memory_copy:
                raise Exception(f"Error! Instruction {instruction} already processed!")

            instruction.has_virtual_memory_copy = True
            instruction.virtual_memory_copy = new_instruction
            new_instruction.virtual_memory_target = VirtualMemoryEnum.VOLATILE
            new_instruction.basic_block_id = instruction.basic_block_id
            new_instruction.is_virtual_memory_copy = True
            new_instruction.memory_tag = copy.deepcopy(instruction.memory_tag)
            new_instruction.memory_tag_dependency = copy.deepcopy(instruction.memory_tag_dependency)
            new_instruction.metric_group = "checkpoint"

            basic_block.instructions.insert(index, new_instruction)

    @staticmethod
    def _consolidate_reads(reads):
        """
        Makes all the instructions in the reads list target volatile memory
        """
        for instruction in reads:
            instruction.virtual_memory_target = VirtualMemoryEnum.VOLATILE

    @staticmethod
    def reset_builtin_mapping(functions):
        """
        Resets the memory mapping of builtin/input functions
        """
        for function in functions.values():
            if function.is_builtin or function.is_input:
                for instruction in function.body:
                    if isinstance(instruction, LoadOperation) or isinstance(instruction, StoreOperation):
                        instruction.virtual_memory_target = VirtualMemoryEnum.VOLATILE
                        instruction.virtual_memory_normalized = False
                        instruction.has_virtual_memory_copy = False
                        instruction.virtual_memory_copy = None
