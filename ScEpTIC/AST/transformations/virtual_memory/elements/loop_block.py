from ScEpTIC.exceptions import NotImplementedException
from .base_element import BaseASTElement
from .basic_block import BasicBlock

from ScEpTIC.AST.elements.instructions.memory_operations import StoreOperation, LoadOperation
from ScEpTIC.AST.transformations.virtual_memory.analysis.memory_instructions_indentifier import (
    MemoryInstructionsIdentifier,
)
from ScEpTIC.tools import flatten_multilevel_list


class LoopBlock(BaseASTElement):
    """
    Representation of a loop in its natural form
    -> header, body, latch
    """

    all_elements = {}

    @staticmethod
    def reset_all_elements():
        """
        Resets the list of loop blocks
        """
        LoopBlock.all_elements = {}

    def __init__(self, function_name):
        super().__init__()

        self.blocks = {"header": {}, "body": {}, "latch": {}}

        self.custom_preheader = None
        self.custom_exit_block = None

        if function_name not in self.all_elements:
            self.all_elements[function_name] = []

        self.all_elements[function_name].append(self)

        self.dummy_writes = {"before": [], "after": []}

    def get_code_len(self):
        return len(self.blocks["header"].keys()) + len(self.blocks["body"].keys())

    def get_label(self):
        """
        Returns element label
        """
        # the label of the entry block is included as next label in the latch
        return min(self._get_elements_next_labels())

    def _get_next_labels(self):
        """
        Returns labels of the elements directly reachable from the current one

        The next basic block is the one with highest label id
        """
        return [max(self._get_elements_next_labels())]

    def _get_elements_next_labels(self):
        """
        Returns all the labels of the basic blocks that came after the ones included in the loop
        """
        return [
            label
            for _, section in self.blocks.items()
            for _, basic_block in section.items()
            for label in basic_block.get_next_labels()
        ]

    def _get_previous_labels(self):
        """
        Returns the labels of the basic blocks that precede the current one
        """
        return [min(self._get_elements_previous_labels())]

    def _get_elements_previous_labels(self):
        """
        Returns all the labels of the basic blocks that came before the ones included in the loop
        """
        return [
            label
            for _, section in self.blocks.items()
            for _, basic_block in section.items()
            for label in basic_block.get_previous_labels()
        ]

    def add_basic_block(self, block_type, basic_block):
        """
        Adds a basic block in the loop

        :param block_type: type of the basic block (header, body, latch)
        :param basic_block: the basic block
        """
        self.blocks[block_type][basic_block.get_label()] = basic_block

    def get_labels(self):
        """
        Returns the labels of all the basic blocks included inside the loop
        """
        return [
            label
            for _, section in self.blocks.items()
            for _, basic_block in section.items()
            for label in basic_block.get_labels()
        ]

    def flatten(self):
        """
        Returns a flatten representation of the conditional block
        """
        sections_order = ["header", "body", "latch"]

        data = []

        if self.custom_preheader is not None:
            data.append(self.custom_preheader)

        for section in sections_order:
            for block in self.blocks[section].values():
                for element in block.flatten():
                    data.append(element)

        if self.custom_exit_block is not None:
            data.append(self.custom_exit_block)

        return data

    def get_basic_blocks(self):
        """
        Returns the sequence of elements contained in the header, body, and latch (in correct order)
        """
        sections_order = ["header", "body", "latch"]
        basic_blocks = []

        for section in sections_order:
            for block in self.blocks[section].values():
                basic_blocks.append(block)

        return basic_blocks

    def _create_custom_preheader(self):
        """
        Creates a custom basic block that contains instructions that need to be executed before the loop
        """

        if self.custom_preheader is None:
            self.custom_preheader = BasicBlock(f"%loop-pre-header-{self.get_label()}", "custom")

            # append previous labels
            for label in self._get_previous_labels():
                self.custom_preheader.add_prev_label(label)

            if self.is_conditional_exit_block:
                label = self.conditional_exit_label[0]
                self.custom_preheader.set_conditional_exit_label(label)

            self.custom_preheader.add_next_label(self.get_label())
            self.custom_preheader.merge_in_previous = True

    def _create_custom_exit_block(self):
        """
        Creates a custom basic block that contains instructions that need to be executed after the loop
        """

        if self.custom_exit_block is None:
            self.custom_exit_block = BasicBlock(f"%loop-exit-{self.get_label()}", "custom")

            # append previous labels
            for label in self._get_next_labels():
                self.custom_exit_block.add_next_label(label)

            if self.is_conditional_entry_block:
                label = self.conditional_entry_label[0]
                self.custom_exit_block.set_conditional_entry_label(label)

            self.custom_exit_block.add_prev_label(self.get_label())
            self.custom_exit_block.merge_in_subsequent = True

    def add_to_custom_preheader(self, instruction):
        """
        Adds a given instruction at the end of the custom pre-header
        """

        self._create_custom_preheader()
        self.custom_preheader.instructions.append(instruction)

    def add_to_custom_exit_block(self, instruction):
        """
        Adds a given instruction at the end of the custom exit block
        """

        self._create_custom_exit_block()
        self.custom_exit_block.instructions.append(instruction)

    def has_nested_loop(self):
        """
        Returns if the element has a nested loop
        """
        for basic_blocks in self.blocks.values():
            for basic_block in basic_blocks.values():
                if isinstance(basic_block, LoopBlock) or basic_block.has_nested_loop():
                    return True

        return False

    def get_nested_loops(self):
        """
        Returns a list with the loop blocks contained in this element (depth 1, no nested loop of nested loops)
        """
        loops = []

        for basic_blocks in self.blocks.values():
            for basic_block in basic_blocks.values():
                if isinstance(basic_block, LoopBlock):
                    loops.append(basic_block)

                else:
                    # get nested elements
                    for loop in basic_block.get_nested_loops():
                        loops.append(loop)

        return loops

    def calls_to(self, function_names, is_checkpoint=True):
        """
        Returns if the loop contains a basic block that calls given functions
        """
        for basic_blocks in self.blocks.values():
            for basic_block in basic_blocks.values():
                if basic_block.calls_to(function_names, is_checkpoint):
                    return True

        return False

    def calls_checkpoint(self, checkpoint_function_name, ignore_nested_checkpoint=False):
        """
        Returns if the loop contains a basic block that calls a checkpoint
        """
        for basic_blocks in self.blocks.values():
            for basic_block in basic_blocks.values():

                if not ignore_nested_checkpoint or isinstance(basic_block, BasicBlock):
                    if basic_block.calls_checkpoint(checkpoint_function_name, ignore_nested_checkpoint):
                        return True

        return False

    def get_memory_tag_first_reads_writes(self, only_nvm=False):
        """
        Returns two dictionaries which contains the first reads and writes instructions for each memory tag
            - reads: contains the memory reads operations that happen before the first write
            - writes: contains the first memory write instruction
        """
        basic_blocks = self.get_basic_blocks()
        return MemoryInstructionsIdentifier.resolve_first_reads_writes(basic_blocks, only_nvm)

    def get_memory_tag_last_reads_writes(self):
        """
        Returns two dictionaries which contains the last reads and writes instructions for each memory tag
            - reads: contains the memory reads operations that happen after the last write
            - writes: contains the last memory write instruction
        """
        basic_blocks = self.get_basic_blocks()
        return MemoryInstructionsIdentifier.resolve_last_reads_writes(basic_blocks)

    def insert_dummy_writes(self, create_dummy_write):
        """
        Inserts the dummy writes before and after the loop
        :param create_dummy_write function to create a dummy write
        """
        self._insert_dummy_writes(create_dummy_write, "before")
        self._insert_dummy_writes(create_dummy_write, "after")

    def _insert_dummy_writes(self, create_dummy_write, target):
        """
        Inserts the dummy writes before/after the loop
        """
        if target == "before":
            # for the dummy write before the loop, the master is the LOAD, that is, the 2nd instruction from the bottom
            master_index = -2
            add_instruction = self.add_to_custom_preheader
        elif target == "after":
            # for the dummy write after the loop, the master is the STORE, that is, the last instruction
            master_index = -1
            add_instruction = self.add_to_custom_exit_block
        else:
            raise NotImplementedException(f"No target '{target}' for loop custom block!")

        for memory_tag in self.dummy_writes[target]:
            instructions = create_dummy_write(memory_tag)
            master_instruction = instructions[master_index]

            for instruction in instructions:
                instruction.is_part_of_dummy_write = True
                instruction.dummy_write_master = master_instruction
                add_instruction(instruction)

            master_instruction.is_dummy_write_master = True

    def identify_uncertain_instructions(self):
        basic_blocks = self.get_basic_blocks()
        first_reads, first_writes = MemoryInstructionsIdentifier.resolve_first_reads_writes(basic_blocks)

        writes = self._identify_uncertain_writes(first_writes)
        reads = self._identify_uncertain_reads(first_reads, writes)

        # Each unceratin write need a dummy write after the loop
        self.dummy_writes["after"] = sorted(writes, key=len)

        # Each uncertain read need a dummy write before the loop
        self.dummy_writes["before"] = sorted(reads, key=len)

        self._process_nested_loops_uncertain_instructions()

    def _process_nested_loops_uncertain_instructions(self):
        """
        Processes the dummy writes required in nested loops.
        Dummy writes in nested loops can be removed if the outer loop has the same dummy write.
        """
        nested_loops = self.get_nested_loops()

        for loop in nested_loops:
            for operation_type, memory_tags in self.dummy_writes.items():
                for memory_tag in memory_tags:
                    if memory_tag in loop.dummy_writes[operation_type]:
                        loop.dummy_writes[operation_type].remove(memory_tag)

    def _identify_uncertain_reads(self, read_instructions, dummy_writes):
        dummy_reads = []

        reads = self._get_tag_dependencies(read_instructions)
        # note: ordered by memory_tag length
        scalar_reads = self._get_scalar_tags(reads)
        non_scalar_reads = self._get_non_scalar_tags(reads)

        # scalar reads require a dummy write before the loop latch
        for memory_tag in scalar_reads:
            dummy_reads.append(memory_tag)

        # non-scalar reads require a dummy write only if they do not depend on loop-modified data
        # dummy_writes contains the data that the loop modifies and that it promotes to volatile memory
        # if the read depends on such a data, then it must target NVM (changes at every iteration)
        # as it depends on data written by the loop during some iterations and we need to treat such a read as first
        for memory_tag in non_scalar_reads:
            dependencies = reads[memory_tag]

            independent = True

            for dependency in dependencies:
                if dependency in dummy_writes:
                    independent = False
                    break

            if independent:
                dummy_reads.append(memory_tag)

        return dummy_reads

    def _identify_uncertain_writes(self, write_instructions):
        dummy_writes = []

        writes = self._get_tag_dependencies(write_instructions)
        # note: ordered by memory_tag length
        scalar_writes = self._get_scalar_tags(writes)
        non_scalar_writes = self._get_non_scalar_tags(writes)

        # scalar writes require a dummy write after the loop latch
        for memory_tag in scalar_writes:
            dummy_writes.append(memory_tag)

        # non-scalar writes require a dummy write only if they do not depend on loop-modified data
        # -> parse in order of tag len and check if a tag dependency is in dummy_writes
        # -> if so, depends on data written by the loop during some iterations and we need to treat such a write as last
        for memory_tag in non_scalar_writes:
            dependencies = writes[memory_tag]

            independent = True

            for dependency in dependencies:
                if dependency in dummy_writes:
                    independent = False
                    break

            if independent:
                dummy_writes.append(memory_tag)

        return dummy_writes

    @staticmethod
    def _get_tag_dependencies(operations):
        data = {}

        for memory_tag, metadata in operations.items():
            for element in metadata:
                if element.instruction is not None and memory_tag not in data:
                    dependency_list = element.instruction.memory_tag_dependency

                    if isinstance(dependency_list, list):
                        # flatten the list and remove first element, that is, the base data structure
                        dependency_list = list(
                            set([x for x in flatten_multilevel_list(dependency_list[1:]) if x is not None])
                        )
                    else:
                        dependency_list = []

                    data[memory_tag] = dependency_list

        return data

    @staticmethod
    def _get_scalar_tags(memory_tags):
        return sorted([memory_tag for memory_tag in memory_tags if "[" not in memory_tag], key=len)

    @staticmethod
    def _get_non_scalar_tags(memory_tags):
        return sorted([memory_tag for memory_tag in memory_tags if "[" in memory_tag], key=len)

    def get_memory_tag_reads(self):
        return self._get_memory_tag(LoadOperation)

    def get_memory_tag_writes(self):
        return self._get_memory_tag(StoreOperation)

    def _get_memory_tag(self, instruction_type):
        data = {}

        for basic_block in self.flatten():
            for instruction in basic_block.instructions:
                if isinstance(instruction, instruction_type):
                    memory_tag = instruction.memory_tag

                    dependency_list = instruction.memory_tag_dependency
                    if not isinstance(dependency_list, list):
                        data[memory_tag] = []
                    else:
                        dependency_list = [x for x in flatten_multilevel_list(dependency_list[1:]) if x is not None]
                        data[memory_tag] = set(dependency_list)

        scalar_tags = [memory_tag for memory_tag in data if "[" not in memory_tag]
        non_scalar_tags = [memory_tag for memory_tag in data if "[" in memory_tag]

        return {"scalar": scalar_tags, "non_scalar": non_scalar_tags, "data": data}

    def __str__(self):
        add_str = self.get_real_next_prev()
        retstr = f"LoopBlock(id={self.get_label()}, labels={self.get_labels()}, prev={self.get_previous_labels()}, next={self.get_next_labels()}{add_str})\n"

        if self.custom_preheader is not None:
            retstr += f"  [+] Custom pre-header:\n"
            bb_str = str(self.custom_preheader).replace("\n", "\n      ")
            retstr += f"      - {bb_str}\n"

        for block_type, bb_list in self.blocks.items():
            retstr += f"  [+] {block_type}:\n"

            for basic_block in bb_list.values():
                bb_str = str(basic_block).replace("\n", "\n      ")
                retstr += f"      - {bb_str}\n"

        if self.custom_exit_block is not None:
            retstr += f"  [+] Custom exit block:\n"
            bb_str = str(self.custom_exit_block).replace("\n", "\n      ")
            retstr += f"      - {bb_str}\n"

        return retstr
