class MemoryAccessMetadata:
    """
    Class representing the metadata required for identifying the first/last memory read/write instructions
    """

    def __init__(self, basic_block, index, instruction, memory_tag, missing):
        self.basic_block = basic_block
        self.index = index
        self.instruction = instruction
        self.memory_tag = memory_tag
        self.missing = missing
        self.to_normalize = False
        self.write_covered = False
        self.conditional = None
        self.path = None
        self.surrounding_sequence = None

    def set_conditional_container(self, conditional, path):
        """
        Sets the conditional element that contains the basic block
        """
        if self.conditional is None:
            self.conditional = conditional
            self.path = path

    def set_surrounding_sequence(self, surrounding_sequence):
        """
        Sets the list of instructions where the instruction resides
        """
        if self.surrounding_sequence is None:
            self.surrounding_sequence = surrounding_sequence

    def __str__(self):
        retstr = f"{self.instruction}" if self.instruction is not None else "MISSING"

        retstr += f" - index: {self.index} - path: {self.path}"

        if self.write_covered:
            retstr += " (W)"

        if self.to_normalize:
            retstr += " (N)"

        return retstr


class CompositeMemoryAccessMetadata:
    def __init__(self, memory_tag, basic_block, instruction):
        self.memory_tag = memory_tag
        self.basic_block = basic_block
        self.conditional = None
        self.path = None
        self.surrounding_sequence = None
        self.elements = []
        self.instruction = instruction

    def set_conditional_container(self, conditional, path):
        """
        Sets the conditional element that contains the basic block
        """
        if self.conditional is None:
            self.conditional = conditional
            self.path = path

    def set_surrounding_sequence(self, surrounding_sequence):
        """
        Sets the list of instructions where the instruction resides
        """
        if self.surrounding_sequence is None:
            self.surrounding_sequence = surrounding_sequence

    def __str__(self):
        retstr = "CompositeMemoryAccessMetadata:\n"

        for element in self.elements:
            retstr += f"  {element}\n"

        return retstr
