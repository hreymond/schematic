from .base_element import BaseASTElement

from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.elements.instructions.memory_operations import LoadOperation, StoreOperation
from ScEpTIC.AST.transformations.virtual_memory.elements.memory_access_metadata import MemoryAccessMetadata

from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum


class BasicBlock(BaseASTElement):
    """
    Basic block of the AST
    """

    all_elements = {}

    str_print_instructions = False

    @staticmethod
    def reset_all_elements():
        """
        Resets the list of basic blocks
        """
        BasicBlock.all_elements = {}

    def __init__(self, label, function_name):
        super().__init__()

        self.instructions = []
        self.label = label
        self.next = []
        self.prev = []
        self.function_name = function_name

        # if set to True, the re-parser will merge the instructions of the basic block into the previous or next one
        self.merge_in_previous = False
        self.merge_in_subsequent = False

        self.is_path_fill = False

        if function_name not in self.all_elements:
            self.all_elements[function_name] = []

        self.all_elements[function_name].append(self)

    def add_prev_label(self, label):
        """
        Adds the label of a basic block that precedes the one of this object
        """
        label = self.label_to_int(label)

        if label not in self.prev:
            self.prev.append(label)

    def _get_previous_labels(self):
        """
        Returns the labels of the basic blocks that precede the current one
        """
        return self.prev

    def get_label(self):
        """
        Returns element label
        """
        return self.label

    def _get_next_labels(self):
        """
        Returns labels of the elements directly reachable from the current one
        """
        return self.next

    def add_instruction(self, instruction):
        """
        Adds an instruction in the basic block
        """
        instruction.basic_block_id = f"%{self.label}"
        self.instructions.append(instruction)

    def add_next_label(self, label):
        """
        Adds the label of a subsequent basic block
        """
        label = self.label_to_int(label)

        if label not in self.next:
            self.next.append(label)

    def __str__(self):
        add_str = self.get_real_next_prev()
        retstr = f"BasicBlock(id={self.get_label()}, instr_count={len(self.instructions)}, prev={self.get_previous_labels()}, next={self.get_next_labels()}{add_str})"

        if self.merge_in_previous:
            retstr += " (merge-previous)"

        if self.merge_in_subsequent:
            retstr += " (merge-subsequent)"

        if self.is_path_fill:
            retstr += " {PATHFILL}"

        if self.str_print_instructions:

            retstr += "\n"
            for instruction in self.instructions:
                retstr += f"  > {instruction}"

                if instruction.memory_tag is not None:
                    retstr += f" [tag: {instruction.memory_tag}]"

                retstr += "\n"

        return retstr

    def calls_to(self, function_names, is_checkpoint=True):
        """
        Returns if the basic block has a call to given functions
        """

        for instruction in self.instructions:
            if isinstance(instruction, CallOperation) and instruction.name in function_names:
                return True

        return False

    def calls_checkpoint(self, checkpoint_function_name, ignore_nested_checkpoint=False):
        """
        Returns if the basic block has a call to a checkpoint
        """

        for instruction in self.instructions:
            if isinstance(instruction, CallOperation) and instruction.name == checkpoint_function_name:
                return True

        return False

    def has_nested_loop(self):
        """
        Returns if the element has a nested loop
        """
        return False

    def get_nested_loops(self):
        """
        Returns a list with the loop blocks contained in this element (depth 1, no nested loop of nested loops)
        """
        return []

    def get_memory_tag_first_reads_writes(self, only_nvm=False):
        """
        Returns two dictionaries which contains the first reads and writes instructions for each memory tag
            - reads: contains the memory reads operations that happen before the first write
            - writes: contains the first memory write instruction
        """
        reads = {}
        writes = {}

        for index, instruction in enumerate(self.instructions):
            if isinstance(instruction, LoadOperation):
                # skip if need to identify only NVM
                if only_nvm and instruction.virtual_memory_target != VirtualMemoryEnum.NON_VOLATILE:
                    continue

                memory_tag = instruction.memory_tag

                # skip
                if memory_tag in writes:
                    continue

                metadata = MemoryAccessMetadata(self, index, instruction, memory_tag, False)

                if memory_tag not in reads:
                    reads[memory_tag] = [metadata]
                else:
                    reads[memory_tag].append(metadata)

            elif isinstance(instruction, StoreOperation):
                # skip if need to identify only NVM
                if only_nvm and instruction.virtual_memory_target != VirtualMemoryEnum.NON_VOLATILE:
                    continue

                memory_tag = instruction.memory_tag
                metadata = MemoryAccessMetadata(self, index, instruction, memory_tag, False)

                if memory_tag not in writes:
                    writes[memory_tag] = [metadata]

        return reads, writes

    def get_memory_tag_last_reads_writes(self, only_nvm=False):
        """
        Returns two dictionaries which contains the last reads and writes instructions for each memory tag
            - reads: contains the memory reads operations that happen after the last write
            - writes: contains the last memory write instruction
        """
        reads = {}
        writes = {}

        for index, instruction in enumerate(self.instructions):
            if isinstance(instruction, LoadOperation):
                # skip if need to identify only NVM
                if only_nvm and instruction.virtual_memory_target != VirtualMemoryEnum.NON_VOLATILE:
                    continue

                memory_tag = instruction.memory_tag
                metadata = MemoryAccessMetadata(self, index, instruction, memory_tag, False)

                if memory_tag not in reads:
                    reads[memory_tag] = [metadata]
                else:
                    reads[memory_tag].append(metadata)

            elif isinstance(instruction, StoreOperation):
                # skip if need to identify only NVM
                if only_nvm and instruction.virtual_memory_target != VirtualMemoryEnum.NON_VOLATILE:
                    continue

                memory_tag = instruction.memory_tag
                metadata = MemoryAccessMetadata(self, index, instruction, memory_tag, False)

                writes[memory_tag] = [metadata]

                if memory_tag in reads:
                    del reads[memory_tag]

        return reads, writes

    def get_all_instructions_targeting_nvm(self, surrounding_sequence):
        """
        Returns a list of all the instructions that target NVM contained in the element
        """

        nvm_instructions = {}

        for index, instruction in enumerate(self.instructions):
            if isinstance(instruction, LoadOperation) or isinstance(instruction, StoreOperation):
                # skip instructions that target volatile memory
                if instruction.virtual_memory_target != VirtualMemoryEnum.NON_VOLATILE:
                    continue

                memory_tag = instruction.memory_tag

                if memory_tag not in nvm_instructions:
                    nvm_instructions[memory_tag] = []

                metadata = MemoryAccessMetadata(self, index, instruction, memory_tag, False)
                metadata.set_surrounding_sequence(surrounding_sequence)

                nvm_instructions[memory_tag].append(metadata)

        return nvm_instructions

    def get_all_instructions_targeting_nvm_after(self, first_instruction, memory_tag, from_conditional=False):
        instructions = {
            "n_reads": 0,
            "reads": [],
            "writes": [],
            "n_writes": 0,
            "writes_complete": False,
            "reads_after_write": [],
            "n_reads_after_write": 0,
        }

        if first_instruction is None:
            index = 0
        else:
            index = self.instructions.index(first_instruction) + 1

        for instruction in self.instructions[index:]:
            if isinstance(instruction, LoadOperation) or isinstance(instruction, StoreOperation):
                if (
                    instruction.memory_tag != memory_tag
                    or instruction.virtual_memory_target != VirtualMemoryEnum.NON_VOLATILE
                ):
                    continue

                if isinstance(instruction, LoadOperation):
                    if len(instructions["writes"]) == 0:
                        instructions["reads"].append(instruction)
                        instructions["n_reads"] += 1
                    else:
                        instructions["reads_after_write"].append(instruction)
                        instructions["n_reads_after_write"] += 1

                if isinstance(instruction, StoreOperation):
                    if len(instructions["writes"]) > 0:
                        raise Exception("Error! Something went wrong: only one write must target NVM per basic block")

                    if from_conditional and instruction.virtual_memory_normalized:
                        # if the analysis comes from a conditional and is a normalized first write, consider it as a read
                        instructions["reads"].append(instruction)
                        instructions["n_reads"] += 1
                    else:
                        instructions["writes"].append({"basic_block": self, "instruction": instruction})
                        instructions["n_writes"] += 1
                        instructions["writes_complete"] = True

        return instructions
