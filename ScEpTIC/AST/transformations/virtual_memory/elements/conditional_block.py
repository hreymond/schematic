from .base_element import BaseASTElement
from .basic_block import BasicBlock
from .loop_block import LoopBlock

from ScEpTIC.AST.elements.instructions.memory_operations import LoadOperation, StoreOperation
from ScEpTIC.AST.transformations.virtual_memory.elements.memory_access_metadata import (
    MemoryAccessMetadata,
    CompositeMemoryAccessMetadata,
)
from ScEpTIC.AST.transformations.virtual_memory.analysis.memory_instructions_indentifier import (
    MemoryInstructionsIdentifier,
)
from ScEpTIC.AST.transformations.virtual_memory.analysis.nvm_instructions_identifier import NVMInstructionsIdentifier


class ConditionalBlock(BaseASTElement):
    """
    Representation of conditionally-executed basic blocks.
    This corresponds to a bifurcation in the CFG

               Entry Block
                ||     \\
               ||       \\
          Branch A     Branch B
               \\        ||
                \\      ||
                Exit Block
    """

    all_elements = {}

    @staticmethod
    def reset_all_elements():
        """
        Resets the list of conditional blocks
        """
        ConditionalBlock.all_elements = {}

    def __init__(self, entry_block, exit_block, paths, function_name):
        super().__init__()

        self.branches = {"A": paths[0], "B": paths[1]}
        self.label = None
        self.next = [exit_block.get_label()]
        self.prev = [entry_block.get_label()]

        self._set_node_label()

        entry_block.set_conditional_entry_label(self.label)
        exit_block.set_conditional_exit_label(self.label)

        if function_name not in self.all_elements:
            self.all_elements[function_name] = []

        self.all_elements[function_name].append(self)

        self.custom_entry_block = None
        self.custom_exit_block = None

    def _set_node_label(self):
        """
        Sets the label of the conditional block
        """
        for basic_blocks in self.branches.values():
            if len(basic_blocks) > 0:
                label = basic_blocks[0].get_label()
                self.label = label if self.label is None else min(self.label, label)

    def get_label(self):
        """
        Returns element label
        """
        return self.label

    def _get_next_labels(self):
        """
        Returns labels of the elements directly reachable from the current one
        """
        return self.next

    def _get_previous_labels(self):
        """
        Returns labels of the elements directly reachable from the current one
        """
        return self.prev

    def _add_basic_block_to_branch(self, branch_path, basic_block):
        """
        Adds a basic block to a branch
        """
        self.branches[branch_path].append(basic_block)

    def get_labels(self):
        """
        Returns the labels of all the basic blocks included inside the conditional block
        """
        all_labels = [
            label for path in self.branches.values() for basic_block in path for label in basic_block.get_labels()
        ]

        unique_labels = set(all_labels)

        return list(unique_labels)

    def flatten(self):
        """
        Returns a flatten representation of the conditional block
        """
        data = []

        if self.custom_entry_block is not None:
            data.append(self.custom_entry_block)

        for blocks in self.branches.values():
            for block in blocks:
                for element in block.flatten():
                    data.append(element)

        if self.custom_exit_block is not None:
            data.append(self.custom_exit_block)

        return data

    def calls_to(self, function_names, is_checkpoint=True):
        """
        Returns if the conditional contains a basic block that calls given functions
        """

        for basic_blocks in self.branches.values():
            for basic_block in basic_blocks:
                if basic_block.calls_to(function_names, is_checkpoint):
                    return True

        return False

    def calls_checkpoint(self, checkpoint_function_name, ignore_nested_checkpoint=False):
        """
        Returns if the conditional contains a basic block that calls a checkpoint
        """

        for basic_blocks in self.branches.values():
            for basic_block in basic_blocks:

                if not ignore_nested_checkpoint or isinstance(basic_block, BasicBlock):
                    if basic_block.calls_checkpoint(checkpoint_function_name, ignore_nested_checkpoint):
                        return True

        return False

    def has_nested_loop(self):
        """
        Returns if the element has a nested loop
        """

        for basic_blocks in self.branches.values():
            for basic_block in basic_blocks:
                if isinstance(basic_block, LoopBlock) or basic_block.has_nested_loop():
                    return True

        return False

    def get_nested_loops(self):
        """
        Returns a list with the loop blocks contained in this element (depth 1, no nested loop of nested loops)
        """
        loops = []

        for basic_blocks in self.branches.values():
            for basic_block in basic_blocks:
                if isinstance(basic_block, LoopBlock):
                    loops.append(basic_block)

                else:
                    # get nested elements
                    for loop in basic_block.get_nested_loops():
                        loops.append(loop)

        return loops

    def __str__(self):
        add_str = self.get_real_next_prev()
        retstr = f"ConditionalBlock(id={self.get_label()}, labels={self.get_labels()}, prev={self.get_previous_labels()}, next={self.get_next_labels()}{add_str})\n"

        for path_id, path in self.branches.items():
            retstr += f"  [+] Path {path_id}:\n"

            for basic_block in path:
                bb_str = str(basic_block).replace("\n", "\n        ")
                retstr += f"      - {bb_str}\n"

        return retstr

    def add_to_custom_basic_block(self, bb_type, instruction):
        """
        Adds a given instruction at the end of a custom basic block
        """
        self._create_custom_basic_block(bb_type)

        if bb_type == "entry":
            self.custom_entry_block.instructions.append(instruction)

        elif bb_type == "exit":
            self.custom_exit_block.instructions.append(instruction)

    def _create_custom_basic_block(self, bb_type):
        """
        Creates a custom basic block that contains instructions that need to be executed after or before the conditional
        """
        if bb_type == "entry":
            self._create_custom_entry_block()
        elif bb_type == "exit":
            self._create_custom_exit_block()
        else:
            raise Exception(f"Custom basic block {bb_type} does not exists!")

    def _create_custom_entry_block(self):
        """
        Creates a custom entry block, which is then merged at the end of the actual entry block
        """
        if self.custom_entry_block is None:
            self.custom_entry_block = BasicBlock(f"%conditional-entry-block-{self.get_label()}", "custom")

            # append previous labels
            for label in self._get_previous_labels():
                self.custom_entry_block.add_prev_label(label)

            if self.is_conditional_exit_block:
                label = self.conditional_exit_label[0]
                self.custom_entry_block.set_conditional_exit_label(label)

            self.custom_entry_block.add_next_label(self.get_label())
            self.custom_entry_block.merge_in_previous = True

    def _create_custom_exit_block(self):
        """
        Creates a custom exit block, which is then merged at the beginning of the actual exit block
        """
        if self.custom_exit_block is None:
            self.custom_exit_block = BasicBlock(f"%conditional-exit-block-{self.get_label()}", "custom")

            # append next labels
            for label in self._get_next_labels():
                self.custom_exit_block.add_next_label(label)

            self.custom_exit_block.add_prev_label(self.get_label())
            self.custom_exit_block.merge_in_subsequent = True

    def get_memory_tag_first_reads_writes(self, only_nvm=False):
        """
        Returns two dictionaries which contains the first reads and writes instructions for each memory tag
            - reads: contains the memory reads operations that happen before the first write
            - writes: contains the first memory write instruction
        """
        branch_a_reads, branch_a_writes = MemoryInstructionsIdentifier.resolve_first_reads_writes(
            self.branches["A"], only_nvm, self, "A"
        )
        branch_b_reads, branch_b_writes = MemoryInstructionsIdentifier.resolve_first_reads_writes(
            self.branches["B"], only_nvm, self, "B"
        )

        return self._merge_memory_tag_reads_writes(branch_a_reads, branch_a_writes, branch_b_reads, branch_b_writes)

    def get_memory_tag_last_reads_writes(self, only_nvm=False):
        """
        Returns two dictionaries which contains the last reads and writes instructions for each memory tag
            - reads: contains the memory reads operations that happen after the last write
            - writes: contains the last memory write instruction
        """
        branch_a_reads, branch_a_writes = MemoryInstructionsIdentifier.resolve_last_reads_writes(
            self.branches["A"], only_nvm, self, "A"
        )
        branch_b_reads, branch_b_writes = MemoryInstructionsIdentifier.resolve_last_reads_writes(
            self.branches["B"], only_nvm, self, "B"
        )

        return self._merge_memory_tag_reads_writes(branch_a_reads, branch_a_writes, branch_b_reads, branch_b_writes)

    def _merge_memory_tag_reads_writes(self, branch_a_reads, branch_a_writes, branch_b_reads, branch_b_writes):
        """
        Merges the read/write information of two branches
        """
        reads = {}
        writes = {}

        for memory_tag, metadata in branch_a_reads.items():
            reads[memory_tag] = metadata

        for memory_tag, metadata in branch_b_reads.items():
            if memory_tag in reads:
                for element in metadata:
                    reads[memory_tag].append(element)
            else:
                reads[memory_tag] = metadata

        for memory_tag, metadata in branch_a_writes.items():
            missing_metadata = MemoryAccessMetadata(None, None, None, memory_tag, True)
            missing_metadata.set_conditional_container(self, "B")

            writes[memory_tag] = [metadata, missing_metadata]

        for memory_tag, metadata in branch_b_writes.items():
            if memory_tag not in writes:
                missing_metadata = MemoryAccessMetadata(None, None, None, memory_tag, True)
                missing_metadata.set_conditional_container(self, "A")

                writes[memory_tag] = [missing_metadata, metadata]
            else:
                # overwrites missing marker
                writes[memory_tag][1] = metadata

        return reads, writes

    def get_all_instructions_targeting_nvm_after(self, indexes, memory_tag):
        if isinstance(indexes, list):
            branch_a_data = self._get_all_branch_instructions_targeting_nvm(memory_tag, self.branches["A"], indexes[0])
            branch_b_data = self._get_all_branch_instructions_targeting_nvm(memory_tag, self.branches["B"], indexes[1])

        else:
            branch_a_data = self._get_all_branch_instructions_targeting_nvm(memory_tag, self.branches["A"], None)
            branch_b_data = self._get_all_branch_instructions_targeting_nvm(memory_tag, self.branches["B"], None)

        data = {
            "n_reads": min(branch_b_data["n_reads"], branch_b_data["n_reads"]),
            "reads": branch_a_data["reads"] + branch_b_data["reads"],
            "writes": branch_a_data["writes"] + branch_b_data["writes"],
            "n_writes": max(branch_a_data["n_writes"], branch_b_data["n_writes"]),
            "writes_complete": branch_a_data["writes_complete"] and branch_b_data["writes_complete"],
            "reads_after_write": branch_a_data["reads_after_write"] + branch_b_data["reads_after_write"],
            "n_reads_after_write": min(branch_b_data["n_reads_after_write"], branch_b_data["n_reads_after_write"]),
        }

        return data

    def _get_all_branch_instructions_targeting_nvm(self, memory_tag, branch, initial_instruction):
        instructions = {
            "n_reads": 0,
            "reads": [],
            "writes": [],
            "n_writes": 0,
            "writes_complete": False,
            "reads_after_write": [],
            "n_reads_after_write": 0,
        }

        if initial_instruction is not None:
            index = branch.index(initial_instruction.basic_block)
            instruction_index = initial_instruction.instruction
        else:
            index = 0
            instruction_index = None

        for instruction in branch[index:]:
            # if type(instruction) == ConditionalBlock:
            #     data = instruction.get_all_instructions_targeting_nvm_after(instruction_index, memory_tag)
            # else:
            data = instruction.get_all_instructions_targeting_nvm_after(
                instruction_index, memory_tag, from_conditional=True
            )

            if instructions["n_writes"] == 0:
                instructions["n_reads"] += data["n_reads"]
                instructions["n_writes"] += data["n_writes"]
                instructions["n_reads_after_write"] += data["n_reads_after_write"]
                instructions["writes_complete"] = data["writes_complete"]

                for element in data["reads"]:
                    instructions["reads"].append(element)

                for element in data["writes"]:
                    instructions["writes"].append(element)

                for element in data["reads_after_write"]:
                    instructions["reads_after_write"].append(element)
            else:
                if data["n_writes"] > 0:
                    raise Exception("Error! Something went wrong: only one write must target NVM per basic block")

                instructions["n_reads_after_write"] += data["n_reads"]

                for element in data["reads_after_write"]:
                    instructions["reads"].append(element)

            instruction_index = None

        return instructions

    def get_all_instructions_targeting_nvm(self, surrounding_sequence):
        """
        Returns a list of all the instructions that target NVM contained in the element

        Implementation limitation: no nested conditionals (not needed for evaluation)
        TODO: implement for nested conditionals
        """

        nvm_instructions = {}

        nvm_instructions_a = NVMInstructionsIdentifier.identify_nvm_instruction(self.branches["A"], self, "A")
        nvm_instructions_b = NVMInstructionsIdentifier.identify_nvm_instruction(self.branches["B"], self, "B")

        for memory_tag, metadata in nvm_instructions_a.items():
            nvm_instructions[memory_tag] = []

            if memory_tag in nvm_instructions_b:
                combined_metadata = self._get_combined_metadata(
                    memory_tag, nvm_instructions_a[memory_tag], nvm_instructions_b[memory_tag]
                )
                combined_metadata.set_surrounding_sequence(surrounding_sequence)
                combined_metadata.set_conditional_container(self, None)
                nvm_instructions[memory_tag].append(combined_metadata)

            for element in metadata:
                nvm_instructions[memory_tag].append(element)

        for memory_tag, metadata in nvm_instructions_b.items():
            if memory_tag not in nvm_instructions:
                nvm_instructions[memory_tag] = []

            for element in metadata:
                nvm_instructions[memory_tag].append(element)

        return nvm_instructions

    def _get_combined_metadata(self, memory_tag, branch_a, branch_b):
        """
        Returns combined metadata, consdiering instruction priority:
         I need to consider the instructions of a branch as a "single" if both branches accesses NVM
         However, I need to prioritize which elements to consider
         1. Store / Store
            - all loads before will be part of a different analysis of single branch or previous basic blocks
              as the store in both branches identifies the "last" store (all data previous to store are part of different analysis / domain)
         2. Load / Store - Store/Load
            - one branch only has a STORE -> all loads prior to such a branch are ignored for the same reason of 1.
         3. Load / Load
            - no branch has a write -> consider load
        """
        branch_a_read, branch_a_write = self._get_first_read_write(branch_a)
        branch_b_read, branch_b_write = self._get_first_read_write(branch_b)

        if branch_a_write is not None and branch_b_write is not None:
            return self._gen_composite_metadata(memory_tag, [branch_a_write, branch_b_write], branch_a_write)

        elif branch_a_write is not None and branch_b_read is not None:
            return self._gen_composite_metadata(memory_tag, [branch_a_write, branch_b_read], branch_b_read)

        elif branch_a_read is not None and branch_b_write is not None:
            return self._gen_composite_metadata(memory_tag, [branch_a_read, branch_b_write], branch_a_read)

        else:
            return self._gen_composite_metadata(memory_tag, [branch_a_read, branch_b_read], branch_a_read)

    def _gen_composite_metadata(self, memory_tag, elements, instruction):
        """
        Generate composite metadata of a branch
        """
        element = CompositeMemoryAccessMetadata(memory_tag, self, instruction)
        element.elements = elements
        return element

    def _get_first_read_write(self, branch_metadata):
        """
        Returns the first read and write instructions of a branch that target NVM
        """
        read = None
        write = None

        for element in branch_metadata:
            if isinstance(element.instruction, LoadOperation) and read is None:
                read = element

            if isinstance(element.instruction, StoreOperation):
                write = element
                break

        # adjust for normalized write (prioritize read if write is normalized)
        if read is not None and write is not None:
            if write.instruction.virtual_memory_normalized:
                write = None

        return read, write
