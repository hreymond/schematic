from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.virtual_memory.elements.memory_access_metadata import CompositeMemoryAccessMetadata


class NVMInstructionsIdentifier:
    """
    Class for identifying specific sets of NVM read/write instructions
    """

    @staticmethod
    def identify_nvm_instruction(basic_blocks, conditional_container=None, conditional_path=None):
        """
        Identifies all the NVM read/write instructions in basic_block
        """
        data = {}

        for basic_block in basic_blocks:
            from ScEpTIC.AST.transformations.virtual_memory.elements.basic_block import BasicBlock

            BasicBlock.str_print_instructions = True
            nvm_instructions = basic_block.get_all_instructions_targeting_nvm(basic_blocks)

            for memory_tag, metadata in nvm_instructions.items():
                if memory_tag not in data:
                    data[memory_tag] = []

                for element in metadata:
                    data[memory_tag].append(element)
                    element.set_conditional_container(conditional_container, conditional_path)

        return data

    @staticmethod
    def identify_nvm_instructions_to_consolidate(metadata, n_min_function):

        basic_blocks = metadata.surrounding_sequence
        target_block = metadata.basic_block

        try:
            index = basic_blocks.index(target_block)
        except:
            raise Exception(
                "Error! Container basic block not identified in the surrounding sequence. Nested conditionals currently not supported!"
            )

        memory_tag = metadata.memory_tag

        first_element = metadata

        candidate_initial_elements = NVMInstructionsIdentifier._extract_first_elements(first_element)
        candidate_reads = []
        n_reads = 0
        n_writes_for_volatile_copy = 1

        initial_elements_to_consolidate = []
        reads_to_consolidate = []

        for basic_block in basic_blocks[index:]:
            data = NVMInstructionsIdentifier._extract_metadata(basic_block, memory_tag, first_element)

            for read in data["reads"]:
                candidate_reads.append(read)

            n_reads += data["n_reads"]

            # write reached - check for application conditions
            if data["n_writes"] > 0:
                # check application criteria and apply
                (
                    n_reads,
                    n_writes_for_volatile_copy,
                    candidate_initial_elements,
                    candidate_reads,
                ) = NVMInstructionsIdentifier._check_and_consolidate(
                    n_reads,
                    n_writes_for_volatile_copy,
                    candidate_initial_elements,
                    initial_elements_to_consolidate,
                    candidate_reads,
                    reads_to_consolidate,
                    n_min_function,
                )

                # write certainly execute (in basic block or, if in conditional block, both branches has the write)
                # -> stop
                if data["writes_complete"]:
                    break

                # conditional write -> add new writes to candidate initial elements
                for element in data["writes"]:
                    candidate_initial_elements.append(element)

                # add reads after write to candidate reads
                for element in data["reads_after_write"]:
                    candidate_reads.append(element)

                # account for new candidate reads
                n_reads += data["n_reads_after_write"]

                # account for new writes
                n_writes_for_volatile_copy += data["n_writes"]

            first_element = None

        # check application criteria and apply (required as we may not encounter a write)
        NVMInstructionsIdentifier._check_and_consolidate(
            n_reads,
            n_writes_for_volatile_copy,
            candidate_initial_elements,
            initial_elements_to_consolidate,
            candidate_reads,
            reads_to_consolidate,
            n_min_function,
        )

        return initial_elements_to_consolidate, reads_to_consolidate

    @staticmethod
    def _extract_metadata(basic_block, memory_tag, first_element=None):
        if first_element is not None:
            basic_block = first_element.basic_block

            if isinstance(first_element, CompositeMemoryAccessMetadata):
                index = first_element.elements
            else:
                index = first_element.instruction

        else:
            index = None

        return basic_block.get_all_instructions_targeting_nvm_after(index, memory_tag)

    @staticmethod
    def _extract_first_elements(first_element):
        first_elements = []

        if isinstance(first_element, CompositeMemoryAccessMetadata):
            for element in first_element.elements:
                data = {"basic_block": element.basic_block, "instruction": element.instruction}
                first_elements.append(data)
        else:
            data = {"basic_block": first_element.basic_block, "instruction": first_element.instruction}
            first_elements.append(data)

        return first_elements

    @staticmethod
    def _check_and_consolidate(
        n_reads,
        n_writes_for_volatile_copy,
        candidate_initial_elements,
        initial_elements_to_consolidate,
        candidate_reads,
        reads_to_consolidate,
        n_min_function,
    ):
        # check application criteria
        if n_reads > n_min_function(n_writes_for_volatile_copy):
            # insert candidate elements to consolidate list
            for element in candidate_initial_elements:
                initial_elements_to_consolidate.append(element)

            for element in candidate_reads:
                reads_to_consolidate.append(element)

            # reset candidates
            candidate_initial_elements = []
            candidate_reads = []
            n_reads = 0
            n_writes_for_volatile_copy = 0

        return (n_reads, n_writes_for_volatile_copy, candidate_initial_elements, candidate_reads)

    @staticmethod
    def already_processed(metadata):
        """
        Returns if the metadata has already been processed due to instructions included in previous instructions
        """

        if isinstance(metadata, CompositeMemoryAccessMetadata):
            for element in metadata.elements:
                if (
                    element.instruction.virtual_memory_target == VirtualMemoryEnum.VOLATILE
                    or element.instruction.has_virtual_memory_copy
                ):
                    return True

            return False

        else:
            return (
                metadata.instruction.virtual_memory_target == VirtualMemoryEnum.VOLATILE
                or metadata.instruction.has_virtual_memory_copy
            )
