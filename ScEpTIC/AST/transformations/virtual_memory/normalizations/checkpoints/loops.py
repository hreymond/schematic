from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.transformations.virtual_memory.elements.loop_block import LoopBlock
from ScEpTIC.AST.transformations.virtual_memory.elements.basic_block import BasicBlock


class CheckpointLoopNormalizer:
    """
    Normalization pass for normalizing checkpoint calls inside loops
    """

    def __init__(self, computation_interval_splitter):
        self.splitter = computation_interval_splitter

    def normalize(self):
        """
        Normalizes all the loops containing a checkpoint
        """
        for name in LoopBlock.all_elements.keys():
            self._normalize(name)

    def _normalize(self, name):
        """
        Normalizes all the loops containing a checkpoint, whose execution happens inside the given function
        """
        loops = LoopBlock.all_elements[name]

        for loop in loops:
            # loop calls a checkpoint
            if loop.calls_to(self.splitter.checkpoint_functions):
                self._normalize_loop_checkpoints(loop)

    def _normalize_loop_checkpoints(self, loop):
        """
        Normalizes a given loop, which directly/indirectly contains a checkpoint.
        This methods applies the following transformations:
            (i) moves the last checkpoint at the end of the loop latch (or inserts a new checkpoint)
            (ii) adds a checkpoint before the loop header, in a custom loop pre-header
        """

        # if the loop does not contain any checkpoint, skip
        if not loop.calls_to(self.splitter.checkpoint_functions):
            raise Exception("Error! No checkpoint found inside loop")

        # If the loop does not directly contain a checkpoint -> create a new one
        # note that this may be the case of a loop that contains a nested loop, which contains a checkpoint
        if not loop.calls_checkpoint(self.splitter.checkpoint_function_name, True):
            checkpoint = self.splitter.create_checkpoint()

        # remove the last checkpoint of the loop
        else:
            last_checkpoint = self._get_loop_last_checkpoint(loop)
            target_basic_block = last_checkpoint["basic_block"]
            checkpoint = target_basic_block.instructions.pop(last_checkpoint["index"])

            # Adjust label
            if checkpoint.label is not None:
                target_basic_block.instructions[0].label = checkpoint.label
                checkpoint.label = None

        # place a checkpoint at the end of the latch, before the jump instruction
        last_latch_block_index = sorted(loop.blocks["latch"].keys())[-1]
        last_latch_block = loop.blocks["latch"][last_latch_block_index]
        checkpoint.basic_block_id = last_latch_block.instructions[0].basic_block_id
        last_instruction_index = len(last_latch_block.instructions) - 1
        last_latch_block.instructions.insert(last_instruction_index, checkpoint)

        # Adjust label
        if last_instruction_index == 0:
            last_latch_block.instructions[0].label = last_latch_block.instructions[1].label
            last_latch_block.instructions[1].label = None

        # append checkpoint before header
        checkpoint = self.splitter.create_checkpoint()
        loop.add_to_custom_preheader(checkpoint)

    def _get_loop_last_checkpoint(self, loop):
        """
        Returns the last checkpoint contained in the loop
        If no checkpoint found, returns a None value
        Otherwise, a dictionary {"basic_block", "index"}
        """
        section_order = ["header", "body", "latch"]
        last_checkpoint = None

        # traverse the basic block in order of execution, so to identify the last checkpoint
        for section in section_order:
            for basic_block_index in sorted(loop.blocks[section].keys()):
                basic_block = loop.blocks[section][basic_block_index]

                # if the element is a LoopBlock or ConditionalBlock that will execute a checkpoint -> remove last checkpoint info
                # (current last checkpoint not directly contained in a basic block of the loop)
                if not isinstance(basic_block, BasicBlock) and basic_block.calls_to(self.splitter.checkpoint_functions):
                    last_checkpoint = None

                # if the basic block contains a direct call to a checkpoint (i.e. no nested loop / conditional with checkpoint)
                elif isinstance(basic_block, BasicBlock):
                    for index, instruction in enumerate(basic_block.instructions):
                        if isinstance(instruction, CallOperation):

                            # if it calls a checkpoint -> update last info
                            if instruction.name == self.splitter.checkpoint_function_name:
                                last_checkpoint = {"basic_block": basic_block, "index": index}

                            # if it calls a function that calls a checkpoint -> remove last checkpoint info
                            # (current last checkpoint not directly contained in a basic block of the loop)
                            elif instruction.name in self.splitter.checkpoint_functions:
                                last_checkpoint = None

        return last_checkpoint
