from ScEpTIC.AST.transformations.virtual_memory.analysis.memory_usage import MemoryUsage
from ScEpTIC.AST.transformations.virtual_memory.parsers.ast_parser import ASTParser
from ScEpTIC.AST.transformations.virtual_memory.parsers.memory_tags_parser import MemoryTagsParser
from ScEpTIC.AST.transformations.virtual_memory.computation_intervals_manager import ComputationIntervalsManager
from ScEpTIC.AST.transformations.virtual_memory.elements.loop_block import LoopBlock

from ScEpTIC.AST.transformations.virtual_memory.analysis.registers_usage import RegistersUsage


def apply_transformation(functions, vmstate):
    config = vmstate.transformation_options["virtual_memory"]
    main_function_name = vmstate._main_function_name
    n_min_function = config["n_min_function"]
    memory_optimization_enabled = config["memory_optimization"]

    parser = ASTParser(functions)
    parser.parse()

    mem_parser = MemoryTagsParser(parser)
    mem_parser.resolve_memory_tags()

    comp_int_manager = ComputationIntervalsManager(
        parser, mem_parser, vmstate.vm.checkpoint_manager.routine_names["checkpoint"], n_min_function, main_function_name
    )
    comp_int_manager.split_computation_intervals()
    comp_int_manager.apply_virtual_memory_transformations(memory_optimization_enabled)

    if config["optimize_saved_registers"]:
        RegistersUsage.set_checkpoints_regs(comp_int_manager)

    comp_int_manager.synchronize_to_ast()
    parser.syncrhonize_ast_to_functions()
    # comp_int_manager.print_computation_intervals()
