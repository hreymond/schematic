from ScEpTIC.AST.elements.instructions.termination_instructions import BranchOperation
from ScEpTIC.AST.transformations.virtual_memory.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.virtual_memory.normalizations.labels_normalizer import LabelsNormalizer


class BasicBlocksParser:
    """
    Module for parsing the AST into a sequence of basic blocks
    """

    def __init__(self, function, function_name):
        self.function = function
        self.function_name = function_name

    def parse_ast(self):
        """
        Parses the AST and identifies all the BasicBlocks
        """
        basic_blocks = {}
        basic_block = None

        # Split into basic blocks
        for instruction in self.function.body:
            # If the instruction has a label -> first instruction of the basic block
            if instruction.label is not None:
                label = BasicBlock.label_to_int(instruction.label)
                basic_block = BasicBlock(label, self.function_name)
                basic_blocks[label] = basic_block

            if isinstance(instruction, BranchOperation):
                if instruction.target_true is not None:
                    basic_block.add_next_label(instruction.target_true)

                if instruction.target_false is not None:
                    basic_block.add_next_label(instruction.target_false)

            basic_block.add_instruction(instruction)

        # fill previous basic block information
        for label, basic_block in basic_blocks.items():
            for next_label in basic_block.get_next_labels():
                basic_blocks[next_label].add_prev_label(label)

        basic_blocks = LabelsNormalizer.normalize_labels([x for x in basic_blocks.values()])
        BasicBlock.all_elements[self.function_name] = [bb for bb in basic_blocks]

        return basic_blocks
