from copy import copy
from typing import Dict

import networkx as nx

from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import CheckpointTypeEnum


class TopologicalIterator:
    """
    An iterator over the basic blocks of a CFG in the program topological order.

    The program topological order is the topological order of a CFG where the backedges have been removed.
    """

    def __init__(self, cfg: CFG):
        # Copy the cfg
        reduced_cfg = cfg.copy()
        # Remove the backedges of the cfg
        for bb in reduced_cfg.nodes():
            if bb.loop is not None and reduced_cfg.has_edge(bb.loop.latch[-1], bb.loop.header[0]):
                reduced_cfg.remove_edge(bb.loop.latch[-1], bb.loop.header[0])
        # The graph should be acyclic, as we removed all the backedges
        if not nx.is_directed_acyclic_graph(reduced_cfg):
            raise Exception("[PFI] This error should not appear. Maybe a wrong identification of backedges ?")
        self.bbs = nx.topological_sort(reduced_cfg)

    def __iter__(self):
        return self

    def __next__(self):
        bb = next(self.bbs)
        if bb is None:
            raise StopIteration
        return bb


class PFITransformation:
    """
    The PFI SAT-based region boundary placement algorithm.

    # Configuration

    The PFI transformation can be configured using the following parameters:
    - `checkpoint_cost`: The cost of a checkpoint in the energy model
    - `energy_budget`: The energy of a full capacitor.
    """

    def __init__(self, config: Dict[str, any]):
        self.checkpoint_cost = config["checkpoint_cost"]
        if "energy_budget" not in config:
            raise Exception("The parameter 'energy_budget' is not defined in the config file for PFI transformation")
        self.energy_budget = config["energy_budget"]

    def apply_sat_region_formation_to_cfg(self, cfg: CFG):
        """
        Apply the SAT-region formation algorithm to the given cfg.

        :param cfg: The cfg to apply the SAT-region formation algorithm to

        The SAT-region formation algorithm is described in the paper "Compiler-Directed High-Performance Intermittent
        Computation with Power Failure Immunity" by Choi, Kittinger, Liu and Jung (RTAS'22).

        The algorithm is as follows:
        1. Set the number of cycles consumed by each basic block to its cost + the cost of the checkpoint and set the
              number of cycles accumulated so far to 0.
        2. Traverse the graph in topological order. For each basic block:
            1. Compute the number of cycles accumulated so far
            2. If the number of cycles accumulated so far is greater than the energy budget, place a region boundary
            3. Compute the number of cycles accumulated after execution of this basic block
            4. Update the successor basic blocks with the number of cycles accumulated so far

        Note that we don't handle the basic block splitting part of the algorithm yet as we believe it is unlikely that
        a basic block will have a cost greater than the energy budget.
        """
        # Set the number the cycles needed by the basic block to its cost + the cost of the checkpoint
        for bb in cfg.nodes():
            setattr(bb, "cycles", bb.cost_all_nvm + self.checkpoint_cost)
            setattr(bb, "income_cycles", 0)
            if not hasattr(bb, "is_region_boundary"):
                setattr(bb, "is_region_boundary", False)
        # Traverse the graph in topological order
        for bb in TopologicalIterator(cfg):
            # Compute the number of cycles accumulated so far
            if bb.is_region_boundary:
                accum_cycles = bb.cycles
            else:
                accum_cycles = bb.cycles + bb.income_cycles
            # If the number of cycles accumulated so far is greater than the energy budget, we need to place a region
            if accum_cycles > self.energy_budget:
                bb.is_region_boundary = True
                accum_cycles = bb.cycles + self.checkpoint_cost
            # If accum_cycles is still greater than the energy budget, it means the basic block cost is greater than the
            # energy budget. We don't handle this case yet.
            if accum_cycles > self.energy_budget:
                raise NotImplementedError("Cannot handle basic block {} with a cost greater than the energy budget yet.")
            # Update the income cycles of the successors
            for succ in cfg.successors(bb):
                succ.income_cycles = max(accum_cycles, succ.income_cycles)

    def apply_sat_region_formation(self, cfgs: Dict[str, CFG], function_depgraph: nx.DiGraph):
        """
        Apply the SAT-region formation algorithm to each cfg in the cfgs dict.
        """
        # We need to process callee functions before their callers so we rely a reverse topological sort of the
        # function dependency tree to find the order (leaves first)
        function_order = list(reversed(list(nx.topological_sort(function_depgraph))))
        for fn_name in function_order:
            if fn_name not in cfgs:
                raise Exception(
                    "The function {} could not be found in the parsed cfg.\n"
                    "Functions found: {}".format(fn_name, ", ".join(cfgs.keys()))
                )
            # Apply the SAT-region formation algorithm to the cfg
            self.apply_sat_region_formation_to_cfg(cfgs[fn_name])
            # # Update the cost of the basic blocks containing calls to the callee functions
            # for pred in function_depgraph.predecessors(fn_name):
            #     if "dependencies" in function_depgraph[pred][fn_name]:
            #         for dependency in function_depgraph[pred][fn_name]["dependencies"]:
            #             dependency.cost_all_nvm += cfgs[fn_name].last_bb.income_cycles + cfgs[fn_name].last_bb.cycles

    def _add_checkpoint_call(self, bb: BasicBlock):
        """
        Add a checkpoint call to the given basic block.

        :param bb: The basic block to add a checkpoint call to
        """
        # Create a call instruction
        call = CallOperation(None, "@checkpoint", None, None, None, {})
        # Save old label
        label = bb.instructions[0].label
        bb.instructions[0].label = None
        # Add the call instruction to the basic block
        bb.instructions.insert(0, call)
        call.label = label

    def add_checkpoint_calls(self, cfgs):
        for cfg in cfgs.values():
            for bb in cfg.nodes():
                if bb.is_region_boundary:
                    self._add_checkpoint_call(bb)
