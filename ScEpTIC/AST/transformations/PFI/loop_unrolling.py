import logging
from copy import copy

from ScEpTIC.AST.misc.trace import LLVMNaturalLoopBB
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.utils.cfg_utils import save_cfg

MAX_UNROLL_FACTOR = 10


def unroll_one(original_loop: LLVMNaturalLoopBB, unrolled_loop: LLVMNaturalLoopBB, cfg: CFG):
    """
    Unroll the loop unrolled_loop once, from the description of the not-unrolled loop original_loop.

    Updates unrolled_loop and the cfg.

    :param original_loop: The loop object describing the loop parameters while not unrolled
    :param unrolled_loop: The loop object holding the unrolled loop parameters
    :param cfg: The cfg containing the loop

    # Example:

    ## Input:

    The following cfg:
    bb0 -> bb1 -> bb2
           ^-------/

    ## Output:

    The following cfg:
    bb0 -> bb1 -> bb2 -> bb1' -> bb2'
             ^---------------------/

    and the following loop object:
    -  original_loop:  LLVMNaturalLoopBB([bb1], [], [bb1, bb2], [bb2], 0)
    -  unrolled_loop:  LLVMNaturalLoopBB([bb1], [], [bb1, bb2, bb1', bb2'], [bb2'], 1)

    ## Code:

    ```python

    loop = LLVMNaturalLoopBB([bb1], [], [bb1, bb2], [bb2], 0)
    original_loop = copy(loop)
    unroll_loop(origin, cfg, 1)
    ```
    """
    # Extract the body of the loop (the loop not unrolled)
    body_cfg = cfg.subgraph(original_loop.basic_blocks)
    save_cfg(f"body_cfg_{unrolled_loop.unroll_factor}", body_cfg)

    # The header of the loop will still be the same
    header = original_loop.header[0]
    # The latch of the loop needs to be updated at every unrolling
    latch = unrolled_loop.latch[-1]

    # Update the unroll factor in the loop object
    if unrolled_loop.unroll_factor is None:
        unrolled_loop.unroll_factor = 0
    unrolled_loop.unroll_factor += 1

    # Clone the body of the loop and add it to the cfg
    cloned = {}
    bb: BasicBlock
    for bb in original_loop.basic_blocks:
        bb_copy = bb.clone()
        bb_copy.llvm_label = str(bb.llvm_label) + "_cloned" + str(unrolled_loop.unroll_factor)
        cfg.add_node(bb_copy)
        unrolled_loop.basic_blocks.append(bb_copy)
        cloned[bb] = bb_copy

    # Recreate the links between the cloned basic blocks
    for edge in body_cfg.edges:
        # If the edge is the back-edge, we don't need to recreate it
        if edge[0] == latch and edge[1] == header:
            continue
        # First, find the cloned bb linked to the edge
        source = cloned[edge[0]]
        target = cloned[edge[1]]
        # Then, add the edge to the cfg
        cfg.add_edge(source, target)
        # Finally, update the target of the last instruction of the source cloned basic block to match the label
        # of the destination basic block
        if source.instructions[-1].target_true == "%" + str(edge[1].label):
            source.instructions[-1].target_true = "%" + str(target.label)
        if source.instructions[-1].target_false == "%" + str(edge[1].label):
            source.instructions[-1].target_false = "%" + str(target.label)

    # New latch of the loop
    new_latch = cloned[original_loop.latch[-1]]
    # Cloned header of the loop, to be updated since it may still be pointing to a basic block outside the loop
    unrolled_header = cloned[header]
    update_cloned_header_branch(unrolled_header, unrolled_loop)

    # Update the branch of the old latch to point to the cloned header,
    latch.instructions[-1].target_true = unrolled_header.label_str
    cfg.add_edge(latch, unrolled_header)
    cfg.remove_edge(latch, header)

    # Update the branch of the new latch to point to the loop header
    new_latch.instructions[-1].target_true = header.label_str
    cfg.add_edge(new_latch, header)

    # Update the loop object, to keep track of the new latch
    unrolled_loop.latch = []
    for bb in list(original_loop.latch):
        unrolled_loop.latch.append(cloned[bb])


def update_cloned_header_branch(cloned_header: BasicBlock, unrolled_loop: LLVMNaturalLoopBB):
    """
    After unrolling the loop, the branch of the cloned header may be pointing to a basic block outside the loop.

    This function updates the branch of the cloned header to remove any link to a basic block outside the loop.
    """
    # Update the header of the loop, if any exiting basic block is linked
    cloned_header_branch = cloned_header.instructions[-1]

    loops_bb_labels = [bb.label_str for bb in unrolled_loop.basic_blocks]
    # If the branch of the header was pointing to a target outside the loop, we need to force the branch to target
    # the next basic block of the loop
    if cloned_header_branch.target_false not in loops_bb_labels:
        cloned_header_branch.target_false = None
        cloned_header_branch.condition = None
        cloned_header_branch.tick_count = 0
    if cloned_header_branch.target_true not in loops_bb_labels:
        cloned_header_branch.target_true = cloned_header_branch.target_false
        cloned_header_branch.condition = None
        cloned_header_branch.tick_count = 0
    if cloned_header_branch.target_true is None:
        raise Exception(
            "No target for a branch when unrolling a loop. This should not happen."
            "Maybe the loop header was not targeting a basic block inside the loop ?"
        )


def is_not_exiting_from_header(loop) -> bool:
    """
    Check if the loop is exiting from a basic block different from the header.

    :return: True if the loop is exiting from a basic block different from the header, False otherwise
    """
    if len(loop.exiting) > 0:
        if loop.exiting[0] != loop.header[0]:
            return True
    return False


def unroll_loop(loop: LLVMNaturalLoopBB, cfg: CFG, unroll_factor: int):
    """
    Unroll a loop by a given factor.

    :param loop: The loop to unroll
    :param cfg: The cfg of the function containing the loop
    :param unroll_factor: The factor by which the loop should be unrolled

    :raise NotImplementedError: If the loop is exiting from a basic block different from the header

    see unroll_one function for more details on how the unrolling is done
    """
    # Check if we are exiting from another basic block than the header
    if is_not_exiting_from_header(loop):
        raise NotImplementedError(
            f"Cannot unroll a loop exiting from a basic block different from the header"
            f"in {cfg.name}, exiting a loop at {loop.exiting[0].llvm_label}"
        )

    original_loop = copy(loop)
    for i in range(unroll_factor):
        unroll_one(original_loop, loop, cfg)


def compute_loop_cost(loop: LLVMNaturalLoopBB, checkpoint_cost: int):
    """
    Compute the cost of the loop.
    """
    sum = 0
    for bb in loop.basic_blocks:
        sum += bb.cost_all_nvm + checkpoint_cost
    return sum


def unroll_loops(cfg: CFG, energy_budget: int, checkpoint_cost: int):
    """
    Unroll the loops of the CFG if possible to reduce the number of checkpoints per loop iteration.

    Loop unrolling is performed if:
    - There is an annotation of the number of iterations of the loop in the body of the loop (__nb_iter(n))
    - The cost of the loop is lower than the energy budget of the platform
    - The loop is exiting from the header, or the loop is never exiting
    - The loop is at the same depth as the innermost loop of the function

    The loop unrolling factor is the smallest number so that the loop cost stays lower than the energy budget and
    the unrolling factor is a divisor of the number of iterations of the loop.

    :param cfg: The CFG of the function to unroll
    :param energy_budget: The energy budget of the platform
    :param checkpoint_cost: The cost of a checkpoint
    """
    # Get the loops of the CFG
    loops = {}
    for bb in cfg.nodes:
        if bb.loop is not None:
            loops[bb.loop.name] = bb.loop

    if len(loops) == 0:
        return
    # Get the loop with the maximum depth
    max_depth = max([loop.depth for loop in loops.values()])

    loops_to_unroll = filter(lambda loop: loop.depth == max_depth, loops.values())
    for loop in loops_to_unroll:
        # If the number of iterations is not known, we cannot unroll the loop
        if loop.nb_iter is None:
            logging.info(
                f"[PFI] Cannot unroll loop {loop.header[0].llvm_label} in {cfg.name} because "
                f"the number of iterations is not known"
            )
            continue

        if is_not_exiting_from_header(loop):
            logging.info(
                f"[PFI] Cannot unroll loop {loop.header[0].llvm_label} in {cfg.name} because "
                f"the loop is exiting from a basic block different from the header"
            )
            continue
        # Compute the maximum number of iterations we can make with the energy budget
        loop_cost = compute_loop_cost(loop, checkpoint_cost)
        max_iter = int(energy_budget // loop_cost)
        # If the loop consume more energy than avaible, we don't unroll it
        if max_iter == 0:
            continue

        # We don't want to unroll the loop more than MAX_UNROLL_FACTOR times
        if max_iter > MAX_UNROLL_FACTOR:
            max_iter = MAX_UNROLL_FACTOR

        unroll_factor = None
        # If we can do more iteration than needed, we unroll the loop at maximum
        if max_iter > loop.nb_iter:
            unroll_factor = loop.nb_iter
        # Else, we unroll the loop at the maximum possible.
        else:
            # The maximum unroll factor must be inferior or equal to max_iter and being a divisor of the number of
            # loop iterations
            for i in range(max_iter, 0, -1):
                if loop.nb_iter % i == 0:
                    unroll_factor = i
                    break

        if loop.nb_iter % unroll_factor != 0:
            raise Exception(
                f"Safety check failed, the unroll factor {unroll_factor} is not a divisor of the number "
                f"of iterations {loop.nb_iter}"
            )
        logging.info(f"[PFI] Unrolling loop {loop.header[0].llvm_label} in {cfg.name} with a factor of {unroll_factor}")
        unroll_loop(loop, cfg, unroll_factor - 1)
