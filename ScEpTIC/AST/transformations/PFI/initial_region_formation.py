from collections import deque
from typing import Dict

import networkx as nx

from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.transformations.schematic.cfg_modification import add_free_branch
from ScEpTIC.AST.transformations.schematic.elements.basic_block import BasicBlock
from ScEpTIC.AST.transformations.schematic.elements.cfg import CFG
from ScEpTIC.AST.transformations.schematic.elements.checkpoint import Checkpoint
from ScEpTIC.AST.transformations.schematic.parsers.cfg_parser import generate_unique_bb_id


def isolate_function_call_at_index(cfg: CFG, bb, i):
    """
    Isolate a function call at the `i`th instruction by splitting its basic block `bb` in three separate basic blocks.

    The first basic block will contain the instructions before the call. The second basic block will contain
    the call instruction. The third basic block contains the instructions after the instruction at index `i`.

    Fake branches are added to the first and second basic blocks to connect the three basic blocks. The basic block
    containing the function call is tagged as a function call basic block.

    :param cfg: The CFG containing the basic block to isolate an instruction from
    :param bb: The basic block to isolate an instruction from
    :param i: The index of the function call to isolate
    """
    if len(bb.instructions) <= i:
        raise ValueError("The basic block {} does not contain an instruction at index {}".format(bb.label, i))
    call = bb.instructions[i]
    if type(call) != CallOperation:
        raise ValueError("The instruction at index {} is not a call operation".format(i))

    # Generate the basic block that will contain the instructions after the call instruction
    bb_label = generate_unique_bb_id()
    next_bb = BasicBlock(bb_label, bb.function_name)
    next_bb.instructions = bb.instructions[i + 1 :]
    next_bb.llvm_label = str(bb.llvm_label) + "_" + str(i)
    next_bb.original_bb = bb.original_bb if bb.original_bb else bb
    # Update the current basic block to only contain instructions before the call instruction
    bb.instructions[0].label = None
    bb.instructions = bb.instructions[0:i]

    # Generate the function basic block
    func = BasicBlock(generate_unique_bb_id(), bb.function_name)
    func.is_function_call = True
    func.llvm_label = "{}_".format(bb.llvm_label) + call.name
    func.instructions = [call]
    func.original_bb = bb.original_bb if bb.original_bb else bb

    # Move the `bb` basic block neighbors to `next_bb` (second half of the original bb split)
    for neighbor in list(cfg.neighbors(bb)):
        chkpt = cfg[bb][neighbor]["checkpoint"]
        if not chkpt:
            raise Exception(
                "A CFG arc does not have a checkpoint: {} -> {} in {}".format(
                    bb.llvm_label, neighbor.llvm_label, bb.function_name
                )
            )
        cfg.remove_edge(bb, neighbor)
        cfg.add_edge(next_bb, neighbor, checkpoint=chkpt)
        chkpt.bb_before = next_bb

    cfg.add_node(func)
    cfg.add_node(next_bb)

    add_free_branch(func, next_bb)
    cfg.add_edge(func, next_bb, checkpoint=Checkpoint(func, next_bb))

    add_free_branch(bb, func)
    cfg.add_edge(bb, func, checkpoint=Checkpoint(bb, func))

    bb.is_splitted = True
    bb.instructions[0].label = "%" + str(bb.label)
    return func, next_bb


def isolate_function_calls(cfgs, ignore_list=()):
    """
    Isolate functions call in basic blocks

    Iterate over the basic blocks in the given CFGs. Each time a function call is found in a basic block,
    the function call is isolated by creating a new basic block `function` and splitting
    the old basic block in two parts.

    :param Dict[str, CFG] cfgs: The cfgs to analyse
    :param List[str] ignore_list: List of functions to ignore
    :return nx.Digraph: Returns the function dependency graph. The attribute "dependencies" on each arc is list containing
        tuples representing the basic block to update after the function has been analysed

    See also: ..function: `~isolate_function_call_at_index`
    """
    function_graph = nx.DiGraph()
    function_graph.add_nodes_from(list(cfgs.keys()))
    for function, cfg in cfgs.items():
        # Create a stack with all the basic block to analyse
        to_analyse = deque(list(cfg.nodes))
        while to_analyse:
            bb = to_analyse.pop()
            i = 0
            while i < len(bb.instructions):
                if (
                    isinstance(bb.instructions[i], CallOperation)
                    and bb.instructions[i].name not in bb.instructions[i].ignore
                    and bb.instructions[i].name not in ignore_list
                ):
                    callee = bb.instructions[i].name
                    func, next_bb = isolate_function_call_at_index(cfg, bb, i)
                    if not function_graph.has_edge(function, callee):
                        function_graph.add_edge(function, callee, dependencies=[func])
                    else:
                        function_graph[function][callee]["dependencies"].append(func)
                    to_analyse.append(next_bb)
                i += 1
    return function_graph


def form_initial_regions(cfgs: Dict[str, CFG]):
    """
    Place checkpoints before function calls and in loop headers
    """
    for cfg in cfgs.values():
        for bb in cfg.nodes():
            setattr(bb, "is_region_boundary", False)
            if bb.is_function_call:
                bb.is_region_boundary = True
            if bb.loop is not None and bb.loop.header[0] == bb:
                bb.is_region_boundary = True
