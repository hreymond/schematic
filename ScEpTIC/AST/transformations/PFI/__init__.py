from typing import Dict

import networkx as nx

from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.transformations.PFI.cfg_analysis import annotate_loops
from ScEpTIC.AST.transformations.PFI.initial_region_formation import form_initial_regions, isolate_function_calls
from ScEpTIC.AST.transformations.PFI.loop_unrolling import unroll_loops
from ScEpTIC.AST.transformations.PFI.pfi import PFITransformation
from ScEpTIC.AST.transformations.schematic import (
    ASTParser,
    asts_to_cfgs,
    EnergyConsumptionEstimationPass,
    synchronise_cfg_to_ast,
)
from ScEpTIC.AST.transformations.schematic.utils.cfg_utils import save_cfg, save_cfgs
from ScEpTIC.emulator.energy.msp430_worst_case import MSP430WorstCaseEnergyCalculator


def relocate_access_to_nvm(functions: Dict):
    """
    Relocated each target of memory accesses (Store, Load, Call, Return, ...) to NVM
    """
    for function in functions.values():
        for inst in function.body:
            if hasattr(inst, "virtual_memory_target"):
                inst.virtual_memory_target = VirtualMemoryEnum.NON_VOLATILE


def apply_transformation(functions, vmstate):
    """
    Apply the PFI transformation to the program source.

    1. Relocate all memory accesses to NVM, since PFI only uses NVM
    2. Create the CFGs of the program functions (Relies on Alfred parser and schematic cfg constructor)
    3. Form initial regions (i.e. place checkpoints before function calls and in loop headers)
    4. Apply the SAT-region formation algorithm
    5. Add call to checkpoint() at region boundaries
    6. Synchronize the modified CFG to a format usable by ScEptiC emulator
    """

    # Relocate all memory accesses to target NVM
    relocate_access_to_nvm(functions)

    # Parse the functions of the program and create an AST usable by Alfred
    parser = ASTParser(functions)
    parser.parse()

    # Replace "conditional" and "Loops" basic blocs in the AST by classic BB
    parser.reparse()

    # Creates the CFGs from the asts
    cfgs = asts_to_cfgs(parser)

    if "PFI" not in vmstate.transformation_options:
        raise KeyError("PFI configuration not found in transformations_options, update config.py")
    config = vmstate.transformation_options["PFI"]
    if "worst_case_current_consumption" not in config:
        raise KeyError("WCEC data not found in PFI configuration, please update config.py")
    worst_case_datasheet = config["worst_case_current_consumption"]
    msp430_energy_calculator = MSP430WorstCaseEnergyCalculator(worst_case_datasheet)
    msp430_energy_calculator.set_test_config()
    # Set the cost of a checkpoint, if not set in the config file
    config["checkpoint_cost"] = config.get("checkpoint_cost", 16 * msp430_energy_calculator.energy_nvm_access)
    # Get builtins functions name
    builtins_functions = set([f.name for f in filter(lambda f: f.is_builtin, functions.values())])
    # Isolate the function calls into new basic blocks
    function_depgraph: nx.DiGraph = isolate_function_calls(cfgs, builtins_functions)
    # Check for cycle in function dependency graph, i.e recursion
    contains_recursive_fn = next(nx.simple_cycles(function_depgraph), False)
    if contains_recursive_fn:
        raise Exception("[PFI] PFI does not support recursion")

    # Estimate energy consumption of the basic blocks
    energy_estimation_pass = EnergyConsumptionEstimationPass(msp430_energy_calculator, vmstate, builtins_functions)
    energy_estimation_pass.apply(cfgs)

    save_cfgs(cfgs, "dot_cfgs/before initial region formation")
    # Find the loops in the CFGs
    annotate_loops(cfgs)
    # Place checkpoints before function calls and in loop headers
    form_initial_regions(cfgs)
    save_cfgs(cfgs, "dot_cfgs/after initial region formation")
    # Unroll loops if possible
    for cfg in cfgs.values():
        unroll_loops(cfg, config["energy_budget"], config["checkpoint_cost"])
        cfg.check_edges_label_coherency()

    save_cfgs(cfgs, "dot_cfgs/after unrolling")
    # Apply the SAT-region formation algorithm
    transformation = PFITransformation(config)
    transformation.apply_sat_region_formation(cfgs, function_depgraph)
    save_cfgs(cfgs, "dot_cfgs/after pfi")
    # Add call to checkpoint() at region boundaries
    transformation.add_checkpoint_calls(cfgs)

    save_cfgs(cfgs, "dot_cfgs/after_code_transformation")
    # Synchronize the modified CFG to the function llvm body
    synchronise_cfg_to_ast(cfgs, parser)
    parser.syncrhonize_ast_to_functions()
