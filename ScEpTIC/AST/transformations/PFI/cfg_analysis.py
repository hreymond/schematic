from typing import List, Dict

from ScEpTIC.AST.misc.trace import LLVMNaturalLoop
from ScEpTIC.AST.transformations.schematic.utils.llvm_loop_analysis import find_loops
from ScEpTIC.AST.transformations.schematic.utils.loop_utils import get_nb_iter


def annotate_loops(cfgs):
    """
    Annotate the basic blocks of CFG with loop information.

    - First, asks LLVM opt to find the loops in the source file and parse the result
    - Then, for each basic block in a loop, attach the loop information to the basic block
    """
    # Find the loops in the source file
    loop_dict: Dict[str, List[LLVMNaturalLoop]] = find_loops("source.ll")

    # For each function, annotate the basic blocks with the loop information
    for fn_name, loops in loop_dict.items():
        fn_name = "@" + fn_name
        if fn_name not in cfgs:
            raise Exception(
                "The function {} could not be found in the parsed cfg.\n"
                "Functions found: {}".format(fn_name, ", ".join(cfgs.keys()))
            )
        # Create a dict that maps a basic block llvm_label to the basic block
        # This allows to convert from a LLVMNaturalLoop to a LLVMNaturalLoopBB
        bb_by_id = {"%" + str(bb.llvm_label): bb for bb in cfgs[fn_name].nodes()}

        # Analyse the loops, starting by the deepest ones
        loops = sorted(loops, key=lambda x: x.depth, reverse=True)
        for loop in loops:
            # Convert the loop labels to basic blocks
            loop_bb = loop.toLLVMNaturalLoopBB(cfgs[fn_name], bb_by_id)
            loop_bb.nb_iter = get_nb_iter(loop_bb, cfgs[fn_name])
            # Attach the loop information to the basic blocks
            for bb in loop_bb.basic_blocks:
                # Attach the loop information to the basic block, except if the basic block is also
                # part of an inner loop. In this case, we keep the inner loop information.
                if bb.loop is None:
                    bb.loop = loop_bb
