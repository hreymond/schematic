from ScEpTIC.AST.transformations.ratchet.checkpoint_placer import CheckpointPlacer
from ScEpTIC.AST.transformations.ratchet.checkpoint_registers_optimization import RegisterSavingOptimization


def apply_transformation(functions, vmstate):
    config = vmstate.transformation_options["ratchet"]

    functions_with_outside_frame_accesses = config["functions_with_outside_frame_accesses"]
    checkpoint_function_name = vmstate.vm.checkpoint_manager.routine_names["checkpoint"]

    checkpoint_placer = CheckpointPlacer(functions, functions_with_outside_frame_accesses, checkpoint_function_name)
    checkpoint_placer.place_checkpoints()

    if config["optimize_saved_registers"]:
        checkpoint_register_optimization = RegisterSavingOptimization(checkpoint_placer)
        checkpoint_register_optimization.set_checkpoints_registers()
