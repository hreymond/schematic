import copy

from ScEpTIC.AST.elements.instructions.memory_operations import AllocaOperation, StoreOperation, LoadOperation


class MemoryTagsIdentifier:
    """
    A memory tag is a virtual representation of an accessed memory location, which captures also the access pattern.
    A memory tag is associated to each LoadOperation and StoreOperation.
    This class contains a collection of methods for parsing the AST and identifying the memory tag of each operation.
    """

    def __init__(self, functions):
        self.functions = functions

    def resolve_memory_tags(self):
        """
        Populates the memory tags of the instructions in each function
        """
        for name, function in self.functions.items():

            # Skip builtins processing
            if function.is_builtin or function.is_input:
                continue

            self._resolve_memory_tags(function.body)

    def _resolve_memory_tags(self, function_body):
        """
        Populates the memory tags of the instructions of a given function
        """
        elements = {}
        first_alloca = True

        for instruction in function_body:
            # If instruction has a target virtual register, keep track of it to resolve memory tags
            if "target" in dir(instruction) and not instruction.target is None and not instruction._omit_target:
                target_reg = instruction.target.value
                elements[target_reg] = instruction

            # Fix for LLVM 6/7/8 first alloca
            if isinstance(instruction, AllocaOperation):
                # Set first Alloca
                instruction.is_first = first_alloca
                first_alloca = False

            # Resolve the memory tag of each LoadOperation and StoreOperation
            if isinstance(instruction, LoadOperation) or isinstance(instruction, StoreOperation):
                instruction.resolve_memory_tag(elements)
