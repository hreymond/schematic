import copy

from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum


def apply_transformation(functions, vmstate):
    """
    Sets the correct memory map for Mementos
    """
    config = vmstate.transformation_options["mementos_memory_map"]

    checkpoint_function_name = vmstate.vm.checkpoint_manager.routine_names["checkpoint"]

    registers_to_save = set([x for x in range(0, config["registers"])])

    for function in functions.values():
        for instruction in function.body:
            if "virtual_memory_target" in dir(instruction):
                instruction.virtual_memory_target = VirtualMemoryEnum.VOLATILE

            if isinstance(instruction, CallOperation) and instruction.name == checkpoint_function_name:
                instruction.checkpoint_save_pc = True
                instruction.checkpoint_save_esp = True
                instruction.checkpoint_save_ram = True
                instruction.checkpoint_save_regs = copy.deepcopy(registers_to_save)
