import copy
from collections import deque
from dataclasses import dataclass, asdict
from typing import Dict, List, Optional
import logging
from ScEpTIC.AST.transformations.schematic.elements.basic_block import *
from ScEpTIC.AST.transformations.schematic.utils.cfg_utils import save_cfg


@dataclass
class LLVMNaturalLoopBB:
    """
    A dataclass representing a loop

    They might be multiple basic block headers as one logical header can be splitted into several basic blocks during
    the function extraction process
    """

    header: [BasicBlock]
    exiting: [BasicBlock]
    basic_blocks: [BasicBlock]
    latch: [BasicBlock]
    depth: int
    max_iter: Optional[int] = None
    nb_iter: Optional[int] = None
    unroll_factor: Optional[int] = None

    @property
    def name(self):
        return self.header[0].llvm_label

    def __copy__(self):
        return LLVMNaturalLoopBB(
            copy.copy(self.header),
            copy.copy(self.exiting),
            copy.copy(self.basic_blocks),
            copy.copy(self.latch),
            self.depth,
            self.max_iter,
            self.unroll_factor,
        )


@dataclass
class LLVMNaturalLoop:
    """
    A dataclass representing a loop

    The string representing the basic blocks might contain the LLVM basic block ids
    """

    header: str
    exiting: [str]
    basic_blocks: [str]
    latch: [str]
    depth: int

    @property
    def name(self):
        return self.header

    def to_dict(self):
        return asdict(self)

    def toLLVMNaturalLoopBB(self, cfg, bbs):
        header = label_array_to_basic_blocks([self.header], cfg, bbs, False)
        basic_blocks = label_array_to_basic_blocks(self.basic_blocks, cfg, bbs, False)
        exiting = label_array_to_basic_blocks(self.exiting, cfg, bbs, False)
        latch = label_array_to_basic_blocks(self.latch, cfg, bbs, False)
        return LLVMNaturalLoopBB(header, exiting, basic_blocks, latch, self.depth, None)


@dataclass
class Trace:
    """
    A representation of a Trace

    :param int hash: number to identify uniquely traces (used to count them efficiently)
    :param List trace: A list of basic blocks, either in string format ("%8") or in BasicBlock object
    :param int nb_executions: How many time the trace has appeared in the program execution
    """

    hash: int
    trace: [str] or [BasicBlock]
    nb_executions: int

    def to_dict(self):
        return {"trace": self.trace, "nb_execution": self.nb_executions}

    def __str__(self) -> str:
        return str([bb for bb in self.trace])

    def __repr__(self) -> str:
        return self.__str__()


@dataclass
class LoopTraces:
    """
    Dataclass containing loop informations and loop traces

    :param LLVMNaturalLoop,LLVMNaturalLoopBB,optional loop: The loop informations
    :param Dict[int, Trace] traces: Trace indexed by the hash of their BasicBlock or label array
    """

    loop: LLVMNaturalLoop or LLVMNaturalLoopBB = None
    traces: Dict[int, Trace] = None

    def append_trace(self, trace):
        """
        Append a Trace to the list of known traces for the loop. If the trace is already known,
        increment it's number of execution.
        """
        if not self.traces:
            self.traces = {}
        h = hash(tuple(trace))
        if h not in self.traces:
            self.traces[h] = Trace(h, tuple(trace), 1)
        else:
            self.traces[h].nb_executions += 1

    def to_dict(self):
        if not self.traces:
            self.traces = {}
        return {"loop": self.loop.to_dict(), "traces": [trace.to_dict() for trace in self.traces.values()]}


@dataclass
class FunctionTraces:
    traces: Dict[int, Trace] = None
    loop_traces: Dict[str, LoopTraces] = None

    def __init__(self) -> None:
        self.traces = {}
        self.loop_traces = {}

    def append_trace(self, trace):
        """
        Append a Trace to the list of known traces for the function. If the trace is already known,
        increment it's number of execution.
        """
        h = hash(tuple(trace))
        if h not in self.traces:
            self.traces[h] = Trace(h, tuple(trace), 1)
        else:
            self.traces[h].nb_executions += 1

    def append_loop_trace(self, loop: LLVMNaturalLoopBB, trace: Trace):
        """
        Append a Trace to the loop traces of the function. If the loop is unknown, creates a LoopTraces object.
        """
        if not self.loop_traces:
            self.loop_traces = {}
        if loop.name not in self.loop_traces:
            self.loop_traces[loop.name] = LoopTraces()
            self.loop_traces[loop.name].loop = loop
        self.loop_traces[loop.name].append_trace(trace)

    def to_dict(self):
        if not self.traces:
            self.traces = {}
        if not self.loop_traces:
            self.loop_traces = {}
        return {
            "traces": [trace.to_dict() for trace in self.traces.values()],
            "loop_traces": {trace.loop.header: trace.to_dict() for trace in self.loop_traces.values()},
        }


class FakeTraceGenerator:
    def __init__(self, name, cfg):
        from ScEpTIC.AST.transformations.schematic.utils.llvm_loop_analysis import find_loops
        from ScEpTIC.AST.transformations.schematic.parsers.trace_parser import process_loop

        self.cfg = cfg
        self.context = deque()
        self.context.append(name)
        self.curr_traces = {}
        loops_by_func = find_loops("source.ll")
        if name[1:] in loops_by_func:
            loops = loops_by_func[name[1:]]
        else:
            loops = {}
        self.old_bb = cfg.first_bb.llvm_label
        self.traces = FunctionTraces()
        self.loops = {}
        bb_list = {"%{}".format(bb.llvm_label): bb for bb in self.cfg.nodes()}
        for loop in loops:
            loop_bb = loop.toLLVMNaturalLoopBB(self.cfg, bb_list)
            self.loops[loop_bb.header[0]] = loop_bb

    def add_bb_to_trace(self, bb: str):
        old_bb = self.old_bb
        # If the current context (loop, function) has no trace, create it
        if self.context[-1] not in self.curr_traces:
            self.curr_traces[self.context[-1]] = []

        # Entering a loop ?
        if bb in self.loops and old_bb not in self.loops[bb].basic_blocks:
            # Append the loop header to the function trace
            self.curr_traces[self.context[-1]].append(bb)
            # Change context to loop context
            self.context.append(bb)
            self.curr_traces[self.context[-1]] = []
        # Already in a loop ?
        if self.context[-1] in self.loops:
            if old_bb in self.loops[self.context[-1]].latch:
                # If we are in a loop and the current bb is a loop latch, save the trace
                self.traces.append_loop_trace(self.loops[self.context[-1]], self.curr_traces[self.context[-1]])
                self.curr_traces[self.context[-1]] = []
            # Leaving the loop ?
            if bb not in self.loops[self.context[-1]].basic_blocks:
                # self.traces.append_loop_trace(self.loops[self.context[-1]], self.curr_traces[self.context[-1]])
                self.context.pop()
                self.curr_traces[self.context[-1]].append(old_bb)
        # # If the last basic block was a loop header and the current one is in the loop, we entered the loop
        # if old_bb and old_bb in self.loops and bb in self.loops[old_bb].basic_blocks:
        #     self.context.append(old_bb)
        #     self.curr_traces[old_bb] = [old_bb]
        # Add the basic block to the current trace
        self.curr_traces[self.context[-1]].append(bb)
        self.old_bb = bb

    def generate_fake_traces(self):
        save_cfg(self.cfg.name, self.cfg)

        bb_stack = deque()
        bb_stack.append(self.cfg.first_bb)
        arcs_analyzed = set()
        trace = []
        back_tracking = False
        while bb_stack:
            bb = bb_stack[-1]
            i = 0
            neigbhours = list(self.cfg.neighbors(bb))

            for neighbor in neigbhours:
                if (bb, neighbor) not in arcs_analyzed:
                    break
                i += 1

            if back_tracking:
                logging.debug(
                    "Back tracking BB %{}: Analyzed {}/{} possibilities".format(bb.llvm_label, i, len(neigbhours))
                )
                if len(neigbhours) == i:
                    bb_stack.pop()
                    continue
                else:
                    back_tracking = False

            if neighbor is self.cfg.last_bb:
                for bb in bb_stack:
                    self.add_bb_to_trace(bb)
                    trace.append("%{}".format(bb.llvm_label))
                self.add_bb_to_trace(self.cfg.last_bb)
                trace = []
                self.traces.append_trace(self.curr_traces[self.cfg.name])
                self.curr_traces[self.cfg.name] = []
                arcs_analyzed.add((bb, neighbor))
                back_tracking = True
            else:
                bb_stack.append(neighbor)
            arcs_analyzed.add((bb, neighbor))

        for trace in self.traces.traces.values():
            trace.trace = list(trace.trace)
        for loop_trace in self.traces.loop_traces.values():
            for trace in loop_trace.traces.values():
                trace.trace = list(trace.trace)
        return self.traces


def label_array_to_basic_blocks(trace: List[str], cfg, bbs, append_start=True):
    """
    Convert a trace containing llvm labels of basic block to BasicBlock objects

    :param List[str] trace: A basic block trace represented by the llvm labels of the basic blocks
    :param CFG cfg: The cfg of the function
    :param Dict[str, BasicBlock] bbs: The basic blocks of the cfg indexed by LLVM label
    :param bool append_start: Should start and end label be added to the input traces ?
    :return: The input trace where the labels are replaced with the basic block objects
    :rtype: List[BasicBlock]
    """
    function = cfg.name
    # Append start and end to the current trace
    if append_start:
        trace.insert(0, "%START_" + function[1:])
        trace.append("%END_" + function[1:])
    new_trace = []
    try:
        for bb_index in trace:
            # Get the basic block from it's llvm_label
            bb = bbs[bb_index]
            new_trace.append(bb)
            # If the basic block is splitted (because a function call has been isolated)
            # Add following basic blocks to the trace
            if bb.is_splitted:
                next_bb = next(cfg.neighbors(bb))
                while next_bb.original_bb == bb:
                    new_trace.append(next_bb)
                    next_bb = next(cfg.neighbors(next_bb))
    except KeyError as e:
        raise KeyError(
            "The basic block {} hasn't been found in the function {}. "
            "Only found: {}. "
            "Do the file trace file contains the trace for the right program ?"
            "".format(e.args[0], function[1:], list(bbs.keys()))
        )
    return new_trace
