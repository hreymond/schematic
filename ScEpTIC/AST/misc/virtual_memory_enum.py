from enum import Enum


class VirtualMemoryEnum(Enum):
    VOLATILE = "VM"
    NON_VOLATILE = "NVM"
