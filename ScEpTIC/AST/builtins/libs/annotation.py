import logging

from ScEpTIC.AST.builtins.builtin import Builtin
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation

"""
Custom annotations
"""


class max_iter(Builtin):
    def get_val(self):
        return 0


class nb_iter(Builtin):
    def get_val(self):
        return 0


max_iter.define_builtin("__max_iter", "i16", "i16")
nb_iter.define_builtin("__nb_iter", "i16", "i16")
CallOperation.ignore.append("@__max_iter")
CallOperation.ignore.append("@__nb_iter")
