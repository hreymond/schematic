import copy
import math
import time

from ScEpTIC.AST.elements.instructions.memory_operations import StoreOperation, LoadOperation
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import ReturnOperation
from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from .base import InterruptionManager


class EnergyMeasureInterruptionManager(InterruptionManager):
    def __init__(self, vmstate, checkpoint_manager):
        super().__init__(vmstate, checkpoint_manager)

        self.base_data = {"energy": 0.0, "clock_cycles": 0, "executed_instructions": 0}
        self.data_groups = ["no_memory_accesses", "volatile_accesses", "non_volatile_accesses", "total"]
        self.metric_groups = [
            "computation",
            "normalization",
            "reexecution",
            "checkpoint",
            "trigger_call",
            "restore",
            "total",
        ]
        self.summary_groups = ["checkpoint", "trigger_call", "restore", "power_failure"]

        self._init_metrics()

        self.last_checkpoint = {
            "registers": 0,
            "memory_cells": 0,
            "computation": {data_group: copy.deepcopy(self.base_data) for data_group in self.data_groups},
        }

        self.checkpoints_check_adc = self.vmstate.energy_simulation_config["use_trigger_calls"]
        self.trigger_to_checkpoint_probability = self.vmstate.energy_simulation_config[
            "trigger_call_to_checkpoint_probability_range"
        ]

        self.simulate_power_failures = self.vmstate.energy_simulation_config["simulate_power_failures"]
        self.clock_cycles_to_power_failure = self.vmstate.energy_simulation_config["clock_cycles_between_power_failures"]

    def intermittent_execution_required(self):
        """
        Runs the entire program using the run_with_intermittent_execution method
        """
        return True

    def _dry_run(self):
        """
        Runs the program to know checkpoint distance, so to enable dumps only when necessary
        This is required otherwise the simulation would take long
        """
        prev_checkpoint_clock = 0
        max_distance = None

        checkpoint_function_name = self.checkpoint_manager.routine_names["checkpoint"]

        while not self.vmstate.program_end_reached:
            current_instruction = self.vmstate.current_instruction

            self._calculate_execution_metrics(current_instruction)

            self.vmstate.run_step()

            if isinstance(current_instruction, CallOperation) and current_instruction.name == checkpoint_function_name:
                current_clock = self.vmstate.global_clock
                distance = current_clock - prev_checkpoint_clock

                if max_distance is None:
                    max_distance = distance
                else:
                    max_distance = max(distance, max_distance)

                prev_checkpoint_clock = current_clock

                if self.checkpoints_check_adc:
                    self._calculate_trigger_call_metric()

                self._calculate_checkpoint_metric(current_instruction)

        # reset measures
        self._init_metrics()
        self.last_checkpoint = {
            "registers": 0,
            "memory_cells": 0,
            "computation": {data_group: copy.deepcopy(self.base_data) for data_group in self.data_groups},
        }

        # reset vm state
        self.vmstate.reset()
        self.vmstate.memory.force_nvm_reset()
        self.checkpoint_manager.reset()
        self.vmstate.stats.reset()

        adjust_factor = 1.10  # add 3% just in case
        if max_distance is None:
            return 999999999999, 999999999999999
        adjusted_max_distance = float(max_distance) * adjust_factor

        return math.ceil(adjusted_max_distance), max_distance

    def run_with_intermittent_execution(self):
        """
        Runs the program and estimates the energy consumption
        """
        if self.simulate_power_failures:
            print(f"Estimate checkpoint distance")
            checkpoint_max_distance, max_distance = self._dry_run()
            complete_str = (
                " - will fail to complete" if max_distance > self.clock_cycles_to_power_failure else " - will complete"
            )
            print(f"Checkpoint distance: {checkpoint_max_distance} {complete_str}")
        else:
            checkpoint_max_distance = 0

        checkpoint_function_name = self.checkpoint_manager.routine_names["checkpoint"]

        next_power_failure = self.vmstate.global_clock + self.clock_cycles_to_power_failure
        next_dumps_start = next_power_failure - checkpoint_max_distance
        last_restore_clock = 0

        previous_checkpoint_overhead = 0
        total_overhead = 0

        if self.simulate_power_failures:
            print(
                f"Current cc: {self.vmstate.global_clock}; next power failure: {next_power_failure}; next dump: {next_dumps_start}"
            )

        able_to_complete = True

        while not self.vmstate.program_end_reached:
            current_instruction = self.vmstate.current_instruction

            self._calculate_execution_metrics(current_instruction)

            self.vmstate.run_step()

            if isinstance(current_instruction, CallOperation) and current_instruction.name == checkpoint_function_name:
                if self.simulate_power_failures and (
                    self.vmstate.global_clock >= next_dumps_start or self.checkpoint_manager.stop_on_checkpoint
                ):
                    # x = time.time()
                    self.checkpoint_manager.do_dump()
                    # print(f"Dump at {self.vmstate.global_clock-total_overhead} ({math.ceil(time.time()-x)}s)")

                if self.checkpoints_check_adc:
                    self._calculate_trigger_call_metric()

                # always consider as trigger calls end up in saving a checkpoint
                # the actual conversion rate is then considered at the end of the simulation, by averaging checkpoints
                self._calculate_checkpoint_metric(current_instruction)

                # account for simulated checkpoint that would not execute
                if self.checkpoints_check_adc and self.simulate_power_failures:
                    # if no power failure is going to happen -> previous checkpoint was not necessary (but current may be)
                    if self.vmstate.global_clock <= next_power_failure:
                        # increase next power failure as if previous checkpoint did not execute
                        next_power_failure += previous_checkpoint_overhead
                        next_dumps_start += previous_checkpoint_overhead
                        total_overhead += previous_checkpoint_overhead

                    previous_checkpoint_overhead = self._get_checkpoint_clock_cycles(current_instruction)
                # Simulate a voluntary power failure (i.e stop the platform)
                if self.checkpoint_manager.stop_on_checkpoint:
                    next_power_failure = self.vmstate.global_clock - 1

                self.last_checkpoint["computation"] = copy.deepcopy(self.vmstate.energy_metrics["data"]["computation"])

            if self.simulate_power_failures:

                # account for extra energy consumption for non-volatile accesses
                n_nvm_accesses = self._number_of_non_volatile_accesses(current_instruction)
                clock_cycles_adjust_factor = n_nvm_accesses * self.vmstate.energy_calculator.non_volatile_increase
                next_power_failure -= clock_cycles_adjust_factor
                next_dumps_start -= clock_cycles_adjust_factor

                # simulate a power failure
                if self.vmstate.global_clock > next_power_failure:
                    print(f"Power failure at {self.vmstate.register_file.pc} {self.vmstate.global_clock-total_overhead}")

                    # restore dump -> simulate power failure and restart
                    self.checkpoint_manager.restore_dump()
                    print(f"Restore at {self.vmstate.register_file.pc} {self.vmstate.global_clock-total_overhead}")

                    if last_restore_clock == self.vmstate.global_clock:
                        able_to_complete = False
                        print("Unable to complete the test!")
                        self.vmstate.force_stop = True
                        break

                    last_restore_clock = self.vmstate.global_clock

                    # identify where to simulate next power failure
                    next_power_failure = self.vmstate.global_clock + self.clock_cycles_to_power_failure
                    next_dumps_start = next_power_failure - checkpoint_max_distance
                    self._calculate_restore_metric()
                    print(
                        f"Current cc: {self.vmstate.global_clock}; next power failure: {next_power_failure}; next dump: {next_dumps_start}"
                    )

                    for data_group, chkpt_data in self.last_checkpoint["computation"].items():
                        element = self.vmstate.energy_metrics["data"]["computation"][data_group]
                        for metric in chkpt_data:
                            delta = element[metric] - chkpt_data[metric]
                            self.vmstate.energy_metrics["data"]["reexecution"][data_group][metric] += delta
                            element[metric] -= delta
                    previous_checkpoint_overhead = 0
                    total_overhead = 0

                    self._update_summary("power_failure")

        self._compute_totals(able_to_complete)
        # self.print_metrics()

    def _init_metrics(self):
        """
        Initializes the metrics
        """

        self.vmstate.energy_metrics = {"summary": {}, "data": {}}

        for metric in self.summary_groups:
            self.vmstate.energy_metrics["summary"][metric] = 0

        for metric_group in self.metric_groups:
            self.vmstate.energy_metrics["data"][metric_group] = {}

            for data_group in self.data_groups:
                self.vmstate.energy_metrics["data"][metric_group][data_group] = copy.deepcopy(self.base_data)

    def _calculate_trigger_call_metric(self):
        """
        Calculates the cost of a trigger call
        """
        energy_per_cycle = self.vmstate.energy_calculator.energy_clock_cycle
        energy_per_cycle_adc = self.vmstate.energy_calculator.energy_clock_cycle_adc
        adc_active_cycles = self.vmstate.energy_calculator.adc_active_cycles
        adc_instructions = self.vmstate.energy_calculator.adc_instructions

        # adc extra energy consumption
        self._update_metrics("trigger_call", "no_memory_accesses", adc_active_cycles, energy_per_cycle_adc, 0)

        # account for threshold comparison
        # ADD R0, ADC_DATA, -THRESHOLD
        # BNEQ CHECKPOINT, RETURN
        n_ops = 2
        n_cycles = adc_active_cycles + n_ops
        n_instructions = adc_instructions + n_ops

        # clock cycles energy consumption
        self._update_metrics("trigger_call", "no_memory_accesses", n_cycles, energy_per_cycle, n_instructions)

        self._update_summary("trigger_call")

        self._account_for_extra_cycles(n_cycles)

    def _calculate_restore_metric(self):
        """
        Calculates the cost of a restore operation
        """
        metric_group = "restore"

        energy_per_cycle = self.vmstate.energy_calculator.energy_clock_cycle
        energy_per_cycle_v = energy_per_cycle + self.vmstate.energy_calculator.energy_volatile_memory_access
        energy_per_cycle_nv = energy_per_cycle + self.vmstate.energy_calculator.energy_non_volatile_memory_access
        normal_cycles = 1
        nvm_cycles = normal_cycles + self.vmstate.energy_calculator.nvm_extra_cycles

        # Load current location of the checkpoint
        #   - 1 load from NVM to identify current version of two checkpoints (2 phase commit)
        # 1. LOAD VERSION_OFFSET_LOCATION, REG0 (nvm) <- PC saved here
        self._update_metrics(metric_group, "non_volatile_accesses", nvm_cycles, energy_per_cycle_nv, 1)
        # Restore ESP
        # 2. LOAD ESP, REG0(0x4) // REG0 + 0x4 (ESP saved after PC)
        self._update_metrics(metric_group, "non_volatile_accesses", nvm_cycles, energy_per_cycle_nv, 1)

        # first, restore ram
        n_ram_cells = self.last_checkpoint["memory_cells"]
        if n_ram_cells > 0:
            # Account for registers offset and initialize start register of ram
            # 3. ADD REG1, REG0, 0x10 (REG1 points to the start of sram checkpoint)
            self._update_metrics(metric_group, "no_memory_accesses", normal_cycles, energy_per_cycle, 1)
            # 4. REG2 <- 0x00 # initial address to restore
            self._update_metrics(metric_group, "no_memory_accesses", normal_cycles, energy_per_cycle, 1)

            # Assume:
            #   - mapping 1:1 volatile/non-volatile memory (address = version_address + nvm_offset + element_address)
            #
            #     5. LOAD R_x, REG3 (nvm)
            #     6. STORE REG3, REG1(REG2) // REG1+REG2 (vm)
            #     7. ADD REG2, REG2, 4
            #     8. BLT REG3, ESP, #4

            # 5. LOAD (non volatile)
            self._update_metrics(
                metric_group, "non_volatile_accesses", n_ram_cells * nvm_cycles, energy_per_cycle_nv, n_ram_cells
            )
            # 6. STORE (volatile)
            self._update_metrics(
                metric_group, "volatile_accesses", n_ram_cells * normal_cycles, energy_per_cycle_v, n_ram_cells
            )
            # 7. ADD
            self._update_metrics(
                metric_group, "no_memory_accesses", n_ram_cells * normal_cycles, energy_per_cycle, n_ram_cells
            )
            # 8. BLT
            self._update_metrics(
                metric_group, "no_memory_accesses", n_ram_cells * normal_cycles, energy_per_cycle, n_ram_cells
            )

        n_regs = self.vmstate.energy_calculator.n_registers
        # Registers
        # Assume:
        #   - Assume registers in order and hardcoded register restore instructions
        #   - For each general purpose reg
        #      9. LOAD REG0(HARDCODED_OFFSET), REG_n  (nvm, restore reg)
        #   - Restore PC:
        #     10. LOAD REG0, PC (nvm, restore PC)

        # 9. LOAD (non-volatile)
        self._update_metrics(metric_group, "non_volatile_accesses", n_regs * nvm_cycles, energy_per_cycle_nv, n_regs)
        # 10. LOAD (non-volatile)
        self._update_metrics(metric_group, "non_volatile_accesses", nvm_cycles, energy_per_cycle_nv, 1)

        self._update_summary("restore")
        self._account_for_extra_cycles(self._get_restore_clock_cycles())

    def _get_restore_clock_cycles(self):
        """
        Calculates and returns the number of clock cycles for restoring a checkpoint
        Check _calculate_restore_metric() for details
        """
        restore_cycles = 0

        normal_cycles = 1
        nvm_cycles = normal_cycles + self.vmstate.energy_calculator.nvm_extra_cycles

        # 1. LOAD VERSION_OFFSET_LOCATION, REG0 (nvm) <- PC saved here
        restore_cycles += nvm_cycles
        # 2. LOAD ESP, REG0(0x4) // REG0 + 0x4 (ESP saved after PC)
        restore_cycles += nvm_cycles

        # first, restore ram
        n_ram_cells = self.last_checkpoint["memory_cells"]

        if n_ram_cells > 0:
            # 3. ADD REG1, REG0, 0x10 (REG1 points to the start of sram checkpoint)
            restore_cycles += normal_cycles

            # 4. REG2 <- 0x00 # initial address to restore
            restore_cycles += normal_cycles

            # 5. LOAD (non volatile)
            restore_cycles += n_ram_cells * nvm_cycles
            # 6. STORE (volatile)
            restore_cycles += n_ram_cells * normal_cycles
            # 7. ADD
            restore_cycles += n_ram_cells * normal_cycles
            # 8. BLT
            restore_cycles += n_ram_cells * normal_cycles

        n_regs = self.vmstate.energy_calculator.n_registers
        # 9. LOAD (non-volatile)
        restore_cycles += n_regs * nvm_cycles
        # 10. LOAD (non-volatile)
        restore_cycles += nvm_cycles

        return restore_cycles

    def _calculate_checkpoint_metric(self, checkpoint):
        """
        Calculate the cost of a checkpoint
        """
        metric_group = "checkpoint"

        energy_per_cycle = self.vmstate.energy_calculator.energy_clock_cycle
        energy_per_cycle_v = energy_per_cycle + self.vmstate.energy_calculator.energy_volatile_memory_access
        energy_per_cycle_nv = energy_per_cycle + self.vmstate.energy_calculator.energy_non_volatile_memory_access
        normal_cycles = 1
        nvm_cycles = normal_cycles + self.vmstate.energy_calculator.nvm_extra_cycles

        # if ESP not saved -> only pc -> no 2PC needed
        # valid for virtual_memory
        if not checkpoint.checkpoint_save_esp:
            # save only PC
            #     1. LOAD VERSION_OFFSET_LOCATION, REG0 (nvm)
            #     2. STORE PC, REG0 (nvm)
            self._update_metrics(metric_group, "non_volatile_accesses", 2 * nvm_cycles, energy_per_cycle_nv, 2)
            self.last_checkpoint["registers"] = 1
            self.last_checkpoint["memory_cells"] = 0
            self._update_summary("checkpoint")
            self._account_for_extra_cycles(self._get_checkpoint_clock_cycles(checkpoint))
            return

        # otherwise 2PC needed
        # Assume:
        #   - 1 load from NVM to identify current version of the two checkpoints (2 phase commit)
        #   - assume iterate from 0x000 to top of NVM: 1 op to calculate element_address (prev_address + 4 byte)
        #     1. LOAD VERSION_OFFSET_LOCATION, REG0 (nvm)
        #     2. ADD REG0, REG0, NEXT_VERSION_LOCATION (assume optimized for 1 op) (start address of new checkpoint in nvm)
        #     3. REG1 <- 0x0

        # 1. LOAD (non volatile)
        self._update_metrics(metric_group, "non_volatile_accesses", nvm_cycles, energy_per_cycle_nv, 1)
        # 2. ADD
        self._update_metrics(metric_group, "no_memory_accesses", normal_cycles, energy_per_cycle, 1)
        # 3. SET
        self._update_metrics(metric_group, "no_memory_accesses", normal_cycles, energy_per_cycle, 1)

        # Save registers
        # Assume for each register (hardcoded): (note: PC saved at 0x00 and ESP at 0x01)
        #  4. STORE REG_TARGET, REG1(HARDCODED_OFFSET)  (nvm)
        n_registers = len(checkpoint.checkpoint_save_regs) + 2  # general purpose regs + PC and STACK POINTER

        # 4. STORE (non volatile)
        self._update_metrics(
            metric_group, "non_volatile_accesses", n_registers * nvm_cycles, energy_per_cycle_nv, n_registers
        )

        if checkpoint.checkpoint_save_ram:
            # 5. ADD REG1, REG1, REG_OFFSET  // account for register area
            self._update_metrics(metric_group, "no_memory_accesses", normal_cycles, energy_per_cycle, 1)

            # Assume:
            #   - mapping 1:1 volatile/non-volatile memory (address = version_address + nvm_offset + element_address)
            #
            #     6. LOAD R_x, REG2 (vm)
            #     7. STORE REG2, REG0(REG1) // REG0+REG1 (nvm)
            #     8. ADD REG1, REG1, 4
            #     9. BLT REG1, ESP, #4
            n_ram_cells = self._calculate_checkpoint_ram_cells()

            # 6. LOAD (volatile)
            self._update_metrics(
                metric_group, "volatile_accesses", n_ram_cells * normal_cycles, energy_per_cycle_v, n_ram_cells
            )
            # 7. STORE (non volatile)
            self._update_metrics(
                metric_group, "non_volatile_accesses", n_ram_cells * nvm_cycles, energy_per_cycle_nv, n_ram_cells
            )
            # 8. ADD
            self._update_metrics(
                metric_group, "no_memory_accesses", n_ram_cells * normal_cycles, energy_per_cycle, n_ram_cells
            )
            # 9. BLT
            self._update_metrics(
                metric_group, "no_memory_accesses", n_ram_cells * normal_cycles, energy_per_cycle, n_ram_cells
            )
        else:
            n_ram_cells = 0

        # Commit 2PC
        # Assume:
        #  10. STORE REG0, VERSION_OFFSET_LOCATION (nvm) (commit new version offset)
        self._update_metrics(metric_group, "non_volatile_accesses", nvm_cycles, energy_per_cycle_nv, 1)

        # update metrics - for restore
        self.last_checkpoint["registers"] = n_registers
        self.last_checkpoint["memory_cells"] = n_ram_cells

        self._update_summary("checkpoint")

        self._account_for_extra_cycles(self._get_checkpoint_clock_cycles(checkpoint))

    def _get_checkpoint_clock_cycles(self, checkpoint):
        """
        Calculate the cost of a checkpoint
        """
        checkpoint_cycles = 0

        normal_cycles = 1
        nvm_cycles = normal_cycles + self.vmstate.energy_calculator.nvm_extra_cycles

        if not checkpoint.checkpoint_save_esp:
            #     1. LOAD VERSION_OFFSET_LOCATION, REG0 (nvm)
            #     2. STORE PC, REG0 (nvm)
            checkpoint_cycles += 2 * nvm_cycles
            return checkpoint_cycles

        # 1. LOAD (non volatile)
        checkpoint_cycles += nvm_cycles
        # 2. ADD
        checkpoint_cycles += normal_cycles
        # 3. SET
        checkpoint_cycles += normal_cycles

        # Save registers
        n_registers = len(checkpoint.checkpoint_save_regs) + 2  # general purpose regs + PC and STACK POINTER

        # 4. STORE (non volatile)
        checkpoint_cycles += n_registers * nvm_cycles

        if checkpoint.checkpoint_save_ram:
            # 5. ADD REG1, REG1, REG_OFFSET  // account for register area
            checkpoint_cycles += normal_cycles

            n_ram_cells = self._calculate_checkpoint_ram_cells()

            # 6. LOAD (volatile)
            checkpoint_cycles += n_ram_cells * normal_cycles
            # 7. STORE (non volatile)
            checkpoint_cycles += n_ram_cells * nvm_cycles
            # 8. ADD
            checkpoint_cycles += n_ram_cells * normal_cycles
            # 9. BLT
            checkpoint_cycles += n_ram_cells * normal_cycles

        #  10. STORE REG0, VERSION_OFFSET_LOCATION (nvm) (commit new version offset)
        checkpoint_cycles += nvm_cycles

        return checkpoint_cycles

    def _calculate_checkpoint_ram_cells(self):
        """
        Returns the number of memory cells that need to be preserved
        """

        ignore_str_bss = "@.str"
        # calculate volatile state dimensionality

        # global variables must be saved
        gst = self.vmstate.memory.sram.gst
        n_global_vars_memory_cells = 0

        for memory_cell in gst._memory.values():
            if ignore_str_bss not in memory_cell.metadata:
                n_global_vars_memory_cells += 1

        n_stack_memory_cells = 0
        stack = self.vmstate.memory.sram.stack
        stack_pointer = stack.top_address

        for address, memory_cell in sorted(stack._memory.items()):
            if address < stack_pointer:
                n_stack_memory_cells += 1

        return n_global_vars_memory_cells + n_stack_memory_cells

    def _calculate_execution_metrics(self, instruction):
        """
        Calculates the energy consumption and clock cycles of instruction
        """
        metric_group = instruction.metric_group
        data_group = "no_memory_accesses"

        n_cycles = instruction.tick_count
        energy_per_cycle = self.vmstate.energy_calculator.energy_clock_cycle

        checkpoint_function_name = self.checkpoint_manager.routine_names["checkpoint"]

        if isinstance(instruction, StoreOperation) or isinstance(instruction, LoadOperation):
            targets_nvm = instruction.virtual_memory_target == VirtualMemoryEnum.NON_VOLATILE

            if targets_nvm:
                data_group = "non_volatile_accesses"
                n_cycles += self.vmstate.energy_calculator.nvm_extra_cycles
                energy_per_cycle += self.vmstate.energy_calculator.energy_non_volatile_memory_access
            else:
                data_group = "volatile_accesses"
                energy_per_cycle += self.vmstate.energy_calculator.energy_volatile_memory_access
        elif isinstance(instruction, CallOperation) or isinstance(instruction, ReturnOperation):
            # remove memory accesses, as we count them separately
            memory_cycles = instruction.memory_tick_count
            n_cycles -= memory_cycles
            n_memory_ops = instruction.n_memory_instructions

            # account for checkpoint return, as checkpoint call is ignored by ScEpTIC VM
            if isinstance(instruction, CallOperation) and instruction.name == checkpoint_function_name:
                memory_cycles += ReturnOperation.memory_tick_count
                n_cycles += ReturnOperation.tick_count - ReturnOperation.memory_tick_count
                n_memory_ops += ReturnOperation.n_memory_instructions

            memory_energy_per_cycle = energy_per_cycle

            targets_nvm = instruction.virtual_memory_target == VirtualMemoryEnum.NON_VOLATILE

            if targets_nvm:
                memory_data_group = "non_volatile_accesses"
                memory_cycles += self.vmstate.energy_calculator.nvm_extra_cycles * n_memory_ops
                memory_energy_per_cycle += self.vmstate.energy_calculator.energy_non_volatile_memory_access

            else:
                memory_data_group = "volatile_accesses"
                memory_energy_per_cycle += self.vmstate.energy_calculator.energy_volatile_memory_access

            self._update_metrics(metric_group, memory_data_group, memory_cycles, memory_energy_per_cycle, n_memory_ops)

        # account for metrics
        self._update_metrics(metric_group, data_group, n_cycles, energy_per_cycle)

        self._account_for_extra_cycles(self._get_execution_extra_cycles(instruction))

    def _get_execution_extra_cycles(self, instruction):
        """
        Calculates the extra clock cycles required for executing instruction
        """
        extra_cycles = 0

        checkpoint_function_name = self.checkpoint_manager.routine_names["checkpoint"]

        if isinstance(instruction, StoreOperation) or isinstance(instruction, LoadOperation):
            targets_nvm = instruction.virtual_memory_target == VirtualMemoryEnum.NON_VOLATILE

            if targets_nvm:
                extra_cycles += self.vmstate.energy_calculator.nvm_extra_cycles

        elif isinstance(instruction, CallOperation) or isinstance(instruction, ReturnOperation):
            targets_nvm = instruction.virtual_memory_target == VirtualMemoryEnum.NON_VOLATILE

            if targets_nvm:
                n_memory_ops = instruction.n_memory_instructions
                extra_cycles += self.vmstate.energy_calculator.nvm_extra_cycles * n_memory_ops

            # adjust global_clock for checkpoint operations, as they are ignored by ScEpTIC VM
            if isinstance(instruction, CallOperation) and instruction.name == checkpoint_function_name:
                extra_cycles += instruction.tick_count

                # account for checkpoint return, as checkpoint call is ignored by ScEpTIC VM
                extra_cycles += ReturnOperation.tick_count
                extra_cycles += self.vmstate.energy_calculator.nvm_extra_cycles * ReturnOperation.memory_tick_count

        return extra_cycles

    def _update_metrics(self, metric_group, data_group, n_cycles, energy_per_cycle, n_instructions=1):
        """
        Updates the metrics
        """
        element = self.vmstate.energy_metrics["data"][metric_group][data_group]

        element["energy"] += float(n_cycles) * float(energy_per_cycle)
        element["clock_cycles"] += n_cycles

        if n_cycles > 0:
            element["executed_instructions"] += n_instructions

    def _account_for_extra_cycles(self, cycles):
        """
        Updates global clock to account for extra clock cycles of NVM accesses
        """
        self.vmstate.global_clock += cycles

    def _number_of_non_volatile_accesses(self, instruction):
        """
        Returns the number of non-volatile memory accesses that instruction executes
        """

        nvm_accesses = 0

        # non-volatile read/write
        if isinstance(instruction, LoadOperation) or isinstance(instruction, StoreOperation):
            if instruction.virtual_memory_target == VirtualMemoryEnum.NON_VOLATILE:
                nvm_accesses += 1

        # call/return with non-volatile stack
        elif isinstance(instruction, CallOperation) or isinstance(instruction, ReturnOperation):
            if instruction.virtual_memory_target == VirtualMemoryEnum.NON_VOLATILE:
                nvm_accesses += instruction.n_memory_instructions

                # account for checkpoint return operation when the call executes with a non-volatile stack
                # checkpoint_function_name = self.checkpoint_manager.routine_names["checkpoint"]
                if isinstance(instruction, CallOperation) and instruction.name in instruction.ignore:
                    nvm_accesses += ReturnOperation.n_memory_instructions

        return nvm_accesses

    def _update_summary(self, metric_group, n=1):
        """
        Updates the summary of executed operations
        """
        self.vmstate.energy_metrics["summary"][metric_group] += n

    def _compute_totals(self, able_to_complete):
        """
        Computes the "total" data group of each metric
        """

        for metric_group in self.metric_groups:
            if metric_group == "total":
                continue

            for metric in self.base_data.keys():
                for data_group in self.data_groups:
                    if data_group == "total":
                        continue

                    value = self.vmstate.energy_metrics["data"][metric_group][data_group][metric]
                    self.vmstate.energy_metrics["data"][metric_group]["total"][metric] += value

        self.vmstate.energy_metrics["able_to_complete"] = able_to_complete

        results = []

        # account for trigger calls
        if self.checkpoints_check_adc:
            n_checkpoints = self.vmstate.energy_metrics["summary"]["checkpoint"]
            n_actual_checkpoints = self.vmstate.energy_metrics["summary"]["restore"]
            n_uncertain_checkpoints = n_checkpoints - n_actual_checkpoints

            single_checkpoint_data = {}

            # calculate single_checkpoint_data
            for data_group in self.data_groups:
                single_checkpoint_data[data_group] = {}

                for metric in self.base_data:
                    value = float(self.vmstate.energy_metrics["data"]["checkpoint"][data_group][metric])
                    single_checkpoint_data[data_group][metric] = value / float(n_checkpoints)

            min_p = self.trigger_to_checkpoint_probability[0]
            max_p = self.trigger_to_checkpoint_probability[1] + 1
            step = self.trigger_to_checkpoint_probability[2]

            for p_chk in range(min_p, max_p, step):
                p_chk = float(p_chk) / 100.0
                n_executed_checkpoints = n_actual_checkpoints + n_uncertain_checkpoints * p_chk

                data = copy.deepcopy(self.vmstate.energy_metrics)

                # update with p_chk data
                data["summary"]["checkpoint"] = n_executed_checkpoints
                for data_group in self.data_groups:
                    for metric in self.base_data:
                        value = single_checkpoint_data[data_group][metric] * n_executed_checkpoints
                        data["data"]["checkpoint"][data_group][metric] = value

                result = {"checkpoint_probability": str(p_chk), "measures": data}
                results.append(result)

        else:
            result = {"checkpoint_probability": str(1.0), "measures": self.vmstate.energy_metrics}
            results.append(result)

        for result in results:
            self._compute_overall_total(result["measures"])

        self.vmstate.energy_metrics = results

    def _compute_overall_total(self, data):
        """
        Populates data with the sum of all the metrics
        """

        for metric_group in self.metric_groups:
            if metric_group == "total":
                continue

            for data_group in self.data_groups:
                for metric in self.base_data:
                    value = data["data"][metric_group][data_group][metric]
                    data["data"]["total"][data_group][metric] += value

    def print_metrics(self, print_data=True):
        retval = ""

        for data in self.vmstate.energy_metrics:
            p_chk = data["checkpoint_probability"]
            energy_metrics = data["measures"]

            retval += f"[Energy Metrics - Checkpoint Probability: {p_chk}]\n"

            for metric_group_name, metric_group in energy_metrics["data"].items():
                retval += f"  --- {metric_group_name} ---\n"
                for data_group_name, data_group in metric_group.items():
                    retval += f"     [-] {data_group_name}:\n"
                    for name, data in data_group.items():
                        retval += f"           - {name}: {data}\n"
                    retval += "\n"
                retval += "\n"

            retval += "\n\n[Summary]\n"

            for name, value in energy_metrics["summary"].items():
                retval += f" [+] {name}: {value}\n"

            retval += "\n"
            retval += "\n"

        if print_data:
            print(retval)

        return retval
