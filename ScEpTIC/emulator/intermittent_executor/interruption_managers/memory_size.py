import math

from ScEpTIC.AST.misc.virtual_memory_enum import VirtualMemoryEnum
from ScEpTIC.AST.elements.instructions.memory_operations import LoadOperation, StoreOperation, AllocaOperation
from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from .base import InterruptionManager


class MemorySizeIdentifier(InterruptionManager):
    """
    Interruption manager for identifying the size of the resulting transformed program.
    The memory size is computed using the target address of memory read and write operations and the size of global variables.
    A single continuous run of the program is sufficient to identify the final memory size
    """

    def __init__(self, vmstate, checkpoint_manager):
        super().__init__(vmstate, checkpoint_manager)

        self.align_bytes = 2
        self.registers_size = 2
        self.memory_usage = {"NVM": [], "VM": []}
        self.double_buffers = set()
        self.checkpoint_max_size = 0
        self.global_vars_dim = 0

        # strings / constants
        self.ignore_str_bss = "@.str"

        self.global_vars_target = self.vmstate.memory_size_config["global_variables_default_memory"]
        self.double_buffer_enabled = self.vmstate.memory_size_config["double_buffer_for_anomalies"]

    def intermittent_execution_required(self):
        """
        The analysis runs inside run_with_intermittent_execution
        """
        return True

    def run_with_intermittent_execution(self):
        """
        Runs the test
        """
        reads = set()
        writes = set()

        self._compute_memory_size_of_global_vars()

        while not self.vmstate.program_end_reached:
            # Get current instruction
            current_instruction = self.vmstate.current_instruction

            if isinstance(current_instruction, StoreOperation):
                address = current_instruction.target.get_val()
                dimension = int(len(current_instruction.value.type) / 8)  # bytes
                memory_target = current_instruction.virtual_memory_target
                self._collect_access(address, dimension, memory_target)

                # check for intermittence anomalies
                if memory_target == VirtualMemoryEnum.NON_VOLATILE:

                    # if no previous write -> search for read
                    if address not in writes:

                        # if previous memory read -> anomaly found -> double buffer required
                        if address in reads:
                            self.double_buffers.add((address, dimension))

                        writes.add(address)

            elif isinstance(current_instruction, LoadOperation):
                address = current_instruction.element.get_val()
                dimension = int(len(current_instruction.type) / 8)  # bytes
                memory_target = current_instruction.virtual_memory_target
                self._collect_access(address, dimension, memory_target)

                # tracks of reads to identify intermittence anomalies
                if memory_target == VirtualMemoryEnum.NON_VOLATILE:
                    reads.add(address)

            elif isinstance(current_instruction, CallOperation):
                if current_instruction.name == self.checkpoint_manager.routine_names["checkpoint"]:
                    writes = set()
                    reads = set()

                    checkpoint_size = self._calculate_checkpoint_size(current_instruction)
                    self.checkpoint_max_size = max(self.checkpoint_max_size, checkpoint_size)

            # Run operation
            self.vmstate.run_step()

        self._get_memory_size()

        print(self.vmstate.memory_size)

    def _collect_access(self, address, dimension, memory_target):
        index = address.index("0x")
        # address = int(address[index:], 16)
        data = (address, dimension)

        if memory_target == VirtualMemoryEnum.VOLATILE:
            if data not in self.memory_usage["VM"]:
                self.memory_usage["VM"].append(data)

        elif memory_target == VirtualMemoryEnum.NON_VOLATILE:
            if data not in self.memory_usage["NVM"]:
                self.memory_usage["NVM"].append(data)

        else:
            raise Exception("Unexpected memory access")

    def _compute_memory_size_of_global_vars(self):
        """
        Computes the memory size of global variables
        Note: this is computed as global variables are allocated to memory, regardless of being accessed or not
        """
        gst = self.vmstate.memory.sram.gst

        t = {}

        for memory_cell in gst._memory.values():
            if self.ignore_str_bss not in memory_cell.metadata:
                if memory_cell.metadata not in t:
                    t[memory_cell.metadata] = 0
                # Add global variables as accesses
                # This allows us to identify the correct memory occupancy of volatile/non-volatile memory
                self._collect_access(memory_cell.absolute_address, memory_cell.dimension, self.global_vars_target)
                self.global_vars_dim += memory_cell.dimension
                t[memory_cell.metadata] += memory_cell.dimension

        print(t)

    def _get_memory_size(self):
        """
        Returns the NVM and VM size
        """
        overall_size = {
            "vm": self._compute_memory_dim_from_accesses(self.memory_usage["VM"]),
            "nvm": self._compute_memory_dim_from_accesses(self.memory_usage["NVM"]),
            "global_vars": self.global_vars_dim,
            "double_buffer": self._compute_double_buffers(),
            "checkpoint": self.checkpoint_max_size,
        }

        overall_size["vm_total"] = self._from_size_to_memory_occupancy(overall_size["vm"])
        overall_size["nvm_total"] = self._from_size_to_memory_occupancy(
            overall_size["nvm"] + overall_size["double_buffer"] + overall_size["checkpoint"]
        )

        self.vmstate.memory_size = overall_size

    def _compute_double_buffers(self):
        """
        Computes the size of double buffers for our technique to avoid anomalies
        """
        if not self.double_buffer_enabled:
            return 0

        gst = self.vmstate.memory.sram.gst
        double_buffer_addresses = [x[0] for x in self.double_buffers if gst.address_prefix in x[0]]

        addresses = {}
        selected = set()

        # identify complete data structures (base_address, total_size)
        for element in gst._memory.values():
            if self.ignore_str_bss not in element.metadata:
                var_name = element.metadata.replace("Global variable ", "")

                if var_name not in addresses:
                    base_addr = element.absolute_address
                    dim = element.dimension
                else:
                    base_addr = addresses[var_name][0]
                    dim = addresses[var_name][1] + element.dimension

                addresses[var_name] = (base_addr, dim)

                if element.absolute_address in double_buffer_addresses:
                    selected.add(var_name)

        # consider entire data structures for double buffers
        for var_name in selected:
            element = (addresses[var_name][0], addresses[var_name][1])
            self.double_buffers.add(element)

        # return overall size
        return self._compute_memory_dim_from_accesses(self.double_buffers)

    def _compute_memory_dim_from_accesses(self, data):
        """
        Calculates the memory usage starting from the memory accesses
        """

        total_dim = 0
        data_by_prefix = {}

        for address, dim in data:
            index = address.index("0x")
            prefix = address[:index]
            address = int(address[index:], 16)

            if prefix not in data_by_prefix:
                data_by_prefix[prefix] = set()

            element = (address, dim)
            data_by_prefix[prefix].add(element)

        for data in data_by_prefix.values():
            data = sorted(data, key=lambda x: x[0])

            memory_bytes = set()

            for start, dim in data:
                for byte in range(start, start + dim):
                    memory_bytes.add(byte)

            total_dim += len(memory_bytes)

        # return occupied memory size
        return total_dim

    def _from_size_to_memory_occupancy(self, dim):
        """
        Converts the virtual memory size to physical size
        (i.e. a data structure of 11 bytes will occupy 12 bytes in a 16 bit architecture)
        """
        diff = dim % self.align_bytes

        if diff == 0:
            return dim
        else:
            return (dim // self.align_bytes) * self.align_bytes + self.align_bytes

    def _calculate_checkpoint_size(self, checkpoint):
        if not checkpoint.checkpoint_save_esp:
            # save only PC, no 2PC needed
            checkpoint_size = self.registers_size

        else:
            # save PC, ESP and general purpose registers
            checkpoint_size = self.registers_size * (len(checkpoint.checkpoint_save_regs) + 2)

            if checkpoint.checkpoint_save_ram:
                checkpoint_size += self._calculate_checkpoint_ram_size()

            # double buffer
            checkpoint_size = checkpoint_size * 2

        # memory location for the version offset (16 bits)
        checkpoint_size += self.registers_size

        return self._from_size_to_memory_occupancy(checkpoint_size)

    def _calculate_checkpoint_ram_size(self):
        """
        Returns the number of memory cells that need to be preserved
        """
        ram_size = 0

        # global variables
        gst = self.vmstate.memory.sram.gst
        for memory_cell in gst._memory.values():
            if self.ignore_str_bss not in memory_cell.metadata:
                ram_size += memory_cell.dimension

        # stack
        n_stack_memory_cells = 0
        stack = self.vmstate.memory.sram.stack
        stack_pointer = stack.top_address

        for address, memory_cell in sorted(stack._memory.items()):
            if address < stack_pointer:
                ram_size += memory_cell.dimension

        return ram_size
