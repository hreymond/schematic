import json
import json
import logging
import os
import time
from collections import deque
from typing import Dict

from ScEpTIC.AST.elements.instructions.other_operations import CallOperation
from ScEpTIC.AST.elements.instructions.termination_instructions import ReturnOperation, BranchOperation
from ScEpTIC.AST.misc.trace import FunctionTraces
from ScEpTIC.AST.transformations.schematic.utils.llvm_loop_analysis import find_loops, LLVMNaturalLoop
from ScEpTIC.exceptions import StopException, StopAnomalyFoundException
from .base import InterruptionManager
from ...vmstate import VMState


def is_annotation(inst):
    if type(inst) == CallOperation:
        if inst.name in ["@__max_iter"]:
            return True
    return False


class TraceInterruptionManager(InterruptionManager):
    """
    Interruption manager for tracing basic block execution
    """

    requires_static_checkpoint = False

    def __init__(self, vmstate: VMState, checkpoint_manager):
        super().__init__(vmstate, checkpoint_manager)
        self.variables = vmstate.vm.trace_vars
        self.traces: Dict[str, FunctionTraces] = {}
        self.f_ignored = set()
        loops_by_func = find_loops("source.ll")
        self.call_stack = deque()
        self.call_stack.append("@main")
        self.context = deque()
        self.context.append("@main")
        self.curr_traces = {}
        self.old_bb = {"@main": "%0"}
        self.loops: Dict[str, Dict[str, LLVMNaturalLoop]] = {}
        for fname, loops in loops_by_func.items():
            fname = "@" + fname
            self.loops[fname] = {}
            for loop in loops:
                self.loops[fname][loop.header] = loop

    def intermittent_execution_required(self):
        return True

    def add_bb_to_trace(self, bb: str):
        old_bb = self.old_bb[self.call_stack[-1]]
        f_name = self.call_stack[-1]
        # If the current context (loop, function) has no trace, create it
        if self.context[-1] not in self.curr_traces:
            self.curr_traces[self.context[-1]] = []

        # Entering a loop ?
        if bb in self.loops[f_name] and old_bb not in self.loops[f_name][bb].basic_blocks:
            # Append the loop header to the function trace
            # self.curr_traces[self.context[-1]].append(bb)
            # Change context to loop context
            self.context.append(f_name + str(bb))
            self.curr_traces[self.context[-1]] = []
        # Already in a loop ?
        curr_loop_bb = self.context[-1].replace(f_name, "")
        if curr_loop_bb in self.loops[f_name]:
            if old_bb in self.loops[f_name][curr_loop_bb].latch:
                # If we are in a loop and the current bb is a loop latch, save the trace
                self.traces[f_name].append_loop_trace(
                    self.loops[f_name][curr_loop_bb], self.curr_traces[self.context[-1]]
                )
                self.curr_traces[self.context[-1]] = []
            # Leaving the loop ?
            if bb not in self.loops[f_name][curr_loop_bb].basic_blocks:
                # self.traces[f_name].append_loop_trace(self.loops[f_name][self.context[-1]], self.curr_traces[self.context[-1]])
                old_context = self.context.pop()
                # print(old_context, ": ", self.curr_traces[old_context])
                for path_bb in self.curr_traces[old_context]:
                    self.curr_traces[self.context[-1]].append(path_bb)
        # # If the last basic block was a loop header and the current one is in the loop, we entered the loop
        # if old_bb and old_bb in self.loops[f_name] and bb in self.loops[f_name][old_bb].basic_blocks:
        #     self.context.append(old_bb)
        #     self.curr_traces[old_bb] = [old_bb]
        # Add the basic block to the current trace
        self.curr_traces[self.context[-1]].append(bb)
        self.old_bb[self.call_stack[-1]] = bb

    def process_branch(self, inst):
        if inst.condition is None:
            target = inst.target_true
        else:
            # evaluates the condition
            value = inst.condition.get_val()
            if int(value) == 1:
                target = inst.target_true
            else:
                target = inst.target_false
        self.add_bb_to_trace(target)

    def remove_annotations(self):
        for function in self.vmstate.functions:
            insts = self.vmstate.functions[function].body
            insts_to_remove = [i for i in range(len(insts)) if is_annotation(insts[i])]
            insts_to_remove.reverse()
            for i in insts_to_remove:
                inst = insts[i]
                if inst.label:
                    insts[i + 1].label = inst.label
                del insts[i]
            self.vmstate.functions[function].update_labels()

    def run_with_intermittent_execution(self):
        logging.info("[TraceInterruptionManager] Running {} to generate traces".format(self.vmstate.register_file.pc))

        self.remove_annotations()
        self.curr_traces = {"@main": []}
        self.curr_traces["@main"] = ["%0"]
        nb_inst = 0
        t_0 = time.time()
        self.traces["@main"] = FunctionTraces()
        while not self.vmstate.program_end_reached:
            inst = self.vmstate.current_instruction
            try:
                self.vmstate.run_step()
            except StopAnomalyFoundException as e:
                self.save_trace()
            except StopException as e:
                self.save_trace()
                raise e

            if isinstance(inst, BranchOperation):
                self.process_branch(inst)

            if isinstance(inst, CallOperation) and inst.name not in inst.ignore:
                func_name = inst.name
                function = self.vmstate.functions[func_name]
                self.call_stack.append(func_name)
                self.context.append(func_name)

                if not function.is_builtin:
                    if inst.name not in self.traces:
                        self.traces[func_name] = FunctionTraces()
                    first_inst = function.body[0]
                    self.old_bb[func_name] = first_inst.basic_block_id
                    self.add_bb_to_trace(first_inst.basic_block_id)
                else:
                    self.f_ignored.add(inst.name)

            if isinstance(inst, ReturnOperation):
                func_name = self.call_stack[-1]
                if func_name not in self.f_ignored:
                    for loop_header in self.loops[func_name]:
                        if loop_header in self.curr_traces:
                            pass
                            # if len(curr_traces[loop_header]) > 0:
                            #     raise Exception("TODO")
                    self.traces[func_name].append_trace(self.curr_traces[func_name])
                    if func_name in self.old_bb:
                        del self.old_bb[func_name]
                    self.curr_traces[func_name] = []
                self.call_stack.pop()
                self.context.pop()
                if len(self.call_stack) > 0:
                    func_name = self.call_stack[-1]
            nb_inst += 1
        delta_t = time.time() - t_0
        print("Trace took {:.2f} seconds with an avereage speed of {:.0f} insts/s".format(delta_t, nb_inst / delta_t))
        self.save_trace()

    def save_trace(self):
        vm = self.vmstate.vm
        final_traces = {name: ftrace.to_dict() for name, ftrace in self.traces.items()}
        if vm.save_test_results:
            if not os.path.exists(vm.save_dir):
                os.makedirs(vm.save_dir, exist_ok=True)
            file_name = os.path.join(vm.save_dir, "traces.json")
            with open(file_name, "w") as fp:
                fp.write(json.dumps(final_traces))
