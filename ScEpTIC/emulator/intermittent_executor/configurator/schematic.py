"""
SCHEMATIC

Checkpoint mechanism: static
SRAM: gst;
NVM: stack, head, gst;
Checkpoint content: registers
"""


memory_configuration = {
    "sram": {"enabled": True, "stack": False, "heap": False, "gst": True, "gst_prefix": "SGST", "gst_base_address": 0},
    "nvm": {"enabled": True, "stack": True, "heap": True, "gst": True, "gst_prefix": "FGST", "gst_base_address": 0},
    "base_addresses": {"stack": 0, "heap": 0},
    "prefixes": {"stack": "S", "heap": "H"},
    "gst": {"default_ram": "NVM", "other_ram_section": "SRAM"},  # SRAM or NVM
    "address_dimension": 32,  # bit
}

# [CHECKPOINT MECHANISM CONFIGURATION]
"""
NB: if the checkpoint mechanism creates incremental checkpoints of nvm, it restores the whole nvm.
This configuration doesn't need to know the granularity of the checkpoint mechanism, it just needs to know
if something is restored (even partially).
checkpoint_placement can be static or dynamic. If it is set to static, checkpoints must be already present
inside the provided code. In such case, the user must provide the names of the checkpoint and restore routines.
on_dynamic_voltage_alert is considered only if checkpoint_placement is set to dynamic. it can be set on continue or stop.
if is set to stop, after a power interrupt the simulator will save a checkpoint and stop the execution (forces the execution_depth to 0).
if is set to continue, the simulator will save a checkpoint and continue the execution for #execution_dept operations (dynamic) or until another
checkpoint is reached (static).
"""
checkpoint_mechanism_configuration = {
    "checkpoint_placement": "static",
    "on_dynamic_voltage_alert": "continue",
    "checkpoint_routine_name": "checkpoint",  # required if checkpoint_placement is static
    "restore_routine_name": "restore",  # required if checkpoint_placement is static
    "restore_register_file": True,
    "sram": {"restore_stack": False, "restore_heap": False, "restore_gst": False},
    "nvm": {"restore_stack": False, "restore_heap": False, "restore_gst": False},
    "environment": False,
    "stop_on_checkpoint": True,
}
