import math

from ScEpTIC.emulator.energy import EnergyCalculator


class MSP430WorstCaseEnergyCalculator(EnergyCalculator):
    """
    Calculates the energy parameters for MSP430 class MCUs
    """

    def __init__(self, datasheet):
        self.datasheet = datasheet

        self.voltage = self._str_to_float(self.datasheet["voltage"])
        self.frequency = self._str_to_float(self.datasheet["frequency"])
        self.nvm_extra_cycles = self._str_to_int(self.datasheet["n_waits"])

        self._calculate_energy_parameters()

    def set_test_config(self):
        """
        Set all the values of the energy calculator to test values:
            energy_clock_cycle = 1
            nvm_extra_cycles = 1
            energy_non_volatile_memory_access = 0
            energy_volatile_memory_access = 0
            non_volatile_increase = 1
            energy_vm_access = 1
            energy_nvm_access = 2
        """
        self.energy_clock_cycle = 1
        if self.frequency == 16e6:
            self.nvm_extra_cycles = 1
        else:
            self.nvm_extra_cycles = 0

        self.energy_non_volatile_memory_access = 0.4
        self.energy_volatile_memory_access = 0

        self.non_volatile_increase = self.nvm_extra_cycles + 0.4
        self.energy_vm_access = 1
        self.energy_nvm_access = 1 + self.nvm_extra_cycles + 0.4

    def _calculate_energy_parameters(self):
        """
        Calculates the energy consumption of:
            - clock cycle
            - volatile memory access (does not account for energy consumption of clock cycle)
            - non-volatile memory access (does not account for energy consumption of clock cycle)
        """

        I_am_fram = self._str_to_float(self.datasheet["I_am_fram"])
        I_am_ram = self._str_to_float(self.datasheet["I_am_ram"])

        # I_am_ram and I_am_fram_uni account for 2 accesses (instruction and data) per clock cycle
        # Identify energy consumption of single SRAM/FRAM access to volatile/non-volatile memory
        I_ram = I_am_ram / 2.0
        self.energy_volatile_memory_access = self._I_to_e(I_ram, self.voltage, self.frequency)

        I_fram = I_am_fram / 2.0
        self.energy_non_volatile_memory_access = self._I_to_e(I_fram, self.voltage, self.frequency)

        # Identify cost of clock cycle
        # I_am_fram accounts for a non-volatile access and a volatile access (+ clock cycle overhead)
        # To identify clock cycle current consumption we need to subtract I_ram (fram access) from I_am_fram
        # as program is in FRAM, but data can be in SRAM/FRAM
        I_cycle = I_am_fram - I_ram
        # Need to account for cache hit ratio -> when does not hit, we need to account for current consumption of the extra n_waits cycles
        # Hit=0.75 -> 75% times we use only 1 cycle, 25% times we use 1 cycle + n_waits
        I_cycle = I_cycle * (1 + self.nvm_extra_cycles)

        self.energy_clock_cycle = self._I_to_e(I_cycle, self.voltage, self.frequency)

        # % increase of a non-volatile access w.r.t. a volatile memory access
        energy_volatile = self.energy_clock_cycle + self.energy_volatile_memory_access
        energy_non_volatile = self.energy_clock_cycle + self.energy_non_volatile_memory_access

        self.non_volatile_increase = float(energy_non_volatile / energy_volatile) - 1.0

        # Compute the energy overhead needed to access non-volatile memory
        self.energy_nvm_access = self.energy_clock_cycle + self.energy_non_volatile_memory_access
        # Multiply by the number of extra cycle needed
        self.energy_nvm_access *= 1 + self.nvm_extra_cycles
        self.energy_vm_access = self.energy_clock_cycle + self.energy_volatile_memory_access
