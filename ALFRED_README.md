# ALFRED prototype

**ALFRED** (**A**utomatic a**L**location o**F** non-volatile memo**R**y for transi**E**ntly-powered **D**evices) is a virtual memory abstraction that resolves the dichotomy between volatile and non-volatile memory in intermittent computing.

ALFRED prototype is implemented as an extension of ScEpTIC.

## Documentation
You can find ALFRED documentation at [https://neslabpolimi.bitbucket.io/ALFRED/](https://neslabpolimi.bitbucket.io/ALFRED/)

## Cite ALFRED
You can cite ALFRED and/or our program transformation techniques as:

_Andrea Maioli, Luca Mottola. 2021. ALFRED: Virtual Memory for Intermittent Computing.  In Proceedings of the the 19th ACM Conference on Embedded Networked Sensor Systems (SenSys '21)._

**Bibtex**:

```latex
    @inproceedings{ALFRED,
        author = {Andrea Maioli and Luca Mottola},
        title  = {ALFRED: Virtual Memory for Intermittent Computing},
        year = {2021},
        series = {SenSys '21},
        booktitle = {Proceedings of the the 19th ACM Conference on Embedded Networked Sensor Systems (SenSys '21)}
    }
```

------

# ScEpTIC
**ScEpTIC** (**S**imulator for **E**xecuting and **T**esting **I**ntermittent **C**omputation) is a python tool that analyzes intermittent execution of programs.

## Documentation
You can find ScEpTIC documentation at [https://neslabpolimi.bitbucket.io/ScEpTIC/](https://neslabpolimi.bitbucket.io/ScEpTIC/)

## Cite ScEpTIC
You can cite ScEpTIC and/or our analysis techniques as:

_Andrea Maioli, Luca Mottola, Muhammad Hamad Alizai, and Junaid Haroon Siddiqui. 2021. Discovering the Hidden Anomalies of Intermittent Computing. In Proceedings of the 2021 International Conference on Embedded Wireless Systems and Networks (EWSN '21)._

**Bibtex**:

```latex
    @inproceedings{ScEpTIC,
        author = {Andrea Maioli and Luca Mottola and Muhammad Hamad Alizai and Junaid Haroon Siddiqui},
        title  = {Discovering the Hidden Anomalies of Intermittent Computing},
        year = {2021},
        series = {EWSN 2021},
        booktitle = {Proceedings of the 2021 International Conference on Embedded Wireless Systems and Networks}
    }
```
